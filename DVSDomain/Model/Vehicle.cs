﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DVSDomain.Model
{
    [Table("DVS_Vehicle")]
    public class Vehicle
    {

        public int VehicleID { get; set; }
        public string CompanyID { get; set; }
        public string VehicleNO { get; set; }
        public int VehicleType { get; set; }
        public string VehicleTypeName { get; set; }
        public string VehicleName { get; set; }
        public string VehicleImage { get; set; }

        public decimal VehicleCapacity { get; set; }
        public string Status { get; set; }
        public bool IsActive { get; set; }
    }
}
