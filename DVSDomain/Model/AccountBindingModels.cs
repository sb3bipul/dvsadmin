﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DVSDomain.Model
{
    // Models used as parameters to AccountController actions.

    public class AddExternalLoginBindingModel
    {
        [Required]
        [Display(Name = "External access token")]
        public string ExternalAccessToken { get; set; }
    }

    public class ChangePasswordBindingModel
    {
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class RegisterBindingModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class RegisterExternalBindingModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class RemoveLoginBindingModel
    {
        [Required]
        [Display(Name = "Login provider")]
        public string LoginProvider { get; set; }

        [Required]
        [Display(Name = "Provider key")]
        public string ProviderKey { get; set; }
    }

    public class SetPasswordBindingModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ForgetPasswordBindingModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Email { get; set; }
        public string NewPassword { get; set; }
        public string UpdateBy { get; set; }
    }

    public class CreateNewUserBindingModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserFullName { get; set; }
        public string Domain { get; set; }
        public string UserTypeCode { get; set; }
    }

    public class LogindBindingModel
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }

        public bool isLoggedIn { get; set; }

    }

    public class VehicleTypeBindingModel
    {
        [Required]
        public int TypeID { get; set; }

        [Required]
        public string TypeName { get; set; }

        [Required]
        public string TypeDescription { get; set; }

        [Required]
        public bool IsActive { get; set; }

        public ActionMode ActionMode { get; set; }
    }

    public class ZoneBindingModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string ZoneCode { get; set; }

        [Required]
        public string ZoneName { get; set; }

        [Required]
        public bool IsActive { get; set; }

        [Required]
        public bool IsDeleted { get; set; }

        public ActionMode ActionMode { get; set; }
    }


    public class GetDataBindingModel
    {
        [Required]
        public int Id { get; set; }

        public string SearchText { get; set; }

        [Required]
        public int PageNo { get; set; }

        [Required]
        public int PageSize { get; set; }

        [Required]
        public string SortBy { get; set; }

        [Required]
        public string SortOrder { get; set; }

        public int Totalrow { get; set; }
    }

    public class LocationBindingModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string LocationName { get; set; }

        [Required]
        public string LocationAddress { get; set; }

        [Required]
        public bool IsActive { get; set; }

        public ActionMode ActionMode { get; set; }
    }

    public class SettingBindingModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public int StartPoint { get; set; }

        [Required]
        public int EndPoint { get; set; }

        [Required]
        public bool IsActive { get; set; }

        public ActionMode ActionMode { get; set; }
    }

    public class GetTripDataBindingModel
    {
        public int ZoneId { get; set; }
        public int SalesmanId { get; set; }

        [Required]
        public int NoOfMonth { get; set; }
        
        public string SearchByCustomer { get; set; }

        public string SearchByCustomerAddress { get; set; }

        public string OrderNo { get; set; }

        public int Totalrow { get; set; }
    }

    public class TripDataBindingModel
    {
        [Required]
        public int TripID { get; set; }
        public string CompanyID { get; set; }
        public string TripName { get; set; }
        public int DriverID { get; set; }
        public string WareHouse { get; set; }
        public decimal Charges { get; set; }
        public int Priority { get; set; }
        public int SortTrip { get; set; }
        public int VehicleID { get; set; }
        public int TripStatus { get; set; }
        public string TripDesc { get; set; }
        //public DateTime StartTime { get; set; }
        //public DateTime EndTime { get; set; }
        //public DateTime TripCreated { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        //public DateTime ActualStartTime { get; set; }
        //public DateTime ActualEndTime { get; set; }
        public List<TripDetailDataBindingModel> TripDetails { get; set; }
        public ActionMode ActionMode { get; set; }
        public int StartLocation { get; set; }
        public int EndLocation { get; set; }
        public DateTime TripCreated { get; set; }

    }

    public class TripDetailDataBindingModel
    {
        [Required]
        public int TripID { get; set; }
        public int TripDetailID { get; set; }
        public string OrderNo { get; set; }
        public int StartLocation { get; set; }
        public int EndLocation { get; set; }
        public int CompanyID { get; set; }
        public string ToDoComments { get; set; }
        public bool IsStopSummeryDone { get; set; }
        public bool IsToDo { get; set; }
        //public DateTime CreatedDate { get; set; }
        public bool IsCustomerSkipped { get; set; }
        public int Priority { get; set; }
        public decimal DeliveryCharges { get; set; }
        public string CustomerNote { get; set; }
    }
    public class GetTripInfoDataBindingModel
    {
        public DateTime StartDate { get; set; }
        public string TripStatus { get; set; }
        public string SearchText { get; set; }
    }
    public class GetOrderInfoDataBindingModel
    {
        public string SearchText { get; set; }
    }
    public class GetOrderDetailBindingModel
    {
        public string PONumber { get; set; }
    }
    
    public class DriverBindingModel
    {
        [Required]
        public int CompanyId { get; set; }

        [Required]
        public string DriverName { get; set; }

        [Required]
        public string DriverAddress { get; set; }

        public string City { get; set; }
        public int State { get; set; }
        public string Zip { get; set; }
        public int Country { get; set; }
        public string DriverEmail { get; set; }
        public string DriverContactNo { get; set; }
        public string DrivingLicense { get; set; }

        public string DriverLoginID { get; set; }
        public bool IsActive { get; set; }
        public string Warehouse { get; set; }
        public string DriverImage { get; set; }
        public int VehicleID { get; set; }
        public ActionMode ActionMode { get; set; }
    }

    public class VehicleBindingModel
    {
        [Required]
        public int VehicleID { get; set; }

        [Required]
        public string CompanyID { get; set; }

        [Required]
        public string VehicleNO { get; set; }
            
        [Required]
        public int VehicleType { get; set; }

        public string VehicleName { get; set; }

        public decimal VehicleCapacity { get; set; }

        [Required]
        public bool IsActive { get; set; }

        public ActionMode ActionMode { get; set; }
    }

    public class SearchARInvoice
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string InvoiceNo { get; set; }
        public string City { get; set; }
        public string OrderNo { get; set; }
        public string Status { get; set; }
    }

    public class approveInv
    {
        public string InvoiceNo { get; set; }
        public string RejectReason { get; set; }
        public string Action { get; set; }
    }

    public class InvoiceStatusHistory
    {
        public string InvoiceNo { get; set; }
        public string Status { get; set; }
        public string RejectReason { get; set; }
    }

    public class EditInvoiceInfo
    {
        public string NewInvoiceNo { get; set; }
        public string InvoiceNo { get; set; }
        public string CustomerId { get; set; }
        public string InvoiceComment { get; set; }
    }

    public class SearchDVSInvoice
    {
        public string DriverId { get; set; }
        public string InvoiceNo { get; set; }
        public string Status { get; set; }
        //public string CustomerId { get; set; }
        public string Fromdate { get; set; }
        public string Todate { get; set; }
    }
}
