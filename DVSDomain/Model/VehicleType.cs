﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DVSDomain.Model
{
    [Table("DVS_VehicleType")]
    public class VehicleType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "TypeID is required")]
        public int TypeID { get; set; }

        [Required(ErrorMessage = "CompanyId is required")]
        public string CompanyId { get; set; }

        [Required(ErrorMessage = "TypeName is required")]
        public string TypeName { get; set; }

        [Required(ErrorMessage = "TypeDescription is required")]
        public string TypeDescription { get; set; }

        [Required(ErrorMessage = "IsActive is required")]
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
