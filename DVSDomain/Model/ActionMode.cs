﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DVSDomain.Model
{
    public enum ActionMode
    {
        Add,
        Edit,
        Delete
    }
}
