﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DVSDomain.Model
{
    public class Product : BaseEntity
    {
        public string ProductName { get; set; }
        public virtual ProductDetails ProductDetails { get; set; }
    }
}
