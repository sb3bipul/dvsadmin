﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DVSDomain.Model
{
    [Table("DVS_UserMaster")]
    public class ApplicationUser
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Id is required")]
        public int UserId { get; set; }

        [Required(ErrorMessage = "CompanyId is required")]
        public int CompanyId { get; set; }

        [StringLength(50, ErrorMessage = "Name cannot be longer than 50 characters")]
        public string Name { get; set; }

        [StringLength(100, ErrorMessage = "Address cannot be longer than 50 characters")]
        public string Address { get; set; }

        [StringLength(100, ErrorMessage = "Email cannot be longer than 100 characters")]
        public string Email { get; set; }

        [StringLength(15, ErrorMessage = "Contact number cannot be longer than 15 characters")]
        public string ContactNo { get; set; }

        [StringLength(50, ErrorMessage = "City cannot be longer than 50 characters")]
        public string City { get; set; }

        public int State { get; set; }

        [StringLength(8, ErrorMessage = "Zip cannot be longer than 8 characters")]
        public string Zip { get; set; }

        [StringLength(100, ErrorMessage = "User name cannot be longer than 100 characters")]
        public string UserLoginID { get; set; }

        [StringLength(20, ErrorMessage = "Password cannot be longer than 20 characters")]
        public string UserPassword { get; set; }

        public int UserRole { get; set; }
        public int IsActive { get; set; }

        [StringLength(10, ErrorMessage = "DefaultLanguage cannot be longer than 10 characters")]
        public string DefaultLanguage { get; set; }

        [StringLength(20, ErrorMessage = "IMEI cannot be longer than 20 characters")]
        public string IMEI { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime DateAuthentication { get; set; }
    }
}
