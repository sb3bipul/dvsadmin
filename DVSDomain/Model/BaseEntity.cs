﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DVSDomain.Model
{
    public class BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string UpdatedBy { get; set; }

        public string CreatedBy { get; set; }

        [DefaultValue(false)]
        public bool IsDeleted { get; set; }

        //[ForeignKey("Client")]
        public int? ClientID { get; set; }

        //[ForeignKey("SourceSystem")]
        public int? SourceSystemID { get; set; }

        public DateTime? UpdateDateTimeServer { get; set; }

        public DateTime? UpdateDateTimeBrowser { get; set; }

        public DateTime CreateDateTimeBrowser { get; set; }

        public DateTime? CreateDateTimeServer { get; set; }

        //public virtual Client Client { get; set; }

        //public virtual SourceSystem SourceSystem { get; set; }
    }
}

