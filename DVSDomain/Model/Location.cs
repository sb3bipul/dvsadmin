﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DVSDomain.Model
{
    [Table("DVS_Location")]
    public class Location
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Id is required")]
        public int Id { get; set; }
        public string LocationName { get; set; }
        public string LocationAddress { get; set; }
        public int CreatedBy { get; set; }
        public bool IsActive { get; set; }   
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int StartPoint { get; set; }
        public int EndPoint { get; set; }
    }
}
