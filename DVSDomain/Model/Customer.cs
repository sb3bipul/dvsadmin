﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DVSDomain.Model
{
    [Table("tbl_OMS_Customer")]
    public class Customer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "UserID is required")]
        public int UserID { get; set; }

        [Required(ErrorMessage = "CompanyId is required")]
        public int CompanyId { get; set; }

        [Required(ErrorMessage = "DriverName is required")]
        public string BillingCO { get; set; }
        public string Zone { get; set; }
        public string Address { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public int Country { get; set; }
        public string Region { get; set; }
        public string ContactNo { get; set; }
        public string EmailID { get; set; }
        public string Organization { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonEmail { get; set; }
        public string WHCode { get; set; }
        public string TelxonNo { get; set; }
        public string Password { get; set; }
        public string UserType { get; set; }
        public string IMEINumber { get; set; }
        public int DepartmentID { get; set; }
        public int SalesManID { get; set; }
        public bool IsAuthenticated { get; set; }
        public DateTime AuthenticatedOn { get; set; }
        public bool UserIsDeleted { get; set; }
        public string AuthenticatedBy { get; set; }
        public decimal CreditLimit { get; set; }
        public string ContractNo { get; set; }
        public string BRROUT { get; set; }
        public string WHCompany { get; set; }
        public DateTime LastTimeUpdate { get; set; }
        public decimal AR30DAYS { get; set; }

        public decimal ARTOTAL { get; set; }
        public int srl { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public decimal StoreEntranceLocation { get; set; }
        public decimal StoreEntranceLatitude { get; set; }
        public decimal StoreEntranceLongitude { get; set; }
        public decimal ReceivingEntranceLocation { get; set; }
        public decimal ReceivingEntranceLatitude { get; set; }
        public decimal ReceivingEntranceLongitude { get; set; }
        public string CustomerImage { get; set; }
        public string CustomerImagePath { get; set; }
        public string Location { get; set; }
        public string Website { get; set; }
        public bool IsInsUpd { get; set; }
        public string CSCreditHold { get; set; }
        public string Terms { get; set; }
        public string EdiStore { get; set; }
        public string StoreImage { get; set; }
        public string StoreImagePath { get; set; }
        public string BillingAddress { get; set; }
        public string ShippingAddress { get; set; }
        public bool SpecificCustomer { get; set; }

    }
}
