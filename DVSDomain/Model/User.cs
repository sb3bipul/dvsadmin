﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DVSDomain.Model
{
    [Table("TBLMUSER")]
    public class User : BaseEntity
    {
        [Required(ErrorMessage = "Id is required")]
        public int Id { get; set; }

        [Required(ErrorMessage = "CompanyId is required")]
        public int CompanyId { get; set; }

        [Required(ErrorMessage = "User Id is required")]
        [StringLength(50, ErrorMessage = "User Id cannot be longer than 50 characters")]
        public string UserId { get; set; }

        [StringLength(100, ErrorMessage = "User name cannot be longer than 100 characters")]
        public string UserName { get; set; }

        [StringLength(20, ErrorMessage = "Password cannot be longer than 20 characters")]
        public string UserPassword { get; set; }

        [StringLength(100, ErrorMessage = "Email cannot be longer than 100 characters")]
        public string Email { get; set; }

        [StringLength(15, ErrorMessage = "Contact number cannot be longer than 15 characters")]
        public string ContactNo { get; set; }

        [StringLength(15, ErrorMessage = "User role cannot be longer than 15 characters")]
        public string UserRole { get; set; }

    }
}
