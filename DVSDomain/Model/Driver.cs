﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DVSDomain.Model
{
    [Table("DVS_DriverMaster")]
    public class Driver
    {
        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        //[Required(ErrorMessage = "DriverId is required")]
        public int DriverId { get; set; }

        [Required(ErrorMessage = "CompanyId is required")]
        public int CompanyId { get; set; }

        [Required(ErrorMessage = "DriverName is required")]
        public string DriverName { get; set; }
        public string DriverAddress { get; set; }
        public string City { get; set; }
        public int State { get; set; }
        public string Zip { get; set; }
        public int Country { get; set; }
        public string DriverEmail { get; set; }
        public string DriverContactNo { get; set; }
        public string DrivingLicense { get; set; }


        public string DriverLoginID { get; set; }

        [Required(ErrorMessage = "IsActive is required")]
        public bool IsActive { get; set; }
        public string Warehouse { get; set; }
        //public string DefaultLanguage { get; set; }
        //public DateTime? FromDate { get; set; }
        //public DateTime ToDate { get; set; }
        //public string? Signature { get; set; }
        //public DateTime DateRegistration { get; set; }
        //public DateTime? DateAuthentication { get; set; }
        public string DriverImage { get; set; }
        //public string DriverImagePath { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public string CreatedBy { get; set; }

        public int VehicleID { get; set; }
        public string VehicleName { get; set; }
        public int VehicleCapacity { get; set; }
        public string VehicleNo { get; set; }
        public int VehicleTypeId { get; set; }


    }

}
