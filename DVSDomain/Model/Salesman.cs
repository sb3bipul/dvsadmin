﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DVSDomain.Model
{
    [Table("tbl_OMS_User")]
    public class Salesman
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "UserID is required")]
        public int UserID { get; set; }

        [Required(ErrorMessage = "CompanyId is required")]
        public int CompanyId { get; set; }
        public string UserGUID { get; set; }
        public string Zone { get; set; }
        public string Address { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public int Country { get; set; }
        public string Region { get; set; }
        public string ContactNo { get; set; }
        public string EmailID { get; set; }
        public string Organization { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonEmail { get; set; }
        public string WHCode { get; set; }
        public string TelxonNo { get; set; }
        public string Password { get; set; }
        public string UserType { get; set; }
        public string IMEINumber { get; set; }
        public int SpotID { get; set; }
        public int DepartmentID { get; set; }
        public int SalesManID { get; set; }
        public int SrlNo { get; set; }

    }
}
