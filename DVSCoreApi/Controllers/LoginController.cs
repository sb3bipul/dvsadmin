﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using DVSDomain.Model;
using DVSRepository.Implementation;
using DVSRepository.Interface;
using DVSService.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using NLog;

namespace DVSCoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAllOrigins")]
    public class LoginController : ControllerBase
    {
        private NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        //IUserRepository userRepository;
        IConfiguration configuration;
        //private UserManager<IdentityUser> _userManager;
        string apiConnectionAuthString;
        public LoginController(IConfiguration configuration)
        {
            //this.userRepository = userRepository;
            this.configuration = configuration;
            apiConnectionAuthString = Microsoft.Extensions.Configuration.ConfigurationExtensions.GetConnectionString(this.configuration, "apidb");
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/api/login")]
        public async Task<IActionResult> LoginUser(LogindBindingModel model)
        {
            IActionResult response = Unauthorized();
            //User user = await userRepository.GetUser(login);
            LoginManager loginManager = new LoginManager();
            ApplicationUser appuser = loginManager.GetUserProfile(model, apiConnectionAuthString);
            if (appuser != null)
            {
                var tokenString = GenerateJWTToken(appuser);
                response = Ok(new
                {
                    token = tokenString,
                    userDetails = appuser,
                });
                return response;
            }
            else
            {
                return BadRequest("Invalid credentials");
            }

        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/api/ChangePassword")]
        public async Task<IActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            IActionResult response = Unauthorized();
            //IUserAuthorizationRepository repObj = new UserAuthorizationRepository();
            LoginManager loginManager = new LoginManager();
            ApplicationUser appuser = loginManager.GetUserProfileAfterPasswordReset(model, apiConnectionAuthString);
            //string apiConnectionAuthString = Microsoft.Extensions.Configuration.ConfigurationExtensions.GetConnectionString(this.configuration, "localdb");
            //ApplicationUser appuser = repObj.GetUserProfile(model, apiConnectionAuthString);
            //ApplicationUser appuser = repObj.GetUserProfileByUserLoginId(model.UserName, apiConnectionAuthString);
            ////User user = await userRepository.GetUser(login);
            //var user = await _userManager.FindByNameAsync(model.UserName);
            //var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            //var resetPassResult = await _userManager.ResetPasswordAsync(user, token, model.NewPassword);
            if (appuser != null)
            {
                var tokenString = GenerateJWTToken(appuser);
                response = Ok(new
                {
                    token = tokenString,
                    userDetails = appuser,
                });
                return response;
            }
            else
            {
                return BadRequest("Password not reset");
            }

        }
        #region user create
        //[HttpPost]
        //[AllowAnonymous]
        //[Route("/api/user")]
        //public async Task<IActionResult> CreateUser(CreateNewUserBindingModel model)
        //{
        //    IActionResult response = Unauthorized();
        //    var user = new IdentityUser { UserName = model.UserName, Email = model.Email };
        //    var result = await _userManager.CreateAsync(user, model.Password);
        //    if (result.Succeeded)
        //    {
        //        var tokenString = GenerateJWTTokenIdentity(user);
        //        response = Ok(new
        //        {
        //            token = tokenString,
        //            userDetails = user,
        //        });
        //        return response;
        //    }
        //    else
        //    {
        //        return BadRequest("Password not reset");
        //    }
        //    //IActionResult response = Unauthorized();
        //    ////User user = await userRepository.GetUser(login);
        //    //var user = await _userManager.FindByNameAsync(model.UserName);
        //    //var token = await _userManager.GeneratePasswordResetTokenAsync(user);
        //    //var resetPassResult = await _userManager.ResetPasswordAsync(user, token, model.NewPassword);
        //    //if (user != null)
        //    //{
        //    //    var tokenString = GenerateJWTTokenIdentity(user);
        //    //    response = Ok(new
        //    //    {
        //    //        token = tokenString,
        //    //        userDetails = user,
        //    //    });
        //    //    return response;
        //    //}
        //    //else
        //    //{
        //    //    return BadRequest("Password not reset");
        //    //}

        //}

        //POST api/Account/ChangePassword
        //[AllowAnonymous]
        //[Route("/api/ChangePassword")]
        // public async Task<IActionResult> ChangePassword(ChangePasswordBindingModel model)
        // {
        //     var jsonResponseObj = "[{}]";
        //     IActionResult response = Unauthorized();

        //     //try
        //     //{
        //         //UserAuthorizationRepository repObj = new UserAuthorizationRepository();
        //         //string userId = repObj.GetUserIDFromUserName(model.UserName);
        //         //var user = await _userManager.FindByNameAsync(model.UserName);
        //         //var token = await _userManager.GeneratePasswordResetTokenAsync(user);

        //         //var resetPassResult = await _userManager.ResetPasswordAsync(user, token, model.NewPassword);
        //         //var user = Users.FirstOrDefault(u => u.Id == userId);
        //         //if (user == null)
        //         //    return new Task<IdentityResult>(() => IdentityResult.Failed());

        //         //var store = Store as IUserPasswordStore<User, int>;
        //         //return base.UpdatePassword(store, user, newPassword);

        //         //UserManager<IdentityUser> userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>());
        //         //userManager.RemovePasswordAsync(model);
        //         //userManager.RemovePassword(userId);

        //         //userManager.AddPassword(userId, model.NewPassword);
        //         //UserController userObj = new UserController();
        //         //var resStr = await userObj.GetUserToken(model.UserName, model.OldPassword);
        //         //string tokerStr = ((System.Web.Http.Results.OkNegotiatedContentResult<string>)(resStr)).Content;

        //         //if (tokerStr != string.Empty)
        //         //{
        //         //    UserAuthorizationRepository repObj = new UserAuthorizationRepository();
        //         //    string userId = repObj.GetUserIDFromUserName(model.UserName);

        //         //    UserManager<IdentityUser> userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>());

        //         //    userManager.RemovePassword(userId);

        //         //    userManager.AddPassword(userId, model.NewPassword);

        //         //jsonResponseObj = "[{"
        //         //    + "\", \"Message\":\"Password changed successfully."
        //         //    + "\", \"Status\":\"success"
        //         //    + "\"}]";

        //         //return Ok(jsonResponseObj);
        //         //}
        //         //else
        //         //{
        //         //    jsonResponseObj = "[{"
        //         //        + "\", \"Message\":\"Wrong old password. Try again..."
        //         //        + "\", \"Status\":\"fail"
        //         //        + "\"}]";

        //         //    BadRequest(jsonResponseObj);
        //         //}
        //         //return response;

        //     //}
        //     //catch (Exception ex)
        //     //{
        //     //    jsonResponseObj = "[{"
        //     //            + "\", \"ErrorMessage\":\"" + ex
        //     //            + "\", \"Message\":\"Sorry, some error occur. Please contact support."
        //     //            + "\", \"Status\":\"fail"
        //     //            + "\"}]";
        //     //    return BadRequest(jsonResponseObj);
        //     //}

        //     //return BadRequest();
        // }
        #endregion

        #region Token generation
        private string GenerateJWTToken(ApplicationUser userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:SecretKey"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, userInfo.UserLoginID),
                new Claim("fullName", userInfo.UserLoginID.ToString()),
                new Claim("role",userInfo.UserRole.ToString()),
                new Claim("userId",userInfo.UserId.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };
            var token = new JwtSecurityToken(
            //issuer: configuration["Jwt:Issuer"],
            //audience: configuration["Jwt:Audience"],
            claims: claims,
            expires: DateTime.Now.AddMinutes(30),
            signingCredentials: credentials
            );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        private string GenerateJWTToken(User userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:SecretKey"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, userInfo.UserName),
                new Claim("fullName", userInfo.UserName.ToString()),
                new Claim("role",userInfo.UserRole.ToString()),
                new Claim("userId",userInfo.UserId.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };
            var token = new JwtSecurityToken(
            issuer: configuration["Jwt:Issuer"],
            audience: configuration["Jwt:Audience"],
            claims: claims,
            expires: DateTime.Now.AddMinutes(30),
            signingCredentials: credentials
            );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        private string GenerateJWTTokenIdentity(IdentityUser userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:SecretKey"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, userInfo.UserName),
                new Claim("fullName", userInfo.UserName.ToString()),
                //new Claim("role",userInfo.UserRole),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };
            var token = new JwtSecurityToken(
            issuer: configuration["Jwt:Issuer"],
            audience: configuration["Jwt:Audience"],
            claims: claims,
            expires: DateTime.Now.AddMinutes(30),
            signingCredentials: credentials
            );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        #endregion
    }
}
