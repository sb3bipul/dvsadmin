﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DVSDomain.Model;
using DVSRepository.Interface;
using DVSService.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NLog;

namespace DVSCoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [EnableCors("AllowAllOrigins")]
    public class ZoneController : Controller
    {
        private NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        IZoneRepository zoneRepository;
        IConfiguration configuration;
        string apiConnectionAuthString;
        public ZoneController(IZoneRepository zoneRepository, IConfiguration configuration)
        {
            this.zoneRepository = zoneRepository;
            this.configuration = configuration;
            apiConnectionAuthString = Microsoft.Extensions.Configuration.ConfigurationExtensions.GetConnectionString(this.configuration, "apidb");
        }

        [HttpGet]
        //[AllowAnonymous]
        //[Authorize]
        [Route("/api/zones")]
        public async Task<IActionResult> GetZone(int Id, string SearchText, int PageNo, int PageSize, string SortBy, string SortOrder)
        {
            IActionResult response = Unauthorized();
            //User user = await userRepository.GetUser(login);
            GetDataBindingModel model = new GetDataBindingModel();
            model.Id = Id;
            model.SearchText = SearchText;
            model.PageNo = PageNo;
            model.PageSize = PageSize;
            model.SortBy = SortBy;
            model.SortOrder = SortOrder;
            model.Totalrow = 0;
            List<VehicleType> lstVehicalType = new List<VehicleType>();
            ZoneManager manager = new ZoneManager(zoneRepository);
            var getData = manager.GetZone(model, apiConnectionAuthString);
            //lstVehicalType = manager.GetAllVehiclesType(model,apiConnectionAuthString);
            if (getData != null)
            {
                //var tokenString = GenerateJWTToken(appuser);
                response = Ok(new
                {
                    //token = tokenString,
                    Zones = getData.Item1,
                    TotalRecord = getData.Item2
                });
                return response;
            }
            else
            {
                return BadRequest("Vehical type fetching error.");
            }

        }

        [HttpGet]
        //[AllowAnonymous]
        //[Authorize]
        [Route("/api/zonedetail")]
        public async Task<IActionResult> GetZoneById(int Id)
        {
            IActionResult response = Unauthorized();
            //User user = await userRepository.GetUser(login);
            GetDataBindingModel model = new GetDataBindingModel();
            model.Id = Id;
            model.SearchText = "";
            model.PageNo = 1;
            model.PageSize = 0;
            model.SortBy = "ZoneName";
            model.SortOrder = "ASC";
            model.Totalrow = 0;
            List<VehicleType> lstVehicalType = new List<VehicleType>();
            ZoneManager manager = new ZoneManager(zoneRepository);
            var getData = manager.GetZone(model, apiConnectionAuthString);
            //lstVehicalType = manager.GetAllVehiclesType(model,apiConnectionAuthString);
            if (lstVehicalType != null)
            {
                //var tokenString = GenerateJWTToken(appuser);
                response = Ok(new
                {
                    //token = tokenString,
                    Zones = getData.Item1,
                    TotalRecord = getData.Item2
                });
                return response;
            }
            else
            {
                return BadRequest("Vehical type fetching error.");
            }

        }

        [HttpPost]
        [Route("/api/savezone")]
        public async Task<IActionResult> CreateZone(ZoneBindingModel model)
        {
            IActionResult response = Unauthorized();
            ZoneManager manager = new ZoneManager(zoneRepository);
            string strMessage = manager.CreateZone(model, apiConnectionAuthString);
            if (strMessage != "")
            {
                response = Ok(new
                {
                    Message = strMessage,
                });
                return response;
            }
            else
            {
                return BadRequest("Error on CreateZone");
            }

        }

        [HttpPost]
        [Route("/api/updatezone")]
        public async Task<IActionResult> UpdateZone(ZoneBindingModel model)
        {
            IActionResult response = Unauthorized();
            ZoneManager manager = new ZoneManager(zoneRepository);
            string strMessage = manager.UpdateZone(model, apiConnectionAuthString);
            if (strMessage != "")
            {
                response = Ok(new
                {
                    Message = strMessage,
                });
                return response;
            }
            else
            {
                return BadRequest("Error on UpdateZone");
            }

        }

        [HttpPost]
        [Route("/api/removezone")]
        public async Task<IActionResult> RemoveZone(ZoneBindingModel model)
        {
            IActionResult response = Unauthorized();
            ZoneManager manager = new ZoneManager(zoneRepository);

            //ZoneBindingModel model = new ZoneBindingModel();
            //model.Id = Convert.ToInt32(Id);
            model.ActionMode = ActionMode.Delete;
            model.ZoneCode = "";
            model.ZoneName = "";
            model.IsActive = true;
            model.IsDeleted = true;

            string strMessage = manager.DeleteZone(model, apiConnectionAuthString);
            if (strMessage != "")
            {
                response = Ok(new
                {
                    Message = strMessage,
                });
                return response;
            }
            else
            {
                return BadRequest("Error on RemoveZone");
            }

        }
    }
}
