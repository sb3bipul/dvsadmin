﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DVSDomain.Model;
using DVSRepository.Interface;
using DVSService.Contracts;
using DVSService.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NLog;

namespace DVSCoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [EnableCors("AllowAllOrigins")]
    public class SettingController : Controller
    {
        private NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        ISettingRepository settingRepository;
        IConfiguration configuration;
        string apiConnectionAuthString;
        public SettingController(ISettingRepository settingRepository, IConfiguration configuration)
        {
            this.settingRepository = settingRepository;
            this.configuration = configuration;
            apiConnectionAuthString = Microsoft.Extensions.Configuration.ConfigurationExtensions.GetConnectionString(this.configuration, "apidb");
        }

        [HttpGet]
        //[AllowAnonymous]
        //[Authorize]
        [Route("/api/locations")]
        public async Task<IActionResult> GetLocation(int Id, string SearchText, int PageNo, int PageSize, string SortBy, string SortOrder)
        {
            IActionResult response = Unauthorized();
            //User user = await userRepository.GetUser(login);
            GetDataBindingModel model = new GetDataBindingModel();
            model.Id = Id;
            model.SearchText = SearchText;
            model.PageNo = PageNo;
            model.PageSize = PageSize;
            model.SortBy = SortBy;
            model.SortOrder = SortOrder;
            model.Totalrow = 0;
            List<VehicleType> lstVehicalType = new List<VehicleType>();
            ISettingManager manager = new SettingManager(settingRepository);
            var getData = manager.GetLocation(model, apiConnectionAuthString);
            //lstVehicalType = manager.GetAllVehiclesType(model,apiConnectionAuthString);
            if (getData != null)
            {
                //var tokenString = GenerateJWTToken(appuser);
                response = Ok(new
                {
                    //token = tokenString,
                    locations = getData.Item1,
                    TotalRecord = getData.Item2
                });
                return response;
            }
            else
            {
                return BadRequest("Location fetching error.");
            }

        }

        [HttpPost]
        [Route("/api/savelocation")]
        public async Task<IActionResult> CreateLocation(LocationBindingModel model)
        {
            IActionResult response = Unauthorized();
            ISettingManager manager = new SettingManager(settingRepository);
            string strMessage = manager.CreateLocation(model, apiConnectionAuthString);
            if (strMessage != "")
            {
                response = Ok(new
                {
                    Message = strMessage,
                });
                return response;
            }
            else
            {
                return BadRequest("Error on CreateLocation");
            }

        }

        [HttpPut]
        [Route("/api/updatelocation")]
        public async Task<IActionResult> UpdateVehicleType(LocationBindingModel model)
        {
            IActionResult response = Unauthorized();
            ISettingManager manager = new SettingManager(settingRepository);
            string strMessage = manager.UpdateLocation(model, apiConnectionAuthString);
            if (strMessage != "")
            {
                response = Ok(new
                {
                    Message = strMessage,
                });
                return response;
            }
            else
            {
                return BadRequest("Error on UpdateLocation");
            }

        }

        [HttpDelete]
        [Route("/api/removelocation")]
        public async Task<IActionResult> RemoveLocation(LocationBindingModel model)
        {
            IActionResult response = Unauthorized();
            SettingManager manager = new SettingManager(settingRepository);
            string strMessage = manager.DeleteLocation(model, apiConnectionAuthString);
            if (strMessage != "")
            {
                response = Ok(new
                {
                    Message = strMessage,
                });
                return response;
            }
            else
            {
                return BadRequest("Error on RemoveLocation");
            }

        }

        [HttpGet]
        //[AllowAnonymous]
        //[Authorize]
        [Route("/api/Setting")]
        public async Task<IActionResult> GetSetting(int Id, string SearchText, int PageNo, int PageSize, string SortBy, string SortOrder)
        {
            IActionResult response = Unauthorized();
            //User user = await userRepository.GetUser(login);
            GetDataBindingModel model = new GetDataBindingModel();
            model.Id = Id;
            model.SearchText = SearchText;
            model.PageNo = PageNo;
            model.PageSize = PageSize;
            model.SortBy = SortBy;
            model.SortOrder = SortOrder;
            model.Totalrow = 0;
            List<VehicleType> lstVehicalType = new List<VehicleType>();
            ISettingManager manager = new SettingManager(settingRepository);
            var getData = manager.GetSetting(model, apiConnectionAuthString);
            if (lstVehicalType != null)
            {
                //var tokenString = GenerateJWTToken(appuser);
                response = Ok(new
                {
                    //token = tokenString,
                    Settings = getData,
                });
                return response;
            }
            else
            {
                return BadRequest("Setting fetching error.");
            }

        }

        [HttpPost]
        [Route("/api/updatesetting")]
        public async Task<IActionResult> UpdateSetting(SettingBindingModel model)
        {
            IActionResult response = Unauthorized();
            ISettingManager manager = new SettingManager(settingRepository);
            string strMessage = manager.UpdateSetting(model, apiConnectionAuthString);
            if (strMessage != "")
            {
                response = Ok(new
                {
                    Message = strMessage,
                });
                return response;
            }
            else
            {
                return BadRequest("Error on UpdateSetting");
            }

        }
    }
}
