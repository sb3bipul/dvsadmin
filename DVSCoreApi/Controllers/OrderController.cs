﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DVSDomain.Model;
using DVSRepository.Interface;
using DVSService.Contracts;
using DVSService.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NLog;

namespace DVSCoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [EnableCors("AllowAllOrigins")]
    public class OrderController : Controller
    {
        private NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        IOrderRepository orderRepository;
        IConfiguration configuration;
        string apiConnectionAuthString;
        public OrderController(IOrderRepository orderRepository, IConfiguration configuration)
        {
            this.orderRepository = orderRepository;
            this.configuration = configuration;
            apiConnectionAuthString = Microsoft.Extensions.Configuration.ConfigurationExtensions.GetConnectionString(this.configuration, "apidb");
        }

        [HttpGet]
        [Route("/api/orderInfos")]
        public async Task<IActionResult> GetAllOrderInfo(string? SearchText)
        {
            IActionResult response = Unauthorized();

            GetOrderInfoDataBindingModel model = new GetOrderInfoDataBindingModel();
            model.SearchText = SearchText;

            IOrderManager manager = new OrderManager(orderRepository);
            var getData = manager.GetAllOrderInfo(model, apiConnectionAuthString);

            if (getData != null)
            {
                response = Ok(new
                {
                    OrderInfo = getData.Item1,
                    TotalRecord = getData.Item2
                });
                return response;
            }
            else
            {
                return BadRequest("GetAllTripInfo fetching error.");
            }

        }

        [HttpGet]
        [Route("/api/orderdetail")]
        public async Task<IActionResult> GetOrderDetail(string PONumber)
        {
            IActionResult response = Unauthorized();

            GetOrderDetailBindingModel model = new GetOrderDetailBindingModel();
            model.PONumber = PONumber;

            IOrderManager manager = new OrderManager(orderRepository);
            var getData = manager.GetOrderDetail(model, apiConnectionAuthString);

            if (getData != null)
            {
                response = Ok(new
                {
                    OrderDetailInfo = getData.Item1,
                    TotalRecord = getData.Item2
                });
                return response;
            }
            else
            {
                return BadRequest("GetOrderDetail fetching error.");
            }

        }
    }
}
