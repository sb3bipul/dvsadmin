﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DVSDomain.Model;
using DVSRepository.Interface;
using DVSService.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NLog;

namespace DVSCoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [EnableCors("AllowAllOrigins")]
    public class CustomerController : Controller
    {
        private NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        ICustomerRepository customerRepository;
        IConfiguration configuration;
        string apiConnectionAuthString;
        public CustomerController(ICustomerRepository customerRepository, IConfiguration configuration)
        {
            this.customerRepository = customerRepository;
            this.configuration = configuration;
           apiConnectionAuthString = Microsoft.Extensions.Configuration.ConfigurationExtensions.GetConnectionString(this.configuration, "apidb");
        }

        [HttpGet]
        [Route("/api/customer")]
        public async Task<IActionResult> GetCutomer(int Id, string SearchText, int PageNo, int PageSize, string SortBy, string SortOrder)
        {
            IActionResult response = Unauthorized();
            GetDataBindingModel model = new GetDataBindingModel();
            model.Id = Id;
            model.SearchText = SearchText;
            model.PageNo = PageNo;
            model.PageSize = PageSize;
            model.SortBy = SortBy;
            model.SortOrder = SortOrder;
            model.Totalrow = 0;
            CustomerManager manager = new CustomerManager(customerRepository);
            var getData = manager.GetCustomer(model, apiConnectionAuthString);
            if (getData != null)
            {
                response = Ok(new
                {
                    Customers = getData.Item1,
                    TotalRecord = getData.Item2
                });
                return response;
            }
            else
            {
                return BadRequest("Customer fetching error.");
            }

        }

        [HttpGet]
        [Route("/api/customerdetail")]
        public async Task<IActionResult> GetCutomerByID(int Id)
        {
            IActionResult response = Unauthorized();
            GetDataBindingModel model = new GetDataBindingModel();
            model.Id = Id;
            model.SearchText = "";
            model.PageNo = 1;
            model.PageSize = 0;
            model.SortBy = "Name";
            model.SortOrder = "ASC";
            model.Totalrow = 0;
            CustomerManager manager = new CustomerManager(customerRepository);
            var getData = manager.GetCustomer(model, apiConnectionAuthString);
            if (getData != null)
            {
                response = Ok(new
                {
                    Customers = getData.Item1,
                    TotalRecord = getData.Item2
                });
                return response;
            }
            else
            {
                return BadRequest("Customer fetching error.");
            }

        }
    }
}
