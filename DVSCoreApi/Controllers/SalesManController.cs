﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DVSDomain.Model;
using DVSRepository.Interface;
using DVSService.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NLog;

namespace DVSCoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [EnableCors("AllowAllOrigins")]
    public class SalesManController : Controller
    {
        private NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        ISalesmanRepository salesmanRepository;
        IConfiguration configuration;
        string apiConnectionAuthString;
        public SalesManController(ISalesmanRepository salesmanRepository, IConfiguration configuration)
        {
            this.salesmanRepository = salesmanRepository;
            this.configuration = configuration;
            apiConnectionAuthString = Microsoft.Extensions.Configuration.ConfigurationExtensions.GetConnectionString(this.configuration, "apidb");
        }

        [HttpGet]
        //[AllowAnonymous]
        //[Authorize]
        [Route("/api/salesman")]
        public async Task<IActionResult> GetSalesMan(int Id, string SearchText, int PageNo, int PageSize, string SortBy, string SortOrder)
        {
            IActionResult response = Unauthorized();
            GetDataBindingModel model = new GetDataBindingModel();
            model.Id = Id;
            model.SearchText = SearchText;
            model.PageNo = PageNo;
            model.PageSize = PageSize;
            model.SortBy = SortBy;
            model.SortOrder = SortOrder;
            model.Totalrow = 0;
            SalesmanManager manager = new SalesmanManager(salesmanRepository);
            var getData = manager.GetSalesMan(model, apiConnectionAuthString);
            if (getData != null)
            {
                response = Ok(new
                {
                    salesmans = getData.Item1,
                    TotalRecord = getData.Item2
                });
                return response;
            }
            else
            {
                return BadRequest("All SalesMan fetching error.");
            }

        }

        [HttpGet]

        [Route("/api/salesmandetail")]
        public async Task<IActionResult> GetSalesManById(int Id)
        {
            IActionResult response = Unauthorized();
            GetDataBindingModel model = new GetDataBindingModel();
            model.Id = Id;
            model.SearchText = "";
            model.PageNo = 1;
            model.PageSize = 0;
            model.SortBy = "Name";
            model.SortOrder = "ASC";
            model.Totalrow = 0;
            SalesmanManager manager = new SalesmanManager(salesmanRepository);
            var getData = manager.GetSalesMan(model, apiConnectionAuthString);
            if (getData != null)
            {
                response = Ok(new
                {
                    salesmans = getData.Item1,
                    TotalRecord = getData.Item2
                });
                return response;
            }
            else
            {
                return BadRequest("SalesMan fetching error.");
            }

        }
    }
}
