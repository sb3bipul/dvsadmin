﻿using DVSDomain.Model;
using DVSRepository.Interface;
using DVSService.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NLog;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DVSCoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [EnableCors("AllowAllOrigins")]
    public class VehicleTypeController : Controller
    {
        private NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        IVehicleTypeRepository vehicalTypeRepository;
        IConfiguration configuration;
        string apiConnectionAuthString;
        public VehicleTypeController(IVehicleTypeRepository vehicalTypeRepository, IConfiguration configuration)
        {
            this.vehicalTypeRepository = vehicalTypeRepository;
            this.configuration = configuration;
            apiConnectionAuthString = Microsoft.Extensions.Configuration.ConfigurationExtensions.GetConnectionString(this.configuration, "apidb");
        }

        [HttpGet]
        //[AllowAnonymous]
        //[Authorize]
        [Route("/api/vehiclestypes")]
        public async Task<IActionResult> GetAllVehiclesType(int Id, string SearchText, int PageNo, int PageSize, string SortBy, string SortOrder)
        {
            IActionResult response = Unauthorized();
            //User user = await userRepository.GetUser(login);
            GetDataBindingModel model = new GetDataBindingModel();
            model.Id = Id;
            model.SearchText = SearchText;
            model.PageNo = PageNo;
            model.PageSize = PageSize;
            model.SortBy = SortBy;
            model.SortOrder = SortOrder;
            model.Totalrow = 0;
            List<VehicleType> lstVehicalType = new List<VehicleType>();
            VehicleTypeManager manager = new VehicleTypeManager(vehicalTypeRepository);
            var getData = manager.GetAllVehiclesType(model, apiConnectionAuthString);
            //lstVehicalType = manager.GetAllVehiclesType(model,apiConnectionAuthString);
            if (lstVehicalType != null)
            {
                //var tokenString = GenerateJWTToken(appuser);
                response = Ok(new
                {
                    //token = tokenString,
                    VehicleTypes = getData.Item1,
                    TotalRecord = getData.Item2
                });
                return response;
            }
            else
            {
                return BadRequest("Vehical type fetching error.");
            }

        }

        [HttpGet]
        //[AllowAnonymous]
        //[Authorize]
        [Route("/api/vehiclestypedetail")]
        public async Task<IActionResult> GetVehiclesTypeById(int Id)
        {
            IActionResult response = Unauthorized();
            //User user = await userRepository.GetUser(login);
            GetDataBindingModel model = new GetDataBindingModel();
            model.Id = Id;
            model.SearchText = "";
            model.PageNo = 1;
            model.PageSize = 0;
            model.SortBy = "TypeName";
            model.SortOrder = "asc";
            model.Totalrow = 0;
            List<VehicleType> lstVehicalType = new List<VehicleType>();
            VehicleTypeManager manager = new VehicleTypeManager(vehicalTypeRepository);
            var getData = manager.GetAllVehiclesType(model, apiConnectionAuthString);
            //lstVehicalType = manager.GetAllVehiclesType(model,apiConnectionAuthString);
            if (lstVehicalType != null)
            {
                //var tokenString = GenerateJWTToken(appuser);
                response = Ok(new
                {
                    //token = tokenString,
                    VehicleTypes = getData.Item1,
                    TotalRecord = getData.Item2
                });
                return response;
            }
            else
            {
                return BadRequest("Vehical type fetching error.");
            }

        }

        [HttpPost]
        [Route("/api/savevehicletype")]
        public async Task<IActionResult> CreateVehicleType(VehicleTypeBindingModel model)
        {
            IActionResult response = Unauthorized();
            VehicleTypeManager manager = new VehicleTypeManager(vehicalTypeRepository);
            string strMessage = manager.CreateVehiclesType(model, apiConnectionAuthString);
            if (strMessage != "")
            {
                response = Ok(new
                {
                    Message = strMessage,
                });
                return response;
            }
            else
            {
                return BadRequest("Error on CreateVehicalType");
            }

        }

        [HttpPost]
        [Route("/api/updatevehicletype")]
        public async Task<IActionResult> UpdateVehicleType(VehicleTypeBindingModel model)
        {
            IActionResult response = Unauthorized();
            VehicleTypeManager manager = new VehicleTypeManager(vehicalTypeRepository);
            string strMessage = manager.UpdateVehiclesType(model, apiConnectionAuthString);
            if (strMessage != "")
            {
                response = Ok(new
                {
                    Message = strMessage,
                });
                return response;
            }
            else
            {
                return BadRequest("Error on UpdateVehicleType");
            }

        }

        [HttpPost]
        [Route("/api/removevehicletype")]
        public async Task<IActionResult> RemoveVehicleType(VehicleTypeBindingModel model)
        {
            IActionResult response = Unauthorized();
            VehicleTypeManager manager = new VehicleTypeManager(vehicalTypeRepository);
            string strMessage = manager.DeleteVehiclesType(model, apiConnectionAuthString);
            if (strMessage != "")
            {
                response = Ok(new
                {
                    Message = strMessage,
                });
                return response;
            }
            else
            {
                return BadRequest("Error on RemoveVehicleType");
            }

        }
    }
}
