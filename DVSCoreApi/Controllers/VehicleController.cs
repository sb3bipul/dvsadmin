﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DVSDomain.Model;
using DVSRepository.Interface;
using DVSService.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NLog;

namespace DVSCoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [EnableCors("AllowAllOrigins")]
    public class VehicleController : Controller
    {
        private NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        IVehicleRepository vehicleRepository;
        IConfiguration configuration;
        string apiConnectionAuthString;
        public VehicleController(IVehicleRepository vehicleRepository, IConfiguration configuration)
        {
            this.vehicleRepository = vehicleRepository;
            this.configuration = configuration;
            apiConnectionAuthString = Microsoft.Extensions.Configuration.ConfigurationExtensions.GetConnectionString(this.configuration, "apidb");
        }

        [HttpGet]
        //[AllowAnonymous]
        //[Authorize]
        [Route("/api/vehicles")]
        public async Task<IActionResult> GetAllVehicles(int Id, string SearchText, int PageNo, int PageSize, string SortBy, string SortOrder)
        {
            IActionResult response = Unauthorized();
            //User user = await userRepository.GetUser(login);
            GetDataBindingModel model = new GetDataBindingModel();
            model.Id = Id;
            model.SearchText = SearchText;
            model.PageNo = PageNo;
            model.PageSize = PageSize;
            model.SortBy = SortBy;
            model.SortOrder = SortOrder;
            model.Totalrow = 0;
            List<VehicleType> lstVehicalType = new List<VehicleType>();
            VehicleManager manager = new VehicleManager(vehicleRepository);
            var getData = manager.GetVehicle(model, apiConnectionAuthString);
            //lstVehicalType = manager.GetAllVehiclesType(model,apiConnectionAuthString);
            if (lstVehicalType != null)
            {
                //var tokenString = GenerateJWTToken(appuser);
                response = Ok(new
                {
                    //token = tokenString,
                    Vehicles = getData.Item1,
                    TotalRecord = getData.Item2
                });
                return response;
            }
            else
            {
                return BadRequest("Vehical type fetching error.");
            }

        }

        [HttpPost]
        [Route("/api/savevehicle")]
        public async Task<IActionResult> CreateVehicle(VehicleBindingModel model)
        {
            IActionResult response = Unauthorized();
            VehicleManager manager = new VehicleManager(vehicleRepository);
            string strMessage = manager.CreateVehicle(model, apiConnectionAuthString);
            if (strMessage != "")
            {
                response = Ok(new
                {
                    Message = strMessage,
                });
                return response;
            }
            else
            {
                return BadRequest("Error on CreateVehicle");
            }

        }

        [HttpPost]
        [Route("/api/updatevehicle")]
        public async Task<IActionResult> UpdateVehicle(VehicleBindingModel model)
        {
            IActionResult response = Unauthorized();
            VehicleManager manager = new VehicleManager(vehicleRepository);
            string strMessage = manager.UpdateVehicleType(model, apiConnectionAuthString);
            if (strMessage != "")
            {
                response = Ok(new
                {
                    Message = strMessage,
                });
                return response;
            }
            else
            {
                return BadRequest("Error on UpdateVehicle");
            }

        }

        [HttpPost]
        [Route("/api/removevehicle")]
        public async Task<IActionResult> RemoveVehicle(VehicleBindingModel model)
        {
            IActionResult response = Unauthorized();
            VehicleManager manager = new VehicleManager(vehicleRepository);
            string strMessage = manager.DeleteVehicleType(model, apiConnectionAuthString);
            if (strMessage != "")
            {
                response = Ok(new
                {
                    Message = strMessage,
                });
                return response;
            }
            else
            {
                return BadRequest("Error on RemoveVehicle");
            }

        }

    }
}
