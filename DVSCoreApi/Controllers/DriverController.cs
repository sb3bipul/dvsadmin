﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DVSDomain.Model;
using DVSRepository.Implementation;
using DVSRepository.Interface;
using DVSService.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NLog;

namespace DVSCoreApi.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [EnableCors("AllowAllOrigins")]
    public class DriverController : Controller
    {
        private NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        IDriverRepository driverRepository;
        IConfiguration configuration;
        string apiConnectionAuthString;
        public DriverController(IDriverRepository driverRepository, IConfiguration configuration)
        {
            this.driverRepository = driverRepository;
            this.configuration = configuration;
            apiConnectionAuthString = Microsoft.Extensions.Configuration.ConfigurationExtensions.GetConnectionString(this.configuration, "apidb");
        }

        [HttpGet]
        //[AllowAnonymous]
        //[Authorize]
        [Route("/api/drivers")]
        public async Task<IActionResult> GetDriver(int Id, string SearchText, int PageNo, int PageSize, string SortBy, string SortOrder)
        {
            IActionResult response = Unauthorized();
            GetDataBindingModel model = new GetDataBindingModel();
            model.Id = Id;
            model.SearchText = SearchText;
            model.PageNo = PageNo;
            model.PageSize = PageSize;
            model.SortBy = SortBy;
            model.SortOrder = SortOrder;
            model.Totalrow = 0;
            DriverManager manager = new DriverManager(driverRepository);
            var getData = manager.GetDriver(model, apiConnectionAuthString);
            if (getData != null)
            {
                response = Ok(new
                {
                    drivers = getData.Item1,
                    TotalRecord = getData.Item2
                });
                return response;
            }
            else
            {
                return BadRequest("All Driver fetching error.");
            }

        }

        [HttpGet]
        //[AllowAnonymous]
        //[Authorize]
        [Route("/api/driverdetail")]
        public async Task<IActionResult> GetDriverById(int Id)
        {
            IActionResult response = Unauthorized();
            GetDataBindingModel model = new GetDataBindingModel();
            model.Id = Id;
            model.SearchText = "";
            model.PageNo = 1;
            model.PageSize = 0;
            model.SortBy = "DriverName";
            model.SortOrder = "ASC";
            model.Totalrow = 0;
            DriverManager manager = new DriverManager(driverRepository);
            var getData = manager.GetDriver(model, apiConnectionAuthString);
            if (getData != null)
            {
                response = Ok(new
                {
                    drivers = getData.Item1,
                    TotalRecord = getData.Item2
                });
                return response;
            }
            else
            {
                return BadRequest("Driver fetching error.");
            }

        }

        [HttpPost]
        [Route("/api/saveDriver")]
        public async Task<IActionResult> CreateDriver(DriverBindingModel model)
        {
            IActionResult response = Unauthorized();
            DriverManager manager = new DriverManager(driverRepository);
            string strMessage = manager.CreateDriver(model, apiConnectionAuthString);
            if (strMessage != "")
            {
                response = Ok(new
                {
                    Message = strMessage,
                });
                return response;
            }
            else
            {
                return BadRequest("Error on CreateDriver");
            }
        }

        [HttpPost]
        [Route("/api/updateDriver")]
        public async Task<IActionResult> UpdateDriver(DriverBindingModel model)
        {
            IActionResult response = Unauthorized();
            DriverManager manager = new DriverManager(driverRepository);
            string strMessage = manager.UpdateDriver(model, apiConnectionAuthString);
            if (strMessage != "")
            {
                response = Ok(new
                {
                    Message = strMessage,
                });
                return response;
            }
            else
            {
                return BadRequest("Error on UpdateDriver");
            }
        }

        [HttpPost]
        [Route("/api/removeDriver")]
        public async Task<IActionResult> RemoveDriver(DriverBindingModel model)
        {
            IActionResult response = Unauthorized();
            DriverManager manager = new DriverManager(driverRepository);
            string strMessage = manager.DeleteDriver(model, apiConnectionAuthString);
            if (strMessage != "")
            {
                response = Ok(new
                {
                    Message = strMessage,
                });
                return response;
            }
            else
            {
                return BadRequest("Error on RemoveDriver");
            }

        }
    }
}
