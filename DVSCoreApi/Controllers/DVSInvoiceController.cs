﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DVSDomain.Model;
using DVSRepository.Interface;
using DVSService.Contracts;
using DVSService.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Configuration;
using NLog;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DVSCoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize(AuthenticationSchemes = "Bearer")]
    [EnableCors("AllowAllOrigins")]
    public class DVSInvoiceController : ControllerBase
    {
        private NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        IDVSInvoiceRepository DVSInvoiceRepo;
        IConfiguration configuration;
        string apiConnectionAuthString;
        public DVSInvoiceController(IDVSInvoiceRepository DVSInvoiceRepo, IConfiguration configuration)
        {
            this.DVSInvoiceRepo = DVSInvoiceRepo;
            this.configuration = configuration;
            apiConnectionAuthString = Microsoft.Extensions.Configuration.ConfigurationExtensions.GetConnectionString(this.configuration, "apidb");
        }

        /// <summary>
        /// Get DVS invoice list for Admin portal
        /// </summary>
        /// <param name="DriverId"></param>
        /// <param name="InvoiceNo"></param>
        /// <param name="Status"></param>
        /// <param name="fromdate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/DVSInvoiceList")]
        public async Task<IActionResult> GetDVSInvoiceListForAdmin(string DriverId, string InvoiceNo, string Status, string fromdate, string toDate)
        {
            IActionResult response = Unauthorized();

            SearchDVSInvoice model = new SearchDVSInvoice();
            model.Fromdate = fromdate;//Convert.ToDateTime(fromdate);
            model.Todate = toDate;// Convert.ToDateTime(toDate);
            model.DriverId = DriverId;
            model.InvoiceNo = InvoiceNo;
            model.Status = Status;

            IDVSInvoiceManager manager = new DVSInvoiceManager(DVSInvoiceRepo);
            var getData = manager.GetDVSInvoiceListForAdmin(model, apiConnectionAuthString);

            if (getData != null)
            {
                response = Ok(new
                {
                    DVSInvoiceList = getData.Item1,
                    TotalRecord = getData.Item1.Count()
                });
                return response;
            }
            else
            {
                return BadRequest("GetDVSInvoiceListForAdmin fetching error.");
            }

        }

        /// <summary>
        /// Insert Invoice status change history
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/InsertInvoiceStatusHistory")]
        public async Task<IActionResult> InsertInvoiceStatusHistory(InvoiceStatusHistory model)
        {
            IActionResult response = Unauthorized();
            IDVSInvoiceManager manager = new DVSInvoiceManager(DVSInvoiceRepo);
            string strMessage = manager.InsertInvoiceStatusHistory(model, apiConnectionAuthString);
            if (strMessage != "")
            {
                response = Ok(new
                {
                    Message = strMessage,
                });
                return response;
            }
            else
            {
                return BadRequest("Error on InsertInvoiceStatusHistory");
            }
        }


        /// <summary>
        /// Update invoice information from admin end
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/UpdateInvoiceInfo")]
        public async Task<IActionResult> UpdateInvoiceInfo(EditInvoiceInfo model)
        {
            IActionResult response = Unauthorized();
            IDVSInvoiceManager manager = new DVSInvoiceManager(DVSInvoiceRepo);
            string strMessage = manager.UpdateInvoiceInfo(model, apiConnectionAuthString);
            if (strMessage != "")
            {
                response = Ok(new
                {
                    Message = strMessage,
                });
                return response;
            }
            else
            {
                return BadRequest("Error on UpdateInvoiceInfo");
            }
        }

        /// <summary>
        /// Get invoice status changes log by invoice no.
        /// </summary>
        /// <param name="InvoiceNo"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/GetInvoiceStatusChangeHistory")]
        public async Task<IActionResult> GetInvoiceStatusChangeHistory(string InvoiceNo)
        {
            IActionResult response = Unauthorized();
            SearchDVSInvoice model = new SearchDVSInvoice();
            model.InvoiceNo = InvoiceNo;

            IDVSInvoiceManager manager = new DVSInvoiceManager(DVSInvoiceRepo);
            var getData = manager.GetInvoiceStatusChangeHistory(model, apiConnectionAuthString);

            if (getData != null)
            {
                response = Ok(new
                {
                    DVSStatusLog = getData.Item1,
                    TotalRecord = getData.Item1.Count()
                });
                return response;
            }
            else
            {
                return BadRequest("GetInvoiceStatusChangeHistory fetching error.");
            }
        }

    }
}
