﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DVSDomain.Model;
using DVSRepository.Interface;
using DVSService.Contracts;
using DVSService.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Configuration;
using NLog;

namespace DVSCoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize(AuthenticationSchemes = "Bearer")]
    [EnableCors("AllowAllOrigins")]
    public class ARInvoiceController : ControllerBase
    {
        private NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        IARInvoiceRepository ARInvoiceRepo;
        IConfiguration configuration;
        string apiConnectionAuthString;
        public ARInvoiceController(IARInvoiceRepository ARInvoiceRepo, IConfiguration configuration)
        {
            this.ARInvoiceRepo = ARInvoiceRepo;
            this.configuration = configuration;
            apiConnectionAuthString = Microsoft.Extensions.Configuration.ConfigurationExtensions.GetConnectionString(this.configuration, "apidb");
        }

        /// <summary>
        /// Get inserted/ updated invoice list
        /// </summary>
        /// <param name="fromdate"></param>
        /// <param name="toDate"></param>
        /// <param name="InvoiceNo"></param>
        /// <param name="OrderNo"></param>
        /// <param name="City"></param>
        /// <param name="CustCode"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/ARInvoiceList")]
        public async Task<IActionResult> GetInsertedInvoiceList(string fromdate, string toDate, string InvoiceNo, string OrderNo, string City, string Status)
        {
            IActionResult response = Unauthorized();

            SearchARInvoice model = new SearchARInvoice();
            model.FromDate = fromdate;//Convert.ToDateTime(fromdate);
            model.ToDate = toDate;// Convert.ToDateTime(toDate);
            model.OrderNo = OrderNo;
            model.InvoiceNo = InvoiceNo;
            model.City = City;
            model.Status = Status;

            IARInvoiceManager manager = new ARInvoiceManager(ARInvoiceRepo);
            var getData = manager.GetInsertedInvoiceList(model, apiConnectionAuthString);

            if (getData != null)
            {
                response = Ok(new
                {
                    InvoiceList = getData.Item1,
                    TotalRecord = getData.Item1.Count()
                });
                return response;
            }
            else
            {
                return BadRequest("GetInsertedInvoiceList fetching error.");
            }

        }

        /// <summary>
        /// Update Invoice status Quality check
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/UpdateInvoiceStatus")]
        public async Task<IActionResult> UpdateInvoiceStatus(approveInv model)
        {
            IActionResult response = Unauthorized();
            IARInvoiceManager manager = new ARInvoiceManager(ARInvoiceRepo);
            string strMessage = manager.UpdateInvoiceStatus(model, apiConnectionAuthString);
            if (strMessage != "")
            {
                response = Ok(new
                {
                    Message = strMessage,
                });
                return response;
            }
            else
            {
                return BadRequest("Error on UpdateInvoiceStatus");
            }
        }

        /// <summary>
        /// Get AR customer city list
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/ARCustCity")]
        public async Task<IActionResult> GetCustCity()
        {
            IActionResult response = Unauthorized();
            IARInvoiceManager manager = new ARInvoiceManager(ARInvoiceRepo);
            var getData = manager.GetCustCity(apiConnectionAuthString);
            if (getData != null)
            {
                response = Ok(new
                {
                    customerList = getData.Item1,
                    TotalRecord = getData.Item1.Count()
                });
                return response;
            }
            else
            {
                return BadRequest("GetCustCity fetching error.");
            }
        }

        /// <summary>
        /// Get Data For CSv Export
        /// </summary>
        /// <param name="fromdate"></param>
        /// <param name="toDate"></param>
        /// <param name="InvoiceNo"></param>
        /// <param name="OrderNo"></param>
        /// <param name="City"></param>
        /// <param name="Status"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/ARCSVDataExport")]
        public async Task<IActionResult> GetDataForCSvExport(string fromdate, string toDate, string InvoiceNo, string OrderNo, string City, string Status)
        {
            IActionResult response = Unauthorized();

            SearchARInvoice model = new SearchARInvoice();
            model.FromDate = fromdate;//Convert.ToDateTime(fromdate);
            model.ToDate = toDate;// Convert.ToDateTime(toDate);
            model.OrderNo = OrderNo;
            model.InvoiceNo = InvoiceNo;
            model.City = City;
            model.Status = Status;

            IARInvoiceManager manager = new ARInvoiceManager(ARInvoiceRepo);
            var getData = manager.GetDataForCSvExport(model, apiConnectionAuthString);

            if (getData != null)
            {
                response = Ok(new
                {
                    InvoiceList = getData.Item1,
                    TotalRecord = getData.Item1.Count()
                });
                return response;
            }
            else
            {
                return BadRequest("GetDataForCSvExport fetching error.");
            }

        }
    }
}
