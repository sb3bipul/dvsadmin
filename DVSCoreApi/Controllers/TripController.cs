﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DVSDomain.Model;
using DVSRepository.Interface;
using DVSService.Contracts;
using DVSService.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NLog;

namespace DVSCoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [EnableCors("AllowAllOrigins")]
    public class TripController : Controller
    {
        private NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        ITripRepository tripRepository;
        IConfiguration configuration;
        string apiConnectionAuthString;
        public TripController(ITripRepository tripRepository, IConfiguration configuration)
        {
            this.tripRepository = tripRepository;
            this.configuration = configuration;
            apiConnectionAuthString = Microsoft.Extensions.Configuration.ConfigurationExtensions.GetConnectionString(this.configuration, "apidb");
        }

        [HttpGet]
        //[AllowAnonymous]
        //[Authorize]
        [Route("/api/triporders")]
        public async Task<IActionResult> GetTripOrder(int ZoneId,int SalesmanId, int NoOfMonth, string SearchByCustomer, string SearchByCustomerAddress, string OrderNo)
        {
            IActionResult response = Unauthorized();

            GetTripDataBindingModel model = new GetTripDataBindingModel();
            model.ZoneId = ZoneId;
            model.SalesmanId = SalesmanId;
            model.NoOfMonth = NoOfMonth;
            model.SearchByCustomer = SearchByCustomer;
            model.SearchByCustomerAddress = SearchByCustomerAddress;
            model.OrderNo = OrderNo;
            model.Totalrow = 0;

            ITripManager manager = new TripManager(tripRepository);
            var getData = manager.GetTripOrder(model, apiConnectionAuthString);

            if (getData != null)
            {
                response = Ok(new
                {
                    TripOrders = getData
                });
                return response;
            }
            else
            {
                return BadRequest("TripOrder fetching error.");
            }

        }

        [HttpPost]
        [Route("/api/addtrip")]
        public async Task<IActionResult> CreateTrip(TripDataBindingModel model)
        {
            IActionResult response = Unauthorized();
            TripManager manager = new TripManager(tripRepository);
            string strMessage = manager.CreateTrip(model, apiConnectionAuthString);
            if (strMessage != "")
            {
                response = Ok(new
                {
                    Message = strMessage,
                });
                return response;
            }
            else
            {
                return BadRequest("Error on CreateTrip");
            }

        }

        [HttpPost]
        [Route("/api/edittrip")]
        public async Task<IActionResult> EditTrip(TripDataBindingModel model)
        {
            IActionResult response = Unauthorized();
            TripManager manager = new TripManager(tripRepository);
            string strMessage = manager.EditTrip(model, apiConnectionAuthString);
            if (strMessage != "")
            {
                response = Ok(new
                {
                    Message = strMessage,
                });
                return response;
            }
            else
            {
                return BadRequest("Error on CreateTrip");
            }

        }

        [HttpGet]
        [Route("/api/tripInfos")]
        public async Task<IActionResult> GetAllTripInfo(DateTime StartDate, string TripStatus, string SearchText)
        {
            IActionResult response = Unauthorized();

            GetTripInfoDataBindingModel model = new GetTripInfoDataBindingModel();
            model.StartDate = StartDate;
            model.TripStatus = TripStatus;
            model.SearchText = SearchText;

            ITripManager manager = new TripManager(tripRepository);
            var getData = manager.GetAllTripInfo(model, apiConnectionAuthString);

            if (getData != null)
            {
                response = Ok(new
                {
                    TripInfo = getData.Item1,
                    TotalRecord = getData.Item2
                });
                return response;
            }
            else
            {
                return BadRequest("GetAllTripInfo fetching error.");
            }

        }

        [HttpGet]
        //[AllowAnonymous]
        //[Authorize]
        [Route("/api/triporder")]
        public async Task<IActionResult> GetTripById(string TripId)
        {
            IActionResult response = Unauthorized();

            ITripManager manager = new TripManager(tripRepository);
            var getData = manager.GetTripById(TripId, apiConnectionAuthString);

            if (getData != null)
            {
                response = Ok(new
                {
                    TripOrders = getData
                });
                return response;
            }
            else
            {
                return BadRequest("GetTripById fetching error.");
            }

        }

        [HttpGet]
        //[AllowAnonymous]
        //[Authorize]
        [Route("/api/orderbytrip")]
        public async Task<IActionResult> GetAllOrderByTripId(string TripId)
        {
            IActionResult response = Unauthorized();

            ITripManager manager = new TripManager(tripRepository);
            var getData = manager.GetAllOrderByTripId(TripId, apiConnectionAuthString);

            if (getData != null)
            {
                response = Ok(new
                {
                    TripOrders = getData
                });
                return response;
            }
            else
            {
                return BadRequest("GetAllOrderByTripId fetching error.");
            }

        }

        [HttpGet]
        [Route("/api/tripTrackingInfos")]
        public async Task<IActionResult> GetAllTripTrackingInfo(string SearchText)
        {
            IActionResult response = Unauthorized();

            ITripManager manager = new TripManager(tripRepository);
            var getData = manager.GetAllTripTrackingInfo(SearchText, apiConnectionAuthString);

            if (getData != null)
            {
                response = Ok(new
                {
                    TripInfo = getData.Item1,
                    TotalRecord = getData.Item2
                });
                return response;
            }
            else
            {
                return BadRequest("GetAllTripInfo fetching error.");
            }

        }

        [HttpGet]
        [Route("/api/tripTrackingDetail")]
        public async Task<IActionResult> GetTripTrackingDetail(string TripId)
        {
            IActionResult response = Unauthorized();

            ITripManager manager = new TripManager(tripRepository);
            var getData = manager.GetTripTrackingDetail(TripId, apiConnectionAuthString);

            if (getData != null)
            {
                response = Ok(new
                {
                    TripInfo = getData,
                });
                return response;
            }
            else
            {
                return BadRequest("GetTripTrackingDetail fetching error.");
            }

        }

        [HttpGet]
        //[AllowAnonymous]
        //[Authorize]
        [Route("/api/deliverysummerytrip")]
        public async Task<IActionResult> GetDeliverySummeryTripId(string TripId)
        {
            IActionResult response = Unauthorized();

            ITripManager manager = new TripManager(tripRepository);
            var getData = manager.GetDeliverySummeryTripId(TripId, apiConnectionAuthString);

            if (getData != null)
            {
                response = Ok(new
                {
                    TripSummaries = getData
                });
                return response;
            }
            else
            {
                return BadRequest("GetAllOrderByTripId fetching error.");
            }

        }
    }
}
