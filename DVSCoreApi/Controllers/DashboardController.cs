﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DVSRepository.Interface;
using DVSService.Contracts;
using DVSService.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NLog;

namespace DVSCoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [EnableCors("AllowAllOrigins")]
    public class DashboardController : Controller
    {
        private NLog.ILogger logger = LogManager.GetCurrentClassLogger();
        IDashboardRepository dashboardRepository;
        IConfiguration configuration;
        string apiConnectionAuthString;
        public DashboardController(IDashboardRepository dashboradRepository, IConfiguration configuration)
        {
            this.dashboardRepository = dashboradRepository;
            this.configuration = configuration;
            apiConnectionAuthString = Microsoft.Extensions.Configuration.ConfigurationExtensions.GetConnectionString(this.configuration, "apidb");
        }

        [HttpGet]
        //[AllowAnonymous]
        //[Authorize]
        [Route("/api/dashboard")]
        public async Task<IActionResult> GetDashboardItem(int companyId)
        {
            IActionResult response = Unauthorized();
            IDashboardManager manager = new DashboardManager(dashboardRepository);
            var getData = manager.GetDashboardItem(companyId, apiConnectionAuthString);
            if (getData != null)
            {
                //var tokenString = GenerateJWTToken(appuser);
                response = Ok(new
                {
                    //token = tokenString,
                    DashboardItem = getData,
                });
                return response;
            }
            else
            {
                return BadRequest("Setting fetching error.");
            }

        }

    }
}
