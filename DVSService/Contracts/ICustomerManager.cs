﻿using DVSDomain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Contracts
{
    public interface ICustomerManager
    {
        public Tuple<List<Customer>, int> GetCustomer(GetDataBindingModel model, string apiConnectionAuthString);
        public Tuple<List<Customer>, int> GetCutomerByID(GetDataBindingModel model, string apiConnectionAuthString);
    }
}
