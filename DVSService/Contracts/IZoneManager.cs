﻿using DVSDomain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Contracts
{
    public interface IZoneManager
    {
        public Tuple<List<Zone>, int> GetZone(GetDataBindingModel model, string apiConnectionAuthString);
        public string CreateZone(ZoneBindingModel model, string apiConnectionAuthString);
        public string UpdateZone(ZoneBindingModel model, string apiConnectionAuthString);
        public string DeleteZone(ZoneBindingModel model, string apiConnectionAuthString);
    }
}
