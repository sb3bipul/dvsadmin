﻿using DVSDomain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Contracts
{
    public interface ISettingManager
    {
        public Tuple<List<Location>, int> GetLocation(GetDataBindingModel model, string apiConnectionAuthString);
        public string CreateLocation(LocationBindingModel model, string apiConnectionAuthString);
        public string UpdateLocation(LocationBindingModel model, string apiConnectionAuthString);
        public string DeleteLocation(LocationBindingModel model, string apiConnectionAuthString);
        public Setting GetSetting(GetDataBindingModel model, string apiConnectionAuthString);
        public string UpdateSetting(SettingBindingModel model, string apiConnectionAuthString);
    }
}
