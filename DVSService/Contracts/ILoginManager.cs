﻿using DVSDomain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Contracts
{
    public interface ILoginManager
    {
        public ApplicationUser GetUserProfile(LogindBindingModel model, string apiConnectionAuthString);
        public ApplicationUser GetUserProfileAfterPasswordReset(ChangePasswordBindingModel model, string apiConnectionAuthString);
    }
}
