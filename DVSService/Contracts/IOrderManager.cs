﻿using DVSDomain.Model;
using DVSRepository.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Contracts
{
    public interface IOrderManager
    {
        public Tuple<List<OrderInfoViewModel>, int> GetAllOrderInfo(GetOrderInfoDataBindingModel model, string apiConnectionAuthString);
        public Tuple<OrderDetailViewModel, int> GetOrderDetail(GetOrderDetailBindingModel model, string apiConnectionAuthString);
    }
}
