﻿using DVSDomain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Contracts
{
    public interface IVehicleTypeManager
    {
        public Tuple<List<VehicleType>, int> GetAllVehiclesType(GetDataBindingModel model, string apiConnectionAuthString);
        public string CreateVehiclesType(VehicleTypeBindingModel model, string apiConnectionAuthString);
        public string UpdateVehiclesType(VehicleTypeBindingModel model, string apiConnectionAuthString);
    }
}
