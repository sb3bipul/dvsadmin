﻿using DVSDomain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Contracts
{
    public interface ISalesmanManager
    {
        public Tuple<List<Salesman>, int> GetSalesMan(GetDataBindingModel model, string apiConnectionAuthString);
    }
}
