﻿using DVSDomain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Contracts
{
    public interface IDriverManager
    {
        Tuple<List<Driver>, int> GetDriver(GetDataBindingModel model, string apiConnectionAuthString);
        public string CreateDriver(DriverBindingModel model, string apiConnectionAuthString);
        public string UpdateDriver(DriverBindingModel model, string apiConnectionAuthString);
        public string DeleteDriver(DriverBindingModel model, string apiConnectionAuthString);

    }
}
