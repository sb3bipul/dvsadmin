﻿using DVSRepository.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Contracts
{
    public interface IDashboardManager
    {
        public DashboardViewModel GetDashboardItem(int companyId, string apiConnectionAuthString);
    }
}
