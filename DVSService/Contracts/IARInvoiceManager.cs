﻿using System;
using System.Collections.Generic;
using System.Text;
using DVSDomain.Model;
using DVSRepository.ViewModel;

namespace DVSService.Contracts
{
    public interface IARInvoiceManager
    {
        public Tuple<List<ARInvoiceViewModel>, int> GetInsertedInvoiceList(SearchARInvoice model, string apiConnectionAuthString);
        //public Tuple<List<ARApproveInvoiceViewModel>, int> UpdateInvoiceStatus(approveInv model, string apiConnectionAuthString);
        public string UpdateInvoiceStatus(approveInv model, string apiConnectionAuthString);

        public Tuple<List<ARCustCityViewModel>, int> GetCustCity(string apiConnectionAuthString);
        public Tuple<List<ARCSVDataViewModel>, int> GetDataForCSvExport(SearchARInvoice model, string apiConnectionAuthString);
    }
}
