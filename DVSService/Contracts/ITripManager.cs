﻿using DVSDomain.Model;
using DVSRepository.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Contracts
{
    public interface ITripManager
    {
        public List<TripOrderViewModel> GetTripOrder(GetTripDataBindingModel model, string apiConnectionAuthString);
        public string CreateTrip(TripDataBindingModel model, string apiConnectionAuthString);
        public string EditTrip(TripDataBindingModel model, string apiConnectionAuthString);
        public Tuple<List<TripInfoViewModel>, int> GetAllTripInfo(GetTripInfoDataBindingModel model, string apiConnectionAuthString);
        public List<TripOrderViewModel> GetTripById(string TripId, string apiConnectionAuthString);
        public List<TripOrderViewModel> GetAllOrderByTripId(string TripId, string apiConnectionAuthString);
        public Tuple<List<TripInfoViewModel>, int> GetAllTripTrackingInfo(string searchText, string apiConnectionAuthString);
        public TripTrackingViewModel GetTripTrackingDetail(string TripId, string apiConnectionAuthString);
        List<DeliverySummeryCustomerViewModel> GetDeliverySummeryTripId(string TripId, string apiConnectionAuthString);
    }
}
