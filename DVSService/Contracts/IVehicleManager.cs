﻿using DVSDomain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Contracts
{
    public interface IVehicleManager
    {
        public Tuple<List<Vehicle>, int> GetVehicle(GetDataBindingModel model, string apiConnectionAuthString);
        public string CreateVehicle(VehicleBindingModel model, string apiConnectionAuthString);
        public string UpdateVehicleType(VehicleBindingModel model, string apiConnectionAuthString);
        public string DeleteVehicleType(VehicleBindingModel model, string apiConnectionAuthString);

    }
}
