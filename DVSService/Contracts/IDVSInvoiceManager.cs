﻿using System;
using System.Collections.Generic;
using System.Text;
using DVSDomain.Model;
using DVSRepository.ViewModel;

namespace DVSService.Contracts
{
    public interface IDVSInvoiceManager
    {
        public Tuple<List<DVSInvoiceViewModel>, int> GetDVSInvoiceListForAdmin(SearchDVSInvoice model, string apiConnectionAuthString);
        public string InsertInvoiceStatusHistory(InvoiceStatusHistory model, string apiConnectionAuthString);
        public string UpdateInvoiceInfo(EditInvoiceInfo model, string apiConnectionAuthString);
        public Tuple<List<StatusChangeHistoryViewModel>, int> GetInvoiceStatusChangeHistory(SearchDVSInvoice model, string apiConnectionAuthString);
    }
}
