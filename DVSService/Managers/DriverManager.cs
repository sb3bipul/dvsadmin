﻿using DVSDomain.Model;
using DVSRepository.Implementation;
using DVSRepository.Interface;
using DVSService.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Managers
{
    public class DriverManager : IDriverManager
    {
        private IDriverRepository repObj;
        public DriverManager(IDriverRepository driverRepository)
        {
            this.repObj = driverRepository;
        }
        public Tuple<List<Driver>, int> GetDriver(GetDataBindingModel model, string apiConnectionAuthString)
        {
            List<Driver> lstVehicalType = new List<Driver>();
            repObj = new DriverRepository();
            var getData = repObj.GetDriver(model, apiConnectionAuthString);
            return getData;
        }

        public string CreateDriver(DriverBindingModel model, string apiConnectionAuthString)
        {
            string strMessage = "";
            repObj = new DriverRepository();
            model.ActionMode = ActionMode.Add;
            strMessage = repObj.CRUDDriver(model, apiConnectionAuthString);
            return strMessage;
        }

        public string UpdateDriver(DriverBindingModel model, string apiConnectionAuthString)
        {
            string strMessage = "";
            repObj = new DriverRepository();
            model.ActionMode = ActionMode.Edit;
            strMessage = repObj.CRUDDriver(model, apiConnectionAuthString);
            return strMessage;
        }

        public string DeleteDriver(DriverBindingModel model, string apiConnectionAuthString)
        {
            string strMessage = "";
            repObj = new DriverRepository();
            model.ActionMode = ActionMode.Delete;
            strMessage = repObj.CRUDDriver(model, apiConnectionAuthString);
            return strMessage;
        }
    }
}
