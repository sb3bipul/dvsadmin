﻿using DVSDomain.Model;
using DVSRepository.Implementation;
using DVSRepository.Interface;
using DVSRepository.ViewModel;
using DVSService.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Managers
{
    public class OrderManager : IOrderManager
    {
        private IOrderRepository repObj;
        public OrderManager(IOrderRepository OrderRepository)
        {
            this.repObj = OrderRepository;
            //apiConnectionAuthString = connectionString;
        }
        public Tuple<List<OrderInfoViewModel>, int> GetAllOrderInfo(GetOrderInfoDataBindingModel model, string apiConnectionAuthString)
        {
            repObj = new OrderRepository();
            var getData = repObj.GetAllOrderInfo(model, apiConnectionAuthString);
            return getData;
        }
        public Tuple<OrderDetailViewModel, int> GetOrderDetail(GetOrderDetailBindingModel model, string apiConnectionAuthString)
        {
            repObj = new OrderRepository();
            var getData = repObj.GetOrderDetail(model, apiConnectionAuthString);
            return getData;
        }
    }
}
