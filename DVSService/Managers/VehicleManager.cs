﻿using DVSDomain.Model;
using DVSRepository.Implementation;
using DVSRepository.Interface;
using DVSService.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Managers
{
    public class VehicleManager : IVehicleManager
    {
        private IVehicleRepository repObj;
        public VehicleManager(IVehicleRepository vehicleRepository)
        {
            this.repObj = vehicleRepository;
        }
        public Tuple<List<Vehicle>, int> GetVehicle(GetDataBindingModel model, string apiConnectionAuthString)
        {
            repObj = new VehicleRepository();
            var getData = repObj.GetVehicles(model, apiConnectionAuthString);
            return getData;
        }

        public string CreateVehicle(VehicleBindingModel model, string apiConnectionAuthString)
        {
            string strMessage = "";
            repObj = new VehicleRepository();
            model.ActionMode = ActionMode.Add;
            strMessage = repObj.CRUDVehicle(model, apiConnectionAuthString);
            return strMessage;
        }
        public string UpdateVehicleType(VehicleBindingModel model, string apiConnectionAuthString)
        {
            string strMessage = "";
            repObj = new VehicleRepository();
            model.ActionMode = ActionMode.Edit;
            strMessage = repObj.CRUDVehicle(model, apiConnectionAuthString);
            return strMessage;
        }

        public string DeleteVehicleType(VehicleBindingModel model, string apiConnectionAuthString)
        {
            string strMessage = "";
            repObj = new VehicleRepository();
            model.ActionMode = ActionMode.Delete;
            strMessage = repObj.CRUDVehicle(model, apiConnectionAuthString);
            return strMessage;
        }
    }
}
