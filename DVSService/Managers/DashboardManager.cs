﻿using DVSRepository.Implementation;
using DVSRepository.Interface;
using DVSRepository.ViewModel;
using DVSService.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Managers
{
    public class DashboardManager : IDashboardManager
    {
        private IDashboardRepository repObj;
        public DashboardManager(IDashboardRepository dashboardRepository)
        {
            this.repObj = dashboardRepository;
            //apiConnectionAuthString = connectionString;
        }
        public DashboardViewModel GetDashboardItem(int companyId, string apiConnectionAuthString)
        {
            repObj = new DashboardRepository();
            var getData = repObj.GetDashboardItem(companyId, apiConnectionAuthString);
            return getData;
        }
    }
}
