﻿using DVSDomain.Model;
using DVSRepository.Implementation;
using DVSRepository.Interface;
using DVSService.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Managers
{
    public class VehicleTypeManager : IVehicleTypeManager
    {
        private IVehicleTypeRepository repObj;
        public VehicleTypeManager(IVehicleTypeRepository vehicalTypeRepository)
        {
            this.repObj = vehicalTypeRepository;
            //apiConnectionAuthString = connectionString;
        }
        public Tuple<List<VehicleType>, int> GetAllVehiclesType(GetDataBindingModel model, string apiConnectionAuthString)
        {
            List<VehicleType> lstVehicalType = new List<VehicleType>();
            repObj = new VehicleTypeRepository();
            var getData = repObj.GetAllVehiclesType(model, apiConnectionAuthString);
            //lstVehicalType = repObj.GetAllVehiclesType(model, apiConnectionAuthString);
            return getData;
        }
        public string CreateVehiclesType(VehicleTypeBindingModel model, string apiConnectionAuthString)
        {
            string strMessage = "";
            repObj = new VehicleTypeRepository();
            model.ActionMode = ActionMode.Add;
            strMessage = repObj.CRUDVehicleType(model, apiConnectionAuthString);
            return strMessage;
        }
        public string UpdateVehiclesType(VehicleTypeBindingModel model, string apiConnectionAuthString)
        {
            string strMessage = "";
            repObj = new VehicleTypeRepository();
            model.ActionMode = ActionMode.Edit;
            strMessage = repObj.CRUDVehicleType(model, apiConnectionAuthString);
            return strMessage;
        }

        public string DeleteVehiclesType(VehicleTypeBindingModel model, string apiConnectionAuthString)
        {
            string strMessage = "";
            repObj = new VehicleTypeRepository();
            model.ActionMode = ActionMode.Delete;
            strMessage = repObj.CRUDVehicleType(model, apiConnectionAuthString);
            return strMessage;
        }
    }
}
