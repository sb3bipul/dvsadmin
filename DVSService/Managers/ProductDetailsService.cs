﻿using DVSDomain.Model;
using DVSRepository.Interface;
using DVSService.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Managers
{
    public class ProductDetailsService : IProductDetailsService
    {
        private IRepository<ProductDetails> productDetailsRepository;

        public ProductDetailsService(IRepository<ProductDetails> productDetailsRepository)
        {
            this.productDetailsRepository = productDetailsRepository;
        }

        public ProductDetails GetProductDetail(int id)
        {
            return productDetailsRepository.Get(id);
        }
    }
}
