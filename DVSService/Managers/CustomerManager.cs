﻿using DVSDomain.Model;
using DVSRepository.Implementation;
using DVSRepository.Interface;
using DVSService.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Managers
{
    public class CustomerManager : ICustomerManager
    {
        private ICustomerRepository repObj;
        public CustomerManager(ICustomerRepository customerRepository)
        {
            this.repObj = customerRepository;
        }
        public Tuple<List<Customer>, int> GetCustomer(GetDataBindingModel model, string apiConnectionAuthString)
        {
            repObj = new CustomerRepository();
            var getData = repObj.GetCustomer(model, apiConnectionAuthString);
            return getData;
        }
        public Tuple<List<Customer>, int> GetCutomerByID(GetDataBindingModel model, string apiConnectionAuthString)
        {
            repObj = new CustomerRepository();
            var getData = repObj.GetCustomers(model, apiConnectionAuthString);
            return getData;
        }
    }
}
