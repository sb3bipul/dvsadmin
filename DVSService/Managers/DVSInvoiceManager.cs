﻿using System;
using System.Collections.Generic;
using System.Text;
using DVSDomain.Model;
using DVSRepository.Implementation;
using DVSRepository.Interface;
using DVSRepository.ViewModel;
using DVSService.Contracts;

namespace DVSService.Managers
{
    public class DVSInvoiceManager : IDVSInvoiceManager
    {
        private IDVSInvoiceRepository repObj;
        public DVSInvoiceManager(IDVSInvoiceRepository DVSInvoiceRepo)
        {
            this.repObj = DVSInvoiceRepo;
        }
        public Tuple<List<DVSInvoiceViewModel>, int> GetDVSInvoiceListForAdmin(SearchDVSInvoice model, string apiConnectionAuthString)
        {
            repObj = new DVSInvoiceRepository();
            var getData = repObj.GetDVSInvoiceListForAdmin(model, apiConnectionAuthString);
            return getData;
        }

        public string InsertInvoiceStatusHistory(InvoiceStatusHistory model, string apiConnectionAuthString)
        {
            string strMessage = "";
            repObj = new DVSInvoiceRepository();
            strMessage = repObj.InsertInvoiceStatusHistory(model, apiConnectionAuthString);
            return strMessage;
        }

        public string UpdateInvoiceInfo(EditInvoiceInfo model, string apiConnectionAuthString)
        {
            string strMessage = "";
            repObj = new DVSInvoiceRepository();
            strMessage = repObj.UpdateInvoiceInfo(model, apiConnectionAuthString);
            return strMessage;
        }

        public Tuple<List<StatusChangeHistoryViewModel>, int> GetInvoiceStatusChangeHistory(SearchDVSInvoice model, string apiConnectionAuthString)
        {
            repObj = new DVSInvoiceRepository();
            var getData = repObj.GetInvoiceStatusChangeHistory(model, apiConnectionAuthString);
            return getData;
        }
    }
}
