﻿using DVSDomain.Model;
using DVSRepository.Implementation;
using DVSRepository.Interface;
using DVSService.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Managers
{
    public class SalesmanManager : ISalesmanManager
    {
        private ISalesmanRepository repObj;
        public SalesmanManager(ISalesmanRepository salesmanRepository)
        {
            this.repObj = salesmanRepository;
        }
        public Tuple<List<Salesman>, int> GetSalesMan(GetDataBindingModel model, string apiConnectionAuthString)
        {
            repObj = new SalesmanRepository();
            var getData = repObj.GetSalesMan(model, apiConnectionAuthString);
            return getData;
        }
        
    }
}
