﻿using DVSDomain.Model;
using DVSRepository.Implementation;
using DVSRepository.Interface;
using DVSService.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Managers
{
    public class ZoneManager : IZoneManager
    {
        private IZoneRepository repObj;
        public ZoneManager(IZoneRepository zoneRepository)
        {
            this.repObj = zoneRepository;
            //apiConnectionAuthString = connectionString;
        }
        public Tuple<List<Zone>, int> GetZone(GetDataBindingModel model, string apiConnectionAuthString)
        {
            repObj = new ZoneRepository();
            var getData = repObj.GetZone(model, apiConnectionAuthString);
            //lstVehicalType = repObj.GetAllVehiclesType(model, apiConnectionAuthString);
            return getData;
        }
        public string CreateZone(ZoneBindingModel model, string apiConnectionAuthString)
        {
            string strMessage = "";
            repObj = new ZoneRepository();
            model.ActionMode = ActionMode.Add;
            strMessage = repObj.CRUDZone(model, apiConnectionAuthString);
            return strMessage;
        }
        public string UpdateZone(ZoneBindingModel model, string apiConnectionAuthString)
        {
            string strMessage = "";
            repObj = new ZoneRepository();
            model.ActionMode = ActionMode.Edit;
            strMessage = repObj.CRUDZone(model, apiConnectionAuthString);
            return strMessage;
        }

        public string DeleteZone(ZoneBindingModel model, string apiConnectionAuthString)
        {
            string strMessage = "";
            repObj = new ZoneRepository();
            model.ActionMode = ActionMode.Delete;
            strMessage = repObj.CRUDZone(model, apiConnectionAuthString);
            return strMessage;
        }
    }
}
