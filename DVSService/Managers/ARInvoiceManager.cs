﻿using System;
using System.Collections.Generic;
using System.Text;
using DVSDomain.Model;
using DVSRepository.Implementation;
using DVSRepository.Interface;
using DVSRepository.ViewModel;
using DVSService.Contracts;

namespace DVSService.Managers
{
    public class ARInvoiceManager : IARInvoiceManager
    {
        private IARInvoiceRepository repObj;
        public ARInvoiceManager(IARInvoiceRepository ARInvoiceRepo)
        {
            this.repObj = ARInvoiceRepo;
            //apiConnectionAuthString = connectionString;
        }
        public Tuple<List<ARInvoiceViewModel>, int> GetInsertedInvoiceList(SearchARInvoice model, string apiConnectionAuthString)
        {
            repObj = new ARInvoiceRepository();
            var getData = repObj.GetInsertedInvoiceList(model, apiConnectionAuthString);
            return getData;
        }

        public string UpdateInvoiceStatus(approveInv model, string apiConnectionAuthString)
        {
            string strMessage = "";
            repObj = new ARInvoiceRepository();
            strMessage = repObj.UpdateInvoiceStatus(model, apiConnectionAuthString);
            return strMessage;
        }

        public Tuple<List<ARCustCityViewModel>, int> GetCustCity(string apiConnectionAuthString)
        {
            repObj = new ARInvoiceRepository();
            var getData = repObj.GetCustCity(apiConnectionAuthString);
            return getData;
        }

        public Tuple<List<ARCSVDataViewModel>, int> GetDataForCSvExport(SearchARInvoice model, string apiConnectionAuthString)
        {
            repObj = new ARInvoiceRepository();
            var getData = repObj.GetDataForCSvExport(model, apiConnectionAuthString);
            return getData;
        }
    }
}
