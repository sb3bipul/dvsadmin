﻿using DVSDomain.Model;
using DVSRepository.Implementation;
using DVSRepository.Interface;
using DVSService.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Managers
{
    public class LoginManager : ILoginManager
    {
        //private IUserRepository userRepository;
        string apiConnectionAuthString;
        private IUserAuthorizationRepository repObj;
        private ApplicationUser appuser;
        public LoginManager()
        {
            //this.userRepository = userRepository;
            //apiConnectionAuthString = connectionString;
        }
        public ApplicationUser GetUserProfile(LogindBindingModel model, string apiConnectionAuthString)
        {
            repObj = new UserAuthorizationRepository();
            appuser = repObj.GetUserProfile(model, apiConnectionAuthString);
            return appuser;
        }
        public ApplicationUser GetUserProfileAfterPasswordReset(ChangePasswordBindingModel model, string apiConnectionAuthString)
        {
            repObj = new UserAuthorizationRepository();
            LogindBindingModel loginuser = new LogindBindingModel { UserName = model.UserName, Password = model.OldPassword };
    
            appuser = repObj.GetUserProfile(loginuser, apiConnectionAuthString);

            if(appuser != null)
            {
              bool  appuserupdate = repObj.UpdateUserPassword(model, apiConnectionAuthString);
                if (!appuserupdate)
                {
                    appuser = null;
                }
            }
            
            return appuser;
        }
        //public IEnumerable<Product> GetProduct()
        //{
        //    return productRepository.GetAll();
        //}

        //public Product GetProduct(int id)
        //{
        //    return productRepository.Get(id);
        //}
    }
}
