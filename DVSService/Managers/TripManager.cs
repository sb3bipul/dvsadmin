﻿using DVSDomain.Model;
using DVSRepository.Implementation;
using DVSRepository.Interface;
using DVSRepository.ViewModel;
using DVSService.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Managers
{
    public class TripManager : ITripManager
    {
        private ITripRepository repObj;
        public TripManager(ITripRepository tripRepository)
        {
            this.repObj = tripRepository;
            //apiConnectionAuthString = connectionString;
        }
        public List<TripOrderViewModel> GetTripOrder(GetTripDataBindingModel model,string apiConnectionAuthString)
        {
            repObj = new TripRepository();
            var getData = repObj.GetTripOrder(model, apiConnectionAuthString);
            return getData;
        }

        public string CreateTrip(TripDataBindingModel model, string apiConnectionAuthString)
        {
            string strMessage = "";
            repObj = new TripRepository();
            model.ActionMode = ActionMode.Add;
            strMessage = repObj.CRUDTrip(model, apiConnectionAuthString);
            return strMessage;
        }

        public string EditTrip(TripDataBindingModel model, string apiConnectionAuthString)
        {
            string strMessage = "";
            repObj = new TripRepository();
            model.ActionMode = ActionMode.Edit;
            strMessage = repObj.CRUDTrip(model, apiConnectionAuthString);
            return strMessage;
        }

        public Tuple<List<TripInfoViewModel>, int> GetAllTripInfo(GetTripInfoDataBindingModel model, string apiConnectionAuthString)
        {
            repObj = new TripRepository();
            var getData = repObj.GetAllTripInfo(model, apiConnectionAuthString);
            return getData;
        }

        public List<TripOrderViewModel> GetTripById(string TripId, string apiConnectionAuthString)
        {
            repObj = new TripRepository();
            var getData = repObj.GetTripById(TripId, apiConnectionAuthString);
            return getData;
        }
        public List<TripOrderViewModel> GetAllOrderByTripId(string TripId, string apiConnectionAuthString)
        {
            repObj = new TripRepository();
            var getData = repObj.GetAllOrderByTripId(TripId, apiConnectionAuthString);
            return getData;
        }
        public Tuple<List<TripInfoViewModel>, int> GetAllTripTrackingInfo(string searchText, string apiConnectionAuthString)
        {
            repObj = new TripRepository();
            var getData = repObj.GetAllTripTrackingInfo(searchText, apiConnectionAuthString);
            return getData;
        }

        public TripTrackingViewModel GetTripTrackingDetail(string TripId, string apiConnectionAuthString)
        {
            repObj = new TripRepository();
            var getData = repObj.GetTripTrackingDetail(TripId, apiConnectionAuthString);
            return getData;
        }
        public List<DeliverySummeryCustomerViewModel> GetDeliverySummeryTripId(string TripId, string apiConnectionAuthString)
        {
            repObj = new TripRepository();
            var getData = repObj.GetDeliverySummeryTripId(TripId, apiConnectionAuthString);
            return getData;
        }
    }
}
