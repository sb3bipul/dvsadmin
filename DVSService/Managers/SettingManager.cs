﻿using DVSDomain.Model;
using DVSRepository.Implementation;
using DVSRepository.Interface;
using DVSService.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSService.Managers
{
    public class SettingManager : ISettingManager
    {
        private ISettingRepository repObj;
        public SettingManager(ISettingRepository settingRepository)
        {
            this.repObj = settingRepository;
            //apiConnectionAuthString = connectionString;
        }
        public Tuple<List<Location>, int> GetLocation(GetDataBindingModel model, string apiConnectionAuthString)
        {
            repObj = new SettingRepository();
            var getData = repObj.GetLocation(model, apiConnectionAuthString);
            return getData;
        }
        public string CreateLocation(LocationBindingModel model, string apiConnectionAuthString)
        {
            string strMessage = "";
            repObj = new SettingRepository();
            model.ActionMode = ActionMode.Add;
            strMessage = repObj.CRUDLocation(model, apiConnectionAuthString);
            return strMessage;
        }
        public string UpdateLocation(LocationBindingModel model, string apiConnectionAuthString)
        {
            string strMessage = "";
            repObj = new SettingRepository();
            model.ActionMode = ActionMode.Edit;
            strMessage = repObj.CRUDLocation(model, apiConnectionAuthString);
            return strMessage;
        }

        public string DeleteLocation(LocationBindingModel model, string apiConnectionAuthString)
        {
            string strMessage = "";
            repObj = new SettingRepository();
            model.ActionMode = ActionMode.Delete;
            strMessage = repObj.CRUDLocation(model, apiConnectionAuthString);
            return strMessage;
        }
        public Setting GetSetting(GetDataBindingModel model, string apiConnectionAuthString)
        {
            repObj = new SettingRepository();
            var getData = repObj.GetSetting(model, apiConnectionAuthString);
            return getData;
        }
        public string UpdateSetting(SettingBindingModel model, string apiConnectionAuthString)
        {
            string strMessage = "";
            repObj = new SettingRepository();
            model.ActionMode = ActionMode.Edit;
            strMessage = repObj.CRUDSetting(model, apiConnectionAuthString);
            return strMessage;
        }

    }
}
