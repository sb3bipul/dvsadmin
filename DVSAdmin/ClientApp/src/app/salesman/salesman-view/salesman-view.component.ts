import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import {Router, ActivatedRoute} from "@angular/router";
import { SalesmanService } from '../../services/salesman.service';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
declare var $: any;

@Component({
  selector: 'app-salesman-view',
  templateUrl: './salesman-view.component.html',
  styleUrls: ['./salesman-view.component.css']
})
export class SalesmanViewComponent implements OnInit {

  id: number;
  userID:string;
  name:string;
  region:string;
  street:string;
  city:string;
  state:string;
  zip:string;
  contactNo:string;
  emailID:string;
  salesmanInfoData=[];
  salesmanData=[];

  public p: number = 1;
 constructor(private salesmanService: SalesmanService,private SpinnerService: NgxSpinnerService,private router: Router, private route: ActivatedRoute) { 
 }

  ngOnInit() {
    console.log('Ok salesman view .... ' );
    this.id = + this.route.snapshot.params['id'];
    console.log('Login ID : ' + this.id);
    this.SpinnerService.show(); 

    this.salesmanService.getSalesmanInfoByID(this.id).subscribe(    
      (result: any) => {    
       
        this.salesmanData = result["salesmans"];  
        for (var i = 0; i < this.salesmanData.length; i++) {
          this.salesmanInfoData.push(this.salesmanData[i]);
          
        }
        console.log('result: ' + this.salesmanInfoData + ' userID: '+this.salesmanInfoData[0].userID);
        this.userID= this.salesmanInfoData[0].userID;
        this.name= this.salesmanInfoData[0].name;
        this.region= this.salesmanInfoData[0].region;
        this.contactNo= this.salesmanInfoData[0].contactNo;
        this.emailID= this.salesmanInfoData[0].emailID;
        this.street= this.salesmanInfoData[0].street;
        this.city= this.salesmanInfoData[0].city;
        this.state= this.salesmanInfoData[0].state;
        this.zip= this.salesmanInfoData[0].zip;
        this.SpinnerService.hide(); 
        }
    );    
  }

}
