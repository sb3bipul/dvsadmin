import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import {Router} from "@angular/router";
import { SalesmanService } from '../services/salesman.service';

@Component({
  selector: 'app-salesman',
  templateUrl: './salesman.component.html',
  styleUrls: ['./salesman.component.css']
})
export class SalesmanComponent implements OnInit {

  salesmanInfoData=[];
  salesmanData=[];
  StartDate = '';
  SearchText = '';
  public p: number = 1;
 constructor(private salesmanService: SalesmanService,private SpinnerService: NgxSpinnerService,private router: Router) { 
  let today = new Date();
  let dd = String(today.getDate()).padStart(2, '0');
  let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  let yyyy = today.getFullYear();

  this.StartDate = yyyy + '-' + mm + '-' + dd;
  
  this.loadSalesmanData();
 }

  ngOnInit() {
  }

  loadSalesmanData() {   
    this.SpinnerService.show(); 

    this.salesmanService.getSalesmanInfo().subscribe(    
      (result: any) => {    
        //alert(JSON.parse(result["salesmans"]));
        
        this.salesmanData = result["salesmans"];  
        for (var i = 0; i < this.salesmanData.length; i++) {
          this.salesmanInfoData.push(this.salesmanData[i]);
          
        }
        console.log(this.salesmanInfoData);
        this.SpinnerService.hide();  
        }
       
    );    
  }

  onEditSalesman(salesmanId){
    window.localStorage.removeItem("salesmanId");
    window.localStorage.setItem("salesmanId", salesmanId.toString());
    this.router.navigate(['../editview-salesman']);

  }

}
