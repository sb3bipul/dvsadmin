import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddTripComponent } from './add-trip/add-trip.component';
import { AddVehicletypeComponent } from './add-vehicletype/add-vehicletype.component';
import { AddZoneComponent } from './add-zone/add-zone.component';
import { AllOrderComponent } from './all-order/all-order.component';
import { AllTripComponent } from './all-trip/all-trip.component';
import { CustomerComponent } from './customer/customer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DriverEditComponent } from './driver/driver-edit/driver-edit.component';
import { DriverNewComponent } from './driver/driver-new/driver-new.component';
import { DriverViewComponent } from './driver/driver-view/driver-view.component';
import { DriverComponent } from './driver/driver.component';
import { AddLocationComponent } from './dvssetting/add-location/add-location.component';
import { DvssettingComponent } from './dvssetting/dvssetting.component';
import { EditLocationComponent } from './dvssetting/edit-location/edit-location.component';
import { EditTripComponent } from './edit-trip/edit-trip.component';
import { EditVehicletypeComponent } from './edit-vehicletype/edit-vehicletype.component';
import { EditZoneComponent } from './edit-zone/edit-zone.component';
import { EditviewTripComponent } from './editview-trip/editview-trip.component';
import { AdminGuard } from './guards/admin.guard';
import { LoginComponent } from './login/login.component';
import { SalesmanViewComponent } from './salesman/salesman-view/salesman-view.component';
import { SalesmanComponent } from './salesman/salesman.component';
import { TripTrackingComponent } from './trip-tracking/trip-tracking.component';
import { VehicleTypeComponent } from './vehicle-type/vehicle-type.component';
import { AddVehicleComponent } from './vehicle/add-vehicle/add-vehicle.component';
import { EditVehicleComponent } from './vehicle/edit-vehicle/edit-vehicle.component';
import { VehicleComponent } from './vehicle/vehicle.component';
import { ViewVehicleComponent } from './vehicle/view-vehicle/view-vehicle.component';
import { ViewOrderdetailComponent } from './view-orderdetail/view-orderdetail.component';
import { ViewTrackingtripComponent } from './view-trackingtrip/view-trackingtrip.component';
import { ViewTriptrackingComponent } from './view-triptracking/view-triptracking.component';
import { ZoneComponent } from './zone/zone.component';
import { ARInvoiceComponent } from './ar-invoice/ar-invoice.component';
import { DVSInvoiceComponent } from './dvs-invoice/dvs-invoice.component';


const routes: Routes = [
{ path: '', component: LoginComponent },
{ path: 'login', component: LoginComponent },
{ path: 'dashboard', component: DashboardComponent, canActivate: [AdminGuard] },  
{ path: '*', component: LoginComponent},

// { path: 'customer', component: CustomerComponent },
{ path: 'all-trip', component: AllTripComponent },
{ path: 'add-trip', component: AddTripComponent , canActivate: [AdminGuard] },
{ path: 'editview-trip', component: EditviewTripComponent , canActivate: [AdminGuard] },
{ path: 'edit-trip', component: EditTripComponent , canActivate: [AdminGuard] },
{ path: 'trip-tracking', component: TripTrackingComponent , canActivate: [AdminGuard] },
{ path: 'view-triptracking', component: ViewTriptrackingComponent , canActivate: [AdminGuard] },
{ path: 'view-trackingtrip', component: ViewTrackingtripComponent , canActivate: [AdminGuard] },
{ path: 'zone', component: ZoneComponent , canActivate: [AdminGuard] },
{ path: 'add-zone', component: AddZoneComponent , canActivate: [AdminGuard] },
{ path: 'edit-zone', component: EditZoneComponent , canActivate: [AdminGuard] },
{ path: 'customer', component: CustomerComponent , canActivate: [AdminGuard] },
{ path: 'order', component: AllOrderComponent , canActivate: [AdminGuard] },
{ path: 'view-orderdetail', component: ViewOrderdetailComponent , canActivate: [AdminGuard] },
{ path: 'vehicletype', component: VehicleTypeComponent , canActivate: [AdminGuard] },
{ path: 'add-vehicletype', component: AddVehicletypeComponent , canActivate: [AdminGuard] },
{ path: 'edit-vehicleType', component: EditVehicletypeComponent , canActivate: [AdminGuard] },
{ path: 'vehicle', component: VehicleComponent , canActivate: [AdminGuard] },
{ path: 'view-vehicle/:id', component: ViewVehicleComponent , canActivate: [AdminGuard] },
{ path: 'add-vehicle', component: AddVehicleComponent , canActivate: [AdminGuard] },
{ path: 'edit-vehicle/:id', component: EditVehicleComponent , canActivate: [AdminGuard] },
{ path: 'dvssetting', component: DvssettingComponent , canActivate: [AdminGuard] },
{ path: 'add-location', component: AddLocationComponent , canActivate: [AdminGuard] },
{ path: 'edit-location/:id', component: EditLocationComponent , canActivate: [AdminGuard] },
{ path: 'driver', component: DriverComponent , canActivate: [AdminGuard] },
{ path: 'driver-new', component: DriverNewComponent , canActivate: [AdminGuard] },
{ path: 'driver-edit/:id', component: DriverEditComponent, canActivate: [AdminGuard] },
{ path: 'driver-view/:id', component: DriverViewComponent, canActivate: [AdminGuard] },
{ path: 'salesman', component: SalesmanComponent , canActivate: [AdminGuard] },
{ path: 'salesman-view/:id', component: SalesmanViewComponent, canActivate: [AdminGuard] },
{ path: 'arinvoice', component: ARInvoiceComponent, canActivate: [AdminGuard] },
{ path: 'dvsInvoice', component: DVSInvoiceComponent, canActivate: [AdminGuard] },


//{ path: 'trip-tracking', component: TripTrackingComponent },
 //{ path: '', component: DashboardComponent, pathMatch: 'full' },
// { path: 'dashboard', component: DashboardComponent },
// { path: 'customer', component: CustomerComponent },
// { path: 'all-trip', component: AllTripComponent },
// { path: 'add-trip', component: AddTripComponent },
// { path: 'trip-tracking', component: TripTrackingComponent },
// { path: '', component: DashboardComponent, pathMatch: 'full' },
// { path: 'dashboard', component: DashboardComponent },
// { path: 'customer', component: CustomerComponent },
// { path: 'all-trip', component: AllTripComponent },
// { path: 'add-trip', component: AddTripComponent },
// { path: 'trip-tracking', component: TripTrackingComponent },
  ];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 
  
}
export const  routingcomponents =[LoginComponent,DashboardComponent,AddTripComponent]
