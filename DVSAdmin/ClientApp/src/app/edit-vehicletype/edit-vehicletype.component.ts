import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { VehicletypeService } from '../services/vehicletype.service';

@Component({
  selector: 'app-edit-vehicletype',
  templateUrl: './edit-vehicletype.component.html',
  styleUrls: ['./edit-vehicletype.component.css']
})
export class EditVehicletypeComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,private router: Router, private vehicletypeService: VehicletypeService) {

   }
editVehicleTypeForm: FormGroup;
  ngOnInit() {
    let vehicleTypeId = window.localStorage.getItem("vehicleTypeId");
    if(!vehicleTypeId) {
      alert("Invalid action.")
      this.router.navigate(['zone']);
      return;
    }
    this.editVehicleTypeForm = this.formBuilder.group({
      TypeID: [0],
      TypeName: ['', Validators.required],
      TypeDescription: ['', Validators.required],
      IsActive: [true, Validators.required],
      ActionMode: [1, Validators.required],
    });

    this.vehicletypeService.getZoneInfoById(vehicleTypeId)
    .subscribe( data => {
      //this.editZoneForm.setValue(data["zones"]);
      console.log(data["vehicleTypes"]);
      let vehicleTypedetail = data["vehicleTypes"][0];
      //alert(vehicleTypedetail.isActive);
      this.editVehicleTypeForm.setValue({TypeID: vehicleTypedetail.typeID, TypeName: vehicleTypedetail.typeName, TypeDescription: vehicleTypedetail.typeDescription, IsActive : vehicleTypedetail.isActive, ActionMode : 1});
      //this.editZoneForm.setValue(zonedetail);
      //console.log(data["zones"]);
    });
  }
  get addVehicleTypeFormControl() { return this.editVehicleTypeForm.controls; }

  setVehicleTypeStatus(statusvalue){
    //alert(statusvalue);
    this.editVehicleTypeForm.controls["IsActive"].setValue(Boolean(statusvalue));
  }
  
  onSubmit(){
    //alert("hi");
     this.vehicletypeService.putVehicleType(this.editVehicleTypeForm.value)
       .subscribe( data => {
         alert(data["message"])
         this.router.navigate(['vehicletype']);
       });
   }
}
