import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditVehicletypeComponent } from './edit-vehicletype.component';

describe('EditVehicletypeComponent', () => {
  let component: EditVehicletypeComponent;
  let fixture: ComponentFixture<EditVehicletypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditVehicletypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditVehicletypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
