import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { TriptrackingService } from '../services/triptracking.service';

@Component({
  selector: 'app-view-triptracking',
  templateUrl: './view-triptracking.component.html',
  styleUrls: ['./view-triptracking.component.css']
})
export class ViewTriptrackingComponent implements OnInit {
  tripTrackingDetailInfoData=[];
  tripTrackingInfoData;
  tripData=[];
  StartDate = '';
  TripStatus = '';
  SearchText = '';
  public p: number = 1;
  tripId;
  TripName ='';
  DriverName = ''
  constructor(private tripTrackingServise: TriptrackingService,private SpinnerService: NgxSpinnerService,private router: Router) { 
    this.loadTripTrackingInfoData();
   }

  ngOnInit() {
  }

  loadTripTrackingInfoData() {   
    this.tripId = window.localStorage.getItem("tripId");
    this.SpinnerService.show(); 

    this.tripTrackingServise.getTripTrackingDetail(this.tripId).subscribe(    
      (result: any) => {    
        //alert(JSON.parse(result["zones"]));
        
        this.tripTrackingInfoData = result["tripInfo"];  
        console.log(this.tripTrackingInfoData);
       if(this.tripTrackingInfoData){
         this.TripName = this.tripTrackingInfoData.tripName;
         this.DriverName = this.tripTrackingInfoData.driverName;
         this.StartDate = this.tripTrackingInfoData.startDate;
         this.tripTrackingDetailInfoData = this.tripTrackingInfoData.trackings;
       }
        console.log(this.tripTrackingDetailInfoData);
        this.SpinnerService.hide(); 
        //alert(this.tripInfoData);  
        }
        //this.orderData = this.collection.data
        
       
    );    
  }

}
