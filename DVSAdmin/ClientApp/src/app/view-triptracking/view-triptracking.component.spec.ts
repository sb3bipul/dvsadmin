import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTriptrackingComponent } from './view-triptracking.component';

describe('ViewTriptrackingComponent', () => {
  let component: ViewTriptrackingComponent;
  let fixture: ComponentFixture<ViewTriptrackingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTriptrackingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTriptrackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
