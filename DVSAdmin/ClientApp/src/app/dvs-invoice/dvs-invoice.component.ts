import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from "@angular/router";
import { DVSInvoiceService } from '../services/dvsinvoice.service';
declare var $: any;
import { ModalService } from '../services/modal.service';
import { DVSInvoiceModel, EditInvoice } from '../models/dvsinvoice-model';

@Component({
  selector: 'app-dvs-invoice',
  templateUrl: './dvs-invoice.component.html',
  styleUrls: ['./dvs-invoice.component.css']
})
export class DVSInvoiceComponent implements OnInit {
  StartDate: string; ToDate: string; InvoiceNo: string; RejectReason: string; DriverID: string;
  status: string; DVSInvoice = []; DVSInvoiceList = []; InvNumber: string; commentText: string;
  modelid: string; info: string; successMsg: string; invoiceRejected: Boolean = false;
  InvNumberEdit: string; customer_id: string; customer_name: string; driver_id: string; driver_name: string; signatory_name: string;
  invoice_comment: string; infoEd: string; invoiceEdit: Boolean = false; NewInvNumberEdit: string; InvCommentEdit: string;
  public p: number = 1; SInvNumberEdit: string; Scustomer_id: string; Scustomer_name: string;
  DVSInvoiceLog = []; DVSInvoiceStatusLog = []; smodelid: string;
  constructor(private dvsInvoice: DVSInvoiceService, private SpinnerService: NgxSpinnerService, private router: Router, private route: ActivatedRoute, private modalService: ModalService) { }

  ngOnInit() {
    var Todt = new Date();
    var stDt = new Date();
    Todt.setDate(Todt.getDate() - 7);
    this.StartDate = Todt.toISOString().split('T')[0];
    this.ToDate = stDt.toISOString().split('T')[0];
    this.status = '0';
    this.GetDVSInvliceList(this.StartDate, this.ToDate);
  }

  search() {
    this.GetDVSInvliceList(this.StartDate, this.ToDate);
  }

  reset() {
    var Todt = new Date();
    var stDt = new Date();
    Todt.setDate(Todt.getDate() - 7);
    this.StartDate = Todt.toISOString().split('T')[0];
    this.ToDate = stDt.toISOString().split('T')[0];
    this.status = '0';
    this.InvoiceNo = '';
    this.DriverID = '';
    this.GetDVSInvliceList(this.StartDate, this.ToDate);
  }

  public GetDVSInvliceList(FromDate: string, Todate: string) {
    this.DVSInvoiceList = [];
    this.DVSInvoice = [];
    var _invoiceNo = ''; var _status = ''; var _DriverId = '';
    if (this.InvoiceNo == null || this.InvoiceNo == undefined) {
      _invoiceNo = '';
    }
    else {
      _invoiceNo = this.InvoiceNo;
    }
    if (this.status == null || this.status == undefined) {
      _status = '';
    }
    else {
      _status = this.status;
    }

    if (this.DriverID == null || this.DriverID == undefined) {
      _DriverId = '';
    }
    else {
      _DriverId = this.DriverID;
    }
    this.SpinnerService.show();
    this.dvsInvoice.getDVSInvoiceListForAdmin(_DriverId, _invoiceNo, _status, FromDate, Todate).subscribe(
      (result: any) => {
        this.DVSInvoice = result["dvsInvoiceList"];
        for (var i = 0; i < this.DVSInvoice.length; i++) {
          this.DVSInvoiceList.push(this.DVSInvoice[i]);
        }
        this.SpinnerService.hide();
      }
    );
  }

  InvoiceApproval(invoice_number: string) {
    var _RejectReason = '';
    if (this.commentText == null || this.commentText == undefined) {
      _RejectReason = '';
    }

    var approveinv = new DVSInvoiceModel();
    approveinv.invoiceNo = invoice_number;
    approveinv.rejectReason = _RejectReason;
    approveinv.status = '3';
    this.SpinnerService.show();
    this.dvsInvoice.insertInvoiceStatusLog(approveinv).subscribe(
      (result: any) => {
        alert('Invoice Successfully Approved');
        this.GetDVSInvliceList(this.StartDate, this.ToDate);
      });
  }

  RejectInvoice() {
    var _RejectReason = '';
    if (this.commentText == null || this.commentText == undefined) {
      _RejectReason = '';
    }
    else {
      _RejectReason = this.commentText;
    }

    var approveinv = new DVSInvoiceModel();
    approveinv.invoiceNo = this.InvNumber;
    approveinv.rejectReason = _RejectReason;
    approveinv.status = '4';
    this.SpinnerService.show();
    this.dvsInvoice.insertInvoiceStatusLog(approveinv).subscribe(
      (result: any) => {
        this.closeModal(this.modelid)
        alert('Invoice Successfully Rejected');        
        this.GetDVSInvliceList(this.StartDate, this.ToDate);
      });
  }

  openInvoiceInfoForEdit(id: string, InvNumberEdit: string, customer_id: string, customer_name: string, driver_id: string, driver_name: string, signatory_name: string) {
    this.modalService.open(id);
    this.InvNumberEdit = InvNumberEdit;
    this.customer_id = customer_id;
    this.customer_name = customer_name;
    this.driver_id = driver_id;
    this.driver_name = driver_name;
    this.signatory_name = signatory_name;
    this.smodelid = id;
  }

  EditInvoiceSave() {
    var _newInvoiceNo = '';
    var _invComment = '';
    if (this.InvCommentEdit == null || this.InvCommentEdit == undefined) {
      _invComment = '';
    }
    else {
      _invComment = this.InvCommentEdit;
    }

    if (this.NewInvNumberEdit == null || this.NewInvNumberEdit == undefined) {
      this.infoEd = "Please insert new invoice number";
    }
    else {
      _newInvoiceNo = this.NewInvNumberEdit;
      var editInvoice = new EditInvoice();
      editInvoice.invoiceNo = this.InvNumberEdit;
      editInvoice.newInvoiceNo = _newInvoiceNo;
      editInvoice.customerId = this.customer_id;
      editInvoice.invoiceComment = _invComment;

      this.SpinnerService.show();
      this.dvsInvoice.editInvoiceInfo(editInvoice).subscribe(
        (result: any) => {
          alert('Invoice Successfully Edited');
          this.closeModal(this.smodelid);
          this.GetDVSInvliceList(this.StartDate, this.ToDate);
        });
    }
  }

  openInvoiceStatusHistory(id: string, InvoiceNo: string, customer_id: string, customer_name: string) {
    this.DVSInvoiceLog = [];
    this.DVSInvoiceStatusLog = [];
    this.modalService.open(id);

    this.dvsInvoice.getStatusLogByInvoiceNo(InvoiceNo).subscribe(
      (result: any) => {
        this.DVSInvoiceLog = result["dvsStatusLog"];
        for (var i = 0; i < this.DVSInvoiceLog.length; i++) {
          this.DVSInvoiceStatusLog.push(this.DVSInvoiceLog[i]);
        }
        this.SpinnerService.hide();
      }
    );

    this.SInvNumberEdit = InvoiceNo;
    this.Scustomer_id = customer_id;
    this.Scustomer_name = customer_name;
  }

  openModal(id: string, invoiceNo: string) {
    this.modalService.open(id);
    this.InvNumber = invoiceNo;
    this.modelid = id;
  }
  closeModal(id: string) {
    this.modalService.close(id);
  }

  checkComment(e) {
    this.info = '';
    if (this.commentText == undefined || this.commentText == '') {
      this.info = 'Please Put Any Commnents!';
      this.invoiceRejected = true;
    }
    else if (e.keyCode == 13) {
      console.log('Entered..');
    }
    else
      this.invoiceRejected = false;
  }
  checkInvNumberEdit(e) {
    this.infoEd = '';
    this.invoiceEdit = false;
  }
}
