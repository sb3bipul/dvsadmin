import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DvsInvoiceComponent } from './dvs-invoice.component';

describe('DvsInvoiceComponent', () => {
  let component: DvsInvoiceComponent;
  let fixture: ComponentFixture<DvsInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DvsInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DvsInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
