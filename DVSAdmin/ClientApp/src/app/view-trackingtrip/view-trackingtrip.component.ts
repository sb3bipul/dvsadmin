import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Dictionary } from '../models/Dictionary';
import { TripService } from '../services/trip.service';

@Component({
  selector: 'app-view-trackingtrip',
  templateUrl: './view-trackingtrip.component.html',
  styleUrls: ['./view-trackingtrip.component.css']
})
export class ViewTrackingtripComponent implements OnInit {
  zoneData;
  zoneId = 0;
  tripId='95535';
  SelectedDriver="";
  SelectedStartLocation = "sl1";
  SelectedEndLocation = "el1";
  salesmanData;
  salesmanId = 0;
  noOfMonth = 12;
  customerSearch = '';
  customerAddressSearch = '';
  orderNo = '0';
  orderData;
  orderDetailDataList = [];
  tripOrderList = [];
  drivers;
  locations;
  formGroup;
  submitData;
  StartLocation = "1";
  EndLocation = "1";
  currentName;
  tStartdate;
  driverId=1;
  isAllSelected=false;
  isaleastAddreeSeleted = false;
  keyvalueSelectCustomer=[];
  dict;
  stopNo =0;
  isDriverSelected = false;
  public origin: any = '56 Elliott Pl, Rutherford, NJ 07070, USA';
  public destination: any = '56 Elliott Pl, Rutherford, NJ 07070, USA';
  customerWaypointList = [];
  waypoints = [];
  markerOptions : any;
  waypointColorOption =[];
  public vehicleCapacity: string = "0";
  public modelselected:boolean = false;
  public totalStops: string = "0";
  public weight: string = "0";
  public totalCost: string = "0";
  public p: number = 1;
  ordrs: Array<String> = [];
  public Trnum: string;
  public collection = { count: 60, data: [] };


  public collectionOrder = { count: 60, data: [] };
  public collectionTripOrder = { count: 60, data: [] };
  public searchString: string;
  constructor(private http: HttpClient, private formBuilder: FormBuilder,private tripServise: TripService,private cdr: ChangeDetectorRef, private SpinnerService: NgxSpinnerService,private router: Router) {
    this.loadZoneData();
    this.loadSalesmanData();
    this.loadDriverData();
    this.loadLocationData();
    this.loadOrderData();
    this.dict = new Dictionary();
   }

   ngOnInit(): void {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = today.getFullYear();

    //today = mm + '/' + dd + '/' + yyyy;
    //this.tStartdate = mm + '/' + dd + '/' + yyyy;
    this.tStartdate = yyyy + '-' + mm + '-' + dd;
    //document.write(today);
  }
   //------------------------trip api--------------
   loadZoneData() {    
    this.SpinnerService.show();  
    this.tripServise.getZone().subscribe(    
      (result: any) => {    
        //alert(JSON.parse(result["zones"]));
        this.zoneData = result["zones"];    
        this.SpinnerService.hide();  
        
      } 
         
    );    
  }

  loadSalesmanData(){
    this.SpinnerService.show(); 
    this.tripServise.getSalesman().subscribe(    
      (result: any) => {    
        //alert(JSON.parse(result["zones"]));
        this.salesmanData = result["salesmans"];  
        this.SpinnerService.hide();   
      }    
    );    
  }

  loadDriverData(){
    this.SpinnerService.show(); 
    this.tripServise.getDriver().subscribe(    
      (result: any) => {    
        //alert(JSON.parse(result["zones"]));
        this.drivers = result["drivers"];    
        this.SpinnerService.hide(); 
      }    
    );    
  }

  loadLocationData(){
    this.SpinnerService.show(); 
    this.tripServise.getLocation().subscribe(    
      (result: any) => {    
        //alert(JSON.parse(result["zones"]));
        this.locations = result["locations"];  
        console.log(this.locations);  
        this.renderMap();
        this.SpinnerService.hide(); 
      }    
    );    
  }

  loadOrderData() {    
    this.tripId = window.localStorage.getItem("tripId");
    //alert(this.tripId);
    this.SpinnerService.show(); 
    this.tripServise.getTrip(this.tripId).subscribe(    
      (result: any) => {    
        //alert(JSON.parse(result["zones"]));
        this.orderData = result["tripOrders"];    
        if(this.orderData){
        this.Trnum = this.orderData[0].tripName;
        this.driverId = this.orderData[0].driveId;
        this.SelectedDriver = "d" + this.orderData[0].driverID;
        this.SelectedStartLocation = "sl" + this.orderData[0].startLocation;
        this.SelectedEndLocation = "el" + this.orderData[0].endLocation;
        this.tStartdate = this.orderData[0].startTime;
        var drv = this.drivers.find(x => x.driverId === Number(this.orderData[0].driverID));
        this.vehicleCapacity = drv.vehicleCapacity;
        }
        this.weight="0";
        this.totalCost = "0";
        this.totalStops = this.orderData.length;

        for (var i = 0; i < this.orderData.length; i++) {
          //this.collectionOrder.count = this.orderData[i].orders.length
          
          for (var j = 0; j < this.orderData[i].orders.length; j++) {
            this.orderDetailDataList.push(this.orderData[i].orders[j]);
            this.weight = (parseFloat(this.weight) + parseFloat(this.orderData[i].orders[j].grossWeight)).toString();
            this.totalCost = (parseFloat(this.totalCost) + parseFloat(this.orderData[i].orders[j].billAmount)).toString();
          }
        }
        //this.orderData = this.collection.data
        console.log(this.orderData);
        this.SpinnerService.hide(); 
        this.onOptimizeRoute();
      }    
    );    
  }

  renderMap(){
    var originlocation = this.locations.find(x => x.id === Number(this.StartLocation));
    var destinationlocation = this.locations.find(x => x.id === Number(this.EndLocation));
    this.origin = originlocation.locationAddress;
    this.destination = destinationlocation.locationAddress;
  }

  onEditTrip(){

    this.router.navigate(['../edit-trip']);

  }
  
  //------------------------end trip--------------
  onOptimizeRoute() {
    var originlocation = this.locations.find(x => x.id === Number(this.StartLocation));
    var endlocation = this.locations.find(x => x.id === Number(this.EndLocation));
    console.log(originlocation);
      this.origin = originlocation.locationAddress;
      this.orderData = this.orderData.sort((a, b) => parseFloat(a.stopCount) - parseFloat(b.stopCount));
      
      console.log(this.orderData);
      this.waypoints = [];
      this.waypointColorOption = [];
      for (var i = 0; i < this.orderData.length; i++) {
        if(this.orderData[i].stopCount !=0){
          this.waypoints.push({ location: this.orderData[i].address, stopover: false });
          this.waypointColorOption.push({ label: { color: 'white', text: (i+1).toString() }, infoWindow: this.orderData[i].address, });

        }

      }
      console.log(this.waypoints);
      this.destination = endlocation.locationAddress;
   
    //this.markerOptions = { origin: { label: { color: 'blue', text: '1'} }, destination: { label: { color: 'white', text: (this.waypointColorOption.length + 2).toString() } }, waypoints: this.waypointColorOption };
    this.markerOptions = { origin: { label: { color: 'white'} }, destination: { label: { color: 'white' } }, waypoints: this.waypointColorOption };

  }
}
