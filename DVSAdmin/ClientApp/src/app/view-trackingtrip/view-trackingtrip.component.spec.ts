import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTrackingtripComponent } from './view-trackingtrip.component';

describe('ViewTrackingtripComponent', () => {
  let component: ViewTrackingtripComponent;
  let fixture: ComponentFixture<ViewTrackingtripComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTrackingtripComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTrackingtripComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
