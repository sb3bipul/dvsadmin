import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewOrderdetailComponent } from './view-orderdetail.component';

describe('ViewOrderdetailComponent', () => {
  let component: ViewOrderdetailComponent;
  let fixture: ComponentFixture<ViewOrderdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewOrderdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewOrderdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
