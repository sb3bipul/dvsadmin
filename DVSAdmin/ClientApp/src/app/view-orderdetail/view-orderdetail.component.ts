import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { OrderService } from '../services/order.service';

@Component({
  selector: 'app-view-orderdetail',
  templateUrl: './view-orderdetail.component.html',
  styleUrls: ['./view-orderdetail.component.css']
})
export class ViewOrderdetailComponent implements OnInit {
  orderId='30340';
  orderDetail;
  public p: number = 1;
  constructor(private http: HttpClient, private formBuilder: FormBuilder,private orderServise: OrderService, private SpinnerService: NgxSpinnerService,private router: Router) {
    this.orderId = window.localStorage.getItem("orderId");
    this.loadOrderDetailData();
   }

  ngOnInit() {
  }

  loadOrderDetailData(){
    //this.orderId='30340'
    this.SpinnerService.show(); 
      this.orderServise.getOrderDetailInfo(this.orderId).subscribe(    
        (result: any) => {    
          //alert(JSON.parse(result["zones"]));
          
          this.orderDetail = result["orderDetailInfo"];  
          // for (var i = 0; i < this.orderData.length; i++) {
          //   this.orderInfoData.push(this.orderData[i]);
            
          // }
          console.log(this.orderDetail);
          this.SpinnerService.hide(); 
          //alert(this.tripInfoData);  
          }
          //this.orderData = this.collection.data
          
         
      ); 
  }
}
