import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { TriptrackingService } from '../services/triptracking.service';

@Component({
  selector: 'app-trip-tracking',
  templateUrl: './trip-tracking.component.html',
  styleUrls: ['./trip-tracking.component.css']
})
export class TripTrackingComponent implements OnInit {
  tripInfoData=[];
  tripData=[];
  StartDate = '';
  TripStatus = '';
  SearchText = '';
  public p: number = 1;
  constructor(private tripTrackingServise: TriptrackingService,private SpinnerService: NgxSpinnerService,private router: Router) { 
    this.loadTripTrackingInfoData();
   }

  ngOnInit() {
  }

  loadTripTrackingInfoData() {   
    this.SpinnerService.show(); 
    //delay(20000);
    this.tripTrackingServise.getTripTrackingInfo(this.SearchText).subscribe(    
      (result: any) => {    
        //alert(JSON.parse(result["zones"]));
        
        this.tripInfoData = result["tripInfo"];  
        for (var i = 0; i < this.tripData.length; i++) {
          this.tripInfoData.push(this.tripData[i]);
          
        }
        console.log(this.tripInfoData);
        this.SpinnerService.hide(); 
        //alert(this.tripInfoData);  
        }
        //this.orderData = this.collection.data
        
       
    );    
  }

  onEditTrip(tripId){
    //alert(tripId);
    window.localStorage.removeItem("tripId");
    window.localStorage.setItem("tripId", tripId.toString());
    this.router.navigate(['../view-trackingtrip']);


  }
  onViewTripTracking(tripId){
    window.localStorage.removeItem("tripId");
    window.localStorage.setItem("tripId", tripId.toString());
    this.router.navigate(['../view-triptracking']);
  }

}
