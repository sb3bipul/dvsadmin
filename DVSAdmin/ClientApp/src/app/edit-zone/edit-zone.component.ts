import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { ZoneService } from '../services/zone.service';

@Component({
  selector: 'app-edit-zone',
  templateUrl: './edit-zone.component.html',
  styleUrls: ['./edit-zone.component.css']
})
export class EditZoneComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,private router: Router, private zoneService: ZoneService) { 
    
  }
  editZoneForm: FormGroup;
  ngOnInit() {
    let zoneId = window.localStorage.getItem("zoneId");
    if(!zoneId) {
      alert("Invalid action.")
      this.router.navigate(['zone']);
      return;
    }
    this.editZoneForm = this.formBuilder.group({
      id: [0],
      ZoneName: ['', Validators.required],
      ZoneCode: ['', Validators.required],
      IsActive: [true, Validators.required],
      IsDeleted: [true, Validators.required],
      ActionMode: [1, Validators.required],
    });

    this.zoneService.getZoneInfoById(zoneId)
    .subscribe( data => {
      //this.editZoneForm.setValue(data["zones"]);
      console.log(data["zones"]);
      let zonedetail = data["zones"][0];
     //alert(zonedetail.isActive);
      this.editZoneForm.setValue({id: zonedetail.id, ZoneName: zonedetail.zoneName, ZoneCode: zonedetail.zoneCode, IsActive : zonedetail.isActive, IsDeleted : zonedetail.isDeleted, ActionMode : 1});
      //this.editZoneForm.setValue(zonedetail);
      //console.log(data["zones"]);
    });
  }

  setZonetatus(statusvalue){
    //alert(statusvalue);
    this.editZoneForm.controls["IsActive"].setValue(Boolean(statusvalue));
  }

  onSubmit() {
    this.zoneService.putZone(this.editZoneForm.value)
      .subscribe( data => {
        this.router.navigate(['zone']);
      });
  }
}
