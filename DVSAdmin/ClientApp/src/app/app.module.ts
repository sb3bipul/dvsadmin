import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule, routingcomponents } from './app-routing.module';
//import { MatIconModule } from '@angular/material/icon';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
//import { DashboardComponent } from './dashboard/dashboard.component';
import { LeftMenuComponent } from './left-menu/left-menu.component';
import { HeaderComponent } from './header/header.component';
//import { AddTripComponent } from './add-trip/add-trip.component';
//import { LoginComponent } from './login/login.component';
//import { AdminGuard } from './guards/admin.guard';
import { HttpInterceptorService } from './services/http-interceptor.service';
import { ErrorInterceptorService } from './services/error-interceptor.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { FooterComponent } from './footer/footer.component';
import { NgxSpinnerModule, NgxSpinnerService } from "ngx-spinner";  
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction'; 
import { GooglemaptripComponent } from './googlemaptrip/googlemaptrip.component';
import { GooglemapploatComponent } from './googlemapploat/googlemapploat.component';
import { GooglemapdirectionComponent } from './googlemapdirection/googlemapdirection.component';
import { AllTripComponent } from './all-trip/all-trip.component';
import { environment } from 'src/environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EditviewtripComponent } from './editviewtrip/editviewtrip.component';
import { EditviewTripComponent } from './editview-trip/editview-trip.component';
import { EditTripComponent } from './edit-trip/edit-trip.component';
import { ZoneComponent } from './zone/zone.component';
import { CustomerComponent } from './customer/customer.component';
import { AllOrderComponent } from './all-order/all-order.component';
import { SalesmanComponent } from './salesman/salesman.component';
import { VehicleTypeComponent } from './vehicle-type/vehicle-type.component';
import { AddZoneComponent } from './add-zone/add-zone.component';
import { EditZoneComponent } from './edit-zone/edit-zone.component';
import { AddVehicletypeComponent } from './add-vehicletype/add-vehicletype.component';
import { EditVehicletypeComponent } from './edit-vehicletype/edit-vehicletype.component';
import { ViewOrderdetailComponent } from './view-orderdetail/view-orderdetail.component';
import { DvssettingComponent } from './dvssetting/dvssetting.component';
import { DriverComponent } from './driver/driver.component';
import { DriverViewComponent } from './driver/driver-view/driver-view.component';
import { DriverNewComponent } from './driver/driver-new/driver-new.component';
import { DriverEditComponent } from './driver/driver-edit/driver-edit.component';
import { SalesmanViewComponent } from './salesman/salesman-view/salesman-view.component';
import { TripTrackingComponent } from './trip-tracking/trip-tracking.component';
import { ViewTriptrackingComponent } from './view-triptracking/view-triptracking.component';
import { AddLocationComponent } from './dvssetting/add-location/add-location.component';
import { EditLocationComponent } from './dvssetting/edit-location/edit-location.component';
import { ViewTrackingtripComponent } from './view-trackingtrip/view-trackingtrip.component';
import { VehicleComponent } from './vehicle/vehicle.component';
import { AddVehicleComponent } from './vehicle/add-vehicle/add-vehicle.component';
import { EditVehicleComponent } from './vehicle/edit-vehicle/edit-vehicle.component';
import { ViewVehicleComponent } from './vehicle/view-vehicle/view-vehicle.component';
import { ARInvoiceComponent } from './ar-invoice/ar-invoice.component';
import { ModalModule } from './_modal';
import { DVSInvoiceComponent } from './dvs-invoice/dvs-invoice.component';
import { OnlynumberDirective } from './onlynumber.directive';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    //DashboardComponent,
    LeftMenuComponent,
    HeaderComponent,
    routingcomponents,
    FooterComponent,
    GooglemaptripComponent,
    GooglemapploatComponent,
    GooglemapdirectionComponent,
    AllTripComponent,
    EditviewtripComponent,
    EditviewTripComponent,
    EditTripComponent,
    ZoneComponent,
    CustomerComponent,
    AllOrderComponent,
    SalesmanComponent,
    VehicleTypeComponent,
    AddZoneComponent,
    EditZoneComponent,
    AddVehicletypeComponent,
    EditVehicletypeComponent,
    ViewOrderdetailComponent,
    DvssettingComponent,
    DriverComponent,
    DriverViewComponent,
    DriverNewComponent,
    DriverEditComponent,
    SalesmanViewComponent,
    TripTrackingComponent,
    ViewTriptrackingComponent,
    AddLocationComponent,
    EditLocationComponent,
    ViewTrackingtripComponent,
    VehicleComponent,
    AddVehicleComponent,
    EditVehicleComponent,
    ViewVehicleComponent,
    ARInvoiceComponent,
    DVSInvoiceComponent,
    OnlynumberDirective,
    //,AddTripComponent,
    //LoginComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    NgxPaginationModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    ModalModule,
    //RouterModule.forRoot([
      //{ path: '', component: HomeComponent, pathMatch: 'full' },
      //{ path: 'counter', component: CounterComponent },
      //{ path: 'fetch-data', component: FetchDataComponent },
      //{ path: 'login', component: LoginComponent },
      //{ path: 'dashboard', component: DashboardComponent, canActivate: [AdminGuard] },  
      //{ path: '*', component: LoginComponent}
    //])
    AgmCoreModule.forRoot({
      apiKey: environment.googleAPIKey,
      /* apiKey is required, unless you are a
      premium customer, in which case you can
      use clientId
      */
    }),
    AgmDirectionModule,
    ToastrModule.forRoot()
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptorService, multi: true },NgxSpinnerService],
  exports: [
      NgxPaginationModule,
      NgxSpinnerModule,
      AgmCoreModule 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
