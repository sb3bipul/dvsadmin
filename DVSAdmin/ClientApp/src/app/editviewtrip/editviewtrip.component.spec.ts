import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditviewtripComponent } from './editviewtrip.component';

describe('EditviewtripComponent', () => {
  let component: EditviewtripComponent;
  let fixture: ComponentFixture<EditviewtripComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditviewtripComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditviewtripComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
