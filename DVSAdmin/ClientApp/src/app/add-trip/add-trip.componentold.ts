import { Component, OnInit, Output, EventEmitter, Input, ChangeDetectorRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//import { NgxPaginationModule } from 'ngx-pagination';
import { FormBuilder } from '@angular/forms';
import { TripService } from '../services/trip.service';
import { DatePipe } from '@angular/common';
import { NgxSpinnerService } from "ngx-spinner"; 
import { TripModel } from '../models/TripModel';
import { TripDetailModel } from '../models/TripDetailModel';
import { Dictionary } from '../models/Dictionary';
import { WaypointModel } from '../models/WaypointModel';
import { CompileShallowModuleMetadata } from '@angular/compiler';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-add-trip',
  templateUrl: './add-trip.component.html',
  styleUrls: ['./add-trip.component.css']
})
export class AddTripComponent implements OnInit {
  zoneData;
  zoneId = 0;
  salesmanData;
  salesmanId = 0;
  noOfMonth = 18;
  customerSearch = '';
  customerAddressSearch = '';
  orderNo = '0';
  orderData;
  orderDetailDataList = [];
  tripOrderList = [];
  drivers;
  locations;
  formGroup;
  submitData;
  StartLocation = "1";
  EndLocation = "1";
  currentName;
  tStartdate;
  driverId=1;
  isAllSelected=false;
  isaleastAddreeSeleted = false;
  keyvalueSelectCustomer=[];
  dict;
  stopNo =0;
  isDriverSelected = false;
  public origin: any = '56 Elliott Pl, Rutherford, NJ 07070, USA';
  public destination: any = '56 Elliott Pl, Rutherford, NJ 07070, USA';
  customerWaypointList = [];
  waypoints = [];
  markerOptions : any;
  waypointColorOption =[];
  public vehicleCapacity: string = "0";
  public modelselected:boolean = false;
  public totalStops: string = "0";
  public weight: string = "0";
  public totalCost: string = "0";
  public p: number = 1;
  public empList: Array<String> = [];
  //public detailList: Array<String> = [];
  ordrs: Array<String> = [];
  public temp: Object = false;
  public Trnum: string;
  public num: number;
  public num2: number;
  public cfg: any;
  public collection = { count: 60, data: [] };


  public collectionOrder = { count: 60, data: [] };
  public collectionTripOrder = { count: 60, data: [] };
  public searchString: string;
  public checkCount=0;
  constructor(private http: HttpClient, private formBuilder: FormBuilder,private tripServise: TripService,private cdr: ChangeDetectorRef, private SpinnerService: NgxSpinnerService, private router: Router) {
    this.loadZoneData();
    this.loadSalesmanData();
    this.loadDriverData();
    this.loadLocationData();
    this.loadOrderData();
    this.dict = new Dictionary();
    
    //this.tStartdate =

    // this.http.get('https://localhost:44389/api/drivers').subscribe((resp: Response) => {
      // this.drivers = resp;
    // });
    // this.http.get('http://localhost:5001/api/DVS_WareHouseMasters').subscribe((resp: Response) => {
      // this.locations = resp;
    // });
    // this.http.get('http://localhost:5001/api/orders').subscribe((resp: Response) => {
      // this.orderData = resp;
      // this.collection.count = this.orderData.length;
      // //console.log(this.orderData);
      // this.temp = true;
      // this.num = Math.floor((Math.random() * 100000) + 1);
      // this.Trnum = 'Tr ' + this.num + '000';
      // this.num2 = Math.floor((Math.random() * 1000) + 2);

      // for (var i = 0; i < this.collection.count; i++) {

        // if (resp[i].name === this.currentName) {
          // resp[i].name = '';
          // resp[i].street = '';
          // resp[i].city = '';
          // resp[i].state = '';
          // resp[i].zip = '';
        // } else {
          // this.currentName = resp[i].name;
        // }

        // this.collection.data.push(
          // {
            // id: i,
            // value: resp[i]
          // }
        // );
      // }
      // this.orderData = this.collection.data
      // console.log(this.orderData);
    // });
    this.cfg = {
      itemsPerPage: 5,
      currentPage: 1,
      totalItems: this.collection.count
    };
    this.formGroup = this.formBuilder.group({
      TripName: '',
      StartTime: '',
      DriverID: '',
      terms: false,
      checkcontroler:'',
      checkcontrolerstop:'',
      checkcontrolerorder:''
    });
  }
  pageChanged(event) {
    this.cfg.currentPage = event;
  }
  setStartLocation(value: string) {
    this.StartLocation = value.substr(2,1);
    // console.log(this.StartLocation);
    //alert(this.StartLocation);
  }
  setEndLocation(value: string) {
    this.EndLocation = value.substr(2,1);;
    // console.log(this.StartLocation);w
    //alert(this.EndLocation);
    
  }
  ngAfterViewInit(): void {
    this.cdr.detectChanges();
  }
  setDriver(event) {
    //alert(event.target.value);
    //console.log(value);
    var id = event.target.value.substr(1,1);
    if(Number(id) === 0){
     this.isDriverSelected = false;
    }
    else{
      this.isDriverSelected = true;
    }
    //alert(id);
    this.driverId = Number(id);
    //alert(this.driverId);
    var drv = this.drivers.find(x => x.driverId === Number(id));
    //alert(drv.vehicleCapacity);
    console.log(drv);
    this.vehicleCapacity = drv.vehicleCapacity;
    //this.totalStops = event.target.value;
    //this.weight = event.target.value;
    //this.totalCost = event.target.value;
  }
  ngOnInit(): void {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = today.getFullYear();

    //today = mm + '/' + dd + '/' + yyyy;
    //this.tStartdate = mm + '/' + dd + '/' + yyyy;
    this.tStartdate = yyyy + '-' + mm + '-' + dd;
    //document.write(today);
  }

  OnChangePriority(event,customerId){
    let orderpr = this.orderData.find(x => x.customerId === customerId);
    let onderIndex = this.orderData.indexOf(orderpr);
    orderpr.stopCount = event.target.value;
    this.orderData[onderIndex] = orderpr;
    //alert(event.target.value);
  }

  childCheck(parentObj, childObj) {
    parentObj.isSelected = childObj.every(function (itemChild: any) {
      return itemChild.isSelected == true;
    })
  }
  parentCheck(parentObj) {
    for (var i = 0; i < parentObj.childList.length; i++) {
      parentObj.childList[i].isSelected = parentObj.isSelected;
    }
  }
  //------------------------trip api--------------
  loadZoneData() {    
    this.SpinnerService.show();  
    this.tripServise.getZone().subscribe(    
      (result: any) => {    
        //alert(JSON.parse(result["zones"]));
        this.zoneData = result["zones"];    
        this.SpinnerService.hide();  
        
      } 
         
    );    
  }

  loadSalesmanData(){
    this.SpinnerService.show(); 
    this.tripServise.getSalesman().subscribe(    
      (result: any) => {    
        //alert(JSON.parse(result["zones"]));
        this.salesmanData = result["salesmans"];  
        this.SpinnerService.hide();   
      }    
    );    
  }

  loadDriverData(){
    this.SpinnerService.show(); 
    this.tripServise.getDriver().subscribe(    
      (result: any) => {    
        //alert(JSON.parse(result["zones"]));
        this.drivers = result["drivers"];    
        this.SpinnerService.hide(); 
      }    
    );    
  }

  loadLocationData(){
    this.SpinnerService.show(); 
    this.tripServise.getLocation().subscribe(    
      (result: any) => {    
        //alert(JSON.parse(result["zones"]));
        this.locations = result["locations"];  
       
        console.log(this.locations);  
        this.renderMap();
        this.SpinnerService.hide(); 
        if(this.locations){
          this.StartLocation = this.locations[0].startPoint;
          this.EndLocation = this.locations[0].endPoint;
          console.log(this.StartLocation +  " , " + this.EndLocation ); 
          }
      }    
    );    
  }

  loadOrderData() {    
    this.SpinnerService.show(); 
    this.tripServise.getOrder(this.zoneId,this.salesmanId,this.noOfMonth,this.customerSearch,this.customerAddressSearch,this.orderNo).subscribe(    
      (result: any) => {    
        //alert(JSON.parse(result["zones"]));
        this.orderData = result["tripOrders"];    
       
        //this.collection.count = this.orderData.length;
        //console.log(this.orderData);
        this.temp = true;
        this.num = Math.floor((Math.random() * 100000) + 1);
        this.Trnum = 'Tr ' + this.num + '000';
        this.num2 = Math.floor((Math.random() * 1000) + 2);

        for (var i = 0; i < this.orderData.length; i++) {
          //this.collectionOrder.count = this.orderData[i].orders.length
          for (var j = 0; j < this.orderData[i].orders.length; j++) {
            this.orderDetailDataList.push(this.orderData[i].orders[j]);
          }
           
          // if (this.orderData[i].name === this.currentName) {
            // this.orderData[i].name = '';
            // this.orderData[i].street = '';
            // this.orderData[i].city = '';
            // this.orderData[i].state = '';
            // this.orderData[i].zip = '';
          // } else {
            // this.currentName = this.orderData[i].name;
          // }
          // this.collection.data.push(
            // {
              // id: i,
              // value: this.orderData[i]
            // }
          // );
        }
        //this.orderData = this.collection.data
        //console.log(this.orderData);
        this.SpinnerService.hide(); 
      }    
    );    
  }

  renderMap(){
    var originlocation = this.locations.find(x => x.id === Number(this.StartLocation));
    var destinationlocation = this.locations.find(x => x.id === Number(this.EndLocation));
    this.origin = originlocation.locationAddress;
    this.destination = destinationlocation.locationAddress;
  }
  
  //------------------------end trip--------------

  onChangeSalesman(event){
    this.salesmanId = event.target.value.substr(1,1);
    this.SpinnerService.show(); 
    this.tripServise.getOrder(this.zoneId,this.salesmanId,this.noOfMonth,this.customerSearch,this.customerAddressSearch,this.orderNo).subscribe(    
      (result: any) => {    
        this.orderData = result["tripOrders"];    
        this.temp = true;
        this.num = Math.floor((Math.random() * 100000) + 1);
        this.Trnum = 'Tr ' + this.num + '000';
        this.num2 = Math.floor((Math.random() * 1000) + 2);
        this.orderDetailDataList=[];
        for (var i = 0; i < this.orderData.length; i++) {
          for (var j = 0; j < this.orderData[i].orders.length; j++) {
            this.orderDetailDataList.push(this.orderData[i].orders[j]);
          }
           
        }
        //console.log(this.orderData);
        this.SpinnerService.hide(); 
      }    
    );    
  }

  onChangeZone(event){
    this.SpinnerService.show(); 
    this.zoneId = event.target.value.substr(1,1);
    this.tripServise.getOrder(this.zoneId,this.salesmanId,this.noOfMonth,this.customerSearch,this.customerAddressSearch,this.orderNo).subscribe(    
      (result: any) => {    
        this.orderData = result["tripOrders"];    
        this.temp = true;
        this.num = Math.floor((Math.random() * 100000) + 1);
        this.Trnum = 'Tr ' + this.num + '000';
        this.num2 = Math.floor((Math.random() * 1000) + 2);
        this.orderDetailDataList=[];
        for (var i = 0; i < this.orderData.length; i++) {
          for (var j = 0; j < this.orderData[i].orders.length; j++) {
            this.orderDetailDataList.push(this.orderData[i].orders[j]);
          }
           
        }
        //console.log(this.orderData);
        this.SpinnerService.hide(); 
      }    
    ); 
  }

onCreateTrip(){
  if(!this.isDriverSelected){
    alert("Please select driver");
  }
  else if (!this.isaleastAddreeSeleted) {
    alert("Please select atleast one order");
  }
  else{
  var tripDetailData=[];
  var detailList = [];
  var tripdetaildata = null;
  this.collectionTripOrder.count = this.tripOrderList.length
  for (var i = 0; i < this.collectionTripOrder.count; i++) {
     //this.tripOrderList
      tripdetaildata = new TripDetailModel()
      tripdetaildata.TripID=0;
      tripdetaildata.TripDetailID = i;
      tripdetaildata.OrderNo = this.tripOrderList[i].poNumber;
      tripdetaildata.StartLocation = Number(this.StartLocation);
      tripdetaildata.EndLocation = Number(this.EndLocation);
      tripdetaildata.CompanyID = 2;
      tripdetaildata.ToDoComments="test todo coments";
      tripdetaildata.IsStopSummeryDone =false;
      tripdetaildata.IsToDo = false;
      tripdetaildata.IsCustomerSkipped = false;
      tripdetaildata.Priority = this.tripOrderList[i].priority;
      detailList.push(tripdetaildata);
    }
    console.log(detailList);

    var tripdata = new TripModel()
    tripdata.TripID=0;
    tripdata.CompanyID="2";
    tripdata.TripName = this.Trnum;
    tripdata.DriverID = Number(this.driverId);
    tripdata.WareHouse = "01";
    tripdata.Charges = 12.00;
    tripdata.Priority=1;
    tripdata.SortTrip =1;
    tripdata.VehicleID = 1;
    tripdata.TripStatus = 1;
    tripdata.TripDesc = "Route_Trip_1_Desc"
    tripdata.IsActive = true;
    tripdata.IsDeleted = false;
    tripdata.TripDetails = detailList;
    tripdata.ActionMode = 0;
    tripdata.StartLocation = Number(this.StartLocation);
    tripdata.EndLocation = Number(this.EndLocation);
    tripdata.TripCreated = this.tStartdate;

    this.SpinnerService.show(); 
     //--Uncomment for save Trip
    this.tripServise.postTrip(tripdata).subscribe(    
      (result: any) => {    
     //alert(JSON.parse(result["zones"]));
     //this.salesmanData = result["salesmans"];  
        alert(result["message"]);
        this.router.navigate(['all-trip']);
        this.SpinnerService.hide(); 
        }    
      ); 
  }   
}

onDiscardChanges(event){
  this.orderData.forEach((item) => {
    item.isSelected = false;
    item.stopCount = 0;
    item.orders.forEach((iteminner) =>{
      iteminner.isSelected = false;
      this.isaleastAddreeSeleted = false;
      });
  });
  this.ResetTripSWT();
  this.customerWaypointList=[];
  this.waypoints = [];
  this.waypointColorOption = [];
}

onOptimizeRoute() {
  var originlocation = this.locations.find(x => x.id === Number(this.StartLocation));
  var endlocation = this.locations.find(x => x.id === Number(this.EndLocation));
  console.log(originlocation);
  if (this.isaleastAddreeSeleted) {
    this.origin = originlocation.locationAddress;
    this.orderData = this.orderData.sort((a, b) => parseFloat(a.stopCount) - parseFloat(b.stopCount));
    this.customerWaypointList = this.customerWaypointList.sort((a, b) => parseFloat(a.waypointstopno) - parseFloat(b.waypointstopno));
    
    console.log(this.orderData);
    this.waypoints = [];
    this.waypointColorOption = [];
    for (var i = 0; i < this.customerWaypointList.length; i++) {
      //alert(this.customerWaypointList[i].location);
      this.waypoints.push({ location: this.customerWaypointList[i].location, stopover: false });   
      this.waypointColorOption.push({ label: { color: 'white', text: (i+1).toString() }, infoWindow: this.customerWaypointList[i].location, });    
    }
    // for (var i = 0; i < this.orderData.length; i++) {
    //   if(this.orderData.stopCount !=0){
    //     this.waypoints.push({ location: this.orderData[i].address, stopover: false });
    //     //this.waypointColorOption.push({ label: { color: 'white', text: (i+2).toString() } });
    //   this.waypointColorOption.push({ label: { color: 'white', text: (i+1).toString() }, infoWindow: this.orderData[i].address, });

    //   }

    // }
    console.log(this.waypoints);
    // for (var i = 0; i < this.customerWaypointList.length; i++) {
    //   // if (i === this.customerWaypointList.length - 1) {
    //   //   this.destination = this.customerWaypointList[i].location;
    //   //   console.log(this.destination);
    //   // }
    //   // else {
    //   //   this.waypoints.push(this.customerWaypointList[i]);
    //   //   this.waypointColorOption.push({ label: { color: 'white', text: (i+2).toString() } });
    //   // }

    //   this.waypoints.push(this.customerWaypointList[i]);
    //     //this.waypointColorOption.push({ label: { color: 'white', text: (i+2).toString() } });
    //   this.waypointColorOption.push({ label: { color: 'white', text: (i+1).toString() }, infoWindow: this.customerWaypointList[i].location, });

    // }
    this.destination = endlocation.locationAddress;
  }
  else{
    alert("Please select atleast one order");
  }
  //this.markerOptions = { origin: { label: { color: 'blue', text: '1'} }, destination: { label: { color: 'white', text: (this.waypointColorOption.length + 2).toString() } }, waypoints: this.waypointColorOption };
  this.markerOptions = { origin: { label: { color: 'white'} }, destination: { label: { color: 'white' } }, waypoints: this.waypointColorOption };
  //this.waypoints = this.customerWaypointList;
  //console.log(this.origin);

}
  onOptimizeRouteOld() {
    var originlocation = this.locations.find(x => x.id === Number(this.StartLocation));
    var endlocation = this.locations.find(x => x.id === Number(this.EndLocation));
    console.log(originlocation);
    if (this.isaleastAddreeSeleted) {
      this.origin = originlocation.locationAddress;
      this.orderData = this.orderData.sort((a, b) => parseFloat(a.stopCount) - parseFloat(b.stopCount));
      
      console.log(this.orderData);
      this.waypoints = [];
      this.waypointColorOption = [];
      for (var i = 0; i < this.orderData.length; i++) {
        if(this.orderData.stopCount !=0){
          this.waypoints.push({ location: this.orderData[i].address, stopover: false });
          //this.waypointColorOption.push({ label: { color: 'white', text: (i+2).toString() } });
        this.waypointColorOption.push({ label: { color: 'white', text: (i+1).toString() }, infoWindow: this.orderData[i].address, });

        }

      }
      console.log(this.waypoints);
      // for (var i = 0; i < this.customerWaypointList.length; i++) {
      //   // if (i === this.customerWaypointList.length - 1) {
      //   //   this.destination = this.customerWaypointList[i].location;
      //   //   console.log(this.destination);
      //   // }
      //   // else {
      //   //   this.waypoints.push(this.customerWaypointList[i]);
      //   //   this.waypointColorOption.push({ label: { color: 'white', text: (i+2).toString() } });
      //   // }

      //   this.waypoints.push(this.customerWaypointList[i]);
      //     //this.waypointColorOption.push({ label: { color: 'white', text: (i+2).toString() } });
      //   this.waypointColorOption.push({ label: { color: 'white', text: (i+1).toString() }, infoWindow: this.customerWaypointList[i].location, });

      // }
      this.destination = endlocation.locationAddress;
    }
    else{
      alert("Please select atleast one order");
    }
    //this.markerOptions = { origin: { label: { color: 'blue', text: '1'} }, destination: { label: { color: 'white', text: (this.waypointColorOption.length + 2).toString() } }, waypoints: this.waypointColorOption };
    this.markerOptions = { origin: { label: { color: 'white'} }, destination: { label: { color: 'white' } }, waypoints: this.waypointColorOption };
    //this.waypoints = this.customerWaypointList;
    //console.log(this.origin);

  }

onOptimizeRouteCreateTrip(){
  this.onOptimizeRoute();
  this.onCreateTrip();
}
  updateChk1(event,customerId) {

    
    let orderpr = this.orderData.find(x => x.customerId === customerId);
    let updateorderData  = this.orderData.filter(item => item !== orderpr);
    orderpr.isSelected = true;
    updateorderData.push(orderpr)
    this.orderData = updateorderData;
    console.log(updateorderData);
    console.log(this.orderData);
  }
  updateChk(event,customerId) {
    //this.empList.push(chk);
    let orderdt = this.orderDetailDataList.find(x => x.poNumber === event.target.value);
    let orderpr = this.orderData.find(x => x.customerId === customerId);
    //alert()
    let onderIndex = this.orderData.indexOf(orderpr);
    //let updateorderData  = this.orderData.filter(item => item !== orderpr);
    //alert(onderIndex);
   
    //let index = this.orderData.indexOf(orderpr);
    
    //this.orderData[index] = orderpr;

    this.stopNo =0;
    if ( event.target.checked ) {
      //this.keyvalueSelectCustomer.push({ "key": customerId, "value": Number(this.totalStops) + 1 })
      this.checkCount++;
      this.stopNo = Number(this.dict.get(customerId.toString()));
      if(this.stopNo === -1 || isNaN(this.stopNo)) {
        this.dict.set(customerId.toString(), 1); 
        orderpr.isSelected = true;
        orderpr.stopCount = 1;
        // var waypoint = new WaypointModel();
        // waypoint.location = orderpr.address;
        // waypoint.stopover = true;
        //this.customerWaypointList.push({ location: orderpr.address, stopover: true });
        this.isaleastAddreeSeleted = true;
      }
      else{
        this.dict.set(customerId.toString(), this.stopNo + 1); 
        orderpr.isSelected = true;
       
      }

      if(Number(this.dict.get(customerId.toString())) === 1){
        this.totalStops = String(Number(this.totalStops) + 1);
        let sameorderpr = this.orderData.find(x => x.stopCount === this.totalStops);
        //alert()
        if(sameorderpr){
        let smaonderIndex = this.orderData.indexOf(sameorderpr);
        sameorderpr.stopCount = String(Number(this.totalStops) -1);
        this.orderData[smaonderIndex] = sameorderpr;
        let samewaypoint = this.customerWaypointList.find(x => x.waypointstopno === this.totalStops);
         if(samewaypoint){
          let waypointndex = this.customerWaypointList.indexOf(samewaypoint);
          samewaypoint.waypointstopno = sameorderpr.stopCount;
          this.customerWaypointList[waypointndex] = samewaypoint;
         }
        }
        orderpr.stopCount = this.totalStops;
        // var waypoint = new WaypointModel();
        // waypoint.location = orderpr.address;
        // waypoint.stopover = true;
      
        this.customerWaypointList.push({ location: orderpr.address, stopover: false, waypointstopno : orderpr.stopCount});
      }
      //this.totalStops = String(Number(this.totalStops) + 1);
      orderdt.priority = Number(this.totalStops);
      this.tripOrderList.push(orderdt);
      //alert(event.target.value);
      //console.log(this.orderDetailDataList);
      //console.log(orderdt);
      //console.log(this.tripOrderList);  
      this.weight = (parseFloat(this.weight) + parseFloat(orderdt.grossWeight)).toFixed(2).toString();
      this.totalCost = (parseFloat(this.totalCost) + parseFloat(orderdt.billAmount)).toFixed(2).toString();
      //this.modelthis.orderDetailDataListe;
    }
    else{
      this.checkCount--;
      this.stopNo = Number(this.dict.get(customerId.toString()));
      this.dict.set(customerId.toString(), this.stopNo - 1); 
      if(Number(this.dict.get(customerId.toString())) === 0){
        this.totalStops = String(Number(this.totalStops) - 1);
        orderpr.isSelected = false;
        orderpr.stopCount = 0;
        let customerWaypoint = this.customerWaypointList.find(x => x.location === orderpr.address);
        if(customerWaypoint){
          this.customerWaypointList  = this.customerWaypointList.filter(item => item !== customerWaypoint);
        }
        
        //this.isaleastAddreeSeleted = false;
      }
      //console.log( "waypoint are" + this.customerWaypointList);
      //this.removeOrdr(orderdt,event.target.value);
      this.tripOrderList = this.tripOrderList.filter(item => item !== orderdt);
       
      if(this.checkCount === 0){
        this.weight = "0";
        this.totalCost = "0.00";
      }
      else{
      this.weight = (parseFloat(this.weight) - parseFloat(orderdt.grossWeight)).toFixed(2).toString();
      this.totalCost = (parseFloat(this.totalCost) - parseFloat(orderdt.billAmount)).toFixed(2).toString();
      }
      //this.modelselected = false;
      //this.orderData = this.orderData.filter(item => item !== orderpr);
      //this.orderData.push(orderpr);
    }
    //chkArray.push(chk);
    //alert(chk);
    //updateorderData.push(orderpr);
    //this.orderData=[];
    //this.orderData = updateorderData;
    //let updateItem = this.orderData.items.find(this.findIndexToUpdate, newItem.id);

    this.orderData[onderIndex] = orderpr;
    //console.log(this.orderData);
    console.log(this.customerWaypointList); 
    //alert(this.totalStops);
    //console.log(this.empList);
  }

  CheckAllOptions (event){
    //alert('hi');
    if ( event.target.checked ) {
      let stopCount = 0;
      this.orderData.forEach((item) => {
        item.isSelected = true;
        item.stopCount = stopCount + 1;
        stopCount ++;
        item.orders.forEach((iteminner) => {
          iteminner.isSelected = true;
           this.CaculateTripSWT(item.customerId,iteminner);
           this.isaleastAddreeSeleted = true;
          });
      });
    }
    else{
      this.orderData.forEach((item) => {
        item.isSelected = false;
        item.stopCount = 0;
        item.orders.forEach((iteminner) =>{
          iteminner.isSelected = false;
          this.isaleastAddreeSeleted = false;
          });
      });
      this.ResetTripSWT();
    }
   // if(this.isAllSelected){
     // this.isAllSelected =false;
   // }
   // else{
     // this.isAllSelected = false;
   // }
 }
 
 ResetTripSWT(){
  this.totalStops = "0";
  this.weight = "0";
  this.totalCost = "0"
 }
 CaculateTripSWT(customerId,iteminner){
    //var orderdt = this.orderDetailDataList.find(x => x.poNumber === event.target.value);
    //var orderpr = this.orderData.find(x => x.customerId === customerId);
    if(iteminner.isSelected){
        this.stopNo = Number(this.dict.get(customerId.toString()));
        if(this.stopNo === -1 || isNaN(this.stopNo)) {
            this.dict.set(customerId.toString(), 1); 
          }
        else{
            this.dict.set(customerId.toString(), this.stopNo + 1); 
          }
          if(Number(this.dict.get(customerId.toString())) === 1){
            this.totalStops = String(Number(this.totalStops) + 1);
          }
          this.tripOrderList = this.tripOrderList.filter(item => item !== iteminner);
          this.tripOrderList.push(iteminner);
          this.weight = (parseFloat(this.weight) + parseFloat(iteminner.grossWeight)).toString();
          this.totalCost = (parseFloat(this.totalCost) + parseFloat(iteminner.billAmount)).toString();
    }
    else{
          this.stopNo = Number(this.dict.get(customerId.toString()));
          this.dict.set(customerId.toString(), this.stopNo - 1); 
          if(Number(this.dict.get(customerId.toString())) === 0){
              this.totalStops = String(Number(this.totalStops) - 1);
            }
          this.tripOrderList = this.tripOrderList.filter(item => item !== iteminner);
          console.log(this.tripOrderList);  
          this.weight = (parseFloat(this.weight) - parseFloat(iteminner.grossWeight)).toString();
          this.totalCost = (parseFloat(this.totalCost) - parseFloat(iteminner.billAmount)).toString();
    }
    
 }
  removeOrdr(orderdt,orderNo) {
    this.tripOrderList = this.tripOrderList.filter(item => item !== orderdt);
    console.log(this.tripOrderList);      
}
checkCustomerOrder(event,customerId) {
     //alert(customerId);
     var orderpr = this.orderData.find(x => x.customerId === customerId);
     
     if ( event.target.checked ) {
      orderpr.stopCount = (this.totalStops == "0" ? "1" : Number(this.totalStops) + 1);
      this.isaleastAddreeSeleted = true;
        orderpr.orders.forEach((iteminner) => {
        iteminner.isSelected = true;
         this.CaculateTripSWT(customerId,iteminner);
         this.calculateCheck(true,iteminner.poNumber,customerId)
        });
     }
     else{
        orderpr.stopCount =0;
        orderpr.orders.forEach((iteminner) => {
        iteminner.isSelected = false;
         this.CaculateTripSWT(customerId,iteminner);
         this.calculateCheck(false,iteminner.poNumber,customerId)
        });
     } 
    
  }

  toggleEditable(event) {
    if ( event.target.checked ) {
        //this.contentEditable = true;
   }
  }

  calculateCheck(isCheck, orderno,customerId) {
    //this.empList.push(chk);
    let orderdt = this.orderDetailDataList.find(x => x.poNumber === orderno);
    let orderpr = this.orderData.find(x => x.customerId === customerId);
    //alert()
    let onderIndex = this.orderData.indexOf(orderpr);
    //let updateorderData  = this.orderData.filter(item => item !== orderpr);
    //alert(onderIndex);
   
    //let index = this.orderData.indexOf(orderpr);
    
    //this.orderData[index] = orderpr;

    this.stopNo =0;
    if ( isCheck ) {
      //this.keyvalueSelectCustomer.push({ "key": customerId, "value": Number(this.totalStops) + 1 })
      this.checkCount++;
      this.stopNo = Number(this.dict.get(customerId.toString()));
      if(this.stopNo === -1 || isNaN(this.stopNo)) {
        this.dict.set(customerId.toString(), 1); 
        orderpr.isSelected = true;
        orderpr.stopCount = 1;
        // var waypoint = new WaypointModel();
        // waypoint.location = orderpr.address;
        // waypoint.stopover = true;
        //this.customerWaypointList.push({ location: orderpr.address, stopover: true });
        this.isaleastAddreeSeleted = true;
      }
      else{
        this.dict.set(customerId.toString(), this.stopNo + 1); 
        orderpr.isSelected = true;
       
      }

      if(Number(this.dict.get(customerId.toString())) === 1){
        this.totalStops = String(Number(this.totalStops) + 1);
        let sameorderpr = this.orderData.find(x => x.stopCount === this.totalStops);
        //alert()
        if(sameorderpr){
        let smaonderIndex = this.orderData.indexOf(sameorderpr);
        sameorderpr.stopCount = String(Number(this.totalStops) -1);
        this.orderData[smaonderIndex] = sameorderpr;
        let samewaypoint = this.customerWaypointList.find(x => x.waypointstopno === this.totalStops);
         if(samewaypoint){
          let waypointndex = this.customerWaypointList.indexOf(samewaypoint);
          samewaypoint.waypointstopno = sameorderpr.stopCount;
          this.customerWaypointList[waypointndex] = samewaypoint;
         }
        }
        orderpr.stopCount = this.totalStops;
        // var waypoint = new WaypointModel();
        // waypoint.location = orderpr.address;
        // waypoint.stopover = true;
      
        this.customerWaypointList.push({ location: orderpr.address, stopover: false, waypointstopno : orderpr.stopCount});
      }
      //this.totalStops = String(Number(this.totalStops) + 1);
      orderdt.priority = Number(this.totalStops);
      this.tripOrderList.push(orderdt);
      //alert(event.target.value);
      //console.log(this.orderDetailDataList);
      //console.log(orderdt);
      //console.log(this.tripOrderList);  
      this.weight = (parseFloat(this.weight) + parseFloat(orderdt.grossWeight)).toFixed(2).toString();
      this.totalCost = (parseFloat(this.totalCost) + parseFloat(orderdt.billAmount)).toFixed(2).toString();
      //this.modelthis.orderDetailDataListe;
    }
    else{
      this.checkCount--;
      this.stopNo = Number(this.dict.get(customerId.toString()));
      this.dict.set(customerId.toString(), this.stopNo - 1); 
      if(Number(this.dict.get(customerId.toString())) === 0){
        this.totalStops = String(Number(this.totalStops) - 1);
        orderpr.isSelected = false;
        orderpr.stopCount = 0;
        let customerWaypoint = this.customerWaypointList.find(x => x.location === orderpr.address);
        if(customerWaypoint){
          this.customerWaypointList  = this.customerWaypointList.filter(item => item !== customerWaypoint);
        }
        
        //this.isaleastAddreeSeleted = false;
      }
      //console.log( "waypoint are" + this.customerWaypointList);
      //this.removeOrdr(orderdt,event.target.value);
      this.tripOrderList = this.tripOrderList.filter(item => item !== orderdt);
       
      if(this.checkCount === 0){
        this.weight = "0";
        this.totalCost = "0.00";
      }
      else{
      this.weight = (parseFloat(this.weight) - parseFloat(orderdt.grossWeight)).toFixed(2).toString();
      this.totalCost = (parseFloat(this.totalCost) - parseFloat(orderdt.billAmount)).toFixed(2).toString();
      }
      //this.modelselected = false;
      //this.orderData = this.orderData.filter(item => item !== orderpr);
      //this.orderData.push(orderpr);
    }
    //chkArray.push(chk);
    //alert(chk);
    //updateorderData.push(orderpr);
    //this.orderData=[];
    //this.orderData = updateorderData;
    //let updateItem = this.orderData.items.find(this.findIndexToUpdate, newItem.id);

    this.orderData[onderIndex] = orderpr;
    //console.log(this.orderData);
    console.log(this.customerWaypointList); 
    //alert(this.totalStops);
    //console.log(this.empList);
  }

}
