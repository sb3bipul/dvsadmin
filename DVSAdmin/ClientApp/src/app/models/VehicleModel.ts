export class VehicleModel {
  VehicleID: Number;
  CompanyID: string;
  VehicleNO: string;
  VehicleType: Number;
  VehicleName: string;
  VehicleCapacity:Number;
  IsActive: boolean;
  ActionMode:Number;
  } 