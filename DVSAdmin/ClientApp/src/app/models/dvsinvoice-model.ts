export class DVSInvoiceModel {
    invoiceNo: string;
    status: string;
    rejectReason: string;
}

export class EditInvoice {
    newInvoiceNo: string;
    invoiceNo: string;
    customerId: string;
    invoiceComment: string;
}