import { TripDetailModel } from "./TripDetailModel";

export class TripModel {
    TripID: Number;
    CompanyID: string;
    TripName: string;
    DriverID: Number;
    WareHouse: string
    Charges: Number;
    Priority: Number;
    SortTrip: Number
    VehicleID: Number;
    TripStatus: Number;
    TripDesc: string
    IsActive: boolean;
    IsDeleted: boolean;
    TripDetails: TripDetailModel[];
    ActionMode: Number;
    StartLocation: Number;
    EndLocation: Number;
    TripCreated: Date
  } 