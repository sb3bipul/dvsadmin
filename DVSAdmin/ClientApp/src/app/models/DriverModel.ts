export class DriverModel {
  CompanyId: Number;
  DriverName: string;
  DriverAddress: string;
  City: string
  State: Number;
  Zip: string;
  Country: Number;
  DriverEmail: string;
  DriverContactNo: string;
  DrivingLicense: string
  DriverLoginID: string;
  IsActive: boolean;
  Warehouse: string;
  DriverImage: string;
  VehicleID:number;
  ActionMode: Number;
} 
export class DriverUserModel {
  Email: string;
  Password: string;
  CompanyID: string;
  UserName: string
  UserFullName: string;
  Domain: string;
  UserTypeCode:string;
} 