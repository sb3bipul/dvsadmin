export class TripDetailModel {
    TripID: Number;
    TripDetailID: Number;
    OrderNo: string;
    StartLocation: Number;
    EndLocation: Number
    CompanyID: Number;
    ToDoComments: string;
    IsStopSummeryDone: boolean
    IsToDo: boolean;
    IsCustomerSkipped:boolean;
    Priority: Number;
    DeliveryCharges: Number;
    CustomerNote: string;
  } 