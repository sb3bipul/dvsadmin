import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { VehicleModel } from 'src/app/models/VehicleModel';
import { VehicleService } from 'src/app/services/vehicle.service';

@Component({
  selector: 'app-edit-vehicle',
  templateUrl: './edit-vehicle.component.html',
  styleUrls: ['./edit-vehicle.component.css']
})
export class EditVehicleComponent implements OnInit {
  vehicleId: number;
  vehicleNO:string;
  VehicleTypeText:string;
  vehicleName:string;
  vehicleCapacity:string;
  isActive:Boolean;
  vehicleType:Number;
  vehicleInfoData=[];
  vehicleData=[];
  vehicleAdd: boolean = false;
  info: string = '';
  public p: number = 1;
  vehicleTypeInfoData=[];
  vehicleTypeData=[];
  constructor(private vehicleService: VehicleService,private SpinnerService: NgxSpinnerService,private router: Router, private route: ActivatedRoute) { 

  }

  ngOnInit() {
    this.vehicleAdd = false;
    this.LoadVehicleType();
    this.loadVehicleInfoData();
  }

  loadVehicleInfoData() {   
    this.vehicleId = + this.route.snapshot.params['id'];
    console.log('Vehicle ID : ' + this.vehicleId);
    this.SpinnerService.show(); 

    this.vehicleService.getVehicleDetail(Number(this.vehicleId)).subscribe(    
      (result: any) => {    
       
        this.vehicleData = result["vehicles"];  
        for (var i = 0; i < this.vehicleData.length; i++) {
          this.vehicleInfoData.push(this.vehicleData[i]);
          
        }
      
        //this.vehicleId= this.vehicleInfoData[0].VehicleID;
        this.vehicleNO= this.vehicleInfoData[0].vehicleNO;
        this.vehicleName= this.vehicleInfoData[0].vehicleName;
        this.vehicleCapacity= this.vehicleInfoData[0].vehicleCapacity;
        this.VehicleTypeText= this.vehicleInfoData[0].vehicleTypeName;
        this.vehicleType = this.vehicleInfoData[0].vehicleType;
        this.isActive = this.vehicleInfoData[0].isActive;
        // if(Number(this.vehicleInfoData[0].status) === 1){
        //   this.VehicleStatus = 'Active';
        // }
        // else if(Number(this.vehicleInfoData[0].status) === 0){
        //   this.VehicleStatus = 'InActive';
        // }
        // else {
        // this.VehicleStatus = this.vehicleInfoData[0].status;
        // }
        this.SpinnerService.hide(); 
        }
    ); 
  }

  onVehicleEdit(vehicleNO: string, vehicleType: Number, vehicleName: string, vehicleCapacity: Number, isActive: Boolean) {
    if (vehicleNO != undefined && vehicleNO != '') {
      if (vehicleType != undefined) {
        if (vehicleName != undefined && vehicleName != '') {
          if (vehicleCapacity != undefined) {
            if (isActive != undefined) {
              var vehicleData = new VehicleModel()
              vehicleData.VehicleID = Number(this.vehicleId);
              vehicleData.CompanyID =String(2);
              vehicleData.VehicleNO = vehicleNO;
              vehicleData.VehicleType =Number(vehicleType);
              vehicleData.VehicleName = vehicleName;
              vehicleData.VehicleCapacity = Number(vehicleCapacity)
              vehicleData.IsActive = Boolean(isActive);
              vehicleData.ActionMode = 1;
              this.info = '';
              this.SpinnerService.show(); 
              this.vehicleService.updateVehicleInfo(vehicleData).subscribe(    
                (result: any) => {    
  
                  alert(result["message"]);
                  this.SpinnerService.hide(); 
                  this.vehicleAdd=true;
                  }    
                );
            }
            else {
              this.info = 'Please Enter Status';
            }
          }
          else {
            this.info = 'Please Enter Vehicle Capacity';
          }
        }
        else {
          this.info = 'Please Enter Vehicle Name';
        }
      }
      else {
        this.info = 'Please Enter Vehicle Type';
      }
    }
    else {
      this.info = 'Please Enter vehicleNO';
    }
  }
  private LoadVehicleType() {
    this.SpinnerService.show(); 

    this.vehicleService.getVehicleTypeInfo().subscribe(    
      (result: any) => {    
        console.log('result '  );
        this.vehicleTypeData = result["vehicleTypes"];  
        for (var i = 0; i < this.vehicleTypeData.length; i++) {
          this.vehicleTypeInfoData.push(this.vehicleTypeData[i]);
          this.vehicleType = this.vehicleInfoData[0].vehicleType;
        }
        console.log(this.vehicleTypeInfoData);
        this.SpinnerService.hide(); 
        }
    );    
      }
      numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
        }
        return true;
    
      }
      onChangeVehicleType(TypeId){

      }
      onChangeStatus(Isactive){

      }

}
