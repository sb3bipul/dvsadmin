import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { VehicleModel } from '../models/VehicleModel';
import { VehicleService } from '../services/vehicle.service';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css']
})
export class VehicleComponent implements OnInit {
  vehicleInfoData=[];
  vehicleData=[];
  StartDate = '';
  TripStatus = '';
  SearchText = '';
  public p: number = 1;
  constructor(private vehicleService: VehicleService,private SpinnerService: NgxSpinnerService,private router: Router) { 
    
  }

  ngOnInit() {
    this.SearchText = '';
    this.loadVehicleInfoData();
  }

  loadVehicleInfoData() {   
    this.SpinnerService.show(); 
    //delay(20000);
    
    this.vehicleService.getVehicleInfo(this.SearchText).subscribe(    
      (result: any) => {    
        //alert(JSON.parse(result["zones"]));
        
        this.vehicleData = result["vehicles"];  
        for (var i = 0; i < this.vehicleData.length; i++) {
          this.vehicleInfoData.push(this.vehicleData[i]);
          
        }
        console.log(this.vehicleInfoData);
        this.SpinnerService.hide(); 
        //alert(this.tripInfoData);  
        }
        //this.orderData = this.collection.data
        
       
    );    
  }
  removeVehicle(vehicleId){
    var vehicleData = new VehicleModel()
    vehicleData.VehicleID = Number(vehicleId);
    vehicleData.CompanyID =String(2);
    vehicleData.VehicleNO = '123';
    vehicleData.VehicleType = 1;
    vehicleData.VehicleName = 'test';
    vehicleData.VehicleCapacity = Number(1000)
    vehicleData.IsActive = true;
    vehicleData.ActionMode = 2;

    this.SpinnerService.show(); 
    this.vehicleService.deleteVehicleInfo(vehicleData).subscribe(    
      (result: any) => {    
     //alert(JSON.parse(result["salesmans"]));
     //this.salesmanData = result["salesmans"];  
        alert(result["message"]);
        let vehiclepr = this.vehicleInfoData.find(x => x.vehicleId === vehicleId);
        this.vehicleInfoData = this.vehicleInfoData.filter(item => item !== vehiclepr);
        console.log(this.vehicleInfoData);
        this.SpinnerService.hide(); 
        //this.loadDriverData();
        }    
      ); 
  }

  onViewVehicle(){

  }
  onAddVehicle(){
    this.router.navigate(['../add-vehicle']);
  }

}
