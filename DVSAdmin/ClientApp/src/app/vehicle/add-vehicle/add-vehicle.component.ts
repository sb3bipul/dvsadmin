import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { VehicleModel } from 'src/app/models/VehicleModel';
import { VehicleService } from 'src/app/services/vehicle.service';

@Component({
  selector: 'app-add-vehicle',
  templateUrl: './add-vehicle.component.html',
  styleUrls: ['./add-vehicle.component.css']
})
export class AddVehicleComponent implements OnInit {
  vehicleAdd: boolean = false;
  info: string = '';
  public p: number = 1;
  vehicleTypeInfoData=[];
  vehicleTypeData=[];
  private vehicleType:string = '';
  constructor(private vehicleService: VehicleService, private SpinnerService: NgxSpinnerService, private router: Router) { }

  ngOnInit() {
    this.vehicleAdd = false;
    this.LoadVehicleType();
  }

  onVehicleAdd(vehicleNO: string, vehicleType: Number, vehicleName: string, vehicleCapacity: Number, isActive: Boolean) {
    if (vehicleNO != undefined && vehicleNO != '') {
      if (vehicleType != undefined) {
        if (vehicleName != undefined && vehicleName != '') {
          if (vehicleCapacity != undefined) {
            if (isActive != undefined) {
              var vehicleData = new VehicleModel()
              vehicleData.VehicleID = 0;
              vehicleData.CompanyID =String(2);
              vehicleData.VehicleNO = vehicleNO;
              vehicleData.VehicleType =Number(vehicleType);
              vehicleData.VehicleName = vehicleName;
              vehicleData.VehicleCapacity = Number(vehicleCapacity)
              vehicleData.IsActive = Boolean(isActive);
              vehicleData.ActionMode = 1;
              this.info = '';
              this.SpinnerService.show(); 
              this.vehicleService.createVehicleInfo(vehicleData).subscribe(    
                (result: any) => {    
  
                  alert(result["message"]);
                  this.SpinnerService.hide(); 
                  this.vehicleAdd=true;
                  }    
                );
            }
            else {
              this.info = 'Please Enter Status';
            }
          }
          else {
            this.info = 'Please Enter Vehicle Capacity';
          }
        }
        else {
          this.info = 'Please Enter Vehicle Name';
        }
      }
      else {
        this.info = 'Please Enter Vehicle Type';
      }
    }
    else {
      this.info = 'Please Enter vehicleNO';
    }
  }
  private LoadVehicleType() {
    this.SpinnerService.show(); 

    this.vehicleService.getVehicleTypeInfo().subscribe(    
      (result: any) => {    
        console.log('result '  );
        this.vehicleTypeData = result["vehicleTypes"];  
        for (var i = 0; i < this.vehicleTypeData.length; i++) {
          this.vehicleTypeInfoData.push(this.vehicleTypeData[i]);
          this.vehicleType = '1';
        }
        console.log(this.vehicleTypeInfoData);
        this.SpinnerService.hide(); 
        }
    );    
      }
      numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
        }
        return true;
    
      }
      onChangeVehicleType(TypeId){

      }
      onChangeStatus(Isactive){

      }
}
