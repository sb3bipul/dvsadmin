import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { VehicleService } from 'src/app/services/vehicle.service';

@Component({
  selector: 'app-view-vehicle',
  templateUrl: './view-vehicle.component.html',
  styleUrls: ['./view-vehicle.component.css']
})
export class ViewVehicleComponent implements OnInit {
  VehicleId: number;
  VehicleNO:string;
  VehicleTypeText:string;
  VehicleName:string;
  VehicleCapacity:string;
  VehicleStatus:string;

  vehicleInfoData=[];
  vehicleData=[];
  constructor(private vehicleService: VehicleService,private SpinnerService: NgxSpinnerService,private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.loadVehicleInfoData();
  }

  loadVehicleInfoData() {   
    this.VehicleId = + this.route.snapshot.params['id'];
    console.log('Vehicle ID : ' + this.VehicleId);
    this.SpinnerService.show(); 

    this.vehicleService.getVehicleDetail(Number(this.VehicleId)).subscribe(    
      (result: any) => {    
       
        this.vehicleData = result["vehicles"];  
        for (var i = 0; i < this.vehicleData.length; i++) {
          this.vehicleInfoData.push(this.vehicleData[i]);
          
        }
      
        this.VehicleId= this.vehicleInfoData[0].VehicleID;
        this.VehicleNO= this.vehicleInfoData[0].vehicleNO;
        this.VehicleName= this.vehicleInfoData[0].vehicleName;
        this.VehicleCapacity= this.vehicleInfoData[0].vehicleCapacity;
        this.VehicleTypeText= this.vehicleInfoData[0].vehicleTypeName;
        if(Number(this.vehicleInfoData[0].status) === 1){
          this.VehicleStatus = 'Active';
        }
        else if(Number(this.vehicleInfoData[0].status) === 0){
          this.VehicleStatus = 'InActive';
        }
        else {
        this.VehicleStatus = this.vehicleInfoData[0].status;
        }
        this.SpinnerService.hide(); 
        }
    ); 
  }

}
