import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { CustomerService } from '../services/customer.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  customerInfoData=[];
  customerData=[];
  StartDate = '';
  ZoneStatus = '';
  SearchText = '';
  public p: number = 1;
 constructor(private customerServise: CustomerService,private SpinnerService: NgxSpinnerService,private router: Router) { 
    
  this.loadCustomerInfoData();

  }

  ngOnInit() {
  }

  loadCustomerInfoData() {   
    this.SpinnerService.show(); 
    //delay(20000);
    this.customerServise.getCustomerInfo().subscribe(    
      (result: any) => {            
        this.customerData = result["customers"];  
        for (var i = 0; i < this.customerData.length; i++) {
          this.customerInfoData.push(this.customerData[i]); 
        }
        console.log(this.customerInfoData);
        this.SpinnerService.hide();  
        }
    );    
  }

}
