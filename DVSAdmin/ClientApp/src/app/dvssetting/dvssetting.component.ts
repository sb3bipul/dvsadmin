import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { DVSSetting } from '../models/DVSSetting';
import { DvssettingService } from '../services/dvssetting.service';

@Component({
  selector: 'app-dvssetting',
  templateUrl: './dvssetting.component.html',
  styleUrls: ['./dvssetting.component.css']
})
export class DvssettingComponent implements OnInit {
  locations;
  StartLocation;
  EndLocation;
  constructor(private http: HttpClient, private formBuilder: FormBuilder,private dvsSettingServise: DvssettingService, private SpinnerService: NgxSpinnerService) { 
    this.loadLocationData();
  }

  ngOnInit() {
  }

  loadLocationData(){
    this.SpinnerService.show(); 
    this.dvsSettingServise.getLocation().subscribe(    
      (result: any) => {    
        //alert(JSON.parse(result["zones"]));
        this.locations = result["locations"];  
        console.log(this.locations);  
        //this.renderMap();
        this.SpinnerService.hide(); 
        if(this.locations){
          this.StartLocation = this.locations[0].startPoint;
          this.EndLocation = this.locations[0].endPoint;
          console.log(this.StartLocation +  " , " + this.EndLocation ); 
          }
      }    
    );    
  }

  setStartLocation(value: string) {
    this.StartLocation = value.substr(2,1);
    // console.log(this.StartLocation);
    //alert(this.StartLocation);
  }
  setEndLocation(value: string) {
    this.EndLocation = value.substr(2,1);;
    // console.log(this.StartLocation);w
    //alert(this.EndLocation);
    
  }
  onEditSetting(locationId){
    alert(locationId);
  }

  updateSetting(StartLocation:string, EndLocation:string){
    console.log('StartLocation: ' + StartLocation);
    console.log('EndLocation: ' + EndLocation);
    var dvsSettingData = new DVSSetting()
    dvsSettingData.Id =1;
    dvsSettingData.StartPoint = Number(this.StartLocation);
    dvsSettingData.EndPoint = Number(this.EndLocation);
    dvsSettingData.IsActive = true;
    dvsSettingData.ActionMode = 1;
    this.SpinnerService.show(); 
    this.dvsSettingServise.updateSettingInfo(dvsSettingData).subscribe(    
      (result: any) => {    

        alert(result["message"]);
        this.SpinnerService.hide(); 
        }    
      ); 
  }

}
