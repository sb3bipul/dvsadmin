import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { LocationModel } from 'src/app/models/LocationModel';
import { DvssettingService } from 'src/app/services/dvssetting.service';

@Component({
  selector: 'app-edit-location',
  templateUrl: './edit-location.component.html',
  styleUrls: ['./edit-location.component.css']
})
export class EditLocationComponent implements OnInit {
  id: number;
  LocationName: string;
  LocationAddress: string;
  IsActive: boolean;
  locationInfoData = [];
  locationData = [];
  locationAdd: boolean = false;
  info: string = '';
  constructor(private dvsSettingService: DvssettingService, private SpinnerService: NgxSpinnerService, private router: Router, private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.id = + this.route.snapshot.params['id'];
    console.log('Login ID : ' + this.id);
    this.SpinnerService.show();

    this.dvsSettingService.getLocationById(Number(this.id)).subscribe(
      (result: any) => {

        this.locationData = result["locations"];
        for (var i = 0; i < this.locationData.length; i++) {
          this.locationInfoData.push(this.locationData[i]);

        }
        console.log('result: ' + this.locationInfoData + 'LocationName: ' + this.locationInfoData[0].LocationName);
        this.id = this.locationInfoData[0].id;
        this.LocationName = this.locationInfoData[0].locationName;
        this.LocationAddress = this.locationInfoData[0].locationAddress;
        this.IsActive = true;
        this.SpinnerService.hide();
      }
    );
    this.locationAdd = false;
  }

  onUpdateLocation(LocationName: string, LocationAddress: string) {
    if (LocationName != undefined && LocationName != '') {
      if (LocationAddress != undefined && LocationAddress != '') {
        var LocationData = new LocationModel()
        LocationData.Id = Number(this.id);
        LocationData.LocationName = LocationName;
        LocationData.LocationAddress = LocationAddress;
        LocationData.IsActive = false;
        this.info = '';
        this.SpinnerService.show();
        this.dvsSettingService.putLocation(LocationData).subscribe(
          (result: any) => {

            alert(result["message"]);
            this.SpinnerService.hide();
            this.locationAdd = true;
          }
        );
      }
      else {
        this.info = 'Please Enter Location Address';
      }
    }
    else {
      this.info = 'Please Enter Location Name';
    }
  }

}
