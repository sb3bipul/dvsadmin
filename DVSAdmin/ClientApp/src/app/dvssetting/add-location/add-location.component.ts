import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { LocationModel } from 'src/app/models/LocationModel';
import { DvssettingService } from 'src/app/services/dvssetting.service';

@Component({
  selector: 'app-add-location',
  templateUrl: './add-location.component.html',
  styleUrls: ['./add-location.component.css']
})
export class AddLocationComponent implements OnInit {
  locationAdd:boolean=false;
  info:string='';
  constructor(private dvsSettingService: DvssettingService,private SpinnerService: NgxSpinnerService,private router: Router) {

   }

  ngOnInit() {
    this.locationAdd=false;
  }

  onCreateLocation(LocationName:string, LocationAddress:string){
    if(LocationName != undefined && LocationName != '')
    {
      if(LocationAddress != undefined && LocationAddress != '')
      {
        var LocationData = new LocationModel()
        LocationData.Id = 2;
        LocationData.LocationName = LocationName;
        LocationData.LocationAddress = LocationAddress;
        LocationData.IsActive = true;
        this.info = '';
            this.SpinnerService.show(); 
            this.dvsSettingService.postLocation(LocationData).subscribe(    
              (result: any) => {    

                alert(result["message"]);
                this.SpinnerService.hide(); 
                this.locationAdd=true;
                }    
              ); 
      }
      else
      {
        this.info = 'Please Enter Location Address';
      }
    }
    else
    {
      this.info = 'Please Enter Location Name';
    }
  }

}
