import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DvssettingComponent } from './dvssetting.component';

describe('DvssettingComponent', () => {
  let component: DvssettingComponent;
  let fixture: ComponentFixture<DvssettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DvssettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DvssettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
