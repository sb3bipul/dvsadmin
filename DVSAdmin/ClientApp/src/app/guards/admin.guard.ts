import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { User } from '../models/user';
import { map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  userDataSubscription: any;
  userData = new User();
  constructor(private router: Router, private authService: AuthService) {
    // this.userDataSubscription = this.authService.userData.asObservable().subscribe(data => {
      // this.userData = data;
    // });
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    //alert(localStorage.getItem('authToken'));
    if (localStorage.getItem('authToken')) {  
      return true;  
  }  
    // if (this.userData) {
      // return true;
    // }

    //this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    this.router.navigate(['/login']);
    return false;
  }
  // canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    // return this.authService.isLoggedIn.pipe(
      // take(1),
      // map((isLoggedIn: boolean) => {
        // if (!isLoggedIn) {
          // this.router.navigate(['/login']);
          // return false;
        // }
        // return true;
      // })
    // );
  // }
}   
