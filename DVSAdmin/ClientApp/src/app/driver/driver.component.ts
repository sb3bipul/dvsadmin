import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import {Router} from "@angular/router";
import { DriverService } from '../services/driver.service';
import { DriverModel } from '../models/DriverModel';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
declare var $: any;
@Component({
  selector: 'app-driver',
  templateUrl: './driver.component.html',
  styleUrls: ['./driver.component.css']
})
export class DriverComponent implements OnInit {

  driverInfoData=[];
  driverData=[];
  StartDate = '';
  SearchText = '';
  driverId='';
  driverName = '';
  public p: number = 1;
 constructor(private driverService: DriverService,private SpinnerService: NgxSpinnerService,private router: Router) { 

  
  this.loadDriverData();
 }

  ngOnInit() {
  }

  loadDriverData() {   
    this.SpinnerService.show(); 

    this.driverService.getDriverInfo().subscribe(    
      (result: any) => {    
       //console.log('result: '  );
        this.driverData = result["drivers"];  
        for (var i = 0; i < this.driverData.length; i++) {
          this.driverInfoData.push(this.driverData[i]);
          
        }
        console.log(this.driverInfoData);
        this.SpinnerService.hide(); 
        }
       
    );    
  }
  setDrivertData(id:string,name:string){
    this.driverId = id;
    this.driverName = name;
  }
  removeDriver(driverLoginID:string){
    
    var driverData = new DriverModel()
            driverData.CompanyId = 2;
            driverData.DriverName = "d";
            driverData.DriverAddress = "daddres";
            driverData.City = "dcity";
            driverData.State = 31;
            driverData.Zip = "dzip";
            driverData.Country = 243;
            driverData.DriverEmail = "demail";
            driverData.DriverContactNo = "dmobile";
            driverData.DrivingLicense = "dlicence";
            driverData.DriverLoginID = driverLoginID;
            driverData.IsActive = false;
            driverData.Warehouse = "01";
            driverData.DriverImage = "no-image.png";
            driverData.VehicleID = 2;
            driverData.ActionMode = 1;

    this.SpinnerService.show(); 
    this.driverService.deleteDriverInfo(driverData).subscribe(    
      (result: any) => {    
     //alert(JSON.parse(result["salesmans"]));
     //this.salesmanData = result["salesmans"];  
        alert(result["message"]);
        this.SpinnerService.hide(); 
        //this.loadDriverData();
        }    
      ); 
  }
  addNewDrive(){
    this.router.navigate(['../driver-new']);
  }

  onEditDriver(driverId){
    window.localStorage.removeItem("driverId");
    window.localStorage.setItem("driverId", driverId.toString());
    this.router.navigate(['../editview-driver']);
  }

}
