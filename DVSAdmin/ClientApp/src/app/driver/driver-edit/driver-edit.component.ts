import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import {Router, ActivatedRoute} from "@angular/router";
import { DriverService } from '../../services/driver.service';
import { DriverModel } from '../../models/DriverModel';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
declare var $: any;

@Component({
  selector: 'app-driver-edit',
  templateUrl: './driver-edit.component.html',
  styleUrls: ['./driver-edit.component.css']
})
export class DriverEditComponent implements OnInit {
  id: number;
  dloginid:string;
  dname:string;
  dmobile:string;
  dlicence:string;
  demail:string;
  daddres:string;
  dcity:string;
  dzip:string;
  driverInfoData=[];
  driverData=[];
  info:string='';
  vehicleData=[];
  vehicleInfoData=[];
  private vehicle:string = '';
  driverAdd:boolean=false;
  public p: number = 1;
 constructor(private driverService: DriverService,private SpinnerService: NgxSpinnerService,private router: Router, private route: ActivatedRoute) { 
 }

  ngOnInit() {
    this.id = + this.route.snapshot.params['id'];
    console.log('Login ID : ' + this.id);
    this.SpinnerService.show(); 
    this.LoadVehicle();
    this.driverService.getDriverInfoByID(Number(this.id)).subscribe(    
      (result: any) => {    
       
        this.driverData = result["drivers"];  
        for (var i = 0; i < this.driverData.length; i++) {
          this.driverInfoData.push(this.driverData[i]);
          
        }
        if(this.driverInfoData){
        //console.log('result: ' + this.driverInfoData + 'driverLoginID: '+this.driverInfoData[0].driverLoginID);
        this.dloginid= this.driverInfoData[0].driverLoginID;
        this.dname= this.driverInfoData[0].driverName;
        this.dmobile= this.driverInfoData[0].driverContactNo;
        this.dlicence= this.driverInfoData[0].drivingLicense;
        this. demail= this.driverInfoData[0].driverEmail;
        this.daddres= this.driverInfoData[0].driverAddress;
        this.dcity= this.driverInfoData[0].city;
        this.dzip= this.driverInfoData[0].zip;
        this.vehicle = JSON.stringify( this.driverInfoData[0].vehicleID);
        console.log('result : '  + this.vehicle);
        this.SpinnerService.hide(); 
        }
      }
    );    
    this.driverAdd=false;
  }

  updateDriver(driverLoginID:string, dname:string, dmobile:string, dlicence:string, demail:string, daddres:string, dcity:string, dzip:string ){
    console.log('dname: ' + dname);
      if(dname != undefined && dname != '')
      {
        if(daddres != undefined && daddres != '')
       {
        if(dmobile != undefined && dmobile != '')
        {
          if(dlicence != undefined && dlicence != '')
         {
          if(demail != undefined && dcity != undefined && dzip != undefined && demail != '' && dcity != '' && dzip != '')
          {
            var driverData = new DriverModel()
            driverData.CompanyId = 2;
            driverData.DriverName = dname;
            driverData.DriverAddress = daddres;
            driverData.City = dcity;
            driverData.State = 31;
            driverData.Zip = dzip;
            driverData.Country = 243;
            driverData.DriverEmail = demail;
            driverData.DriverContactNo = dmobile;
            driverData.DrivingLicense = dlicence;
            driverData.DriverLoginID = driverLoginID;
            driverData.IsActive = true;
            driverData.Warehouse = "01";
            driverData.DriverImage = "no-image.png";
            driverData.VehicleID = parseInt(this.vehicle);
            driverData.ActionMode = 1;
            this.info = '';
            this.SpinnerService.show(); 
            this.driverService.updateDriverInfo(driverData).subscribe(    
              (result: any) => {    

                alert(result["message"]);
                this.SpinnerService.hide(); 
                this.driverAdd=true;
                }    
              ); 
            }
            else
            {
              this.info = 'Please Enter Driver Email , City and Zip ';
            }
           }
            else
            {
              this.info = 'Please Enter Driver License Number';
            }
          }
          else
          {
            this.info = 'Please Enter Driver Mobile Number';
          }
        }
        else
        {
          this.info = 'Please Enter Driver Address';
        }
      }
      else
      {
        this.info = 'Please Enter Driver Name';
      }
    
   
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  private LoadVehicle() {
    this.SpinnerService.show(); 

    this.driverService.getVehicleInfo().subscribe(    
      (result: any) => {    
        
        this.vehicleData = result["vehicles"];  
        for (var i = 0; i < this.vehicleData.length; i++) {
          this.vehicleInfoData.push(this.vehicleData[i]);
          this.vehicle = JSON.stringify(this.vehicleInfoData[0].vehicleID);
          
        }
        console.log(this.vehicleInfoData);
        this.SpinnerService.hide(); 
        }
    );    
}

onChangeCompany(selectedValue:string){
  this.vehicle = selectedValue;
}

}
