import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import {Router, ActivatedRoute} from "@angular/router";
import { DriverService } from '../../services/driver.service';
import { DriverModel } from '../../models/DriverModel';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
declare var $: any;

@Component({
  selector: 'app-driver-view',
  templateUrl: './driver-view.component.html',
  styleUrls: ['./driver-view.component.css']
})
export class DriverViewComponent implements OnInit {

  id: number;
  dloginid:string;
  dname:string;
  dmobile:string;
  dlicence:string;
  demail:string;
  daddres:string;
  dcity:string;
  dzip:string;
  vehicleName:string;
  vehicleCapacity:number;
  vehicleNo:string;
  driverInfoData=[];
  driverData=[];

  public p: number = 1;
 constructor(private driverService: DriverService,private SpinnerService: NgxSpinnerService,private router: Router, private route: ActivatedRoute) { 
 }

  ngOnInit() {
    this.id = + this.route.snapshot.params['id'];
    console.log('Login ID : ' + this.id);
    this.SpinnerService.show(); 

    this.driverService.getDriverInfoByID(Number(this.id)).subscribe(    
      (result: any) => {    
        //console.log('result '  );
        this.driverData = result["drivers"];  
        for (var i = 0; i < this.driverData.length; i++) {
          this.driverInfoData.push(this.driverData[i]);
          
        }
        console.log(this.driverInfoData);
        if(this.driverInfoData){
        //console.log('result: ' + this.driverInfoData + 'driverLoginID: '+this.driverInfoData[0].driverLoginID);
        this.dloginid = this.driverInfoData[0].driverLoginID;
        this.dname = this.driverInfoData[0].driverName;
        this.dmobile = this.driverInfoData[0].driverContactNo;
        this.dlicence = this.driverInfoData[0].drivingLicense;
        this. demail = this.driverInfoData[0].driverEmail;
        this.daddres = this.driverInfoData[0].driverAddress;
        this.dcity = this.driverInfoData[0].city;
        this.dzip = this.driverInfoData[0].zip;
        this.vehicleName = this.driverInfoData[0].vehicleName;
        this.vehicleCapacity = this.driverInfoData[0].vehicleCapacity;
        this.vehicleNo = this.driverInfoData[0].vehicleNo;
        }
        this.SpinnerService.hide(); 
        }
    );    
  }


}
