import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import {Router} from "@angular/router";
import { DriverService } from '../../services/driver.service';
import { DriverModel, DriverUserModel } from '../../models/DriverModel';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
declare var $: any;

@Component({
  selector: 'app-driver-new',
  templateUrl: './driver-new.component.html',
  styleUrls: ['./driver-new.component.css']
})
export class DriverNewComponent implements OnInit {

  driverAdd:boolean=false;
  info:string='';private vehicle:string = '';
  public p: number = 1;
  vehicleData=[];
  vehicleInfoData=[];
 constructor(private driverService: DriverService,private SpinnerService: NgxSpinnerService,private router: Router) { 
 }

  ngOnInit() {
    this.driverAdd=false;
    this.LoadVehicle();
  }
  newDriver(driverLoginID:string, dname:string, dmobile:string, dlicence:string, demail:string, daddres:string, dcity:string, dzip:string ){
    if(driverLoginID != undefined && driverLoginID != '')
    {
      if(dname != undefined && dname != '')
      {
        if(daddres != undefined && daddres != '')
       {
        if(dmobile != undefined && dmobile != '')
        {
          if(dlicence != undefined && dlicence != '')
         {
          if(demail != undefined && dcity != undefined && dzip != undefined && demail != '' && dcity != '' && dzip != '')
          {
            var driverData = new DriverModel()
            driverData.CompanyId = 2;
            driverData.DriverName = dname;
            driverData.DriverAddress = daddres;
            driverData.City = dcity;
            driverData.State = 31;
            driverData.Zip = dzip;
            driverData.Country = 243;
            driverData.DriverEmail = demail;
            driverData.DriverContactNo = dmobile;
            driverData.DrivingLicense = dlicence;
            driverData.DriverLoginID = driverLoginID;
            driverData.IsActive = false;
            driverData.Warehouse = "01";
            driverData.DriverImage = "no-image.png";
            driverData.VehicleID = parseInt(this.vehicle);
            driverData.ActionMode = 1;
            this.info = '';
            this.SpinnerService.show(); 
            this.driverService.createDriverInfo(driverData).subscribe(    
              (result: any) => {    
                var drvUserData = new DriverUserModel();
                  drvUserData.CompanyID = "2";
                  drvUserData.Password = "Pwd@"+driverLoginID;
                  drvUserData.Email = demail;
                  drvUserData.UserName = driverLoginID;
                  drvUserData.UserFullName = dname;
                  drvUserData.Domain = "NULL";
                  drvUserData.UserTypeCode = "BR";
                  this.driverService.createNewDriverUserLogin(drvUserData).subscribe(    
                    (result1: any) => {    
                      console.log('Created New Driver User for Login.'  );
                    });

                  alert(result["message"]);
                  this.SpinnerService.hide(); 
                  this.driverAdd=true;
                }    
              ); 
            }
            else
            {
              this.info = 'Please Enter Driver Email , City and Zip ';
            }
           }
            else
            {
              this.info = 'Please Enter Driver License Number';
            }
          }
          else
          {
            this.info = 'Please Enter Driver Mobile Number';
          }
        }
        else
        {
          this.info = 'Please Enter Driver Address';
        }
      }
      else
      {
        this.info = 'Please Enter Driver Name';
      }
    }
    else
    {
      this.info = 'Please Enter Driver Login ID';
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  private LoadVehicle() {
      this.SpinnerService.show(); 
  
      this.driverService.getVehicleInfo().subscribe(    
        (result: any) => {    
          console.log('result '  );
          this.vehicleData = result["vehicles"];  
          for (var i = 0; i < this.vehicleData.length; i++) {
            this.vehicleInfoData.push(this.vehicleData[i]);
            this.vehicle = '1';
          }
          console.log(this.vehicleInfoData);
          this.SpinnerService.hide(); 
          }
      );    
  }

  onChangeCompany(selectedValue:string){
    this.vehicle = selectedValue;
  }

}
