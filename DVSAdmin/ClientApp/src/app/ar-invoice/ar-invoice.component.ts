import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from "@angular/router";
import { ARInvoiceService } from '../services/arinvoice.service';
declare var $: any;
import { ModalService } from '../services/modal.service';
import { ApproveInv } from '../models/armodel';
import * as jsPDF from 'jspdf';
import * as XLSX from 'xlsx';
import { AngularCsv } from 'angular7-csv/dist/Angular-csv'

@Component({
  selector: 'app-ar-invoice',
  templateUrl: './ar-invoice.component.html',
  styleUrls: ['./ar-invoice.component.css']
})

export class ARInvoiceComponent implements OnInit {
  StartDate: string; ToDate: string; City: string; InvoiceNo: string; RejectReason: string;
  Invoice_Number: string; Action: string; modelid: string; status: string; Inv_Number: string;
  ARInvoice = []; ARInvoiceList = []; Citylist = []; CustCity = []; CSVDt=[]; CSVInvoiceList=[];
  ponumber: string; BasketID: string; OrderAmount: string; BrokerId: string; CheckerID: string; truckNo: string; stopId: string;
  public p: number = 1;
  constructor(private arInvoice: ARInvoiceService, private SpinnerService: NgxSpinnerService, private router: Router, private route: ActivatedRoute, private modalService: ModalService) { }

  @ViewChild('example', { static: false }) pdfTable: ElementRef;

  ngOnInit() {
    var Todt = new Date();
    var stDt = new Date();
    Todt.setDate(Todt.getDate() - 7);
    this.StartDate = Todt.toISOString().split('T')[0]
    this.ToDate = stDt.toISOString().split('T')[0]

    this.GetInvliceList(this.StartDate, this.ToDate);
    this.bindCity();
    this.status = '0';
  }

  search() {
    this.GetInvliceList(this.StartDate, this.ToDate);
  }


  public GetInvliceList(FromDate: string, Todate: string) {
    this.ARInvoiceList = [];
    this.ARInvoice = [];
    var _city = ''; var _invoiceNo = ''; var _status = '';
    if (this.City == null || this.City == undefined) {
      _city = '';
    }
    else {
      _city = this.City;
    }
    if (this.InvoiceNo == null || this.InvoiceNo == undefined) {
      _invoiceNo = '';
    }
    else {
      _invoiceNo = this.InvoiceNo;
    }
    if (this.status == null || this.status == undefined) {
      _status = '';
    }
    else {
      _status = this.status;
    }

    this.SpinnerService.show();
    this.arInvoice.getARInvoiceListForAdmin(FromDate, Todate, _invoiceNo, '', _city, _status).subscribe(
      (result: any) => {
        this.ARInvoice = result["invoiceList"];
        for (var i = 0; i < this.ARInvoice.length; i++) {
          this.ARInvoiceList.push(this.ARInvoice[i]);
        }
        this.SpinnerService.hide();
      }
    );
  }

  InvoiceApproval(invoiceNo: string, Action: string) {
    // this.ARInvoiceList = [];
    // this.ARInvoice = [];
    var approveinv = new ApproveInv();
    approveinv.invoiceNo = invoiceNo;
    approveinv.rejectReason = null;
    approveinv.action = Action;
    this.SpinnerService.show();
    this.arInvoice.updateApproveRejectInvoice(approveinv).subscribe(
      (result: any) => {
        alert('Invoice Successfully Approved');
        this.GetInvliceList(this.StartDate, this.ToDate);
        //this.SpinnerService.hide();
      });
  }

  openModal(id: string, invoiceNo: string, Action: string) {
    this.modalService.open(id);
    this.Invoice_Number = invoiceNo;
    this.Action = Action;
    this.modelid = id;
  }

  RejectInvoice() {
    var _RejectReason = '';
    // this.ARInvoiceList = [];
    // this.ARInvoice = [];
    if (this.RejectReason == null || this.RejectReason == 'undefined') {
      _RejectReason = null;
    }
    else {
      _RejectReason = this.RejectReason;
    }
    var approveinv = new ApproveInv();
    approveinv.invoiceNo = this.Invoice_Number;
    approveinv.rejectReason = _RejectReason;
    approveinv.action = this.Action;
    this.SpinnerService.show();
    this.arInvoice.updateApproveRejectInvoice(approveinv).subscribe(
      (result: any) => {
        alert('Invoice Successfully Rejected');
        this.closeModal(this.modelid)
        this.GetInvliceList(this.StartDate, this.ToDate);
        //this.SpinnerService.show();
      });
  }

  closeModal(id: string) {
    this.modalService.close(id);
    this.modelid = null;
  }

  bindCity() {
    this.arInvoice.getARCustCity().subscribe(
      (result: any) => {
        this.CustCity = result["customerList"];
        for (var i = 0; i < this.CustCity.length; i++) {
          this.Citylist.push(this.CustCity[i]);
        }
        console.log(this.Citylist);
      }
    );
  }

  openInvoiceDetails(id: string, invoiceNo: string, pONumber: string, BasketId: string, BrokerId: string, OrderAmt: string, CheckerId: string, StopId: string, TruckNo: string) {
    this.modalService.open(id);
    this.Inv_Number = invoiceNo;
    this.ponumber = pONumber;
    this.BasketID = BasketId;
    this.OrderAmount = OrderAmt;
    this.BrokerId = BrokerId;
    this.CheckerID = CheckerId;
    this.stopId = StopId;
    this.truckNo = TruckNo;
  }

  reset() {
    this.CustCity = [];
    this.Citylist = [];
    this.bindCity();
    this.status = '0';
    this.InvoiceNo = '';
    this.GetInvliceList(this.StartDate, this.ToDate);
  }

  todayinvoiceList() {
    var stDt = new Date();
    var StartDt = stDt.toISOString().split('T')[0];
    var endDt = stDt.toISOString().split('T')[0];
    this.GetInvliceList(StartDt, endDt);
  }

  YesterdayinvoiceList() {
    var stDt = new Date();
    stDt.setDate(stDt.getDate() - 1);
    var StartDt = stDt.toISOString().split('T')[0];
    var endDt = stDt.toISOString().split('T')[0];
    this.GetInvliceList(StartDt, endDt);
  }

  ThisWeekInvoiceList() {
    var stDt = new Date();
    var Todt = new Date();
    stDt.setDate(stDt.getDate() - 7);
    var StartDt = stDt.toISOString().split('T')[0];
    var endDt = Todt.toISOString().split('T')[0];
    this.GetInvliceList(StartDt, endDt);
  }

  LastWeekInvoiceList() {
    var stDt = new Date();
    var Todt = new Date();
    stDt.setDate(stDt.getDate() - 15);
    var StartDt = stDt.toISOString().split('T')[0];
    var endDt = Todt.toISOString().split('T')[0];
    this.GetInvliceList(StartDt, endDt);
  }

  LastMonthInvoiceList() {
    var stDt = new Date();
    var Todt = new Date();
    stDt.setDate(stDt.getDate() - 30);
    var StartDt = stDt.toISOString().split('T')[0];
    var endDt = Todt.toISOString().split('T')[0];
    this.GetInvliceList(StartDt, endDt);
  }

  public downloadAsPDF() {
    const doc = new jsPDF('p', 'px', 'a4');
    const specialElementHandlers = {
      '#example': function (element, renderer) {
        return true;
      }
    };

    const pdfTable = this.pdfTable.nativeElement;
    doc.fromHTML(pdfTable.innerHTML, 30, 30, { width: 250, 'elementHandlers': specialElementHandlers });
    doc.save('AR-Invoice.pdf');
  }

  fileName = 'AR-Invoice.xlsx';

  exportexcel(): void {
    /* table id is passed over here */
    let element = document.getElementById('example');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'AR-Invoice');

    /* save to file */
    XLSX.writeFile(wb, this.fileName);
  }

  csvOptions = {
    fieldSeparator: ',',
    quoteStrings: '"',
    decimalseparator: '.',
    showLabels: true,
    showTitle: true,
    title: 'AR Invoice List :',
    useBom: true,
    noDownload: false,
    headers: ["Customer Code", "Customer Name", "Order #", "Invoice #", "City", "Order Date", "Trip #", "Driver #", "Status"]
  };

  public downloadCSV() {
    this.CSVInvoiceList = [];
    this.CSVDt = [];
    var _city = ''; var _invoiceNo = ''; var _status = '';
    if (this.City == null || this.City == undefined) {
      _city = '';
    }
    else {
      _city = this.City;
    }
    if (this.InvoiceNo == null || this.InvoiceNo == undefined) {
      _invoiceNo = '';
    }
    else {
      _invoiceNo = this.InvoiceNo;
    }
    if (this.status == null || this.status == undefined) {
      _status = '';
    }
    else {
      _status = this.status;
    }

    this.SpinnerService.show();
    this.arInvoice.getCSVDataForExport(this.StartDate, this.ToDate, _invoiceNo, '', _city, _status).subscribe(
      (result: any) => {
        this.CSVDt = result["invoiceList"];
        for (var i = 0; i < this.CSVDt.length; i++) {
          this.CSVInvoiceList.push(this.CSVDt[i]);
        }
        this.SpinnerService.hide();
        new AngularCsv(JSON.stringify(this.CSVInvoiceList), "AR-InvoiceList", this.csvOptions);
      }
    );
  }
}
