import { Component, OnInit, Output, EventEmitter, Input, ChangeDetectorRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//import { NgxPaginationModule } from 'ngx-pagination';
import { FormBuilder } from '@angular/forms';
import { TripService } from '../services/trip.service';
import { DatePipe } from '@angular/common';
import { NgxSpinnerService } from "ngx-spinner";
import { TripModel } from '../models/TripModel';
import { TripDetailModel } from '../models/TripDetailModel';
import { Dictionary } from '../models/Dictionary';
import { WaypointModel } from '../models/WaypointModel';
import { CompileShallowModuleMetadata } from '@angular/compiler';
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-trip',
  templateUrl: './edit-trip.component.html',
  styleUrls: ['./edit-trip.component.css']
})
export class EditTripComponent implements OnInit {
  zoneData;
  zoneId = 0;
  tripId = '';
  SelectedDriver = "";
  SelectedStartLocation = "sl1";
  SelectedEndLocation = "el1";
  salesmanData;
  salesmanId = 0;
  noOfMonth = 18;
  customerSearch = '';
  customerAddressSearch = '';
  orderNo = '0';
  orderData;
  orderSelectedData;
  orderDetailDataList = [];
  tripOrderList = [];
  drivers;
  locations;
  formGroup;
  submitData;
  StartLocation = "1";
  EndLocation = "1";
  currentName;
  tStartdate;
  driverId = 1;
  isAllSelected = false;
  isaleastAddreeSeleted = false;
  keyvalueSelectCustomer = [];
  dict;
  dictCustomerPriority;
  stopNo = 0;
  isDriverSelected = false;
  public origin: any = '56 Elliott Pl, Rutherford, NJ 07070, USA';
  public destination: any = '56 Elliott Pl, Rutherford, NJ 07070, USA';
  customerWaypointList = [];
  waypoints = [];
  markerOptions: any;
  waypointColorOption = [];
  public vehicleCapacity: string = "0";
  public modelselected: boolean = false;
  public totalStops: string = "0";
  public weight: string = "0";
  public totalCost: string = "0";
  public p: number = 1;
  public empList: Array<String> = [];
  //public detailList: Array<String> = [];
  ordrs: Array<String> = [];
  public temp: Object = false;
  public Trnum: string;
  public num: number;
  public num2: number;
  public cfg: any;
  public collection = { count: 60, data: [] };


  public collectionOrder = { count: 60, data: [] };
  public collectionTripOrder = { count: 60, data: [] };
  public searchString: string;
  public checkCount = 0;
  constructor(private http: HttpClient, private formBuilder: FormBuilder, private tripServise: TripService, private cdr: ChangeDetectorRef, private SpinnerService: NgxSpinnerService, private router: Router, private toster:ToastrService) {

  }

  ngOnInit(): void {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = today.getFullYear();

    //today = mm + '/' + dd + '/' + yyyy;
    //this.tStartdate = mm + '/' + dd + '/' + yyyy;
    this.tStartdate = yyyy + '-' + mm + '-' + dd;
    //document.write(today);
    this.loadZoneData();
    this.loadSalesmanData();
    this.loadDriverData();
    this.loadLocationData();

    this.dict = new Dictionary();
    this.dictCustomerPriority = new Dictionary();
  }

  setStartLocation(value: string) {
    this.StartLocation = value.substr(2, 1);
    // console.log(this.StartLocation);
    //alert(this.StartLocation);
  }
  setEndLocation(value: string) {
    this.EndLocation = value.substr(2, 1);;
    // console.log(this.StartLocation);w
    //alert(this.EndLocation);

  }
  ngAfterViewInit(): void {
    this.cdr.detectChanges();
  }
  setDriver(event) {
    //alert(event.target.value);
    //console.log(value);
    var id = event.target.value.substr(1, 1);
    if (Number(id) === 0) {
      this.isDriverSelected = false;
    }
    else {
      this.isDriverSelected = true;
    }
    //alert(id);
    this.driverId = Number(id);
    //alert(this.driverId);
    var drv = this.drivers.find(x => x.driverId === Number(id));
    //alert(drv.vehicleCapacity);
    this.vehicleCapacity = drv.vehicleCapacity;
    //this.totalStops = event.target.value;
    //this.weight = event.target.value;
    //this.totalCost = event.target.value;
  }


  OnChangePriority(event, customerId) {
    let orderpr = this.orderData.find(x => x.customerId === customerId);
    let onderIndex = this.orderData.indexOf(orderpr);
    let orderDetailList = orderpr.orders;
    let orderDetailUpdateList = [];
    let orderDetail = null;
    let orderDetailIndex = -1;
    orderpr.stopCount = event.target.value;
    for (var j = 0; j < orderDetailList.length; j++) {
      //this.orderDetailDataList.push(this.orderData[i].orders[j]);
      orderDetail = this.tripOrderList.find(x => x.poNumber === orderDetailList[j].poNumber);
      orderDetailIndex = this.tripOrderList.indexOf(orderDetail);
      if (orderDetail) {
        orderDetail.priority = Number(event.target.value);
        this.tripOrderList[orderDetailIndex] = orderDetail;
      }
      /*  else{
         this.tripOrderList.push(orderDetailList[j]);
       } */
      //orderDetailUpdateList.push(orderDetail);
      if (orderDetailIndex > 0) {
        orderDetailUpdateList.push(orderDetail);
      }
      else {
        orderDetailUpdateList.push(orderDetailList[j]);
      }
    }
    orderpr.orders = orderDetailUpdateList;
    this.orderData[onderIndex] = orderpr;
    //alert(event.target.value);
  }

  childCheck(parentObj, childObj) {
    parentObj.isSelected = childObj.every(function (itemChild: any) {
      return itemChild.isSelected == true;
    })
  }
  parentCheck(parentObj) {
    for (var i = 0; i < parentObj.childList.length; i++) {
      parentObj.childList[i].isSelected = parentObj.isSelected;
    }
  }
  //------------------------trip api--------------
  loadZoneData() {
    this.SpinnerService.show();
    this.tripServise.getZone().subscribe(
      (result: any) => {
        //alert(JSON.parse(result["zones"]));
        this.zoneData = result["zones"];
        this.SpinnerService.hide();

      }

    );
  }

  loadSalesmanData() {
    this.SpinnerService.show();
    this.tripServise.getSalesman().subscribe(
      (result: any) => {
        //alert(JSON.parse(result["zones"]));
        this.salesmanData = result["salesmans"];
        this.SpinnerService.hide();
      }
    );
  }

  loadDriverData() {
    this.SpinnerService.show();
    this.tripServise.getDriver().subscribe(
      (result: any) => {
        //alert(JSON.parse(result["zones"]));
        this.drivers = result["drivers"];
        this.SpinnerService.hide();
        this.loadOrderData();
      }
    );
  }

  loadLocationData() {
    this.SpinnerService.show();
    this.tripServise.getLocation().subscribe(
      (result: any) => {
        //alert(JSON.parse(result["zones"]));
        this.locations = result["locations"];
        console.log(this.locations);
        this.renderMap();
        this.SpinnerService.hide();
      }
    );
  }

  loadOrderData() {
    this.tripId = window.localStorage.getItem("tripId");
    //alert(this.tripId);
    this.SpinnerService.show();
    this.tripServise.GetAllOrderByTrip(this.tripId).subscribe(
      (result: any) => {
        //alert(JSON.parse(result["zones"]));
        this.orderData = result["tripOrders"];

        //this.collection.count = this.orderData.length;
        console.log(this.orderData);
        this.temp = true;
        //this.num = Math.floor((Math.random() * 100000) + 1);
        if (this.orderData) {
          //alert(this.orderData[0].tripName);
          this.Trnum = this.orderData[0].tripName;
          this.driverId = Number(this.orderData[0].driverID);
          this.SelectedDriver = "d" + this.orderData[0].driverID;
          this.SelectedStartLocation = "sl" + this.orderData[0].startLocation;
          this.SelectedEndLocation = "el" + this.orderData[0].endLocation;
          this.tStartdate = this.orderData[0].startTime;
          if (this.drivers) {
            var drv = this.drivers.find(x => x.driverId === Number(this.orderData[0].driverID));
            //alert(drv.vehicleCapacity);
            this.vehicleCapacity = drv.vehicleCapacity;
          }
          this.isDriverSelected = true;

          this.StartLocation = this.orderData[0].startLocation;
          this.EndLocation = this.orderData[0].endLocation;

          //this.weight = (parseFloat(this.orderData.TotalGrossWeight)).toString();
          //this.totalCost = (parseFloat(this.orderData.TotalBillAmount)).toString();
        }
        this.num2 = Math.floor((Math.random() * 1000) + 2);
        this.weight = "0";
        this.totalCost = "0";
        this.totalStops = "0";
        this.isaleastAddreeSeleted = true;
        let stopcountNo = 1;
        let precstId = 0;
        let waypointcstId = 0;
        for (var i = 0; i < this.orderData.length; i++) {
          //this.collectionOrder.count = this.orderData[i].orders.length

          for (var j = 0; j < this.orderData[i].orders.length; j++) {
            this.orderDetailDataList.push(this.orderData[i].orders[j]);
            if (this.orderData[i].tripID != "0") {
              if (this.orderData[i].orders[j].isSelected) {
                this.tripOrderList.push(this.orderData[i].orders[j]);
                this.weight = (parseFloat(this.weight) + parseFloat(this.orderData[i].orders[j].grossWeight)).toFixed(2).toString();
                this.totalCost = (parseFloat(this.totalCost) + parseFloat(this.orderData[i].orders[j].billAmount)).toFixed(2).toString();

                if (precstId != this.orderData[i].customerId) {
                  this.totalStops = String(Number(this.totalStops) + 1);
                  this.customerWaypointList.push({ location: this.orderData[i].address, stopover: false, waypointstopno: this.orderData[i].stopCount });
                  precstId = this.orderData[i].customerId;
                  this.dictCustomerPriority.set(this.orderData[i].customerId.toString(), Number(this.totalStops));
                  this.dict.set(this.orderData[i].customerId.toString(), 1);
                }

                stopcountNo = this.dict.get(this.orderData[i].customerId.toString());
                if (Number(stopcountNo) === -1 || isNaN(Number(stopcountNo))) {
                  this.dict.set(this.orderData[i].customerId.toString(), 1);
                }
                else {
                  this.dict.set(this.orderData[i].customerId.toString(), stopcountNo + 1);
                }

              }
            }
          }

        }
        //this.orderData = this.collection.data
        console.log(this.orderData);
        this.SpinnerService.hide();
        //this.loadOrderSelectedData();
        //this.onOptimizeRoute();
        this.isaleastAddreeSeleted = true;
        this.onOptimizeRoute();
      }
    );
  }

  loadOrderSelectedData() {
    this.tripId = window.localStorage.getItem("tripId");
    //alert(this.tripId);
    this.SpinnerService.show();
    this.tripServise.getTrip(this.tripId).subscribe(
      (result: any) => {
        //alert(JSON.parse(result["zones"]));
        this.orderSelectedData = result["tripOrders"];
        if (this.orderSelectedData) {
          this.Trnum = this.orderSelectedData[0].tripName;
          this.driverId = this.orderSelectedData[0].driverID;
          this.SelectedDriver = "d" + this.orderSelectedData[0].driverID;
          this.SelectedStartLocation = "sl" + this.orderSelectedData[0].startLocation;
          this.SelectedEndLocation = "el" + this.orderSelectedData[0].endLocation;
          this.StartLocation = this.orderSelectedData[0].startLocation;
          this.EndLocation = this.orderSelectedData[0].endLocation;
          this.tStartdate = this.orderSelectedData[0].startTime;
          var drv = this.drivers.find(x => x.driverId === Number(this.orderSelectedData[0].driverID));
          this.vehicleCapacity = drv.vehicleCapacity;
          this.isDriverSelected = true;
        }
        this.weight = "0";
        this.totalCost = "0";
        this.totalStops = this.orderSelectedData.length;

        for (var i = 0; i < this.orderSelectedData.length; i++) {
          //this.collectionOrder.count = this.orderData[i].orders.length
          this.customerWaypointList.push(this.orderSelectedData[i]);
          for (var j = 0; j < this.orderSelectedData[i].orders.length; j++) {
            //this.orderDetailDataList.push(this.orderSelectedData[i].orders[j]);
            this.weight = (parseFloat(this.weight) + parseFloat(this.orderSelectedData[i].orders[j].grossWeight)).toString();
            this.totalCost = (parseFloat(this.totalCost) + parseFloat(this.orderSelectedData[i].orders[j].billAmount)).toString();
          }
        }
        //this.orderData = this.collection.data
        console.log(this.orderSelectedData);
        this.SpinnerService.hide();
        this.isaleastAddreeSeleted = true;
        this.onOptimizeRoute();
      }
    );
  }

  renderMap() {
    var originlocation = this.locations.find(x => x.id === Number(this.StartLocation));
    var destinationlocation = this.locations.find(x => x.id === Number(this.EndLocation));
    this.origin = originlocation.locationAddress;
    this.destination = destinationlocation.locationAddress;
  }

  //------------------------end trip--------------

  onChangeSalesman(event) {
    this.salesmanId = event.target.value.substr(1, 1);
    this.SpinnerService.show();
    this.tripServise.getOrder(this.zoneId, this.salesmanId, this.noOfMonth, this.customerSearch, this.customerAddressSearch, this.orderNo).subscribe(
      (result: any) => {
        this.orderData = result["tripOrders"];
        this.temp = true;
        this.num = Math.floor((Math.random() * 100000) + 1);
        //this.Trnum = 'Tr ' + this.num + '000';
        this.num2 = Math.floor((Math.random() * 1000) + 2);
        this.orderDetailDataList = [];
        for (var i = 0; i < this.orderData.length; i++) {
          for (var j = 0; j < this.orderData[i].orders.length; j++) {
            this.orderDetailDataList.push(this.orderData[i].orders[j]);
          }

        }
        console.log(this.orderData);
        this.SpinnerService.hide();
      }
    );
  }

  onChangeZone(event) {
    this.SpinnerService.show();
    this.zoneId = event.target.value.substr(1, 1);
    this.tripServise.getOrder(this.zoneId, this.salesmanId, this.noOfMonth, this.customerSearch, this.customerAddressSearch, this.orderNo).subscribe(
      (result: any) => {
        this.orderData = result["tripOrders"];
        this.temp = true;
        this.num = Math.floor((Math.random() * 100000) + 1);
        //this.Trnum = 'Tr ' + this.num + '000';
        this.num2 = Math.floor((Math.random() * 1000) + 2);
        this.orderDetailDataList = [];
        for (var i = 0; i < this.orderData.length; i++) {
          for (var j = 0; j < this.orderData[i].orders.length; j++) {
            this.orderDetailDataList.push(this.orderData[i].orders[j]);
          }

        }
        console.log(this.orderData);
        this.SpinnerService.hide();
      }
    );
  }

  onEditTrip() {
    debugger;
    if (!this.isDriverSelected) {
      //alert("Please select driver");
      this.toster.warning("Please select driver", "Warning");
    }
    else if (!this.isaleastAddreeSeleted) {
      //alert("Please select atleast one order");
      this.toster.warning("Please select atleast one order", "Warning");
    }
    else {
      var tripDetailData = [];
      var detailList = [];
      var tripdetaildata = null;
      this.collectionTripOrder.count = this.tripOrderList.length;
      let prePriority = 0;
      let isDuplicatePriority = false;
      for (var i = 0; i < this.collectionTripOrder.count; i++) {
        //this.tripOrderList
        tripdetaildata = new TripDetailModel()
        tripdetaildata.TripID = Number(this.tripId)
        tripdetaildata.TripDetailID = i;
        tripdetaildata.OrderNo = this.tripOrderList[i].poNumber;
        tripdetaildata.StartLocation = Number(this.StartLocation);
        tripdetaildata.EndLocation = Number(this.EndLocation);
        tripdetaildata.CompanyID = 2;
        tripdetaildata.ToDoComments = "test todo coments";
        tripdetaildata.IsStopSummeryDone = false;
        tripdetaildata.IsToDo = false;
        tripdetaildata.IsCustomerSkipped = false;
        tripdetaildata.DeliveryCharges = this.tripOrderList[i].deliveryCharges;
        tripdetaildata.CustomerNote = this.tripOrderList[i].customerNote;
        if (prePriority === this.tripOrderList[i].priority) {
          isDuplicatePriority = true;
        }
        tripdetaildata.Priority = this.tripOrderList[i].priority;
        detailList.push(tripdetaildata);
      }
      console.log(detailList);

      var tripdata = new TripModel()
      tripdata.TripID = Number(this.tripId);
      tripdata.CompanyID = "2";
      tripdata.TripName = this.Trnum;
      tripdata.DriverID = Number(this.driverId);
      tripdata.WareHouse = "01";
      tripdata.Charges = 12.00;
      tripdata.Priority = 1;
      tripdata.SortTrip = 1;
      tripdata.VehicleID = 1;
      tripdata.TripStatus = 1;
      tripdata.TripDesc = "Route_Trip_1_Desc"
      tripdata.IsActive = true;
      tripdata.IsDeleted = false;
      tripdata.TripDetails = detailList;
      tripdata.ActionMode = 1;
      tripdata.StartLocation = Number(this.StartLocation);
      tripdata.EndLocation = Number(this.EndLocation);
      tripdata.TripCreated = this.tStartdate;

      this.SpinnerService.show();
      //--Uncomment for save Trip
      this.tripServise.putTrip(tripdata).subscribe(
        (result: any) => {
          //alert(JSON.parse(result["zones"]));
          //this.salesmanData = result["salesmans"];  
          //alert(result["message"]);
          this.toster.success(result["message"], "Success");
          this.SpinnerService.hide();
          this.router.navigate(['all-trip']);
        }
      );
    }
  }

  onDiscardChanges(event) {
    this.orderData.forEach((item) => {
      item.isSelected = false;
      item.stopCount = 0;
      item.orders.forEach((iteminner) => {
        iteminner.isSelected = false;
        this.isaleastAddreeSeleted = false;
      });
    });
    this.ResetTripSWT();
    this.customerWaypointList = [];
    this.waypoints = [];
    this.waypointColorOption = [];
  }

  onOptimizeRoute() {
    //alert(this.StartLocation);
    //alert(this.EndLocation);
    this.SpinnerService.show();
    var originlocation = this.locations.find(x => x.id === Number(this.StartLocation));
    var endlocation = this.locations.find(x => x.id === Number(this.EndLocation));
    console.log(originlocation);
    if (this.isaleastAddreeSeleted) {
      this.origin = originlocation.locationAddress;
      //this.orderData = this.orderData.sort((a, b) => parseFloat(b.stopCount) - parseFloat(a.stopCount));

      //console.log(this.orderData);
      this.waypoints = [];
      this.waypointColorOption = [];
      // for (var i = 0; i < this.orderData.length; i++) {
      //   if(this.orderData[i].stopCount !=0 && this.orderData[i].tripID !="0"){
      //     this.waypoints.push({ location: this.orderData[i].address, stopover: false });
      //     //this.waypointColorOption.push({ label: { color: 'white', text: (i+2).toString() } });
      //     this.waypointColorOption.push({ label: { color: 'white', text: (i+1).toString() }, infoWindow: this.orderData[i].address, });

      //   }

      // }
      // for (var i = 0; i < this.tripOrderList.length; i++) {
      //   this.waypoints.push({ location: this.orderData[i].address, stopover: false });
      //   this.waypointColorOption.push({ label: { color: 'white', text: (i+1).toString() }, infoWindow: this.orderData[i].address, });
      // }
      for (var i = 0; i < this.customerWaypointList.length; i++) {
        //alert(this.customerWaypointList[i].location);
        this.waypoints.push({ location: this.customerWaypointList[i].location, stopover: false });
        this.waypointColorOption.push({ label: { color: 'white', text: (i + 1).toString() }, infoWindow: this.customerWaypointList[i].location });
      }
      console.log(this.waypoints);
      // for (var i = 0; i < this.customerWaypointList.length; i++) {
      //   // if (i === this.customerWaypointList.length - 1) {
      //   //   this.destination = this.customerWaypointList[i].location;
      //   //   console.log(this.destination);
      //   // }
      //   // else {
      //   //   this.waypoints.push(this.customerWaypointList[i]);
      //   //   this.waypointColorOption.push({ label: { color: 'white', text: (i+2).toString() } });
      //   // }

      //   this.waypoints.push(this.customerWaypointList[i]);
      //     //this.waypointColorOption.push({ label: { color: 'white', text: (i+2).toString() } });
      //   this.waypointColorOption.push({ label: { color: 'white', text: (i+1).toString() }, infoWindow: this.customerWaypointList[i].location, });

      // }
      this.destination = endlocation.locationAddress;
    }
    else {
      //alert("Please select atleast one order");
      this.toster.warning("Please select atleast one order", "Warning");
    }
    //this.markerOptions = { origin: { label: { color: 'blue', text: '1'} }, destination: { label: { color: 'white', text: (this.waypointColorOption.length + 2).toString() } }, waypoints: this.waypointColorOption };
    this.markerOptions = { origin: { label: { color: 'white' } }, destination: { label: { color: 'white' } }, waypoints: this.waypointColorOption };
    //this.waypoints = this.customerWaypointList;
    //console.log(this.origin);
    this.SpinnerService.hide();

  }

  onOptimizeRouteCreateTrip() {
    this.onOptimizeRoute();
    this.onEditTrip();
  }

  updateChk(event, customerId) {
    //this.empList.push(chk);
    this.SpinnerService.show();
    let orderdt = this.orderDetailDataList.find(x => x.poNumber === event.target.value);
    let orderpr = this.orderData.find(x => x.customerId === customerId);
    //alert()
    let onderIndex = this.orderData.indexOf(orderpr);
    let updateorderData = this.orderData.filter(item => item !== orderpr);
    //alert(onderIndex);

    //let index = this.orderData.indexOf(orderpr);

    //this.orderData[index] = orderpr;

    this.stopNo = 0;
    if (event.target.checked) {
      //this.keyvalueSelectCustomer.push({ "key": customerId, "value": Number(this.totalStops) + 1 })

      this.stopNo = Number(this.dict.get(customerId.toString()));
      if (this.stopNo === -1 || isNaN(this.stopNo)) {
        this.dict.set(customerId.toString(), 1);
        orderpr.isSelected = true;
        orderpr.stopCount = 1;
        var waypoint = new WaypointModel();
        waypoint.location = orderpr.address;
        waypoint.stopover = true;
        //this.customerWaypointList.push({ location: orderpr.address, stopover: true });
        this.isaleastAddreeSeleted = true;
      }
      else {
        this.dict.set(customerId.toString(), this.stopNo + 1);
        orderpr.isSelected = true;

      }

      if (Number(this.dict.get(customerId.toString())) === 1) {
        this.totalStops = String(Number(this.totalStops) + 1);
        orderpr.stopCount = this.totalStops;
        var waypoint = new WaypointModel();
        waypoint.location = orderpr.address;
        waypoint.stopover = true;
        //this.customerWaypointList.push({ location: orderpr.address, stopover: false });
        this.customerWaypointList.push({ location: orderpr.address, stopover: false, waypointstopno: orderpr.stopCount });
      }
      //this.totalStops = String(Number(this.totalStops) + 1);
      let prtNo = Number(this.dictCustomerPriority.get(customerId.toString()));
      if (prtNo === -1 || isNaN(prtNo)) {
        orderdt.priority = Number(this.totalStops);
      }
      else {
        orderdt.priority = Number(prtNo);
      }

      this.tripOrderList.push(orderdt);
      //alert(event.target.value);
      //console.log(this.orderDetailDataList);
      //console.log(orderdt);
      //console.log(this.tripOrderList);  
      this.weight = (parseFloat(this.weight) + parseFloat(orderdt.grossWeight)).toFixed(2).toString();
      this.totalCost = (parseFloat(this.totalCost) + parseFloat(orderdt.billAmount)).toFixed(2).toString();
      //this.modelthis.orderDetailDataListe;
    }
    else {
      this.stopNo = Number(this.dict.get(customerId.toString()));
      this.dict.set(customerId.toString(), this.stopNo - 1);
      if (Number(this.dict.get(customerId.toString())) === 0) {
        this.totalStops = String(Number(this.totalStops) - 1);
        orderpr.isSelected = false;
        orderpr.stopCount = 0;
        let customerWaypoint = this.customerWaypointList.find(x => x.location === orderpr.address);
        if (customerWaypoint) {
          this.customerWaypointList = this.customerWaypointList.filter(item => item !== customerWaypoint);
        }

        //this.isaleastAddreeSeleted = false;
      }
      console.log("waypoint are" + this.customerWaypointList);
      //this.removeOrdr(orderdt,event.target.value);
      this.tripOrderList = this.tripOrderList.filter(item => item !== orderdt);


      this.weight = (parseFloat(this.weight) - parseFloat(orderdt.grossWeight)).toFixed(2).toString();
      this.totalCost = (parseFloat(this.totalCost) - parseFloat(orderdt.billAmount)).toFixed(2).toString();
      //this.modelselected = false;
      //this.orderData = this.orderData.filter(item => item !== orderpr);
      //this.orderData.push(orderpr);
    }
    //chkArray.push(chk);
    //alert(chk);
    //updateorderData.push(orderpr);
    //this.orderData=[];
    //this.orderData = updateorderData;
    //let updateItem = this.orderData.items.find(this.findIndexToUpdate, newItem.id);

    this.orderData[onderIndex] = orderpr;
    //console.log(this.orderData);
    //console.log(this.customerWaypointList); 
    //alert(this.totalStops);
    //console.log(this.empList);
    this.SpinnerService.hide();
  }

  CheckAllOptions(event) {
    //alert('hi');
    if (event.target.checked) {
      let stopCount = 0;
      this.orderData.forEach((item) => {
        item.isSelected = true;
        item.stopCount = stopCount + 1;
        stopCount++;
        item.orders.forEach((iteminner) => {
          iteminner.isSelected = true;
          //this.CaculateTripSWT(item.customerId,iteminner);
          this.calculateCheck(true, iteminner.poNumber, item.customerId);
          this.isaleastAddreeSeleted = true;
        });
      });
    }
    else {
      this.orderData.forEach((item) => {
        item.isSelected = false;
        item.stopCount = 0;
        item.orders.forEach((iteminner) => {
          iteminner.isSelected = false;
          this.isaleastAddreeSeleted = false;
          this.calculateCheck(false, iteminner.poNumber, item.customerId);
        });
      });
      this.ResetTripSWT();
    }
    // if(this.isAllSelected){
    // this.isAllSelected =false;
    // }
    // else{
    // this.isAllSelected = false;
    // }
  }

  ResetTripSWT() {
    this.totalStops = "0";
    this.weight = "0";
    this.totalCost = "0"
  }
  CaculateTripSWT(customerId, iteminner) {
    //var orderdt = this.orderDetailDataList.find(x => x.poNumber === event.target.value);
    //var orderpr = this.orderData.find(x => x.customerId === customerId);
    if (iteminner.isSelected) {
      this.stopNo = Number(this.dict.get(customerId.toString()));
      if (this.stopNo === -1 || isNaN(this.stopNo)) {
        this.dict.set(customerId.toString(), 1);
      }
      else {
        this.dict.set(customerId.toString(), this.stopNo + 1);
      }
      if (Number(this.dict.get(customerId.toString())) === 1) {
        this.totalStops = String(Number(this.totalStops) + 1);
      }
      this.tripOrderList = this.tripOrderList.filter(item => item !== iteminner);
      this.tripOrderList.push(iteminner);
      this.weight = (parseFloat(this.weight) + parseFloat(iteminner.grossWeight)).toString();
      this.totalCost = (parseFloat(this.totalCost) + parseFloat(iteminner.billAmount)).toString();
    }
    else {
      this.stopNo = Number(this.dict.get(customerId.toString()));
      this.dict.set(customerId.toString(), this.stopNo - 1);
      if (Number(this.dict.get(customerId.toString())) === 0) {
        this.totalStops = String(Number(this.totalStops) - 1);
      }
      this.tripOrderList = this.tripOrderList.filter(item => item !== iteminner);
      console.log(this.tripOrderList);
      this.weight = (parseFloat(this.weight) - parseFloat(iteminner.grossWeight)).toString();
      this.totalCost = (parseFloat(this.totalCost) - parseFloat(iteminner.billAmount)).toString();
    }

  }
  removeOrdr(orderdt, orderNo) {
    this.tripOrderList = this.tripOrderList.filter(item => item !== orderdt);
    console.log(this.tripOrderList);
  }
  checkCustomerOrder(event, customerId) {
    debugger;
    var orderpr = this.orderData.find(x => x.customerId === customerId);

    if (event.target.checked) {
      orderpr.stopCount = (this.totalStops == "0" ? "1" : Number(this.totalStops) + 1);
      orderpr.orders.forEach((iteminner) => {
        iteminner.isSelected = true;
        //this.CaculateTripSWT(customerId,iteminner);
        this.calculateCheck(true, iteminner.poNumber, customerId);
      });
    }
    else {
      orderpr.stopCount = 0;
      orderpr.orders.forEach((iteminner) => {
        iteminner.isSelected = false;
        //this.CaculateTripSWT(customerId,iteminner);
        this.calculateCheck(true, iteminner.poNumber, customerId);
      });
    }

  }

  toggleEditable(event) {
    if (event.target.checked) {
      //this.contentEditable = true;
    }
  }
  calculateCheck(isCheck, orderno, customerId) {
    //this.empList.push(chk);
    let orderdt = this.orderDetailDataList.find(x => x.poNumber === orderno);
    let orderpr = this.orderData.find(x => x.customerId === customerId);
    //alert()
    let onderIndex = this.orderData.indexOf(orderpr);
    //let updateorderData  = this.orderData.filter(item => item !== orderpr);
    //alert(onderIndex);

    //let index = this.orderData.indexOf(orderpr);

    //this.orderData[index] = orderpr;

    this.stopNo = 0;
    if (isCheck) {
      //this.keyvalueSelectCustomer.push({ "key": customerId, "value": Number(this.totalStops) + 1 })
      this.checkCount++;
      this.stopNo = Number(this.dict.get(customerId.toString()));
      if (this.stopNo === -1 || isNaN(this.stopNo)) {
        this.dict.set(customerId.toString(), 1);
        orderpr.isSelected = true;
        orderpr.stopCount = 1;
        // var waypoint = new WaypointModel();
        // waypoint.location = orderpr.address;
        // waypoint.stopover = true;
        //this.customerWaypointList.push({ location: orderpr.address, stopover: true });
        this.isaleastAddreeSeleted = true;
      }
      else {
        this.dict.set(customerId.toString(), this.stopNo + 1);
        orderpr.isSelected = true;

      }

      if (Number(this.dict.get(customerId.toString())) === 1) {
        this.totalStops = String(Number(this.totalStops) + 1);
        let sameorderpr = this.orderData.find(x => x.stopCount === this.totalStops);
        //alert()
        if (sameorderpr) {
          let smaonderIndex = this.orderData.indexOf(sameorderpr);
          sameorderpr.stopCount = String(Number(this.totalStops) - 1);
          this.orderData[smaonderIndex] = sameorderpr;
          let samewaypoint = this.customerWaypointList.find(x => x.waypointstopno === this.totalStops);
          if (samewaypoint) {
            let waypointndex = this.customerWaypointList.indexOf(samewaypoint);
            samewaypoint.waypointstopno = sameorderpr.stopCount;
            this.customerWaypointList[waypointndex] = samewaypoint;
          }
        }
        orderpr.stopCount = this.totalStops;
        // var waypoint = new WaypointModel();
        // waypoint.location = orderpr.address;
        // waypoint.stopover = true;

        this.customerWaypointList.push({ location: orderpr.address, stopover: false, waypointstopno: orderpr.stopCount });
      }
      //this.totalStops = String(Number(this.totalStops) + 1);
      orderdt.priority = Number(this.totalStops);
      this.tripOrderList.push(orderdt);
      //alert(event.target.value);
      //console.log(this.orderDetailDataList);
      //console.log(orderdt);
      //console.log(this.tripOrderList);  
      this.weight = (parseFloat(this.weight) + parseFloat(orderdt.grossWeight)).toFixed(2).toString();
      this.totalCost = (parseFloat(this.totalCost) + parseFloat(orderdt.billAmount)).toFixed(2).toString();
      //this.modelthis.orderDetailDataListe;
    }
    else {
      this.checkCount--;
      this.stopNo = Number(this.dict.get(customerId.toString()));
      this.dict.set(customerId.toString(), this.stopNo - 1);
      if (Number(this.dict.get(customerId.toString())) === 0) {
        this.totalStops = String(Number(this.totalStops) - 1);
        orderpr.isSelected = false;
        orderpr.stopCount = 0;
        let customerWaypoint = this.customerWaypointList.find(x => x.location === orderpr.address);
        if (customerWaypoint) {
          this.customerWaypointList = this.customerWaypointList.filter(item => item !== customerWaypoint);
        }

        //this.isaleastAddreeSeleted = false;
      }
      //console.log( "waypoint are" + this.customerWaypointList);
      //this.removeOrdr(orderdt,event.target.value);
      this.tripOrderList = this.tripOrderList.filter(item => item !== orderdt);

      if (this.checkCount === 0) {
        this.weight = "0";
        this.totalCost = "0.00";
      }
      else {
        this.weight = (parseFloat(this.weight) - parseFloat(orderdt.grossWeight)).toFixed(2).toString();
        this.totalCost = (parseFloat(this.totalCost) - parseFloat(orderdt.billAmount)).toFixed(2).toString();
      }
      //this.modelselected = false;
      //this.orderData = this.orderData.filter(item => item !== orderpr);
      //this.orderData.push(orderpr);
    }
    //chkArray.push(chk);
    //alert(chk);
    //updateorderData.push(orderpr);
    //this.orderData=[];
    //this.orderData = updateorderData;
    //let updateItem = this.orderData.items.find(this.findIndexToUpdate, newItem.id);

    this.orderData[onderIndex] = orderpr;
    //console.log(this.orderData);
    console.log(this.customerWaypointList);
    //alert(this.totalStops);
    //console.log(this.empList);
  }

  OnChangeDelevaryCharge(event, customerId) {
    let orderpr = this.orderData.find(x => x.customerId === customerId);
    let onderIndex = this.orderData.indexOf(orderpr);
    let orderDetailList = orderpr.orders;
    //orderpr.stopCount = event.target.value;
    let orderDetailUpdateList = [];
    let orderDetail = null;
    let orderDetailIndex = -1;
    // orderpr.stopCount = event.target.value;
    for (var j = 0; j < orderDetailList.length; j++) {
      //this.orderDetailDataList.push(this.orderData[i].orders[j]);
      orderDetail = this.tripOrderList.find(x => x.poNumber === orderDetailList[j].poNumber);
      orderDetailIndex = this.tripOrderList.indexOf(orderDetail);
      if (orderDetail) {
        orderDetail.deliveryCharges = Number(event.target.value);
        this.tripOrderList[orderDetailIndex] = orderDetail;
      }
      else {
        this.tripOrderList.push(orderDetailList[j]);
      }
      orderDetailUpdateList.push(orderDetail);
    }
    orderpr.orders = orderDetailUpdateList;
    this.orderData[onderIndex] = orderpr;
  }

  OnChangeCustomerNote(event, customerId) {
    let orderpr = this.orderData.find(x => x.customerId === customerId);
    let onderIndex = this.orderData.indexOf(orderpr);
    let orderDetailList = orderpr.orders;
    //orderpr.stopCount = event.target.value;
    let orderDetailUpdateList = [];
    let orderDetail = null;
    let orderDetailIndex = -1;
    // orderpr.stopCount = event.target.value;
    for (var j = 0; j < orderDetailList.length; j++) {
      //this.orderDetailDataList.push(this.orderData[i].orders[j]);
      orderDetail = this.tripOrderList.find(x => x.poNumber === orderDetailList[j].poNumber);
      orderDetailIndex = this.tripOrderList.indexOf(orderDetail);
      if (orderDetail) {
        orderDetail.customerNote = event.target.value;
        this.tripOrderList[orderDetailIndex] = orderDetail;
      }
      else {
        this.tripOrderList.push(orderDetailList[j]);
      }
      orderDetailUpdateList.push(orderDetail);
    }
    orderpr.orders = orderDetailUpdateList;
    this.orderData[onderIndex] = orderpr;
  }
}
