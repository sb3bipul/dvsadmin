import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner"; 
import { DashboardService } from '../services/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private http: HttpClient, private formBuilder: FormBuilder,private dashboardServise: DashboardService, private SpinnerService: NgxSpinnerService ) { 
     this.loadDashboardInfoData();
  }
  // constructor(){}

  TotalCustomer =25000;
  TotalOrder = 690;
  TotalTrip = 342230;
  TotalDriver = 2345;
  companyId =2;
  DashboardItem;

  ngOnInit() {
  }

  loadDashboardInfoData() {   
    this.SpinnerService.show(); 
    //delay(20000);
    this.companyId = 2;
    
    this.dashboardServise.getDashboardInfo(this.companyId).subscribe(    
      (result: any) => {    
        //alert(JSON.parse(result["zones"]));
        
        this.DashboardItem = result["dashboardItem"];  
        console.log(this.DashboardItem);
        this.TotalCustomer =this.DashboardItem.totalCustomer;
        this.TotalOrder = this.DashboardItem.totalOrder;
        this.TotalTrip = this.DashboardItem.totalTrip;
        this.TotalDriver =this.DashboardItem.totalDriver;
        
        this.SpinnerService.hide(); 
        //alert(this.tripInfoData);  
        }
        //this.orderData = this.collection.data
        
       
    );    
  }
  getDashboardInfo
}
