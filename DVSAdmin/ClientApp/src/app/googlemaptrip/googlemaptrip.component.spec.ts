import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GooglemaptripComponent } from './googlemaptrip.component';

describe('GooglemaptripComponent', () => {
  let component: GooglemaptripComponent;
  let fixture: ComponentFixture<GooglemaptripComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GooglemaptripComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GooglemaptripComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
