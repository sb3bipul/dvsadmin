import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { VehicletypeService } from '../services/vehicletype.service';

@Component({
  selector: 'app-add-vehicletype',
  templateUrl: './add-vehicletype.component.html',
  styleUrls: ['./add-vehicletype.component.css']
})
export class AddVehicletypeComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,private router: Router, private vehicletypeService: VehicletypeService) { 

  }
  addVehicleTypeForm: FormGroup;
  ngOnInit() {
    this.addVehicleTypeForm = this.formBuilder.group({
      id: [0],
      TypeName: ['', Validators.required],
      TypeDescription: ['', Validators.required],
      IsActive: [true, Validators.required],
      ActionMode: [0, Validators.required],
    });
  }
  get addVehicleTypeFormControl() { return this.addVehicleTypeForm.controls; }

  onSubmit(){
   //alert("hi");
    this.vehicletypeService.postVehicleType(this.addVehicleTypeForm.value)
      .subscribe( data => {
        this.router.navigate(['vehicletype']);
      });
  }

}
