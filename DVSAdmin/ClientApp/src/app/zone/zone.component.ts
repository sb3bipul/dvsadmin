import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import {Router} from "@angular/router";
import { ZoneService } from '../services/zone.service';


@Component({
  selector: 'app-zone',
  templateUrl: './zone.component.html',
  styleUrls: ['./zone.component.css']
})
export class ZoneComponent implements OnInit {
  zoneInfoData=[];
  zoneData=[];
  StartDate = '';
  ZoneStatus = '';
  SearchText = '';
  public p: number = 1;
 constructor(private zoneServise: ZoneService,private SpinnerService: NgxSpinnerService,private router: Router) { 
  let today = new Date();
  let dd = String(today.getDate()).padStart(2, '0');
  let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  let yyyy = today.getFullYear();

  this.StartDate = yyyy + '-' + mm + '-' + dd;
  
  this.loadZoneInfoData();
 }

  ngOnInit() {
  }

  loadZoneInfoData() {   
    this.SpinnerService.show(); 
    //delay(20000);
    this.zoneServise.getZoneInfo().subscribe(    
      (result: any) => {    
        //alert(JSON.parse(result["zones"]));
        
        this.zoneData = result["zones"];  
        for (var i = 0; i < this.zoneData.length; i++) {
          this.zoneInfoData.push(this.zoneData[i]);
          
        }
        console.log(this.zoneInfoData);
        this.SpinnerService.hide(); 
        //alert(this.tripInfoData);  
        }
        //this.orderData = this.collection.data
        
       
    );    
  }
  onAddZone(){
    this.router.navigate(['../add-zone']);
  }
  onEditZone(zoneId){
    //alert(tripId);
    window.localStorage.removeItem("zoneId");
    window.localStorage.setItem("zoneId", zoneId.toString());
    this.router.navigate(['../edit-zone']);

  }
  onTaggleActive(zoneId){
    var zoneToggle = this.zoneInfoData.find(x => x.id === Number(zoneId));
    //alert(zoneToggle.isActive);
    zoneToggle.isActive =!zoneToggle.isActive
    //alert(zoneToggle.isActive);
    console.log(zoneToggle);
    this.zoneServise.putZoneStatus(zoneToggle).subscribe(    
      (result: any) => {    
        alert(result["message"]);
        
        // this.zoneData = result["zones"];  
        // for (var i = 0; i < this.zoneData.length; i++) {
        //   this.zoneInfoData.push(this.zoneData[i]);
          
        // }
        // console.log(this.zoneInfoData);
        // this.SpinnerService.hide(); 
        //alert(this.tripInfoData);  
        }
        //this.orderData = this.collection.data
        
       
    );  
  }
  onDeleteZone(zoneId){
    //alert(zoneId);
    var zoneDelete = this.zoneInfoData.find(x => x.id === Number(zoneId));
    this.zoneServise.deleteZone(zoneDelete).subscribe(    
      (result: any) => {    
        alert(result["message"]); 
        this.zoneInfoData = this.zoneInfoData.filter(item => item !== zoneDelete);
        console.log(this.zoneInfoData);
        }
    );  
  }
}
