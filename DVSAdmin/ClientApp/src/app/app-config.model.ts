export interface IAppConfig {
    environment: {
        name: string;
        production: boolean;
    };
    appInsights: {
        instrumentationKey: string;
    };
    logging: {
        console: boolean;
        appInsights: boolean;
    };
    api: {
        url: string;
    };
    buildInfo: {
        number: string;
        createdOn: string;
    };
}