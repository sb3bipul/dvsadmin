import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditviewTripComponent } from './editview-trip.component';

describe('EditviewTripComponent', () => {
  let component: EditviewTripComponent;
  let fixture: ComponentFixture<EditviewTripComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditviewTripComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditviewTripComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
