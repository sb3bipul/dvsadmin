import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GooglemapploatComponent } from './googlemapploat.component';

describe('GooglemapploatComponent', () => {
  let component: GooglemapploatComponent;
  let fixture: ComponentFixture<GooglemapploatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GooglemapploatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GooglemapploatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
