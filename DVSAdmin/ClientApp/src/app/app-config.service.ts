import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IAppConfig } from './app-config.model';

@Injectable()
export class AppConfigService {
  static appConfig: IAppConfig;

  constructor(private http: HttpClient) { }

  load() {
    let jsonFile = '/assets/config/appconfig.json';
    return this.http.get(jsonFile)
      .toPromise()
      .then((response: IAppConfig)=> {AppConfigService.appConfig = <IAppConfig>response;})
      .catch((err: any)=> {
        console.log(`Could not load file '${jsonFile}': ${JSON.stringify(err)}`);    
      });
  }
}