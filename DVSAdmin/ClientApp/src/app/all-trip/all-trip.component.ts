import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { delay } from 'rxjs/operators';
import { TripService } from '../services/trip.service';
import { Router } from "@angular/router";
import { ModalService } from '../services/modal.service';
import { TripsummeryService } from '../services/tripsummery.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-all-trip',
  templateUrl: './all-trip.component.html',
  styleUrls: ['./all-trip.component.css']
})
export class AllTripComponent implements OnInit {
  tripInfoData = [];
  tripData = [];
  StartDate = '';
  TripStatus = '';
  SearchText = '';
  public p: number = 1;
  deliveryTripSummery = [];
  summeryView = [];
  Invoice_Number: string; Action: string; modelid: string; status: string; Inv_Number: string;
  constructor(private tripServise: TripService, private SpinnerService: NgxSpinnerService, private router: Router, private modalService: ModalService, private tripSummeryServise: TripsummeryService, private toastr: ToastrService) {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = today.getFullYear();

    //today = mm + '/' + dd + '/' + yyyy;
    //this.tStartdate = mm + '/' + dd + '/' + yyyy;
    this.StartDate = yyyy + '-' + mm + '-' + dd;

    this.loadTripInfoData();
  }

  ngOnInit() {
    // let today = new Date();
    // let dd = String(today.getDate()).padStart(2, '0');
    // let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    // let yyyy = today.getFullYear();

    // //today = mm + '/' + dd + '/' + yyyy;
    // //this.tStartdate = mm + '/' + dd + '/' + yyyy;
    // this.StartDate = yyyy + '-' + mm + '-' + dd;

    
  }

  loadTripInfoData() {
    this.SpinnerService.show();
    //delay(20000);
    this.tripServise.getTripInfo(this.StartDate, this.TripStatus, this.SearchText).subscribe(
      (result: any) => {
        //alert(JSON.parse(result["zones"]));

        this.tripInfoData = result["tripInfo"];
        for (var i = 0; i < this.tripData.length; i++) {
          this.tripInfoData.push(this.tripData[i]);

        }
        console.log(this.tripInfoData);
        this.SpinnerService.hide();
        //alert(this.tripInfoData);  
      }
      //this.orderData = this.collection.data


    );
  }

  onEditTrip(tripId) {
    //alert(tripId);
    window.localStorage.removeItem("tripId");
    window.localStorage.setItem("tripId", tripId.toString());
    this.router.navigate(['../editview-trip']);

  }
  onViewSummery(tripId) {
    debugger;
    this.SpinnerService.show();
    //delay(20000);
    this.tripSummeryServise.getTripDeliverySummery(tripId).subscribe(
      (result: any) => {
        //alert(JSON.parse(result["zones"]));

        this.deliveryTripSummery = result["tripSummaries"];

        console.log(this.deliveryTripSummery);
        this.openModal('custom-modal-1', this.deliveryTripSummery);
        this.SpinnerService.hide();
        //alert(this.tripInfoData);  
      }
      //this.orderData = this.collection.data


    );

  }

  openModal(id: string, deliveryTripSummery: any[]) {
    debugger
    this.modalService.open(id);
    this.summeryView = deliveryTripSummery;
    //console.log(this.summeryView);
    this.modelid = id;
  }

  onCloseModel(id: string) {
    this.modalService.close(id);
  }

}
