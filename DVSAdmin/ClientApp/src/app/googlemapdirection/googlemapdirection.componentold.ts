import { GoogleMapsAPIWrapper } from '@agm/core';
import { Component, OnInit, SimpleChanges } from '@angular/core';
import { ILatLng } from './directions-map.directive';

@Component({
  selector: 'app-googlemapdirection',
  templateUrl: './googlemapdirection.component.html',
  styleUrls: ['./googlemapdirection.component.css']
})
export class GooglemapdirectionComponentold implements OnInit {
public lat = 24.799448;
public lng = 120.979021;
 
// public origin: any;
// public destination: any;
public origin: any = '234 61TH STREET, New Jersey, WEST NEWYORK, 07093';
public destination: any = '201 BROAD AVE, New Jersey, FAIRVIEW, 07022';
 
  constructor() { }

  ngOnInit() {
    //this.getDirection();
  }

  getDirection() {
    // this.origin = { lat: 24.799448, lng: 120.979021 };
    // this.destination = { lat: 24.799504, lng: 120.975007 };
    // this.destination = { lat: 24.799524, lng: 120.975017 };
   
    //Location within a string
    //this.origin = '153 EAST 43RD STREET , New York City, NEW YORK, 10017,234 61TH STREET, New Jersey, WEST NEWYORK, 07093';
    //this.destination = '201 BROAD AVE, New Jersey, FAIRVIEW, 07022';
  }
  // origin: '234 61TH STREET, New Jersey, WEST NEWYORK, 07093',
  // destination: '201 BROAD AVE, New Jersey, FAIRVIEW, 07022',
  public waypoints: Array<any> = [
    { location: '153 EAST 43RD STREET , New York City, NEW YORK, 10017', stopover: true },
    //{ location: '201 BROAD AVE, New Jersey, FAIRVIEW, 07022', stopover: true },
  ];

    // Hide origin polylines
    public renderOptions = { suppressPolylines: true };

    // Custom polylines
    public polylines: Array<google.maps.Polyline> = [];

     // Current map
  public map: google.maps.Map;

    public onMapReady(event: google.maps.Map): void {
      this.map = event;
    }
    public onResponse(event: google.maps.DirectionsResult): void {
      // Default style
      const polylineOptions: google.maps.PolygonOptions = {
        strokeWeight: 6,
        strokeOpacity: 0.55,
      };
      // Polylines strokeColor
      const colors = ['#0000FF', '#FF0000', '#0000FF'];
      // Clear exist polylines
      this.polylines.forEach(polyline => polyline.setMap(null));
      const { legs } = event.routes[0];
      legs.forEach((leg, index) => {
        leg.steps.forEach(step => {
          const nextSegment = step.path;
          const stepPolyline = new google.maps.Polyline(polylineOptions);
          // Custom color
          stepPolyline.setOptions({ strokeColor: colors[index] });
          nextSegment.forEach(next => stepPolyline.getPath().push(next));
          this.polylines.push(stepPolyline);
          stepPolyline.setMap(this.map);
        });
      });
    }



//   public dirs: Array<any> = [{
//     origin: '234 61TH STREET, New Jersey, WEST NEWYORK, 07093',
//     destination: '201 BROAD AVE, New Jersey, FAIRVIEW, 07022',
    
//     waypoints:[
//       {
//         location: '153 EAST 43RD STREET , New York City, NEW YORK, 10017',
//       },
//       {
//         location: '44-SOUTH STREET, New Jersey, FREEHOLD, 07728',
//       }
//     ],
//     optimizeWaypoints: true,
//     // mapTypeId: google.maps.MapTypeId.ROADMAP,
//     renderOptions: { polylineOptions: { strokeColor: '#f00' } },
//     markerOptions:{
//       origin: {
//         icon: 'https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=1|FE6256|#f00',
//         draggable: true,
//     },
//     destination: {
//       icon: 'https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=4|33B5FF|#f00',
//       draggable: true,
//   },
//   waypoints: {
//       icon: 'https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=3|FE6256|#f00',
//   },
     
//     }
//   }, 
//   // {
//   //   origin: '153 EAST 43RD STREET , New York City, NEW YORK, 10017',
//   //   destination: '201 BROAD AVE, New Jersey, FAIRVIEW, 07022	',
//   //   renderOptions: { polylineOptions: { strokeColor: '#f00' , strokeNumber :1} },
//   // }
// ];
  
// Washington, DC, USA
// origin: ILatLng = {
//   latitude: 38.889931,
//   longitude: -77.009003
// };
// // New York City, NY, USA
// destination: ILatLng = {
//   latitude: 40.730610,
//   longitude: -73.935242
// };
// displayDirections = true;
// zoom = 14;
// showDirection = true;

}
