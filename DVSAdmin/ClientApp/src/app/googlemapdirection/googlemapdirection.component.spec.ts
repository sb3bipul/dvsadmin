import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GooglemapdirectionComponent } from './googlemapdirection.component';

describe('GooglemapdirectionComponent', () => {
  let component: GooglemapdirectionComponent;
  let fixture: ComponentFixture<GooglemapdirectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GooglemapdirectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GooglemapdirectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
