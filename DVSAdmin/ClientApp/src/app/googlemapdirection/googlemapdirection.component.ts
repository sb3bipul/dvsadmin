import { GoogleMapsAPIWrapper } from '@agm/core';
import { Component, OnInit, SimpleChanges, Input } from '@angular/core';
//import { ILatLng } from './directions-map.directive';

@Component({
  selector: 'app-googlemapdirection',
  templateUrl: './googlemapdirection.component.html',
  styleUrls: ['./googlemapdirection.component.css']
})
export class GooglemapdirectionComponent implements OnInit {

    constructor() { }

  ngOnInit() {
    //this.getDirection();
  }
    public lat = 24.799448;
    public lng = 120.979021;
    @Input() origin: any;
    @Input() destination: any;
    @Input() waypoints: any;
    @Input() markerOptions: any
    //public origin: any = '234 61TH STREET, New Jersey, WEST NEWYORK, 07093';
    //public destination: any = '201 BROAD AVE, New Jersey, FAIRVIEW, 07022';
  
    // public waypoints: Array<any> = [
    //   { location: '153 EAST 43RD STREET , New York City, NEW YORK, 10017', stopover: true },
    // ];
  
    // Hide origin polylines
    public renderOptions = { suppressPolylines: false, polylineOptions: { strokeColor: '#FF0000' }};
  
    // Custom polylines
    public polylines: Array<google.maps.Polyline> = [];
    public suppressMarkers = true;
  
    // Current map
    public map: google.maps.Map;
  
    // Save GoogleMap instance
    public onMapReady(event: google.maps.Map): void {
      this.map = event;
    }
    //this.markerOptions = { origin: { label: { color: 'white', text: '1' } }, destination: { label: { color: 'white', text: '4' } }, waypoints: [ { label: { color: 'white', text: '2' } }, { label: { color: 'white', text: '3' } } ] };
    public onResponse(event: google.maps.DirectionsResult): void {
  
      // Default style
      const polylineOptions: google.maps.PolygonOptions = {
        strokeWeight: 20,
      };
  
      // Polylines strokeColor
      const colors = ['#0000FF', '#FF0000', '#0000FF'];
  
      // Clear exist polylines
      this.polylines.forEach(polyline => polyline.setMap(null));
  
      const { legs } = event.routes[0];
  
      legs.forEach((leg, index) => {
  
        leg.steps.forEach(step => {
          const nextSegment = step.path;
          const stepPolyline = new google.maps.Polyline(polylineOptions);
  
          // Custom color
          stepPolyline.setOptions({ strokeColor: colors[index] });
  
          nextSegment.forEach(next => stepPolyline.getPath().push(next));
  
          this.polylines.push(stepPolyline);
          stepPolyline.setMap(this.map);
        });
      });
    }

}
