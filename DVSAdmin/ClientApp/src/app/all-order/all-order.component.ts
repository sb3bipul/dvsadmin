import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { OrderService } from '../services/order.service';

@Component({
  selector: 'app-all-order',
  templateUrl: './all-order.component.html',
  styleUrls: ['./all-order.component.css']
})
export class AllOrderComponent implements OnInit {
  orderInfoData=[];
  orderData=[];
  StartDate = '';
  OrderStatus = '';
  SearchText = '';
  public p: number = 1;
  constructor(private orderServise: OrderService,private SpinnerService: NgxSpinnerService,private router: Router) { 
    
    this.loadOrderInfoData();
   }
  
    ngOnInit() {
    }
  
    loadOrderInfoData() {   
      this.SpinnerService.show(); 
      //delay(20000);
      this.SearchText = "";
      this.orderServise.getOrderInfo(this.SearchText).subscribe(    
        (result: any) => {    
          //alert(JSON.parse(result["zones"]));
          
          this.orderData = result["orderInfo"];  
          for (var i = 0; i < this.orderData.length; i++) {
            this.orderInfoData.push(this.orderData[i]);
            
          }
          console.log(this.orderInfoData);
          this.SpinnerService.hide(); 
          //alert(this.tripInfoData);  
          }
          //this.orderData = this.collection.data
          
         
      );    
    }
  
    onViewOrder(orderId){
      //alert(orderId);
      window.localStorage.removeItem("orderId");
      window.localStorage.setItem("orderId", orderId.toString());
      this.router.navigate(['../view-orderdetail']);
  
    }
    onDeleteOrder(orderId){
      //alert(orderId);
      window.localStorage.removeItem("orderId");
      window.localStorage.setItem("orderId", orderId.toString());
      this.router.navigate(['../view-orderdetail']);
  
    }

}
