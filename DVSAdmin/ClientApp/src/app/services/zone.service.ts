import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ZoneService {

  apiBaseApiUrl = environment.baseUrl;
  constructor(private http: HttpClient) {    
  } 

  getZoneInfo() {    
    var keyvalue = []
    this.getQueryParam("Id", 0, keyvalue);
    this.getQueryParam("SearchText", "", keyvalue);
    this.getQueryParam("PageNo", 1, keyvalue);
    this.getQueryParam("PageSize", 0, keyvalue);
    this.getQueryParam("SortBy", "ZoneName", keyvalue);
    this.getQueryParam("SortOrder", "asc", keyvalue);
    let querparam = ""
    keyvalue.forEach(function (elem, i) {
      if (i == 0) {
        querparam = "?" + elem.key + "=" + elem.value
      } else {
        querparam = querparam + "&" + elem.key + "=" + elem.value
      }
    });
  
    var geturl = this.apiBaseApiUrl + 'api/zones' + querparam;
    //alert(geturl);
    return this.http.get(geturl).pipe(map(result => result));    
  }

  getZoneInfoById(Id) {    
    var keyvalue = []
    this.getQueryParam("Id", Id, keyvalue);
    // this.getQueryParam("SearchText", "", keyvalue);
    // this.getQueryParam("PageNo", 1, keyvalue);
    // this.getQueryParam("PageSize", 0, keyvalue);
    // this.getQueryParam("SortBy", "ZoneName", keyvalue);
    // this.getQueryParam("SortOrder", "asc", keyvalue);
    let querparam = ""
    keyvalue.forEach(function (elem, i) {
      if (i == 0) {
        querparam = "?" + elem.key + "=" + elem.value
      } else {
        querparam = querparam + "&" + elem.key + "=" + elem.value
      }
    });
  
    var geturl = this.apiBaseApiUrl + 'api/zonedetail' + querparam;
    //alert(geturl);
    return this.http.get(geturl).pipe(map(result => result));    
  }

  getQueryParam(key, value, keyvalue) {
    keyvalue.push({ "key": key, "value": value });
  } 

  postZone(zonedata) {    
    var posturl = this.apiBaseApiUrl + 'api/savezone';
    //console.log(JSON.stringify(tripdata));
    return this.http.post(posturl, zonedata).pipe(map(response => {
      return response;
    })); 
    
  }
  putZoneStatus(zonedata) {    
    var posturl = this.apiBaseApiUrl + 'api/updatezone';
    //console.log(JSON.stringify(zonedata));
    return this.http.post(posturl, zonedata).pipe(map(response => {
      return response;
    })); 
    
  }
  putZone(zonedata) {    
    var posturl = this.apiBaseApiUrl + 'api/updatezone';
    //console.log(JSON.stringify(zonedata));
    return this.http.post(posturl, zonedata).pipe(map(response => {
      return response;
    })); 
    
  }

  deleteZone(zonedata) {    
    var posturl = this.apiBaseApiUrl + 'api/removezone';
    console.log(JSON.stringify(zonedata));
    //alert("Delete no " + Id);
    return this.http.post(posturl, zonedata).pipe(map(response => {
      return response;
    })); 
    
  }

  removeZone(Id) {    
    var posturl = this.apiBaseApiUrl + 'api/removezone';
    //console.log(JSON.stringify(zonedata));
    alert("Delete no " + Id);
    return this.http.delete(posturl, Id).pipe(map(response => {
      return response;
    })); 
    
  }

}
