import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { User } from '../models/user';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  apiBaseApiUrl = environment.baseUrl;
  userData = new BehaviorSubject<User>(new User());
  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  get isLoggedIn() : boolean{
    //return this.loggedIn.asObservable();
    //alert(localStorage.getItem('authToken'))
    return (localStorage.getItem('authToken') !== null);  
  }

  constructor(private http: HttpClient, private router: Router) { }

  login(userDetails) {
    //alert(userDetails['UserName']);
    //alert(userDetails['Password']);
    //alert(userDetails['isLoggedIn']);
    //var loginUrl = `${AppConfigService.appConfig.api.url}` + this.loginRoute + '/login';
    //return this.http.post<any>('/api/login', userDetails)
    //var loginUrl='https://localhost:44389/' + 'api/login';
    var loginUrl= this.apiBaseApiUrl + 'api/login';
    //alert(loginUrl);
    return this.http.post<any>(loginUrl, userDetails).pipe(map(response => {
        localStorage.setItem('authToken', response.token);
        this.setUserDetails();
        return response;
      }));
  }

  setUserDetails() {
    if (localStorage.getItem('authToken')) {
      const userDetails = new User();
      const decodeUserDetails = JSON.parse(window.atob(localStorage.getItem('authToken').split('.')[1]));
      //alert(decodeUserDetails);
      userDetails.UserName = decodeUserDetails.sub;
      //userDetails.Password = decodeUserDetails.fullName;
      userDetails.isLoggedIn = true;

      this.userData.next(userDetails);
      //this.loggedIn.next(true);
    }
  }

  logout() {
    localStorage.removeItem('authToken');
    // this.router.navigate(['/login']);
    var logoutUrl='http://localhost:4200/';
    //alert(logoutUrl);
    this.userData.next(new User());
    //localStorage.clear();
    //this.userData = null;
    //this.loggedIn.next(false);
    //this.router.navigate([logoutUrl]);
    this.router.navigate(['/login']);
    
  }
}    
