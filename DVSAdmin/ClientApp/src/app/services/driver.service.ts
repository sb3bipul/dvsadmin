import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DriverService {

  apiBaseApiUrl = environment.baseUrl;
  constructor(private http: HttpClient) {    
  } 

  getDriverInfo() {    
    var keyvalue = []
    this.getQueryParam("Id", 0, keyvalue);
    this.getQueryParam("SearchText", "", keyvalue);
    this.getQueryParam("PageNo", 1, keyvalue);
    this.getQueryParam("PageSize", 0, keyvalue);
    this.getQueryParam("SortBy", "Name", keyvalue);
    this.getQueryParam("SortOrder", "asc", keyvalue);
    let querparam = ""
    keyvalue.forEach(function (elem, i) {
      if (i == 0) {
        querparam = "?" + elem.key + "=" + elem.value
      } else {
        querparam = querparam + "&" + elem.key + "=" + elem.value
      }
    });
  
    var geturl = this.apiBaseApiUrl + 'api/drivers' + querparam;
    //alert(geturl);
    return this.http.get(geturl).pipe(map(result => result));    
  }

  getDriverInfoByID(Id:number) {    
    var keyvalue = []
    this.getQueryParam("Id", Id, keyvalue);
    let querparam = ""
    keyvalue.forEach(function (elem, i) {
      if (i == 0) {
        querparam = "?" + elem.key + "=" + elem.value
      } else {
        querparam = querparam + "&" + elem.key + "=" + elem.value
      }
    });
  
    var geturl = this.apiBaseApiUrl + 'api/driverdetail' + querparam;
    console.log("geturl: " + geturl);
    return this.http.get(geturl).pipe(map(result => result));    
  }
  createDriverInfo(driverdata) {    

    var posturl = this.apiBaseApiUrl + 'api/saveDriver';
    console.log("posturl: "  + posturl + " , JSON: " + JSON.stringify(driverdata));
    return this.http.post(posturl, JSON.stringify(driverdata)).pipe(map(response => {
      return response;
    })); 
  }

  createNewDriverUserLogin(driverUserdata) {    

    var posturl = 'http://107.21.119.157/DVSmobile/api/Account/CreateNewUser';
    console.log("posturl: "  + posturl + " , JSON: " + JSON.stringify(driverUserdata));
    return this.http.post(posturl, JSON.stringify(driverUserdata)).pipe(map(response => {
      return response;
    })); 
  }

  updateDriverInfo(driverdata) {    

    var posturl = this.apiBaseApiUrl + 'api/updateDriver';
    console.log("posturl: "  + posturl + " , JSON: " + JSON.stringify(driverdata));
    return this.http.put(posturl, JSON.stringify(driverdata)).pipe(map(response => {
      return response;
    })); 
  }

  deleteDriverInfo(driverdata) {    

    var posturl = this.apiBaseApiUrl + 'api/removeDriver';
    console.log("posturl: "  + posturl + " , JSON: " + JSON.stringify(driverdata));
    return this.http.post(posturl, JSON.stringify(driverdata)).pipe(map(response => {
      return response;
    })); 
    
  }
  
  // crudDriverInfo(actionMode:number, driverName:string, driverAddress:string, city:string, state:string, zip:string, country:string, driverEmail:string, driverContactNo:string, drivingLicense:string, driverLoginID:string, isActive:boolean, warehouse:string, driverImage:string ) {    
  //   var keyvalue = []
  //   this.getQueryParam("ActionMode", actionMode, keyvalue);
  //   this.getQueryParam("DriverName", driverName, keyvalue);
  //   this.getQueryParam("DriverAddress", driverAddress, keyvalue);
  //   this.getQueryParam("City", city, keyvalue);
  //   this.getQueryParam("State", state, keyvalue);
  //   this.getQueryParam("Zip", zip, keyvalue);
  //   this.getQueryParam("Country", country, keyvalue);
  //   this.getQueryParam("DriverEmail", driverEmail, keyvalue);
  //   this.getQueryParam("DriverContactNo", driverContactNo, keyvalue);
  //   this.getQueryParam("DrivingLicense", drivingLicense, keyvalue);
  //   this.getQueryParam("DriverLoginID", driverLoginID, keyvalue);
  //   this.getQueryParam("IsActive", isActive, keyvalue);
  //   this.getQueryParam("Warehouse", warehouse, keyvalue);
  //   this.getQueryParam("DriverImage", driverImage, keyvalue);
  //   this.getQueryParam("Message", "", keyvalue);
  //   let querparam = ""
  //   keyvalue.forEach(function (elem, i) {
  //     if (i == 0) {
  //       querparam = "?" + elem.key + "=" + elem.value
  //     } else {
  //       querparam = querparam + "&" + elem.key + "=" + elem.value
  //     }
  //   });
  
  //   var geturl = this.apiBaseApiUrl + 'api/saveDriver' + querparam;
  //   //alert(geturl);
  //   return this.http.get(geturl).pipe(map(result => result));    
  // }

  getQueryParam(key, value, keyvalue) {
    keyvalue.push({ "key": key, "value": value });
  } 

  getVehicleInfo() {    
    var geturl = this.apiBaseApiUrl + 'api/vehicles?id=2';
    //alert(geturl);
    return this.http.get(geturl).pipe(map(result => result));    
  }

}

