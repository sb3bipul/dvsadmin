import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DvssettingService {

  apiBaseApiUrl = environment.baseUrl;
  constructor(private http: HttpClient) {    
  } 

  getLocation() {    
    var keyvalue = []
    this.getQueryParam("Id", 0, keyvalue);
    this.getQueryParam("SearchText", "", keyvalue);
    this.getQueryParam("PageNo", 1, keyvalue);
    this.getQueryParam("PageSize", 0, keyvalue);
    this.getQueryParam("SortBy", "LocationName", keyvalue);
    this.getQueryParam("SortOrder", "asc", keyvalue);
    let querparam = ""
    keyvalue.forEach(function (elem, i) {
      if (i == 0) {
        querparam = "?" + elem.key + "=" + elem.value
      } else {
        querparam = querparam + "&" + elem.key + "=" + elem.value
      }
    });
    var geturl = this.apiBaseApiUrl + 'api/locations' + querparam;
    //alert(geturl);
   return this.http.get(geturl).pipe(map(result => result));    
  }



  getQueryParam(key, value, keyvalue) {
    keyvalue.push({ "key": key, "value": value });
  } 

  updateSettingInfo(driverdata) {    

    var posturl = this.apiBaseApiUrl + 'api/updatesetting';
    console.log("posturl: "  + posturl + " , JSON: " + JSON.stringify(driverdata));
    return this.http.post(posturl, JSON.stringify(driverdata)).pipe(map(response => {
      return response;
    })); 
  }

  postLocation(locationdata) {    
    var posturl = this.apiBaseApiUrl + 'api/savelocation';
    console.log(JSON.stringify(locationdata));
    return this.http.post(posturl, JSON.stringify(locationdata)).pipe(map(response => {
      return response;
    })); 
    
  }
  
  putLocation(locationdata) {    
    var posturl = this.apiBaseApiUrl + 'api/updatelocation';
    console.log(JSON.stringify(locationdata));
    return this.http.put(posturl, JSON.stringify(locationdata)).pipe(map(response => {
      return response;
    })); 
    
  }
  getLocationById(Id) {    
    var keyvalue = []
    this.getQueryParam("Id", Id, keyvalue);
    this.getQueryParam("SearchText", "", keyvalue);
    this.getQueryParam("PageNo", 1, keyvalue);
    this.getQueryParam("PageSize", 0, keyvalue);
    this.getQueryParam("SortBy", "LocationName", keyvalue);
    this.getQueryParam("SortOrder", "asc", keyvalue);
    let querparam = ""
    keyvalue.forEach(function (elem, i) {
      if (i == 0) {
        querparam = "?" + elem.key + "=" + elem.value
      } else {
        querparam = querparam + "&" + elem.key + "=" + elem.value
      }
    });
    var geturl = this.apiBaseApiUrl + 'api/locations' + querparam;
    //alert(geturl);
   return this.http.get(geturl).pipe(map(result => result));    
  }
}
