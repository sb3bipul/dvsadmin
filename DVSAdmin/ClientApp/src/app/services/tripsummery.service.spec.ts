import { TestBed } from '@angular/core/testing';

import { TripsummeryService } from './tripsummery.service';

describe('TripsummeryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TripsummeryService = TestBed.get(TripsummeryService);
    expect(service).toBeTruthy();
  });
});
