import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  apiBaseApiUrl = environment.baseUrl;
  constructor(private http: HttpClient) {    
  } 

  getVehicleInfo(SearchText) {    
    var keyvalue = []
    this.getQueryParam("Id", 0, keyvalue);
    this.getQueryParam("SearchText", SearchText, keyvalue);
    this.getQueryParam("PageNo", 1, keyvalue);
    this.getQueryParam("PageSize", 0, keyvalue);
    this.getQueryParam("SortBy", "VehicleName", keyvalue);
    this.getQueryParam("SortOrder", "asc", keyvalue);
    let querparam = ""
    keyvalue.forEach(function (elem, i) {
      if (i == 0) {
        querparam = "?" + elem.key + "=" + elem.value
      } else {
        querparam = querparam + "&" + elem.key + "=" + elem.value
      }
    });
    var geturl = this.apiBaseApiUrl + 'api/vehicles' + querparam;
    //alert(geturl);
   return this.http.get(geturl).pipe(map(result => result));    
  }

  getVehicleTypeInfo() {    
    var keyvalue = []
    this.getQueryParam("Id", 0, keyvalue);
    this.getQueryParam("SearchText", '', keyvalue);
    this.getQueryParam("PageNo", 1, keyvalue);
    this.getQueryParam("PageSize", 0, keyvalue);
    this.getQueryParam("SortBy", "TypeName", keyvalue);
    this.getQueryParam("SortOrder", "asc", keyvalue);
    let querparam = ""
    keyvalue.forEach(function (elem, i) {
      if (i == 0) {
        querparam = "?" + elem.key + "=" + elem.value
      } else {
        querparam = querparam + "&" + elem.key + "=" + elem.value
      }
    });
    var geturl = this.apiBaseApiUrl + 'api/vehiclestypes' + querparam;
    //alert(geturl);
   return this.http.get(geturl).pipe(map(result => result));    
  }

  getVehicleDetail(vehicleId) {    
    var keyvalue = []
    this.getQueryParam("Id", vehicleId, keyvalue);
    this.getQueryParam("SearchText", "", keyvalue);
    this.getQueryParam("PageNo", 1, keyvalue);
    this.getQueryParam("PageSize", 0, keyvalue);
    this.getQueryParam("SortBy", "VehicleName", keyvalue);
    this.getQueryParam("SortOrder", "asc", keyvalue);
    let querparam = ""
    keyvalue.forEach(function (elem, i) {
      if (i == 0) {
        querparam = "?" + elem.key + "=" + elem.value
      } else {
        querparam = querparam + "&" + elem.key + "=" + elem.value
      }
    });
    var geturl = this.apiBaseApiUrl + 'api/vehicles' + querparam;
    //alert(geturl);
   return this.http.get(geturl).pipe(map(result => result));    
  }

  createVehicleInfo(vehicleData) {    

    var posturl = this.apiBaseApiUrl + 'api/savevehicle';
    console.log("posturl: "  + posturl + " , JSON: " + JSON.stringify(vehicleData));
    return this.http.post(posturl, JSON.stringify(vehicleData)).pipe(map(response => {
      return response;
    })); 
  }

  updateVehicleInfo(vehicleData) {    

    var posturl = this.apiBaseApiUrl + 'api/updatevehicle';
    console.log("posturl: "  + posturl + " , JSON: " + JSON.stringify(vehicleData));
    return this.http.post(posturl, JSON.stringify(vehicleData)).pipe(map(response => {
      return response;
    })); 
  }

  deleteVehicleInfo(vehicleData) {    

    var posturl = this.apiBaseApiUrl + 'api/removevehicle';
    console.log("posturl: "  + posturl + " , JSON: " + JSON.stringify(vehicleData));
    return this.http.post(posturl, JSON.stringify(vehicleData)).pipe(map(response => {
      return response;
    })); 
    
  }
  
  getQueryParam(key, value, keyvalue) {
    //if (value) {
      //alert(value);
      keyvalue.push({ "key": key, "value": value })
    //}
  } 

}
