import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DVSInvoiceService {
  apiBaseApiUrl = environment.baseUrl;
  constructor(private http: HttpClient) { }

  getQueryParam(key, value, keyvalue) {
    keyvalue.push({ "key": key, "value": value });
  }

  getDVSInvoiceListForAdmin(DriverId: string, InvoiceNo: string, Status: string, FromDate: string, ToDate: string) {
    var keyvalue = []
    this.getQueryParam("DriverId", DriverId, keyvalue);
    this.getQueryParam("InvoiceNo", InvoiceNo, keyvalue);
    this.getQueryParam("Status", Status, keyvalue);
    this.getQueryParam("fromdate", FromDate, keyvalue);
    this.getQueryParam("toDate", ToDate, keyvalue);

    let querparam = ""
    keyvalue.forEach(function (elem, i) {
      if (i == 0) {
        querparam = "?" + elem.key + "=" + elem.value
      } else {
        querparam = querparam + "&" + elem.key + "=" + elem.value
      }
    });

    var geturl = this.apiBaseApiUrl + 'api/DVSInvoiceList' + querparam;
    return this.http.get(geturl).pipe(map(result => result));
  }

  insertInvoiceStatusLog(statuslog) {
    var posturl = this.apiBaseApiUrl + 'api/InsertInvoiceStatusHistory';
    console.log("posturl: " + posturl + " , JSON: " + JSON.stringify(statuslog));
    return this.http.post(posturl, JSON.stringify(statuslog)).pipe(map(response => {
      return response;
    }));
  }

  editInvoiceInfo(editInvoice) {
    var posturl = this.apiBaseApiUrl + 'api/UpdateInvoiceInfo';
    console.log("posturl: " + posturl + " , JSON: " + JSON.stringify(editInvoice));
    return this.http.post(posturl, JSON.stringify(editInvoice)).pipe(map(response => {
      return response;
    }));
  }

  getStatusLogByInvoiceNo(InvoiceNo: string) {
    var keyvalue = []
    this.getQueryParam("InvoiceNo", InvoiceNo, keyvalue);
    let querparam = ""
    keyvalue.forEach(function (elem, i) {
      if (i == 0) {
        querparam = "?" + elem.key + "=" + elem.value
      } else {
        querparam = querparam + "&" + elem.key + "=" + elem.value
      }
    });

    var geturl = this.apiBaseApiUrl + 'api/GetInvoiceStatusChangeHistory' + querparam;
    return this.http.get(geturl).pipe(map(result => result));
  }
}
