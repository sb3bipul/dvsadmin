import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class VehicletypeService {

  apiBaseApiUrl = environment.baseUrl;
  constructor(private http: HttpClient) {
  }

  getVehicalTypeInfo() {
    var keyvalue = []
    this.getQueryParam("Id", 0, keyvalue);
    this.getQueryParam("SearchText", "", keyvalue);
    this.getQueryParam("PageNo", 1, keyvalue);
    this.getQueryParam("PageSize", 0, keyvalue);
    this.getQueryParam("SortBy", "TypeName", keyvalue);
    this.getQueryParam("SortOrder", "asc", keyvalue);
    let querparam = ""
    keyvalue.forEach(function (elem, i) {
      if (i == 0) {
        querparam = "?" + elem.key + "=" + elem.value
      } else {
        querparam = querparam + "&" + elem.key + "=" + elem.value
      }
    });

    var geturl = this.apiBaseApiUrl + 'api/vehiclestypes' + querparam;
    //alert(geturl);
    return this.http.get(geturl).pipe(map(result => result));
  }

  getZoneInfoById(Id) {    
    var keyvalue = []
    this.getQueryParam("Id", Id, keyvalue);
    let querparam = ""
    keyvalue.forEach(function (elem, i) {
      if (i == 0) {
        querparam = "?" + elem.key + "=" + elem.value
      } else {
        querparam = querparam + "&" + elem.key + "=" + elem.value
      }
    });
  
    var geturl = this.apiBaseApiUrl + 'api/vehiclestypedetail' + querparam;
    //alert(geturl);
    return this.http.get(geturl).pipe(map(result => result));    
  }

  getQueryParam(key, value, keyvalue) {
    keyvalue.push({ "key": key, "value": value });
  }

  postVehicleType(vehicleTypedata) {
    var posturl = this.apiBaseApiUrl + 'api/savevehicletype';
    //console.log(JSON.stringify(tripdata));
    return this.http.post(posturl, vehicleTypedata).pipe(map(response => {
      return response;
    }));
  }

  putVehicleTypeStatus(vehicleTypedata) {    
    var posturl = this.apiBaseApiUrl + 'api/updatevehicletype';
    var data = {"TypeID":vehicleTypedata.typeID,"TypeName":vehicleTypedata.typeName,"TypeDescription":vehicleTypedata.typeDescription,"IsActive":vehicleTypedata.isActive,"ActionMode":1};
   console.log(data);
    return this.http.post(posturl, data).pipe(map(response => {
      return response;
    })); 
    
  }

  deleteVehicleType(vehicleTypeDelete) {    
    var posturl = this.apiBaseApiUrl + 'api/removevehicletype';
    console.log(JSON.stringify(vehicleTypeDelete));
    return this.http.post(posturl, vehicleTypeDelete).pipe(map(response => {
      return response;
    })); 
    
  }
  putVehicleType(vehicleTypedata) {    
    var posturl = this.apiBaseApiUrl + 'api/updatevehicletype';
    //console.log(JSON.stringify(zonedata));
    return this.http.post(posturl, vehicleTypedata).pipe(map(response => {
      return response;
    })); 
    
  }
}
