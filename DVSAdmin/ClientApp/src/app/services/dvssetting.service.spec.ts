import { TestBed } from '@angular/core/testing';

import { DvssettingService } from './dvssetting.service';

describe('DvssettingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DvssettingService = TestBed.get(DvssettingService);
    expect(service).toBeTruthy();
  });
});
