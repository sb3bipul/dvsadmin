import { TestBed } from '@angular/core/testing';

import { DvsinvoiceService } from './dvsinvoice.service';

describe('DvsinvoiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DvsinvoiceService = TestBed.get(DvsinvoiceService);
    expect(service).toBeTruthy();
  });
});
