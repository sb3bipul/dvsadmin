import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class TripService {
  //apiBaseApiUrl = 'https://localhost:44389/'; 
  apiBaseApiUrl = environment.baseUrl;
  constructor(private http: HttpClient) {    
  } 

  getZone() {    
    var loginUrl='https://localhost:44389/' + 'api/login';
    var keyvalue = []
    this.getQueryParam("Id", 0, keyvalue);
    this.getQueryParam("SearchText", "", keyvalue);
    this.getQueryParam("PageNo", 1, keyvalue);
    this.getQueryParam("PageSize", 0, keyvalue);
    this.getQueryParam("SortBy", "ZoneName", keyvalue);
    this.getQueryParam("SortOrder", "asc", keyvalue);
    let querparam = ""
    keyvalue.forEach(function (elem, i) {
      if (i == 0) {
        querparam = "?" + elem.key + "=" + elem.value
      } else {
        querparam = querparam + "&" + elem.key + "=" + elem.value
      }
    });

    var geturl = this.apiBaseApiUrl + 'api/zones' + querparam;
    //alert(geturl);
    return this.http.get(geturl).pipe(map(result => result));    
} 

getZoneInfo() {    
  var keyvalue = []
  this.getQueryParam("Id", 0, keyvalue);
  this.getQueryParam("SearchText", "", keyvalue);
  this.getQueryParam("PageNo", 1, keyvalue);
  this.getQueryParam("PageSize", 0, keyvalue);
  this.getQueryParam("SortBy", "ZoneName", keyvalue);
  this.getQueryParam("SortOrder", "asc", keyvalue);
  let querparam = ""
  keyvalue.forEach(function (elem, i) {
    if (i == 0) {
      querparam = "?" + elem.key + "=" + elem.value
    } else {
      querparam = querparam + "&" + elem.key + "=" + elem.value
    }
  });

  var geturl = this.apiBaseApiUrl + 'api/zones' + querparam;
  //alert(geturl);
  return this.http.get(geturl).pipe(map(result => result));    
} 

getSalesman() {    
  var keyvalue = []
  this.getQueryParam("Id", 0, keyvalue);
  this.getQueryParam("SearchText", "", keyvalue);
  this.getQueryParam("PageNo", 1, keyvalue);
  this.getQueryParam("PageSize", 0, keyvalue);
  this.getQueryParam("SortBy", "Name", keyvalue);
  this.getQueryParam("SortOrder", "asc", keyvalue);
  let querparam = ""
  keyvalue.forEach(function (elem, i) {
    if (i == 0) {
      querparam = "?" + elem.key + "=" + elem.value
    } else {
      querparam = querparam + "&" + elem.key + "=" + elem.value
    }
  });
  var geturl = this.apiBaseApiUrl + 'api/salesman' + querparam;
  //alert(geturl);
  return this.http.get(geturl).pipe(map(result => result));    
}

getDriver() {    
  var keyvalue = []
  this.getQueryParam("Id", 0, keyvalue);
  this.getQueryParam("SearchText", "", keyvalue);
  this.getQueryParam("PageNo", 1, keyvalue);
  this.getQueryParam("PageSize", 0, keyvalue);
  this.getQueryParam("SortBy", "DriverName", keyvalue);
  this.getQueryParam("SortOrder", "asc", keyvalue);
  let querparam = ""
  keyvalue.forEach(function (elem, i) {
    if (i == 0) {
      querparam = "?" + elem.key + "=" + elem.value
    } else {
      querparam = querparam + "&" + elem.key + "=" + elem.value
    }
  });
  var geturl = this.apiBaseApiUrl + 'api/drivers' + querparam;
  //alert(geturl);
  return this.http.get(geturl).pipe(map(result => result));    
}

getLocation() {    
  var keyvalue = []
  this.getQueryParam("Id", 0, keyvalue);
  this.getQueryParam("SearchText", "", keyvalue);
  this.getQueryParam("PageNo", 1, keyvalue);
  this.getQueryParam("PageSize", 0, keyvalue);
  this.getQueryParam("SortBy", "LocationName", keyvalue);
  this.getQueryParam("SortOrder", "asc", keyvalue);
  let querparam = ""
  keyvalue.forEach(function (elem, i) {
    if (i == 0) {
      querparam = "?" + elem.key + "=" + elem.value
    } else {
      querparam = querparam + "&" + elem.key + "=" + elem.value
    }
  });
  var geturl = this.apiBaseApiUrl + 'api/locations' + querparam;
  //alert(geturl);
 return this.http.get(geturl).pipe(map(result => result));    
}


getOrderold() {    
  var keyvalue = []
  this.getQueryParam("NoOfMonth", 12, keyvalue);
  this.getQueryParam("SearchByCustomer", "", keyvalue);
  this.getQueryParam("SearchByCustomerAddress", "", keyvalue);
  this.getQueryParam("OrderNo", "", keyvalue);
  let querparam = ""
  keyvalue.forEach(function (elem, i) {
    if (i == 0) {
      querparam = "?" + elem.key + "=" + elem.value
    } else {
      querparam = querparam + "&" + elem.key + "=" + elem.value
    }
  });
  var geturl = this.apiBaseApiUrl + 'api/triporders' + querparam;
  //alert(geturl);
 return this.http.get(geturl).pipe(map(result => result));    
}

getOrder(zoneId,salesmanId,noOfMonth,customerSearch,customerAddressSearch,orderNo) {    
  var keyvalue = []
  this.getQueryParam("ZoneId", zoneId, keyvalue);
  this.getQueryParam("SalesmanId", salesmanId, keyvalue);
  this.getQueryParam("NoOfMonth", noOfMonth, keyvalue);
  this.getQueryParam("SearchByCustomer", customerSearch, keyvalue);
  this.getQueryParam("SearchByCustomerAddress", customerAddressSearch, keyvalue);
  this.getQueryParam("OrderNo", orderNo, keyvalue);
  let querparam = ""
  keyvalue.forEach(function (elem, i) {
    if (i == 0) {
      querparam = "?" + elem.key + "=" + elem.value
    } else {
      querparam = querparam + "&" + elem.key + "=" + elem.value
    }
  });
  var geturl = this.apiBaseApiUrl + 'api/triporders' + querparam;
  //alert(geturl);
 return this.http.get(geturl).pipe(map(result => result));    
}

getTripInfo(StartDate,TripStatus,SearchText) {    
  var keyvalue = []
  this.getQueryParam("StartDate", StartDate, keyvalue);
  this.getQueryParam("TripStatus", TripStatus, keyvalue);
  this.getQueryParam("SearchText", SearchText, keyvalue);
  let querparam = ""
  keyvalue.forEach(function (elem, i) {
    if (i == 0) {
      querparam = "?" + elem.key + "=" + elem.value
    } else {
      querparam = querparam + "&" + elem.key + "=" + elem.value
    }
  });
  var geturl = this.apiBaseApiUrl + 'api/tripInfos' + querparam;
  //alert(geturl);
 return this.http.get(geturl).pipe(map(result => result));    
}
getTrip(tripId) {    
  var keyvalue = []
  this.getQueryParam("TripId", tripId, keyvalue);
  let querparam = ""
  keyvalue.forEach(function (elem, i) {
    if (i == 0) {
      querparam = "?" + elem.key + "=" + elem.value
    } else {
      querparam = querparam + "&" + elem.key + "=" + elem.value
    }
  });
  var geturl = this.apiBaseApiUrl + 'api/triporder' + querparam;
  //alert(geturl);
 return this.http.get(geturl).pipe(map(result => result));    
}

GetAllOrderByTrip(tripId) {    
  var keyvalue = []
  this.getQueryParam("TripId", tripId, keyvalue);
  let querparam = ""
  keyvalue.forEach(function (elem, i) {
    if (i == 0) {
      querparam = "?" + elem.key + "=" + elem.value
    } else {
      querparam = querparam + "&" + elem.key + "=" + elem.value
    }
  });
  var geturl = this.apiBaseApiUrl + 'api/orderbytrip' + querparam;
  //alert(geturl);
 return this.http.get(geturl).pipe(map(result => result));    
}

postTrip(tripdata) {    
  var posturl = this.apiBaseApiUrl + 'api/addtrip';
  console.log(JSON.stringify(tripdata));
  return this.http.post(posturl, JSON.stringify(tripdata)).pipe(map(response => {
    return response;
  })); 
  
}

putTrip(tripdata) {    
  var posturl = this.apiBaseApiUrl + 'api/edittrip';
  console.log(JSON.stringify(tripdata));
  return this.http.post(posturl, JSON.stringify(tripdata)).pipe(map(response => {
    return response;
  })); 
  
}

getQueryParam(key, value, keyvalue) {
  //if (value) {
    //alert(value);
    keyvalue.push({ "key": key, "value": value })
  //}
} 
  getAdminData() {    
    return this.http.get('/api/user/GetAdminData').pipe(map(result => result));    
  } 
}
