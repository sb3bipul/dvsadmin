import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  apiBaseApiUrl = environment.baseUrl;
  constructor(private http: HttpClient) {  
      
  } 

  getDashboardInfo(companyId) {    
    var keyvalue = []
    this.getQueryParam("companyId", companyId, keyvalue);
    let querparam = ""
    keyvalue.forEach(function (elem, i) {
      if (i == 0) {
        querparam = "?" + elem.key + "=" + elem.value
      } else {
        querparam = querparam + "&" + elem.key + "=" + elem.value
      }
    });
  
    var geturl = this.apiBaseApiUrl + 'api/dashboard' + querparam;
    //alert(geturl);
    return this.http.get(geturl).pipe(map(result => result));    
  }
  
  getQueryParam(key, value, keyvalue) {
    keyvalue.push({ "key": key, "value": value });
  } 
}
