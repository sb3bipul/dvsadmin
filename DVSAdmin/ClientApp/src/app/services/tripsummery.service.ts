import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TripsummeryService {

  apiBaseApiUrl = environment.baseUrl;
  constructor(private http: HttpClient) {    
  } 

  getTripDeliverySummery(tripId) {    
    var keyvalue = []
    this.getQueryParam("tripId", tripId, keyvalue);
    let querparam = ""
    keyvalue.forEach(function (elem, i) {
      if (i == 0) {
        querparam = "?" + elem.key + "=" + elem.value
      } else {
        querparam = querparam + "&" + elem.key + "=" + elem.value
      }
    });
    var geturl = this.apiBaseApiUrl + 'api/deliverysummerytrip' + querparam;
    //alert(geturl);
   return this.http.get(geturl).pipe(map(result => result));    
  }
  
  getQueryParam(key, value, keyvalue) {
    //if (value) {
      //alert(value);
      keyvalue.push({ "key": key, "value": value })
    //}
  } 
}
