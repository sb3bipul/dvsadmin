import { TestBed } from '@angular/core/testing';

import { TriptrackingService } from './triptracking.service';

describe('TriptrackingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TriptrackingService = TestBed.get(TriptrackingService);
    expect(service).toBeTruthy();
  });
});
