import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SalesmanService {

  apiBaseApiUrl = environment.baseUrl;
  constructor(private http: HttpClient) {    
  } 

  getSalesmanInfo() {    
    var keyvalue = []
    this.getQueryParam("Id", 0, keyvalue);
    this.getQueryParam("SearchText", "", keyvalue);
    this.getQueryParam("PageNo", 1, keyvalue);
    this.getQueryParam("PageSize", 0, keyvalue);
    this.getQueryParam("SortBy", "Name", keyvalue);
    this.getQueryParam("SortOrder", "asc", keyvalue);
    let querparam = ""
    keyvalue.forEach(function (elem, i) {
      if (i == 0) {
        querparam = "?" + elem.key + "=" + elem.value
      } else {
        querparam = querparam + "&" + elem.key + "=" + elem.value
      }
    });
  
    var geturl = this.apiBaseApiUrl + 'api/salesman' + querparam;
    //alert(geturl);
    return this.http.get(geturl).pipe(map(result => result));    
  }

  getSalesmanInfoByID(Id:number) {    
    var keyvalue = []
    this.getQueryParam("Id", Id, keyvalue);
    this.getQueryParam("SearchText", "", keyvalue);
    this.getQueryParam("PageNo", 1, keyvalue);
    this.getQueryParam("PageSize", 0, keyvalue);
    this.getQueryParam("SortBy", "Name", keyvalue);
    this.getQueryParam("SortOrder", "asc", keyvalue);
    let querparam = ""
    keyvalue.forEach(function (elem, i) {
      if (i == 0) {
        querparam = "?" + elem.key + "=" + elem.value
      } else {
        querparam = querparam + "&" + elem.key + "=" + elem.value
      }
    });
  
    var geturl = this.apiBaseApiUrl + 'api/salesmandetail' + querparam;
    //alert(geturl);
    return this.http.get(geturl).pipe(map(result => result));    
  }

  getQueryParam(key, value, keyvalue) {
    keyvalue.push({ "key": key, "value": value });
  } 
}
