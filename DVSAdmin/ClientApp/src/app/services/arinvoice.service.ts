import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ARInvoiceService {
  apiBaseApiUrl = environment.baseUrl;
  constructor(private http: HttpClient) { }

  getARInvoiceListForAdmin(FromDate: string, ToDate: string, InvoiceNo: string, OrderNo: string, City: string, Status: string) {
    var keyvalue = []
    this.getQueryParam("fromdate", FromDate, keyvalue);
    this.getQueryParam("toDate", ToDate, keyvalue);
    this.getQueryParam("InvoiceNo", InvoiceNo, keyvalue);
    this.getQueryParam("OrderNo", OrderNo, keyvalue);
    this.getQueryParam("City", City, keyvalue);
    this.getQueryParam("Status", Status, keyvalue);
    let querparam = ""
    keyvalue.forEach(function (elem, i) {
      if (i == 0) {
        querparam = "?" + elem.key + "=" + elem.value
      } else {
        querparam = querparam + "&" + elem.key + "=" + elem.value
      }
    });

    var geturl = this.apiBaseApiUrl + 'api/ARInvoiceList' + querparam;
    return this.http.get(geturl).pipe(map(result => result));
  }

  getQueryParam(key, value, keyvalue) {
    keyvalue.push({ "key": key, "value": value });
  }

  updateApproveRejectInvoice(approveinv) {
    var posturl = this.apiBaseApiUrl + 'api/UpdateInvoiceStatus';
    console.log("posturl: " + posturl + " , JSON: " + JSON.stringify(approveinv));
    return this.http.post(posturl, JSON.stringify(approveinv)).pipe(map(response => {
      return response;
    }));
  }

  getARCustCity() {
    var geturl = this.apiBaseApiUrl + 'api/ARCustCity';
    return this.http.get(geturl).pipe(map(result => result));
  }

  getCSVDataForExport(FromDate: string, ToDate: string, InvoiceNo: string, OrderNo: string, City: string, Status: string) {
    var keyvalue = []
    this.getQueryParam("fromdate", FromDate, keyvalue);
    this.getQueryParam("toDate", ToDate, keyvalue);
    this.getQueryParam("InvoiceNo", InvoiceNo, keyvalue);
    this.getQueryParam("OrderNo", OrderNo, keyvalue);
    this.getQueryParam("City", City, keyvalue);
    this.getQueryParam("Status", Status, keyvalue);
    let querparam = ""
    keyvalue.forEach(function (elem, i) {
      if (i == 0) {
        querparam = "?" + elem.key + "=" + elem.value
      } else {
        querparam = querparam + "&" + elem.key + "=" + elem.value
      }
    });

    var geturl = this.apiBaseApiUrl + 'api/ARCSVDataExport' + querparam;
    return this.http.get(geturl).pipe(map(result => result));
  }
}
