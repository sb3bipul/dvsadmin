import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { VehicletypeService } from '../services/vehicletype.service';

@Component({
  selector: 'app-vehicle-type',
  templateUrl: './vehicle-type.component.html',
  styleUrls: ['./vehicle-type.component.css']
})
export class VehicleTypeComponent implements OnInit {
  vehicalTypeInfoData=[];
  vehicalTypeData=[];
  StartDate = '';
  vehicleTypeStatus = '';
  SearchText = '';
  public p: number = 1;
  constructor(private vehicleTypeServise: VehicletypeService,private SpinnerService: NgxSpinnerService,private router: Router) { 
    
    this.loadVehicalTypeInfoData();
   }
  
    ngOnInit() {
    }
  
    loadVehicalTypeInfoData() {   
      this.SpinnerService.show(); 
      //delay(20000);
      this.vehicleTypeServise.getVehicalTypeInfo().subscribe(    
        (result: any) => {    
          //alert(JSON.parse(result["vehicleTypes"]));
          
          this.vehicalTypeData = result["vehicleTypes"];  
          for (var i = 0; i < this.vehicalTypeData.length; i++) {
            this.vehicalTypeInfoData.push(this.vehicalTypeData[i]);
            
          }
          console.log(this.vehicalTypeInfoData);
          this.SpinnerService.hide(); 
          //alert(this.tripInfoData);  
          }
          //this.orderData = this.collection.data
          
         
      );    
    }
  
    onEditVehicleType(vehicleTypeId){
      //alert(tripId);
      window.localStorage.removeItem("vehicleTypeId");
      window.localStorage.setItem("vehicleTypeId", vehicleTypeId.toString());
      this.router.navigate(['../edit-vehicleType']);
  
    }
    onTaggleActive(vehicleTypeId){
      var vehicleTypeToggle = this.vehicalTypeInfoData.find(x => x.typeID === Number(vehicleTypeId));
      //alert(vehicleTypeToggle);
      vehicleTypeToggle.isActive =!vehicleTypeToggle.isActive;
      //alert(vehicleTypeToggle.typeID);
      //vehicleTypeToggle["id"] = vehicleTypeToggle.typeID;
      //alert(vehicleTypeToggle.id);
      this.vehicleTypeServise.putVehicleTypeStatus(vehicleTypeToggle).subscribe(    
        (result: any) => {    
          alert(result["message"]); 
          }          
         
      );  
    }
    onDeleteVehicleType(vehicleTypeId){
      //alert(vehicleTypeId);
      var vehicleTypeDelete = this.vehicalTypeInfoData.find(x => x.typeID === Number(vehicleTypeId));
      this.vehicleTypeServise.deleteVehicleType(vehicleTypeDelete).subscribe(    
        (result: any) => {    
          alert(result["message"]); 
          this.vehicalTypeInfoData = this.vehicalTypeInfoData.filter(item => item !== vehicleTypeDelete);
          }
      );  
    }
    onAddVehicalType(){
      this.router.navigate(['../add-vehicletype']);
    }

}
