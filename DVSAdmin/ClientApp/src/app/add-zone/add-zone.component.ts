import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { Router } from '@angular/router';
import { ZoneService } from '../services/zone.service';

@Component({
  selector: 'app-add-zone',
  templateUrl: './add-zone.component.html',
  styleUrls: ['./add-zone.component.css']
})
export class AddZoneComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,private router: Router, private zoneService: ZoneService) {
    
   }
   addZoneForm: FormGroup;
  ngOnInit() {
    this.addZoneForm = this.formBuilder.group({
      id: [0],
      ZoneName: ['', Validators.required],
      ZoneCode: ['', Validators.required],
      IsActive: [true, Validators.required],
      IsDeleted: [true, Validators.required],
      ActionMode: [1, Validators.required],
    });
  }
  get addZoneFormControl() { return this.addZoneForm.controls; }
  onSaveZone(){

  }
  onSubmit(){
   //alert("hi");
    this.zoneService.postZone(this.addZoneForm.value)
      .subscribe( data => {
        this.router.navigate(['zone']);
      });
  }

}
