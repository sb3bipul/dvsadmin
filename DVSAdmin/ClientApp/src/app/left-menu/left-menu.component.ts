import { Component, ElementRef, ViewChild, OnInit, AfterViewInit } from "@angular/core";
import { Router, UrlTree, UrlSegment, UrlSegmentGroup, PRIMARY_OUTLET, DefaultUrlSerializer, RouterState, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.css']
})
export class LeftMenuComponent implements AfterViewInit, OnInit {
  isShown: boolean = true; // hidden by default
  urlpath: string;
  toggleShow() {
    this.isShown = !this.isShown;
  }

  @ViewChild('dashboard', { static: false }) dashboard: ElementRef;
  @ViewChild('alltrips', { static: false }) alltrips: ElementRef;
  @ViewChild('addtrip', { static: false }) addtrip: ElementRef;
  @ViewChild('triptracking', { static: false }) triptracking: ElementRef;
  @ViewChild('customer', { static: false }) customer: ElementRef;
  @ViewChild('order', { static: false }) order: ElementRef;
  @ViewChild('zone', { static: false }) zone: ElementRef;
  @ViewChild('salesman', { static: false }) salesman: ElementRef;
  @ViewChild('driver', { static: false }) driver: ElementRef;
  @ViewChild('vehicletype', { static: false }) vehicletype: ElementRef;
  @ViewChild('arinvoice', { static: false }) arinvoice: ElementRef;
  @ViewChild('dvsinvoice', { static: false }) dvsinvoice: ElementRef;
  @ViewChild('vehicle', { static: false }) vehicle: ElementRef;
  @ViewChild('settings', { static: false }) settings: ElementRef;




  constructor(private router: Router) {
    const tree: UrlTree = router.parseUrl(this.router.url);
    const g: UrlSegmentGroup = tree.root.children[PRIMARY_OUTLET];
    const s: UrlSegment[] = g.segments;
    this.urlpath = s[0].path; // returns 'primary segment'
  }

  ngAfterViewInit() {
    if (this.urlpath == "dashboard") {
      this.dashboard.nativeElement.setAttribute("class", "active gradient-45deg-indigo-blue");
    }
    else if (this.urlpath == "all-trip") {
      this.alltrips.nativeElement.setAttribute("class", "active gradient-45deg-indigo-blue");
    }
    else if (this.urlpath == "add-trip") {
      this.addtrip.nativeElement.setAttribute("class", "active gradient-45deg-indigo-blue");
    }
    else if (this.urlpath == "trip-tracking") {
      this.triptracking.nativeElement.setAttribute("class", "active gradient-45deg-indigo-blue");
    }
    else if (this.urlpath == "customer") {
      this.customer.nativeElement.setAttribute("class", "active gradient-45deg-indigo-blue");
    }
    else if (this.urlpath == "order") {
      this.order.nativeElement.setAttribute("class", "active gradient-45deg-indigo-blue");
    }
    else if (this.urlpath == "zone") {
      this.zone.nativeElement.setAttribute("class", "active gradient-45deg-indigo-blue");
    }
    else if (this.urlpath == "salesman") {
      this.salesman.nativeElement.setAttribute("class", "active gradient-45deg-indigo-blue");
    }
    else if (this.urlpath == "driver") {
      this.driver.nativeElement.setAttribute("class", "active gradient-45deg-indigo-blue");
    }
    else if (this.urlpath == "vehicletype") {
      this.vehicletype.nativeElement.setAttribute("class", "active gradient-45deg-indigo-blue");
    }
    else if (this.urlpath == "arinvoice") {
      this.arinvoice.nativeElement.setAttribute("class", "active gradient-45deg-indigo-blue");
    }
    else if (this.urlpath == "dvsInvoice") {
      this.dvsinvoice.nativeElement.setAttribute("class", "active gradient-45deg-indigo-blue");
    }
    else if (this.urlpath == "vehicle") {
      this.vehicle.nativeElement.setAttribute("class", "active gradient-45deg-indigo-blue");
    }
    else if (this.urlpath == "dvssetting") {
      this.settings.nativeElement.setAttribute("class", "active gradient-45deg-indigo-blue");
    }
    else {
      this.dashboard.nativeElement.setAttribute("class", "active gradient-45deg-indigo-blue");
    }

  }

  ngOnInit() {
  }
}
