﻿using DVSDomain.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.DBContext
{
    public class AppDBContext : DbContext
    {
        public AppDBContext(DbContextOptions option) : base(option)
        {

        }
        //public DbSet<Promotions> Promotion { get; set; }
        public DbSet<User> user { get; set; }

        public DbSet<ApplicationUser> appuser { get; set; }
    }
}
