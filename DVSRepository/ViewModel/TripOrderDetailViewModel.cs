﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.ViewModel
{
    public class TripOrderDetailViewModel
    {
        public string PONumber { get; set; }
        public string OrderDate { get; set; }

        public decimal BillAmount { get; set; }
        public string OrderStatus { get; set; }
        public int OrdQty { get; set; }
        public decimal GrossWeight { get; set; }
        public string lbs { get; set; }
        public bool IsSelected { get; set; }
        public int Priority { get; set; }
        public string CstId { get; set; }
    }
}
