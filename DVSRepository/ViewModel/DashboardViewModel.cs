﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.ViewModel
{
    public class DashboardViewModel
    {
        public int TotalCustomer { get; set; }
        public int TotalOrder { get; set; }
        public int TotalTrip { get; set; }
        public int TotalDriver { get; set; }
    }
}
