﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.ViewModel
{
    public class TripInfoViewModel
    {
        public string TripId { get; set; }
        public string TripDate { get; set; }
        public string TripName { get; set; }
        public string DriverName { get; set; }
        public int NoOfStop { get; set; }
        public int TotalOrder { get; set; }
        public decimal OrderAmount { get; set; }
        public string StatusText { get; set; }
    }
}
