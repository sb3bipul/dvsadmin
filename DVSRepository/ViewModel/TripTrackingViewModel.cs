﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.ViewModel
{
    public class TripTrackingViewModel
    {
        public string TripID { get; set; }
        public string TripName { get; set; }
        public string DriverID { get; set; }
        public string DriverName { get; set; }
        public string StartDate{ get; set; }
        public List<TripTrackingDetailViewModel> Trackings { get; set; }
    }
}
