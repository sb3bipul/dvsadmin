﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.ViewModel
{
    public class OrderDetailViewModel
    {
        public string OrderId { get; set; }
        public string OrderStatusName { get; set; }
        public string OrderStatus { get; set; }
        public string SalesmanId { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string CustomerId { get; set; }
        public string OrderDate { get; set; }
        public string BillAmount { get; set; }
        public List<OrderDetailItemsViewModel> DetailItems { get; set; }

    }
}
