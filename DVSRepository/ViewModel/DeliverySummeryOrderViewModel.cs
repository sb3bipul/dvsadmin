﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.ViewModel
{
    public class DeliverySummeryOrderViewModel
    {
        public string DriverID { get; set; }
        public string TripID { get; set; }
        public string OrderNo { get; set; }
        public List<TripDeleveryItemSummeryViewModel> Items { get; set; }
        
    }
}
