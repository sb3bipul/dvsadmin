﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.ViewModel
{
    public class DVSInvoiceViewModel
    {
        public string CustomerId { get; set; }
        public string Name { get; set; }
        public string InvoiceNo { get; set; }
        public string DriverId { get; set; }
        public string DriverName { get; set; }
        public string SignatoryName { get; set; }
        public string Status { get; set; }
        public string UploadDate { get; set; }
        public string SignImage { get; set; }
        public string InvoicePdf { get; set; }
    }

    public class StatusChangeHistoryViewModel
    {
        public string InvoiceNo { get; set; }
        public string Status { get; set; }
        public string RejectReason { get; set; }
        public string UpdateDate { get; set; }
        public string StatusCode { get; set; }
    }
}
