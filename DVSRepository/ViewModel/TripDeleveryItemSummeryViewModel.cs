﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DVSRepository.ViewModel
{
   public class TripDeleveryItemSummeryViewModel
    {    
        public string ProductCode { get; set; }
        public string ItemName { get; set; }
        public int OrdQty { get; set; }
        public int DelQty { get; set; }
        public decimal CasePrice { get; set; }
        public decimal TotalCasePrice { get; set; }
        public string DeliveryDate { get; set; }
        public string ExtraQuantity { get; set; }
        public string ReturnQuantity { get; set; }
    }
}
