﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.ViewModel
{
    public class ARInvoiceViewModel
    {
        public string CustomerNo { get; set; }
        public string InvoiceNo { get; set; }
        public string CustName { get; set; }
        public string InvoiceDate { get; set; }
        public string TripID { get; set; }
        public string DriverID { get; set; }
        public string Status { get; set; }
        public string InvoicePDF { get; set; }
        public string OrderNo { get; set; }
        public string OrderDate { get; set; }
        public string City { get; set; }
        public string PONumber { get; set; }
        public string BasketID { get; set; }
        public string OrderAmount { get; set; }
        public string IsDSD { get; set; }
        public string CheckerID { get; set; }
        public string StopID { get; set; }
        public string TruckID { get; set; }
        public string BrokerID { get; set; }

    }

    public class ARCustCityViewModel
    {
        public string City { get; set; }
    }

    public class ARCSVDataViewModel
    {
        public string CustomerNo { get; set; }
        public string InvoiceNo { get; set; }
        public string CustName { get; set; }
        public string TripID { get; set; }
        public string OrderNo { get; set; }
        public string OrderDate { get; set; }
        public string City { get; set; }
        public string DriverID { get; set; }
        public string Status { get; set; }
    }
}
