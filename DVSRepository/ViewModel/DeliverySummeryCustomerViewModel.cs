﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.ViewModel
{
    public class DeliverySummeryCustomerViewModel
    {
        public string CustomerId { get; set; }
        public int ZoneId { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string ShippingAddress { get; set; }
        public string BillingAddress { get; set; }
        public List<DeliverySummeryOrderViewModel> Orders { get; set; }
    }
}
