﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.ViewModel
{
    public class OrderDetailItemsViewModel
    {
        public string ItemName { get; set; }
        public string GrossWeight { get; set; }
        public string ListPrice { get; set; }
        public string ActualPrice { get; set; }
        public int OrdQty { get; set; }
        public int DeliveredQty { get; set; }
        public int ReturnQty { get; set; }
        public int ExtraQty { get; set; }
        public string UOM { get; set; }
    }
}
