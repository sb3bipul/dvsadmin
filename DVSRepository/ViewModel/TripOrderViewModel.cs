﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DVSRepository.ViewModel
{
    public class TripOrderViewModel
    {
        [Required(ErrorMessage = "CustomerId is required")]
        public string CustomerId { get; set; }
        public bool IsSelected { get; set; }
        public int StopCount { get; set; }
        public int OptStopCount { get; set; }

        [Required(ErrorMessage = "CustomerName is required")]
        public string CustomerName { get; set; }

        [Required(ErrorMessage = "TypeName is required")]
        public string Address { get; set; }
        public string ShippingAddress { get; set; }
        public string BillingAddress { get; set; }
        public List<TripOrderDetailViewModel> Orders {get; set; }
        public string DriverID { get; set; }
        public string TripID { get; set; }
        public string TripName { get; set; }
        public string StartLocation { get; set; }
        public string EndLocation { get; set; }
        public string StartTime { get; set; }
        public string TotalGrossWeight { get; set; }
        public string TotalBillAmount { get; set; }
        public int PriorityCount { get; set; }
        public decimal DeliveryCharges { get; set; }
        public string CustomerNote { get; set; }


    }
}
