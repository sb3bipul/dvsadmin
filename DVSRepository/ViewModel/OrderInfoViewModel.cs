﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.ViewModel
{
    public class OrderInfoViewModel
    {
        public string OrderId { get; set; }
        public string OrderDate { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string DistributorName { get; set; }
        public string Zone { get; set; }
        public string ZoneName { get; set; }
        public string ZoneCode { get; set; }
        public int Status { get; set; }
        public string StatusText { get; set; }
        public int Type { get; set; }
        public string TypeText { get; set; }
    }
}
