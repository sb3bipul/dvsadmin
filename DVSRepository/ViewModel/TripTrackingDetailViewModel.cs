﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.ViewModel
{
    public class TripTrackingDetailViewModel
    {
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string StopNo { get; set; }
        public string StatusText { get; set; }
        public string CustomerAddress { get; set; }
    }
}
