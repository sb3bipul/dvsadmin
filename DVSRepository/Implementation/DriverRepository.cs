﻿using DVSDomain.Model;
using DVSRepository.Helper;
using DVSRepository.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DVSRepository.Implementation
{
    public class DriverRepository : IDriverRepository
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string apiConnectionString;
        DataAccessReposity daRepo = null;
        DataTable dt = null;
        private string MessageReturn = "";
        public Tuple<List<Driver>, int> GetDriver(GetDataBindingModel model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            SqlParameter[] objParam;
            List<Driver> lstobj = new List<Driver>();
            Driver obj = null;
            int totalRecordNo = 0;
            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[6];

                objParam[0] = new SqlParameter("@Id", SqlDbType.Int);
                objParam[0].Value = model.Id;

                objParam[1] = new SqlParameter("@SearchText", SqlDbType.VarChar, 250);
                objParam[1].Value = model.SearchText;

                objParam[2] = new SqlParameter("@PageNo", SqlDbType.Int);
                objParam[2].Value = model.PageNo;

                objParam[3] = new SqlParameter("@PageSize", SqlDbType.Int);
                objParam[3].Value = model.PageSize;

                objParam[4] = new SqlParameter("@SortBy", SqlDbType.VarChar, 100);
                objParam[4].Value = model.SortBy;

                objParam[5] = new SqlParameter("@SortOrder", SqlDbType.VarChar, 100);
                objParam[5].Value = model.SortOrder;

                 dt = daRepo.GetDataTable(ApiConstant.SPocGetDriver, objParam);

                foreach (DataRow dr in dt.Rows)
                {
                    obj = new Driver();
                    obj.DriverId = Convert.ToInt32(dr["DriverId"]);
                    obj.CompanyId = Convert.ToInt32(dr["CompanyId"]);
                    obj.DriverName = Convert.ToString(dr["DriverName"]);
                    obj.DriverAddress = Convert.ToString(dr["DriverAddress"]);
                    obj.City = Convert.ToString(dr["City"]);
                    obj.State = Convert.ToInt32(dr["State"]);
                    obj.Zip = Convert.ToString(dr["Zip"]);
                    obj.Country = Convert.ToInt32(dr["Country"]);
                    obj.DriverEmail = Convert.ToString(dr["DriverEmail"]);
                    obj.DriverContactNo = Convert.ToString(dr["DriverContactNo"]);
                    obj.DrivingLicense = Convert.ToString(dr["DrivingLicense"]);
                    obj.DriverLoginID = Convert.ToString(dr["DriverLoginID"]);
                    obj.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    obj.Warehouse = Convert.ToString(dr["WareHouse"]);
                    obj.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    //obj.DefaultLanguage = Convert.ToString(dr["DefaultLanguage"]);
                    //obj.FromDate = Convert.ToDateTime(dr["FromDate"]);
                    //obj.ToDate = Convert.ToDateTime(dr["ToDate"]);
                    //obj.Signature = Convert.ToString(dr["Signature"]);
                    //obj.DateRegistration = Convert.ToDateTime(dr["DateRegistration"]);
                    //obj.DateAuthentication = dr["DateAuthentication"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dr["DateAuthentication"]);
                    //obj.DriverImage = Convert.ToString(dr["DriverImage"]);
                    //obj.DriverImagePath = Convert.ToString(dr["DriverImagePath"]);
                    //obj.CreatedBy = Convert.ToString(dr["CreatedBy"]);
                    //obj.CreatedOn = Convert.ToDateTime(dr["CreatedOn"]);
                    obj.VehicleID = Convert.ToInt32(dr["VehicleID"]);
                    obj.VehicleName = Convert.ToString(dr["VehicleName"]);
                    obj.VehicleCapacity = Convert.ToInt32(dr["VehicleCapacity"]);
                    obj.VehicleNo = Convert.ToString(dr["VehicleNo"]);
                    obj.VehicleTypeId = Convert.ToInt32(dr["VehicleTypeId"]);

                    lstobj.Add(obj);
                }
                if (dt.Rows.Count > 0)
                {
                    totalRecordNo = Convert.ToInt32(dt.Rows[0]["TotalRowNo"]);
                }
                log.Debug("Debug logging: DriverRepository - GetDriver");

                return new Tuple<List<Driver>, int>(lstobj, totalRecordNo);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }

            return new Tuple<List<Driver>, int>(lstobj, totalRecordNo);
        }

        public string CRUDDriver(DriverBindingModel model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            SqlParameter[] objParam;
            apiConnectionString = apiConnectionAuthString;
            MessageReturn = "";
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[15];

                objParam[0] = new SqlParameter("@ActionMode", SqlDbType.Int);
                objParam[0].Value = model.ActionMode;

                objParam[1] = new SqlParameter("@DriverName", SqlDbType.VarChar, 50);
                objParam[1].Value = model.DriverName;

                objParam[2] = new SqlParameter("@DriverAddress", SqlDbType.NVarChar, 100);
                objParam[2].Value = model.DriverAddress;

                objParam[3] = new SqlParameter("@City", SqlDbType.NVarChar, 50);
                objParam[3].Value = model.City;

                objParam[4] = new SqlParameter("@State", SqlDbType.Int);
                objParam[4].Value = model.State;

                objParam[5] = new SqlParameter("@Zip", SqlDbType.NVarChar, 8);
                objParam[5].Value = model.Zip;

                objParam[6] = new SqlParameter("@Country", SqlDbType.Int);
                objParam[6].Value = model.Country;

                objParam[7] = new SqlParameter("@DriverEmail", SqlDbType.NVarChar);
                objParam[7].Value = model.DriverEmail;

                objParam[8] = new SqlParameter("@DriverContactNo", SqlDbType.NVarChar, 100);
                objParam[8].Value = model.DriverContactNo;

                objParam[9] = new SqlParameter("@DrivingLicense", SqlDbType.VarChar, 50);
                objParam[9].Value = model.DrivingLicense;

                objParam[10] = new SqlParameter("@DriverLoginID", SqlDbType.VarChar, 50);
                objParam[10].Value = model.DriverLoginID;

                objParam[11] = new SqlParameter("@IsActive", SqlDbType.Bit, 10);
                objParam[11].Value = model.IsActive;

                objParam[12] = new SqlParameter("@Warehouse", SqlDbType.VarChar, 10);
                objParam[12].Value = model.Warehouse;

                objParam[13] = new SqlParameter("@DriverImage", SqlDbType.VarChar, 150);
                objParam[13].Value = model.DriverImage;

                objParam[14] = new SqlParameter("@VehicleID", SqlDbType.Int);
                objParam[14].Value = model.VehicleID;

                MessageReturn = daRepo.ExecuteOutParamProc(ApiConstant.SPocCRUDDriver, objParam);

                //if (MessageReturn.Contains("successfully"))
                //{
                //    log.Debug("Debug logging: DriverRepository - SPocCRUDDriver -" + model.ActionMode.ToString());
                //    String DriverImageBinary = Convert.ToString(model.DriverImage);
                //    String DriverImageName = Convert.ToString(model.DriverImage);
                //    log.Info("-> api/Driver : Image Data: " + Convert.ToString(SignImageBinary));

                //    System.Drawing. img;

                //    String _SignPath = "D:\\Koushik\\DVS\\DVS\\DVSRepository\\images\\";
                //    if (!String.IsNullOrEmpty(DriverImageName) && DriverImageName != "~" && !String.IsNullOrEmpty(DriverImageBinary) && DriverImageBinary != "~")
                //    {
                //        string extension = Path.GetExtension(DriverImageName);
                //        ConvertBase64_ToImg objBase64 = new ConvertBase64_ToImg();
                //        img = objBase64.Base64ToImage(DriverImageBinary);              //if (extension == ".png" || extension == ".gif")    img = objBase64.Base64ToImage(SignImageBinary.Substring(22)); else    img = objBase64.Base64ToImage(SignImageBinary.Substring(23));
                //        img.Save(_SignPath + DriverImageName, System.Drawing.Imaging.ImageFormat.Png);
                //    }
                //}

                return MessageReturn;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
            }

            return MessageReturn;
        }

        public string CRUDDriverold(DriverBindingModel model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            SqlParameter[] objParam;
            apiConnectionString = apiConnectionAuthString;
            MessageReturn = "";
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[14];

                objParam[0] = new SqlParameter("@ActionMode", SqlDbType.Int);
                objParam[0].Value = model.ActionMode;

                objParam[1] = new SqlParameter("@DriverName", SqlDbType.VarChar, 50);
                objParam[1].Value = model.DriverName;

                objParam[2] = new SqlParameter("@DriverAddress", SqlDbType.NVarChar, 100);
                objParam[2].Value = model.DriverAddress;

                objParam[3] = new SqlParameter("@City", SqlDbType.NVarChar, 50);
                objParam[3].Value = model.City;

                objParam[4] = new SqlParameter("@State", SqlDbType.Int);
                objParam[4].Value = model.State;

                objParam[5] = new SqlParameter("@Zip", SqlDbType.NVarChar, 8);
                objParam[5].Value = model.Zip;

                objParam[6] = new SqlParameter("@Country", SqlDbType.Int);
                objParam[6].Value = model.Country;

                objParam[7] = new SqlParameter("@DriverEmail", SqlDbType.NVarChar);
                objParam[7].Value = model.DriverEmail;

                objParam[8] = new SqlParameter("@DriverContactNo", SqlDbType.NVarChar, 100);
                objParam[8].Value = model.DriverContactNo;

                objParam[9] = new SqlParameter("@DrivingLicense", SqlDbType.VarChar, 50);
                objParam[9].Value = model.DrivingLicense;

                objParam[10] = new SqlParameter("@DriverLoginID", SqlDbType.VarChar, 50);
                objParam[10].Value = model.DriverLoginID;

                objParam[11] = new SqlParameter("@IsActive", SqlDbType.Bit, 10);
                objParam[11].Value = model.IsActive;

                objParam[12] = new SqlParameter("@Warehouse", SqlDbType.VarChar, 10);
                objParam[12].Value = model.Warehouse;

                objParam[13] = new SqlParameter("@DriverImage", SqlDbType.VarChar, 150);
                objParam[13].Value = model.DriverImage;

                MessageReturn = daRepo.ExecuteOutParamProc(ApiConstant.SPocCRUDDriver, objParam);

                log.Debug("Debug logging: DriverRepository - SPocCRUDDriver -" + model.ActionMode.ToString());

                return MessageReturn;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
            }

            return MessageReturn;
        }
    }
}
