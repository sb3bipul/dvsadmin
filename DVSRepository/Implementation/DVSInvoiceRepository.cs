﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using DVSDomain.Model;
using DVSRepository.Helper;
using DVSRepository.Interface;
using DVSRepository.ViewModel;

namespace DVSRepository.Implementation
{
    public class DVSInvoiceRepository : IDVSInvoiceRepository
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string apiConnectionString;
        DataAccessReposity daRepo = null;
        DataTable dt = null;
        private string MessageReturn = "";

        public Tuple<List<DVSInvoiceViewModel>, int> GetDVSInvoiceListForAdmin(SearchDVSInvoice model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            DVSInvoiceViewModel obj = null;
            SqlParameter[] objParam;
            int totalRecordNo = 0;
            List<DVSInvoiceViewModel> lstobj = new List<DVSInvoiceViewModel>();

            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[5];

                objParam[0] = new SqlParameter("@FromDate", SqlDbType.DateTime);
                objParam[0].Value = (string.IsNullOrEmpty(model.Fromdate) ? DBNull.Value : (object)model.Fromdate);//(model.FromDate == DateTime.MinValue ? DBNull.Value : (object)model.FromDate);

                objParam[1] = new SqlParameter("@ToDate", SqlDbType.DateTime);
                objParam[1].Value = (string.IsNullOrEmpty(model.Todate) ? DBNull.Value : (object)model.Todate);//(model.ToDate == DateTime.MinValue ? DBNull.Value : (object)model.ToDate);

                objParam[2] = new SqlParameter("@DriverId", SqlDbType.VarChar, 100);
                objParam[2].Value = (string.IsNullOrEmpty(model.DriverId) ? DBNull.Value : (object)model.DriverId);

                objParam[3] = new SqlParameter("@InvoiceNo", SqlDbType.VarChar, 100);
                objParam[3].Value = (string.IsNullOrEmpty(model.InvoiceNo) ? DBNull.Value : (object)model.InvoiceNo);

                objParam[4] = new SqlParameter("@Status", SqlDbType.VarChar, 100);
                objParam[4].Value = (string.IsNullOrEmpty(model.Status) ? DBNull.Value : (object)model.Status);


                dt = daRepo.GetDataTable(ApiConstant.UploadedInvoiceListForAdmin, objParam);

                foreach (DataRow dr in dt.Rows)
                {
                    obj = new DVSInvoiceViewModel();
                    obj.CustomerId = Convert.ToString(dr["CustomerId"]);
                    obj.Name = Convert.ToString(dr["Name"]).Trim();
                    obj.InvoiceNo = Convert.ToString(dr["InvoiceNo"]);
                    obj.DriverId = Convert.ToString(dr["DriverId"]);
                    obj.DriverName = Convert.ToString(dr["DriverName"]);
                    obj.SignatoryName = Convert.ToString(dr["SignatoryName"]);
                    obj.Status = Convert.ToString(dr["Status"]).Trim();
                    obj.UploadDate = Convert.ToDateTime(dr["UploadDate"]).ToString("MM-dd-yyyy");
                    obj.SignImage = Convert.ToString(dr["SignImage"]);
                    obj.InvoicePdf = Convert.ToString(dr["InvoicePdf"]);
                    lstobj.Add(obj);

                    log.Debug("Debug logging: DVSInvoiceRepo - GetDVSInvoiceList");
                }
                if (dt.Rows.Count > 0)
                {
                    totalRecordNo = Convert.ToInt32(dt.Rows[0]["TotalRowNo"]);
                }

                return new Tuple<List<DVSInvoiceViewModel>, int>(lstobj, totalRecordNo);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }

            return new Tuple<List<DVSInvoiceViewModel>, int>(lstobj, totalRecordNo);
        }


        public string InsertInvoiceStatusHistory(InvoiceStatusHistory model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            SqlParameter[] objParam;
            apiConnectionString = apiConnectionAuthString;
            MessageReturn = "";
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[3];


                objParam[0] = new SqlParameter("@InvoiceNo", SqlDbType.VarChar, 100);
                objParam[0].Value = (string.IsNullOrEmpty(model.InvoiceNo) ? DBNull.Value : (object)model.InvoiceNo);

                objParam[1] = new SqlParameter("@RejectReason", SqlDbType.NVarChar, 500);
                objParam[1].Value = (string.IsNullOrEmpty(model.RejectReason) ? DBNull.Value : (object)model.RejectReason);

                objParam[2] = new SqlParameter("@Status", SqlDbType.VarChar, 100);
                objParam[2].Value = (string.IsNullOrEmpty(model.Status) ? DBNull.Value : (object)model.Status);

                MessageReturn = daRepo.ExecuteOutParamProc(ApiConstant.InsertInvoiceStatus, objParam);

                return MessageReturn;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
            }

            return MessageReturn;
        }

        public string UpdateInvoiceInfo(EditInvoiceInfo model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            SqlParameter[] objParam;
            apiConnectionString = apiConnectionAuthString;
            MessageReturn = "";
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[4];


                objParam[0] = new SqlParameter("@NewInvoiceNo", SqlDbType.VarChar, 100);
                objParam[0].Value = (string.IsNullOrEmpty(model.NewInvoiceNo) ? DBNull.Value : (object)model.NewInvoiceNo);

                objParam[1] = new SqlParameter("@InvoiceNo", SqlDbType.NVarChar, 500);
                objParam[1].Value = (string.IsNullOrEmpty(model.InvoiceNo) ? DBNull.Value : (object)model.InvoiceNo);

                objParam[2] = new SqlParameter("@CustomerId", SqlDbType.VarChar, 100);
                objParam[2].Value = (string.IsNullOrEmpty(model.CustomerId) ? DBNull.Value : (object)model.CustomerId);

                objParam[3] = new SqlParameter("@InvoiceComment", SqlDbType.VarChar, 100);
                objParam[3].Value = (string.IsNullOrEmpty(model.InvoiceComment) ? DBNull.Value : (object)model.InvoiceComment);

                MessageReturn = daRepo.ExecuteOutParamProc(ApiConstant.UpdateInvoiceInfo, objParam);

                return MessageReturn;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
            }

            return MessageReturn;
        }

        public Tuple<List<StatusChangeHistoryViewModel>, int> GetInvoiceStatusChangeHistory(SearchDVSInvoice model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            StatusChangeHistoryViewModel obj = null;
            SqlParameter[] objParam;
            int totalRecordNo = 0;
            List<StatusChangeHistoryViewModel> lstobj = new List<StatusChangeHistoryViewModel>();

            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@InvoiceNo", SqlDbType.VarChar, 100);
                objParam[0].Value = (string.IsNullOrEmpty(model.InvoiceNo) ? DBNull.Value : (object)model.InvoiceNo);


                dt = daRepo.GetDataTable(ApiConstant.GetInvoiceStatusChangeHistory, objParam);

                foreach (DataRow dr in dt.Rows)
                {
                    obj = new StatusChangeHistoryViewModel();
                    obj.InvoiceNo = Convert.ToString(dr["InvoiceNo"]);
                    obj.Status = Convert.ToString(dr["Status"]);
                    obj.RejectReason = Convert.ToString(dr["RejectReason"]);
                    obj.UpdateDate = Convert.ToDateTime(dr["UpdateDate"]).ToString("MM-dd-yyyy");
                    obj.StatusCode = Convert.ToString(dr["StatusCode"]);
                    lstobj.Add(obj);

                    log.Debug("Debug logging: DVSInvoiceRepo - GetInvoiceStatusChangeHistory");
                }
                if (dt.Rows.Count > 0)
                {
                    totalRecordNo = Convert.ToInt32(dt.Rows[0]["TotalRowNo"]);
                }

                return new Tuple<List<StatusChangeHistoryViewModel>, int>(lstobj, totalRecordNo);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }

            return new Tuple<List<StatusChangeHistoryViewModel>, int>(lstobj, totalRecordNo);
        }

    }
}
