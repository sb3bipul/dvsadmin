﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using DVSDomain.Model;
using DVSRepository.Helper;
using DVSRepository.Interface;
using DVSRepository.ViewModel;

namespace DVSRepository.Implementation
{
    public class ARInvoiceRepository : IARInvoiceRepository
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string apiConnectionString;
        DataAccessReposity daRepo = null;
        DataTable dt = null;
        private string MessageReturn = "";

        public Tuple<List<ARInvoiceViewModel>, int> GetInsertedInvoiceList(SearchARInvoice model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            ARInvoiceViewModel obj = null;
            SqlParameter[] objParam;
            int totalRecordNo = 0;
            List<ARInvoiceViewModel> lstobj = new List<ARInvoiceViewModel>();

            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[6];

                objParam[0] = new SqlParameter("@FromDate", SqlDbType.DateTime);
                objParam[0].Value = (string.IsNullOrEmpty(model.FromDate) ? DBNull.Value : (object)model.FromDate);//(model.FromDate == DateTime.MinValue ? DBNull.Value : (object)model.FromDate);

                objParam[1] = new SqlParameter("@ToDate", SqlDbType.DateTime);
                objParam[1].Value = (string.IsNullOrEmpty(model.ToDate) ? DBNull.Value : (object)model.ToDate);//(model.ToDate == DateTime.MinValue ? DBNull.Value : (object)model.ToDate);

                objParam[2] = new SqlParameter("@City", SqlDbType.VarChar, 100);
                objParam[2].Value = (string.IsNullOrEmpty(model.City) ? DBNull.Value : (object)model.City);

                objParam[3] = new SqlParameter("@InvoiceNo", SqlDbType.VarChar, 100);
                objParam[3].Value = (string.IsNullOrEmpty(model.InvoiceNo) ? DBNull.Value : (object)model.InvoiceNo);

                objParam[4] = new SqlParameter("@OrderNo", SqlDbType.VarChar, 100);
                objParam[4].Value = (string.IsNullOrEmpty(model.OrderNo) ? DBNull.Value : (object)model.OrderNo);

                objParam[5] = new SqlParameter("@Status", SqlDbType.VarChar, 100);
                objParam[5].Value = (string.IsNullOrEmpty(model.Status) ? DBNull.Value : (object)model.Status);


                dt = daRepo.GetDataTable(ApiConstant.GetARInvoiceList, objParam);

                foreach (DataRow dr in dt.Rows)
                {

                    obj = new ARInvoiceViewModel();
                    obj.InvoiceNo = Convert.ToString(dr["InvoiceNo"]);
                    ///obj.InvoiceDate = Convert.ToDateTime(dr["InvoiceDate"]).ToString("yyyy-MM-dd");
                    obj.InvoicePDF = Convert.ToString(dr["InvoicePDF"]).Trim();
                    obj.OrderNo = Convert.ToString(dr["OrderNo"]);
                    obj.OrderDate = Convert.ToDateTime(dr["OrderDate"]).ToString("MM-dd-yy");
                    obj.CustomerNo = Convert.ToString(dr["CustomerNo"]);
                    obj.CustName = Convert.ToString(dr["CustName"]);
                    obj.City = Convert.ToString(dr["City"]).Trim();
                    obj.DriverID = Convert.ToString(dr["DriverID"]);
                    obj.TripID = Convert.ToString(dr["TripID"]);
                    obj.Status = Convert.ToString(dr["Status"]);
                    obj.PONumber = Convert.ToString(dr["PONumber"]);
                    obj.BasketID = Convert.ToString(dr["BasketID"]);
                    obj.OrderAmount = Convert.ToString(dr["OrderAmount"]);
                    obj.IsDSD = Convert.ToString(dr["IsDSD"]);
                    obj.CheckerID = Convert.ToString(dr["CheckerID"]);
                    obj.StopID = Convert.ToString(dr["StopID"]);
                    obj.TruckID = Convert.ToString(dr["TruckID"]);
                    obj.BrokerID = Convert.ToString(dr["BrokerID"]);
                    lstobj.Add(obj);

                    log.Debug("Debug logging: ARInvoiceRepo - GetInsertedInvoiceList");
                }
                if (dt.Rows.Count > 0)
                {
                    totalRecordNo = Convert.ToInt32(dt.Rows[0]["TotalRowNo"]);
                }

                return new Tuple<List<ARInvoiceViewModel>, int>(lstobj, totalRecordNo);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }

            return new Tuple<List<ARInvoiceViewModel>, int>(lstobj, totalRecordNo);
        }

        public string UpdateInvoiceStatus(approveInv model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            SqlParameter[] objParam;
            apiConnectionString = apiConnectionAuthString;
            MessageReturn = "";
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[3];


                objParam[0] = new SqlParameter("@InvoiceNo", SqlDbType.VarChar, 100);
                objParam[0].Value = (string.IsNullOrEmpty(model.InvoiceNo) ? DBNull.Value : (object)model.InvoiceNo);

                objParam[1] = new SqlParameter("@RejectReason", SqlDbType.NVarChar, 500);
                objParam[1].Value = (string.IsNullOrEmpty(model.RejectReason) ? DBNull.Value : (object)model.RejectReason);

                objParam[2] = new SqlParameter("@Action", SqlDbType.VarChar, 100);
                objParam[2].Value = (string.IsNullOrEmpty(model.Action) ? DBNull.Value : (object)model.Action);

                MessageReturn = daRepo.ExecuteOutParamProc(ApiConstant.UpdateInvoiceStatus, objParam);

                return MessageReturn;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
            }

            return MessageReturn;
        }

        public Tuple<List<ARCustCityViewModel>, int> GetCustCity(string apiConnectionAuthString)
        {
            dt = new DataTable();
            ARCustCityViewModel obj = null;
            int totalRecordNo = 0;
            List<ARCustCityViewModel> lstobj = new List<ARCustCityViewModel>();
            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                dt = daRepo.GetDataTable(ApiConstant.GetCustomerCity);
                foreach (DataRow dr in dt.Rows)
                {
                    obj = new ARCustCityViewModel();
                    obj.City = Convert.ToString(dr["City"]).Trim();
                    lstobj.Add(obj);

                    log.Debug("Debug logging: ARInvoiceRepo - GetCustCity");
                }
                if (dt.Rows.Count > 0)
                {
                    totalRecordNo = Convert.ToInt32(dt.Rows[0]["TotalRowNo"]);
                }
                return new Tuple<List<ARCustCityViewModel>, int>(lstobj, totalRecordNo);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }
            return new Tuple<List<ARCustCityViewModel>, int>(lstobj, totalRecordNo);
        }

        public Tuple<List<ARCSVDataViewModel>, int> GetDataForCSvExport(SearchARInvoice model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            ARCSVDataViewModel obj = null;
            SqlParameter[] objParam;
            int totalRecordNo = 0;
            List<ARCSVDataViewModel> lstobj = new List<ARCSVDataViewModel>();

            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[6];

                objParam[0] = new SqlParameter("@FromDate", SqlDbType.DateTime);
                objParam[0].Value = (string.IsNullOrEmpty(model.FromDate) ? DBNull.Value : (object)model.FromDate);//(model.FromDate == DateTime.MinValue ? DBNull.Value : (object)model.FromDate);

                objParam[1] = new SqlParameter("@ToDate", SqlDbType.DateTime);
                objParam[1].Value = (string.IsNullOrEmpty(model.ToDate) ? DBNull.Value : (object)model.ToDate);//(model.ToDate == DateTime.MinValue ? DBNull.Value : (object)model.ToDate);

                objParam[2] = new SqlParameter("@City", SqlDbType.VarChar, 100);
                objParam[2].Value = (string.IsNullOrEmpty(model.City) ? DBNull.Value : (object)model.City);

                objParam[3] = new SqlParameter("@InvoiceNo", SqlDbType.VarChar, 100);
                objParam[3].Value = (string.IsNullOrEmpty(model.InvoiceNo) ? DBNull.Value : (object)model.InvoiceNo);

                objParam[4] = new SqlParameter("@OrderNo", SqlDbType.VarChar, 100);
                objParam[4].Value = (string.IsNullOrEmpty(model.OrderNo) ? DBNull.Value : (object)model.OrderNo);

                objParam[5] = new SqlParameter("@Status", SqlDbType.VarChar, 100);
                objParam[5].Value = (string.IsNullOrEmpty(model.Status) ? DBNull.Value : (object)model.Status);


                dt = daRepo.GetDataTable(ApiConstant.GetDataForCSVExport, objParam);

                foreach (DataRow dr in dt.Rows)
                {
                    obj = new ARCSVDataViewModel();
                    obj.InvoiceNo = Convert.ToString(dr["InvoiceNo"]);
                    obj.OrderNo = Convert.ToString(dr["OrderNo"]);
                    obj.OrderDate = Convert.ToDateTime(dr["OrderDate"]).ToString("MM-dd-yy");
                    obj.CustomerNo = Convert.ToString(dr["CustomerNo"]);
                    obj.CustName = Convert.ToString(dr["CustName"]);
                    obj.City = Convert.ToString(dr["City"]).Trim();
                    obj.DriverID = Convert.ToString(dr["DriverID"]);
                    obj.TripID = Convert.ToString(dr["TripID"]);
                    obj.Status = Convert.ToString(dr["Status"]);
                    lstobj.Add(obj);

                    log.Debug("Debug logging: ARInvoiceRepo - GetDataForCSvExport");
                }
                if (dt.Rows.Count > 0)
                {
                    totalRecordNo = Convert.ToInt32(dt.Rows[0]["TotalRowNo"]);
                }

                return new Tuple<List<ARCSVDataViewModel>, int>(lstobj, totalRecordNo);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }

            return new Tuple<List<ARCSVDataViewModel>, int>(lstobj, totalRecordNo);
        }
    }
}
