﻿using DVSDomain.Model;
using DVSRepository.Helper;
using DVSRepository.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DVSRepository.Implementation
{
    public class CustomerRepository : ICustomerRepository
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string apiConnectionString;
        DataAccessReposity daRepo = null;
        DataTable dt = null;
        public Tuple<List<Customer>, int> GetCustomer(GetDataBindingModel model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            SqlParameter[] objParam;
            List<Customer> lstobj = new List<Customer>();
            Customer obj = null;
            int totalRecordNo = 0;
            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[6];

                objParam[0] = new SqlParameter("@Id", SqlDbType.Int);
                objParam[0].Value = model.Id;

                objParam[1] = new SqlParameter("@SearchText", SqlDbType.VarChar, 250);
                objParam[1].Value = model.SearchText;

                objParam[2] = new SqlParameter("@PageNo", SqlDbType.Int);
                objParam[2].Value = model.PageNo;

                objParam[3] = new SqlParameter("@PageSize", SqlDbType.Int);
                objParam[3].Value = model.PageSize;

                objParam[4] = new SqlParameter("@SortBy", SqlDbType.VarChar, 100);
                objParam[4].Value = model.SortBy;

                objParam[5] = new SqlParameter("@SortOrder", SqlDbType.VarChar, 100);
                objParam[5].Value = model.SortOrder;

                dt = daRepo.GetDataTable(ApiConstant.SPocGetCustomer, objParam);

                foreach (DataRow dr in dt.Rows)
                {
                    obj = new Customer();
                    obj.UserID = dr["UserID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserID"]);
                    obj.CompanyId = dr["CompanyId"] == DBNull.Value ? 0 : Convert.ToInt32(dr["CompanyId"]);
                    obj.BillingCO = dr["BillingCO"] == DBNull.Value ? "" : Convert.ToString(dr["BillingCO"]); 
                    obj.Zone = Convert.ToString(dr["Zone"]);
                    obj.Name = Convert.ToString(dr["Name"]);
                    obj.Street = Convert.ToString(dr["Street"]);
                    obj.City = Convert.ToString(dr["City"]);
                    obj.State = Convert.ToString(dr["State"]);
                    obj.Zip = Convert.ToString(dr["Zip"]);
                    obj.Country = Convert.ToInt32(dr["Country"]);
                    obj.Region = Convert.ToString(dr["Region"]);
                    obj.ContactNo = Convert.ToString(dr["ContactNo"]);
                    obj.EmailID = Convert.ToString(dr["EmailID"]);
                    obj.Address = Convert.ToString(dr["Address"]);
                    //obj.Organization = Convert.ToString(dr["Organization"]);
                    //obj.ContactPersonName = Convert.ToString(dr["ContactPersonName"]);
                    //obj.ContactPersonEmail = Convert.ToString(dr["ContactPersonEmail"]);
                    //obj.WHCode = Convert.ToString(dr["WHCode"]);
                    //obj.TelxonNo = Convert.ToString(dr["TelxonNo"]);
                    //obj.Password = Convert.ToString(dr["Password"]);
                    //obj.UserType = Convert.ToString(dr["UserType"]);
                    //obj.IMEINumber = Convert.ToString(dr["IMEINumber"]); 
                    ////dr["DateAuthentication"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dr["DateAuthentication"]);
                    //obj.DepartmentID = dr["DepartmentID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["DepartmentID"]);
                    //obj.SalesManID = dr["SalesManID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SalesManID"]);
                    //obj.IsAuthenticated = dr["IsAuthenticated"] == DBNull.Value ? false : Convert.ToBoolean(dr["IsAuthenticated"]); 
                    //obj.AuthenticatedOn = dr["AuthenticatedOn"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dr["AuthenticatedOn"]);
                    //obj.UserIsDeleted = dr["UserIsDeleted"] == DBNull.Value ? false : Convert.ToBoolean(dr["UserIsDeleted"]);
                    //obj.AuthenticatedBy = dr["AuthenticatedBy"] == DBNull.Value ? "0" : Convert.ToString(dr["AuthenticatedBy"]); 
                    //obj.CreditLimit = dr["CreditLimit"] == DBNull.Value ? Convert.ToDecimal(0.00) : Convert.ToDecimal(dr["CreditLimit"]);
                    obj.ContractNo = Convert.ToString(dr["ContactNo"]);
                    //obj.BRROUT = Convert.ToString(dr["BRROUT"]);
                    //obj.WHCompany = Convert.ToString(dr["WHCompany"]);
                    //obj.LastTimeUpdate = dr["LastTimeUpdate"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dr["LastTimeUpdate"]);
                    //obj.AR30DAYS = dr["AR30DAYS"] == DBNull.Value ? Convert.ToDecimal(0.00) : Convert.ToDecimal(dr["AR30DAYS"]);
                    //obj.AR30DAYS = dr["ARTOTAL"] == DBNull.Value ? Convert.ToDecimal(0.00) : Convert.ToDecimal(dr["ARTOTAL"]); 
                    //obj.srl = dr["srl"] == DBNull.Value ? 0 : Convert.ToInt32(dr["srl"]); 
                    //obj.Latitude = dr["Latitude"] == DBNull.Value ? Convert.ToDecimal(0.00) : Convert.ToDecimal(dr["Latitude"]);
                    //obj.Longitude = dr["Longitude"] == DBNull.Value ? Convert.ToDecimal(0.00) : Convert.ToDecimal(dr["Longitude"]);
                    ////obj.StoreEntranceLocation = dr["StoreEntranceLocation"] == DBNull.Value ? Convert.ToDecimal(0.00) : Convert.ToDecimal(dr["StoreEntranceLocation"]);
                    //obj.StoreEntranceLatitude =  dr["StoreEntranceLatitude"] == DBNull.Value ? Convert.ToDecimal(0.00) : Convert.ToDecimal(dr["StoreEntranceLatitude"]);
                    //obj.StoreEntranceLongitude = dr["StoreEntranceLongitude"] == DBNull.Value ? Convert.ToDecimal(0.00) : Convert.ToDecimal(dr["StoreEntranceLongitude"]);
                    ////obj.ReceivingEntranceLocation = dr["ReceivingEntranceLocation"] == DBNull.Value ? Convert.ToDecimal(0.00) : Convert.ToDecimal(dr["ReceivingEntranceLocation"]);
                    //obj.ReceivingEntranceLatitude = dr["ReceivingEntranceLatitude"] == DBNull.Value ? Convert.ToDecimal(0.00) : Convert.ToDecimal(dr["ReceivingEntranceLatitude"]);
                    //obj.ReceivingEntranceLongitude = dr["ReceivingEntranceLongitude"] == DBNull.Value ? Convert.ToDecimal(0.00) : Convert.ToDecimal(dr["ReceivingEntranceLongitude"]);
                    //obj.CustomerImage = Convert.ToString(dr["CustomerImage"]);
                    //obj.CustomerImagePath = Convert.ToString(dr["CustomerImagePath"]);
                    ////obj.DateAuthentication = dr["DateAuthentication"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dr["DateAuthentication"]);
                    //obj.Location = Convert.ToString(dr["Location"]);
                    //obj.Website = Convert.ToString(dr["Website"]);
                    //obj.IsInsUpd = dr["IsInsUpd"] == DBNull.Value ? false : Convert.ToBoolean(dr["IsInsUpd"]);
                    //obj.CSCreditHold = Convert.ToString(dr["CSCreditHold"]);
                    //obj.Terms = Convert.ToString(dr["Terms"]);
                    //obj.EdiStore = Convert.ToString(dr["EdiStore"]);
                    //obj.StoreImage = Convert.ToString(dr["StoreImage"]);
                    //obj.StoreImagePath = Convert.ToString(dr["StoreImagePath"]);
                    //obj.BillingAddress = Convert.ToString(dr["BillingAddress"]);
                    //obj.ShippingAddress = Convert.ToString(dr["ShippingAddress"]);
                    //obj.SpecificCustomer = dr["SpecificCustomer"] == DBNull.Value ? false : Convert.ToBoolean(dr["SpecificCustomer"]);

                    lstobj.Add(obj);
                }
                if (dt.Rows.Count > 0)
                {
                    totalRecordNo = Convert.ToInt32(dt.Rows[0]["TotalRowNo"]);
                }
                log.Debug("Debug logging: CustomerRepository - GetCustomer");

                return new Tuple<List<Customer>, int>(lstobj, totalRecordNo);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }

            return new Tuple<List<Customer>, int>(lstobj, totalRecordNo);
        }
        public Tuple<List<Customer>, int> GetCustomers(GetDataBindingModel model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            SqlParameter[] objParam;
            List<Customer> lstobj = new List<Customer>();
            Customer obj = null;
            int totalRecordNo = 0;
            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[6];

                objParam[0] = new SqlParameter("@Id", SqlDbType.Int);
                objParam[0].Value = model.Id;

                objParam[1] = new SqlParameter("@SearchText", SqlDbType.VarChar, 250);
                objParam[1].Value = model.SearchText;

                objParam[2] = new SqlParameter("@PageNo", SqlDbType.Int);
                objParam[2].Value = model.PageNo;

                objParam[3] = new SqlParameter("@PageSize", SqlDbType.Int);
                objParam[3].Value = model.PageSize;

                objParam[4] = new SqlParameter("@SortBy", SqlDbType.VarChar, 100);
                objParam[4].Value = model.SortBy;

                objParam[5] = new SqlParameter("@SortOrder", SqlDbType.VarChar, 100);
                objParam[5].Value = model.SortOrder;

                dt = daRepo.GetDataTable(ApiConstant.SPocGetCustomer, objParam);

                foreach (DataRow dr in dt.Rows)
                {
                    obj = new Customer();
                    obj.UserID = dr["UserID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserID"]);
                    obj.CompanyId = dr["CompanyId"] == DBNull.Value ? 0 : Convert.ToInt32(dr["CompanyId"]);
                    obj.Name = Convert.ToString(dr["Name"]);
                    obj.Street = Convert.ToString(dr["Street"]);
                    obj.City = Convert.ToString(dr["City"]);
                    obj.State = Convert.ToString(dr["State"]);
                    obj.Zip = Convert.ToString(dr["Zip"]);
                    obj.Country = Convert.ToInt32(dr["Country"]);
                    obj.Region = Convert.ToString(dr["Region"]);
                    obj.ContactNo = Convert.ToString(dr["ContactNo"]);
                    obj.EmailID = Convert.ToString(dr["EmailID"]);
                    obj.Address = Convert.ToString(dr["Address"]);


                    lstobj.Add(obj);
                }
                if (dt.Rows.Count > 0)
                {
                    totalRecordNo = Convert.ToInt32(dt.Rows[0]["TotalRowNo"]);
                }
                log.Debug("Debug logging: CustomerRepository - GetCustomer");

                return new Tuple<List<Customer>, int>(lstobj, totalRecordNo);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }

            return new Tuple<List<Customer>, int>(lstobj, totalRecordNo);
        }
    }
}
