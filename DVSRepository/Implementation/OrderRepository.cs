﻿using DVSDomain.Model;
using DVSRepository.Helper;
using DVSRepository.Interface;
using DVSRepository.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DVSRepository.Implementation
{
    public class OrderRepository : IOrderRepository
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string apiConnectionString;
        DataAccessReposity daRepo = null;
        DataTable dt = null;
        private string MessageReturn = "";
        public Tuple<List<OrderInfoViewModel>, int> GetAllOrderInfo(GetOrderInfoDataBindingModel model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            OrderInfoViewModel obj = null;
            SqlParameter[] objParam;
            int totalRecordNo = 0;
            List<OrderInfoViewModel> lstobj = new List<OrderInfoViewModel>();

            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@SearchText", SqlDbType.VarChar, 250);
                objParam[0].Value = model.SearchText;

                dt = daRepo.GetDataTable(ApiConstant.SPocGetOrderInfo);

                foreach (DataRow dr in dt.Rows)
                {

                    obj = new OrderInfoViewModel();
                    obj.OrderId = Convert.ToString(dr["PONumber"]).Trim();
                    obj.OrderDate = Convert.ToDateTime(dr["OrderDate"]).ToString("MM/dd/yyyy");
                    obj.CustomerName = Convert.ToString(dr["CustomerName"]).Trim();
                    obj.Address = Convert.ToString(dr["Address"]);
                    obj.DistributorName = Convert.ToString(dr["DistributorName"]);
                    obj.Zone = Convert.ToString(dr["Zone"]);
                    obj.ZoneName = Convert.ToString(dr["ZoneName"]);
                    obj.ZoneCode = Convert.ToString(dr["ZoneCode"]);
                    obj.StatusText = Convert.ToString(dr["OrderStatus"]);
                    obj.TypeText = Convert.ToString(dr["Type"]);
                    lstobj.Add(obj);

                    log.Debug("Debug logging: TripRepository - GetAllOrderInfo");
                }
                if (dt.Rows.Count > 0)
                {
                    totalRecordNo = Convert.ToInt32(dt.Rows[0]["TotalRowNo"]);
                }

                return new Tuple<List<OrderInfoViewModel>, int>(lstobj, totalRecordNo);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }

            return new Tuple<List<OrderInfoViewModel>, int>(lstobj, totalRecordNo);
        }
        public Tuple<OrderDetailViewModel, int> GetOrderDetail(GetOrderDetailBindingModel model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            OrderDetailViewModel objReturn = null;
            OrderDetailItemsViewModel obj = null;
            SqlParameter[] objParam;
            int totalRecordNo = 0;
            List<OrderDetailItemsViewModel> lstobj = new List<OrderDetailItemsViewModel>();

            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@PONumber", SqlDbType.VarChar, 250);
                objParam[0].Value = model.PONumber;

                dt = daRepo.GetDataTable(ApiConstant.SPocGetOrderDetail, objParam);
                int rowCount = 0;
                objReturn = new OrderDetailViewModel();
                foreach (DataRow dr in dt.Rows)
                {
                    if(rowCount == 0)
                    {
                        
                        objReturn.OrderId = Convert.ToString(dr["PONumber"]).Trim();
                        objReturn.OrderStatusName = (DBNull.Value.Equals(dr["OrderStatusName"]) ? "" : Convert.ToString(dr["OrderStatusName"]).Trim());
                        objReturn.OrderDate = (DBNull.Value.Equals(dr["OrderDate"]) ? "" : Convert.ToDateTime(dr["OrderDate"]).ToString("MM/dd/yyyy"));
                        objReturn.OrderStatus = (DBNull.Value.Equals(dr["OrderStatus"]) ? "" : Convert.ToString(dr["OrderStatus"]).Trim());
                        objReturn.SalesmanId = (DBNull.Value.Equals(dr["SalesmanId"]) ? "" : Convert.ToString(dr["SalesmanId"]));
                        objReturn.CustomerName = (DBNull.Value.Equals(dr["CustomerName"]) ? "" : Convert.ToString(dr["CustomerName"]));
                        objReturn.Address = (DBNull.Value.Equals(dr["Address"]) ? "" : Convert.ToString(dr["Address"]));
                        objReturn.CustomerId = (DBNull.Value.Equals(dr["CustomerId"]) ? "" : Convert.ToString(dr["CustomerId"]));
                        objReturn.BillAmount = (DBNull.Value.Equals(dr["BillAmount"]) ? "" : Convert.ToString(dr["BillAmount"]));
                    }
                    obj = new OrderDetailItemsViewModel();
                    obj.ItemName = (DBNull.Value.Equals(dr["ItemName"]) ? "" : Convert.ToString(dr["ItemName"]).Trim());
                    obj.GrossWeight = (DBNull.Value.Equals(dr["GrossWeight"]) ? "" : Convert.ToString(dr["GrossWeight"]).Trim());
                    obj.ListPrice = (DBNull.Value.Equals(dr["ListPrice"]) ? "" : Convert.ToString(dr["ListPrice"]).Trim());
                    obj.ActualPrice = (DBNull.Value.Equals(dr["ActualPrice"]) ? "" : Convert.ToString(dr["ActualPrice"]));
                    obj.OrdQty = (DBNull.Value.Equals(dr["OrdQty"]) ? 0 : Convert.ToInt32(dr["OrdQty"]));
                    //obj.DeliveredQty = (!DBNull.Value.Equals(dr["DeliveredQty"]) ? 0 : Convert.ToInt32(dr["DeliveredQty"]));
                    //obj.ReturnQty = (!DBNull.Value.Equals(dr["ReturnQty"]) ? 0 : Convert.ToInt32(dr["ReturnQty"]));
                    //obj.ExtraQty = (!DBNull.Value.Equals(dr["ExtraQty"]) ? 0 : Convert.ToInt32(dr["ExtraQty"]));
                    obj.DeliveredQty = 0;
                    obj.ReturnQty = 0;
                    obj.ExtraQty = 0;
                    obj.UOM = (DBNull.Value.Equals(dr["UOM"]) ? "" : Convert.ToString(dr["UOM"]));
                    lstobj.Add(obj);

                    rowCount++;
                    log.Debug("Debug logging: OrderRepository - GetOrderDetail");
                }
                if (dt.Rows.Count > 0)
                {
                    totalRecordNo = dt.Rows.Count;
                }
                objReturn.DetailItems = lstobj;
                return new Tuple<OrderDetailViewModel, int>(objReturn, totalRecordNo);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }

            return new Tuple<OrderDetailViewModel, int>(objReturn, totalRecordNo);
        }
    }
}
