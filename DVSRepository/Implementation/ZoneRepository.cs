﻿using DVSDomain.Model;
using DVSRepository.Helper;
using DVSRepository.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DVSRepository.Implementation
{
    public class ZoneRepository : IZoneRepository
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string apiConnectionString;
        DataAccessReposity daRepo = null;
        DataTable dt = null;
        private string MessageReturn = "";

        public Tuple<List<Zone>, int> GetZone(GetDataBindingModel model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            SqlParameter[] objParam;
            List<Zone> lstobj = new List<Zone>();
            Zone obj = null;
            int totalRecordNo = 0;
            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[6];

                objParam[0] = new SqlParameter("@Id", SqlDbType.Int);
                objParam[0].Value = model.Id;

                objParam[1] = new SqlParameter("@SearchText", SqlDbType.VarChar, 250);
                objParam[1].Value = model.SearchText;

                objParam[2] = new SqlParameter("@PageNo", SqlDbType.Int);
                objParam[2].Value = model.PageNo;

                objParam[3] = new SqlParameter("@PageSize", SqlDbType.Int);
                objParam[3].Value = model.PageSize;

                objParam[4] = new SqlParameter("@SortBy", SqlDbType.VarChar, 100);
                objParam[4].Value = model.SortBy;

                objParam[5] = new SqlParameter("@SortOrder", SqlDbType.VarChar, 100);
                objParam[5].Value = model.SortOrder;

                dt = daRepo.GetDataTable(ApiConstant.SPocGetZone, objParam);

                foreach (DataRow dr in dt.Rows)
                {
                    obj = new Zone();
                    obj.Id = Convert.ToInt32(dr["Id"]);
                    obj.ZoneCode = Convert.ToString(dr["ZoneCode"]);
                    obj.ZoneName = Convert.ToString(dr["ZoneName"]);
                    obj.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    obj.CreatedBy = Convert.ToString(dr["CreatedBy"]);
                    obj.CreatedAt = dr["CreatedAt"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dr["CreatedAt"]);
                    obj.IsDeleted = Convert.ToBoolean(dr["IsDeleted"]);
                    obj.DeletedAt = dr["DeletedAt"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dr["DeletedAt"]);
                    lstobj.Add(obj);
                }
                if (dt.Rows.Count > 0)
                {
                    totalRecordNo = Convert.ToInt32(dt.Rows[0]["TotalRowNo"]);
                }
                log.Debug("Debug logging: ZoneRepository - GetZone");

                return new Tuple<List<Zone>, int>(lstobj, totalRecordNo);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }

            return new Tuple<List<Zone>, int>(lstobj, totalRecordNo);
        }

        public string CRUDZone(ZoneBindingModel model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            SqlParameter[] objParam;
            apiConnectionString = apiConnectionAuthString;
            MessageReturn = "";
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[6];

                objParam[0] = new SqlParameter("@ActionMode", SqlDbType.Int);
                objParam[0].Value = model.ActionMode;

                objParam[1] = new SqlParameter("@Id", SqlDbType.Int);
                objParam[1].Value = model.Id;

                objParam[2] = new SqlParameter("@ZoneCode", SqlDbType.NVarChar, 100);
                objParam[2].Value = model.ZoneCode;

                objParam[3] = new SqlParameter("@ZoneName", SqlDbType.NVarChar, 100);
                objParam[3].Value = model.ZoneName;

                objParam[4] = new SqlParameter("@IsActive", SqlDbType.Bit, 100);
                objParam[4].Value = model.IsActive;

                objParam[5] = new SqlParameter("@IsDeleted", SqlDbType.Bit, 100);
                objParam[5].Value = model.IsDeleted;

                MessageReturn = daRepo.ExecuteOutParamProc(ApiConstant.SPocCRUDZone, objParam);



                log.Debug("Debug logging: ZoneRepository - CRUDZone -" + model.ActionMode.ToString());

                return MessageReturn;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
            }

            return MessageReturn;
        }
    }
}
