﻿using DVSDomain.Model;
using DVSRepository.Helper;
using DVSRepository.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DVSRepository.Implementation
{
    public class SalesmanRepository : ISalesmanRepository
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string apiConnectionString;
        DataAccessReposity daRepo = null;
        DataTable dt = null;
        public Tuple<List<Salesman>, int> GetSalesMan(GetDataBindingModel model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            SqlParameter[] objParam;
            List<Salesman> lstobj = new List<Salesman>();
            Salesman obj = null;
            int totalRecordNo = 0;
            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[6];

                objParam[0] = new SqlParameter("@Id", SqlDbType.Int);
                objParam[0].Value = model.Id;

                objParam[1] = new SqlParameter("@SearchText", SqlDbType.VarChar, 250);
                objParam[1].Value = model.SearchText;

                objParam[2] = new SqlParameter("@PageNo", SqlDbType.Int);
                objParam[2].Value = model.PageNo;

                objParam[3] = new SqlParameter("@PageSize", SqlDbType.Int);
                objParam[3].Value = model.PageSize;

                objParam[4] = new SqlParameter("@SortBy", SqlDbType.VarChar, 100);
                objParam[4].Value = model.SortBy;

                objParam[5] = new SqlParameter("@SortOrder", SqlDbType.VarChar, 100);
                objParam[5].Value = model.SortOrder;

                dt = daRepo.GetDataTable(ApiConstant.SPocSalesMan, objParam);

                foreach (DataRow dr in dt.Rows)
                {
                    obj = new Salesman();
                    obj.UserID = dr["UserID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserID"]);
                    obj.CompanyId = dr["CompanyId"] == DBNull.Value ? 0 : Convert.ToInt32(dr["CompanyId"]);
                    obj.Name = Convert.ToString(dr["Name"]);
                    obj.Street = Convert.ToString(dr["Street"]);
                    obj.City = Convert.ToString(dr["City"]);
                    obj.State = Convert.ToString(dr["State"]);
                    obj.Zip = Convert.ToString(dr["Zip"]);
                    //obj.Country = Convert.ToInt32(dr["Country"]);
                    obj.Region = Convert.ToString(dr["WHCompany"]);
                    obj.ContactNo = Convert.ToString(dr["ContactNo"]);
                    obj.EmailID = Convert.ToString(dr["EmailID"]);
                    obj.Address = Convert.ToString(dr["Address"]);
                    obj.SrlNo = Convert.ToInt32(dr["SrlNo"]);


                    lstobj.Add(obj);
                }
                if (dt.Rows.Count > 0)
                {
                    totalRecordNo = Convert.ToInt32(dt.Rows[0]["TotalRowNo"]);
                }
                log.Debug("Debug logging: SalesmanRepository - GetSalesMan");

                return new Tuple<List<Salesman>, int>(lstobj, totalRecordNo);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }

            return new Tuple<List<Salesman>, int>(lstobj, totalRecordNo);
        }
    }
}
