﻿using DVSDomain.Model;
using DVSRepository.DBContext;
using DVSRepository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DVSRepository.Implementation
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        private AppDBContext context;
        public UserRepository(AppDBContext appDBContext) : base(appDBContext)
        {
            context = appDBContext;
        }
        public async Task<User> GetUser(User user)
        {
            return await context.Set<User>().FirstOrDefaultAsync(e => e.UserId == user.UserId && e.UserPassword == user.UserPassword);
        }
    }
}
