﻿using DVSDomain.Model;
using DVSRepository.Helper;
using DVSRepository.Interface;
using DVSRepository.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DVSRepository.Implementation
{
    public class TripRepository : ITripRepository
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string apiConnectionString;
        DataAccessReposity daRepo = null;
        DataTable dt = null;
        private string MessageReturn = "";
        public List<TripOrderViewModel> GetTripOrder(GetTripDataBindingModel model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            TripOrderViewModel obj = null;
            TripOrderDetailViewModel objdetail = null;
            SqlParameter[] objParam;
            List<TripOrderViewModel> lstobj = new List<TripOrderViewModel>();
            List<TripOrderDetailViewModel> lstdetailobj = new List<TripOrderDetailViewModel>();
            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[6];

                objParam[0] = new SqlParameter("@ZoneId", SqlDbType.Int);
                objParam[0].Value = model.ZoneId;

                objParam[1] = new SqlParameter("@SalesmanId", SqlDbType.Int);
                objParam[1].Value = model.SalesmanId;

                objParam[2] = new SqlParameter("@NoOfMonth", SqlDbType.Int);
                objParam[2].Value = model.NoOfMonth;

                objParam[3] = new SqlParameter("@SearchByCustomer", SqlDbType.VarChar, 250);
                objParam[3].Value = model.SearchByCustomer;

                objParam[4] = new SqlParameter("@SearchByCustomerAddress", SqlDbType.VarChar, 250);
                objParam[4].Value = model.SearchByCustomerAddress;

                objParam[5] = new SqlParameter("@OrderNo", SqlDbType.VarChar, 250);
                objParam[5].Value = model.OrderNo;

                dt = daRepo.GetDataTable(ApiConstant.SPocGetTripOrder, objParam);

                string customerName = "";
                int rowcount = 0;

                foreach (DataRow dr in dt.Rows)
                {
                    rowcount++;

                    if (customerName != Convert.ToString(dr["CustomerName"]).Trim())
                    {
                        if (customerName != "")
                        {
                            obj.Orders = lstdetailobj;
                            lstobj.Add(obj);
                            lstdetailobj = new List<TripOrderDetailViewModel>();
                        }

                        obj = new TripOrderViewModel();
                        obj.CustomerId = Convert.ToString(dr["CustomerId"]).Trim();
                        obj.IsSelected = false;
                        obj.StopCount = 0;
                        obj.OptStopCount = 0;
                        obj.CustomerName = Convert.ToString(dr["CustomerName"]).Trim();
                        obj.Address = Convert.ToString(dr["Address"]);
                        obj.ShippingAddress = Convert.ToString(dr["ShippingAddress"]);
                        obj.BillingAddress = Convert.ToString(dr["BillingAddress"]);
                        customerName = Convert.ToString(dr["CustomerName"]).Trim();
                        obj.DeliveryCharges = Convert.ToDecimal(0.00);
                        obj.CustomerNote = "";
                    }
                    objdetail = new TripOrderDetailViewModel();

                    objdetail.PONumber = Convert.ToString(dr["PONumber"]);
                    objdetail.OrderDate = Convert.ToDateTime(dr["OrderDate"]).ToString("MM/dd/yyyy");
                    //objdetail.BillAmount = Convert.ToDecimal(dr["BillAmount"]);
                    objdetail.BillAmount = Convert.ToDecimal(dr["TotalCasePrice"]);
                    objdetail.OrderStatus = Convert.ToString(dr["OrderStatus"]);
                    objdetail.OrdQty = Convert.ToInt32(dr["TotalOrdQty"]);
                    objdetail.GrossWeight = Convert.ToDecimal(dr["TotalGrossWeight"]);
                    objdetail.IsSelected = false;
                    //obj.lbs = Convert.ToString(dr["OrdQty"]) + '/' + Convert.ToString(dr["GrossWeight"]);
                    objdetail.lbs = Convert.ToString(dr["lbs"]);
                    objdetail.Priority = (Convert.IsDBNull(dr["Priority"]) ? 0 : Convert.ToInt32(dr["Priority"]));
                    objdetail.CstId = obj.CustomerId;
                    lstdetailobj.Add(objdetail);

                    if (rowcount == dt.Rows.Count)
                    {
                        obj.Orders = lstdetailobj;
                        lstobj.Add(obj);
                    }

                    log.Debug("Debug logging: TripRepository - GetTripOrder");
                }

                return lstobj;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }

            return lstobj;
        }

        public Tuple<List<TripInfoViewModel>, int> GetAllTripInfo(GetTripInfoDataBindingModel model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            TripInfoViewModel obj = null;
            SqlParameter[] objParam;
            int totalRecordNo = 0;
            List<TripInfoViewModel> lstobj = new List<TripInfoViewModel>();

            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[3];

                objParam[0] = new SqlParameter("@StartDate", SqlDbType.DateTime);
                objParam[0].Value = model.StartDate;

                objParam[1] = new SqlParameter("@TripStatus", SqlDbType.VarChar, 250);
                objParam[1].Value = model.TripStatus;

                objParam[2] = new SqlParameter("@SearchText", SqlDbType.VarChar, 250);
                objParam[2].Value = model.SearchText;

                dt = daRepo.GetDataTable(ApiConstant.SPocGetAllTrip, objParam);

                foreach (DataRow dr in dt.Rows)
                {

                    obj = new TripInfoViewModel();
                    obj.TripId = Convert.ToString(dr["TRIPID"]).Trim();
                    obj.TripName = Convert.ToString(dr["TRIPNAME"]).Trim();
                    obj.TripDate = Convert.ToDateTime(dr["TRIPDATE"]).ToString("MM/dd/yyyy");
                    obj.DriverName = Convert.ToString(dr["DRIVERNAME"]).Trim();
                    obj.NoOfStop = Convert.ToInt32(dr["NO_OF_STOP"]);
                    obj.TotalOrder = Convert.ToInt32(dr["TOTALORDER"]);
                    obj.OrderAmount = Convert.ToDecimal(dr["ORDERAMOUNT"]);
                    obj.StatusText = Convert.ToString(dr["STATUSTEXT"]);
                    lstobj.Add(obj);

                    log.Debug("Debug logging: TripRepository - GetAllTripInfo");
                }
                if (dt.Rows.Count > 0)
                {
                    totalRecordNo = Convert.ToInt32(dt.Rows[0]["TotalRowNo"]);
                }

                return new Tuple<List<TripInfoViewModel>, int>(lstobj, totalRecordNo);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }

            return new Tuple<List<TripInfoViewModel>, int>(lstobj, totalRecordNo);
        }
        public string CRUDTrip(TripDataBindingModel model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            DataTable dtTripDetail = new DataTable();
            SqlParameter[] objParam;
            apiConnectionString = apiConnectionAuthString;
            MessageReturn = "";
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[18];

                objParam[0] = new SqlParameter("@ActionMode", SqlDbType.Int);
                objParam[0].Value = model.ActionMode;

                objParam[1] = new SqlParameter("@TripID", SqlDbType.Int);
                objParam[1].Value = model.TripID;

                objParam[2] = new SqlParameter("@CompanyID", SqlDbType.Int);
                objParam[2].Value = model.CompanyID;

                objParam[3] = new SqlParameter("@TripName", SqlDbType.NVarChar, 100);
                objParam[3].Value = model.TripName;

                objParam[4] = new SqlParameter("@DriverID", SqlDbType.Int);
                objParam[4].Value = model.DriverID;

                objParam[5] = new SqlParameter("@WareHouse", SqlDbType.VarChar, 100);
                objParam[5].Value = model.WareHouse;

                objParam[6] = new SqlParameter("@Charges", SqlDbType.Decimal);
                objParam[6].Value = model.Charges;

                objParam[7] = new SqlParameter("@Priority", SqlDbType.Int);
                objParam[7].Value = model.Priority;

                objParam[8] = new SqlParameter("@SortTrip", SqlDbType.Int);
                objParam[8].Value = model.SortTrip;

                objParam[9] = new SqlParameter("@VehicleID", SqlDbType.Int);
                objParam[9].Value = model.VehicleID;

                objParam[10] = new SqlParameter("@TripStatus", SqlDbType.Int);
                objParam[10].Value = model.TripStatus;

                objParam[11] = new SqlParameter("@TripDesc", SqlDbType.VarChar, 1000);
                objParam[11].Value = model.TripDesc;

                objParam[12] = new SqlParameter("@IsActive", SqlDbType.Bit, 100);
                objParam[12].Value = model.IsActive;

                objParam[13] = new SqlParameter("@IsDeleted", SqlDbType.Bit, 100);
                objParam[13].Value = model.IsDeleted;

                objParam[14] = new SqlParameter("@StartLocation", SqlDbType.Int);
                objParam[14].Value = model.StartLocation;

                objParam[15] = new SqlParameter("@EndLocation", SqlDbType.Int);
                objParam[15].Value = model.EndLocation;

                objParam[16] = new SqlParameter("@TripCreated", SqlDbType.DateTime);
                objParam[16].Value = model.TripCreated;

                #region convert TripDetail list to datatable and add parameter
                dtTripDetail = ToDataTable<TripDetailDataBindingModel>(model.TripDetails);
                objParam[17] = new SqlParameter("@TripDetail", SqlDbType.Structured);
                objParam[17].Value = dtTripDetail;
                #endregion


                MessageReturn = daRepo.ExecuteOutParamProc(ApiConstant.SPocCRUDTrip, objParam);



                log.Debug("Debug logging: TripRepository - CRUDTrip -" + model.ActionMode.ToString());

                return MessageReturn;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
            }

            return MessageReturn;
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        public List<TripOrderViewModel> GetTripById(string TripId, string apiConnectionAuthString)
        {
            dt = new DataTable();
            TripOrderViewModel obj = null;
            TripOrderDetailViewModel objdetail = null;
            SqlParameter[] objParam;
            List<TripOrderViewModel> lstobj = new List<TripOrderViewModel>();
            List<TripOrderDetailViewModel> lstdetailobj = new List<TripOrderDetailViewModel>();
            List<TripOrderViewModel> SortedList = new List<TripOrderViewModel>();
            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@TripId", SqlDbType.VarChar, 250);
                objParam[0].Value = TripId;

                dt = daRepo.GetDataTable(ApiConstant.SPocGetTripById, objParam);

                string customerName = "";
                int rowcount = 0;
                decimal TotalBillAmount = Convert.ToDecimal(0.00);
                decimal TotalGrossWeight = Convert.ToDecimal(0.00);
                int setHiestPriority = dt.Rows.Count;
                int setPriority = dt.Rows.Count;
                foreach (DataRow dr in dt.Rows)
                {
                    rowcount++;
                    if (customerName != Convert.ToString(dr["CustomerName"]).Trim())
                    {
                        if (customerName != "")
                        {
                            obj.Orders = lstdetailobj;
                            lstobj.Add(obj);
                            lstdetailobj = new List<TripOrderDetailViewModel>();
                            setPriority = setHiestPriority;
                        }

                        obj = new TripOrderViewModel();
                        obj.CustomerId = Convert.ToString(dr["CustomerId"]).Trim();
                        obj.IsSelected = true;
                        obj.StopCount = (DBNull.Value.Equals(dr["Priority"]) ? 0 : Convert.ToInt32(dr["Priority"]));
                        obj.OptStopCount = (DBNull.Value.Equals(dr["Priority"]) ? 0 : Convert.ToInt32(dr["Priority"]));

                        if (obj.StopCount > 0)
                        {
                            setPriority = obj.StopCount;
                        }
                        obj.CustomerName = Convert.ToString(dr["CustomerName"]).Trim();
                        obj.Address = Convert.ToString(dr["Address"]);
                        obj.ShippingAddress = Convert.ToString(dr["ShippingAddress"]);
                        obj.BillingAddress = Convert.ToString(dr["BillingAddress"]);
                        customerName = Convert.ToString(dr["CustomerName"]).Trim();
                        obj.TripID = (DBNull.Value.Equals(dr["TripID"]) ? "0" : Convert.ToString(dr["TripID"]));
                        obj.TripName = (DBNull.Value.Equals(dr["TripName"]) ? "0" : Convert.ToString(dr["TripName"]));
                        obj.DriverID = (DBNull.Value.Equals(dr["DriverID"]) ? "0" : Convert.ToString(dr["DriverID"]));
                        obj.StartLocation = (DBNull.Value.Equals(dr["StartLocation"]) ? "1" : Convert.ToString(dr["StartLocation"]));
                        obj.EndLocation = (DBNull.Value.Equals(dr["EndLocation"]) ? "1" : Convert.ToString(dr["EndLocation"]));
                        //if (Convert.IsDBNull(dr["StartTime"]))
                        //{
                        //    obj.StartTime = DateTime.MinValue;
                        //}
                        //else
                        //{
                        //    obj.StartTime = Convert.ToDateTime(dr["StartTime"]);
                        //}
                        obj.StartTime = (Convert.IsDBNull(dr["StartTime"]) ? "" : Convert.ToDateTime(dr["StartTime"]).ToString("yyyy-MM-dd"));
                        obj.PriorityCount = setPriority;
                        obj.DeliveryCharges = Convert.ToDecimal(dr["DeliveryCharges"]);
                        obj.CustomerNote = Convert.ToString(dr["CustomerNote"]);

                    }
                    objdetail = new TripOrderDetailViewModel();

                    objdetail.PONumber = Convert.ToString(dr["PONumber"]);
                    objdetail.OrderDate = Convert.ToDateTime(dr["OrderDate"]).ToString("MM/dd/yyyy");
                    //objdetail.BillAmount = Convert.ToDecimal(dr["BillAmount"]);
                    objdetail.BillAmount = Convert.ToDecimal(dr["TotalCasePrice"]);
                    objdetail.OrderStatus = Convert.ToString(dr["OrderStatus"]);
                    objdetail.OrdQty = Convert.ToInt32(dr["TotalOrdQty"]);
                    objdetail.GrossWeight = Convert.ToDecimal(dr["TotalGrossWeight"]);
                    objdetail.IsSelected = true;
                    //obj.lbs = Convert.ToString(dr["OrdQty"]) + '/' + Convert.ToString(dr["GrossWeight"]);
                    objdetail.lbs = Convert.ToString(dr["lbs"]);
                    objdetail.Priority = (Convert.IsDBNull(dr["Priority"]) ? 0 : Convert.ToInt32(dr["Priority"]));
                    lstdetailobj.Add(objdetail);

                    TotalBillAmount = TotalBillAmount + Convert.ToDecimal(dr["BillAmount"]);
                    TotalGrossWeight = TotalGrossWeight + Convert.ToDecimal(dr["TotalGrossWeight"]);

                    if (rowcount == dt.Rows.Count)
                    {
                        obj.Orders = lstdetailobj;
                        lstobj.Add(obj);
                    }

                    log.Debug("Debug logging: TripRepository - GetTripOrder");
                }
                SortedList = lstobj.OrderBy(o => o.PriorityCount).ToList();
                return SortedList;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }

            return SortedList;
        }

        public List<TripOrderViewModel> GetAllOrderByTripId(string TripId, string apiConnectionAuthString)
        {
            dt = new DataTable();
            TripOrderViewModel obj = null;
            TripOrderDetailViewModel objdetail = null;
            SqlParameter[] objParam;
            List<TripOrderViewModel> lstobj = new List<TripOrderViewModel>();
            List<TripOrderDetailViewModel> lstdetailobj = new List<TripOrderDetailViewModel>();
            List<TripOrderViewModel> SortedList = new List<TripOrderViewModel>();
            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@TripId", SqlDbType.VarChar, 250);
                objParam[0].Value = TripId;

                dt = daRepo.GetDataTable(ApiConstant.SPocGetAllOrderByTripId, objParam);

                string customerName = "";
                int rowcount = 0;
                decimal TotalBillAmount = Convert.ToDecimal(0.00);
                decimal TotalGrossWeight = Convert.ToDecimal(0.00);
                int setHiestPriority = dt.Rows.Count;
                int setPriority = dt.Rows.Count;
                foreach (DataRow dr in dt.Rows)
                {
                    rowcount++;
                    if (customerName != Convert.ToString(dr["CustomerName"]).Trim())
                    {
                        if (customerName != "")
                        {
                            obj.Orders = lstdetailobj;
                            lstobj.Add(obj);
                            lstdetailobj = new List<TripOrderDetailViewModel>();
                            setPriority = setHiestPriority;
                        }

                        obj = new TripOrderViewModel();
                        obj.CustomerId = Convert.ToString(dr["CustomerId"]).Trim();
                        obj.StopCount = (DBNull.Value.Equals(dr["Priority"]) ? 0 : Convert.ToInt32(dr["Priority"]));
                        obj.OptStopCount = (DBNull.Value.Equals(dr["Priority"]) ? 0 : Convert.ToInt32(dr["Priority"]));

                        if (obj.StopCount > 0)
                        {
                            setPriority = obj.StopCount;
                        }
                        obj.CustomerName = Convert.ToString(dr["CustomerName"]).Trim();
                        obj.Address = Convert.ToString(dr["Address"]);
                        obj.ShippingAddress = Convert.ToString(dr["ShippingAddress"]);
                        obj.BillingAddress = Convert.ToString(dr["BillingAddress"]);
                        customerName = Convert.ToString(dr["CustomerName"]).Trim();
                        obj.TripID = (DBNull.Value.Equals(dr["TripID"]) ? "0" : Convert.ToString(dr["TripID"]));
                        obj.TripName = (DBNull.Value.Equals(dr["TripName"]) ? "0" : Convert.ToString(dr["TripName"]));
                        obj.DriverID = (DBNull.Value.Equals(dr["DriverID"]) ? "0" : Convert.ToString(dr["DriverID"]));
                        obj.StartLocation = (DBNull.Value.Equals(dr["StartLocation"]) ? "1" : Convert.ToString(dr["StartLocation"]));
                        obj.EndLocation = (DBNull.Value.Equals(dr["EndLocation"]) ? "1" : Convert.ToString(dr["EndLocation"]));
                        obj.IsSelected = (DBNull.Value.Equals(dr["TripID"]) ? false : true);
                        //if (Convert.IsDBNull(dr["StartTime"]))
                        //{
                        //    obj.StartTime = DateTime.MinValue;
                        //}
                        //else
                        //{
                        //    obj.StartTime = Convert.ToDateTime(dr["StartTime"]);
                        //}
                        if (!DBNull.Value.Equals(dr["TripID"]))
                        {
                            obj.IsSelected = true;
                        }
                        else
                        {
                            obj.IsSelected = false;
                        }
                        obj.StartTime = (Convert.IsDBNull(dr["StartTime"]) ? "" : Convert.ToDateTime(dr["StartTime"]).ToString("yyyy-MM-dd"));
                        obj.PriorityCount = setPriority;
                        obj.DeliveryCharges = (Convert.IsDBNull(dr["DeliveryCharges"]) ? Convert.ToDecimal(0.00) : Convert.ToDecimal(dr["DeliveryCharges"]));
                        obj.CustomerNote = Convert.ToString(dr["CustomerNote"]);

                    }
                    objdetail = new TripOrderDetailViewModel();

                    objdetail.PONumber = Convert.ToString(dr["PONumber"]);
                    objdetail.OrderDate = Convert.ToDateTime(dr["OrderDate"]).ToString("MM/dd/yyyy");
                    objdetail.BillAmount = Convert.ToDecimal(dr["BillAmount"]);
                    objdetail.OrderStatus = Convert.ToString(dr["OrderStatus"]);
                    objdetail.OrdQty = Convert.ToInt32(dr["TotalOrdQty"]);
                    objdetail.GrossWeight = Convert.ToDecimal(dr["TotalGrossWeight"]);
                    objdetail.IsSelected = (DBNull.Value.Equals(dr["TripID"]) ? false : true);
                    //objdetail.IsSelected = true;
                    //obj.lbs = Convert.ToString(dr["OrdQty"]) + '/' + Convert.ToString(dr["GrossWeight"]);
                    objdetail.lbs = Convert.ToString(dr["lbs"]);
                    objdetail.Priority = (Convert.IsDBNull(dr["Priority"]) ? 0 : Convert.ToInt32(dr["Priority"]));
                    lstdetailobj.Add(objdetail);

                    TotalBillAmount = TotalBillAmount + Convert.ToDecimal(dr["BillAmount"]);
                    TotalGrossWeight = TotalGrossWeight + Convert.ToDecimal(dr["TotalGrossWeight"]);

                    if (rowcount == dt.Rows.Count)
                    {
                        obj.Orders = lstdetailobj;
                        lstobj.Add(obj);
                    }
                    SortedList = lstobj.OrderBy(o => o.PriorityCount).ToList();
                    log.Debug("Debug logging: TripRepository - GetTripOrder");
                }
                if (lstobj.Count > 0)
                {

                }

                return SortedList;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }

            return lstobj;
        }

        public Tuple<List<TripInfoViewModel>, int> GetAllTripTrackingInfo(string searchText, string apiConnectionAuthString)
        {
            dt = new DataTable();
            TripInfoViewModel obj = null;
            SqlParameter[] objParam;
            int totalRecordNo = 0;
            List<TripInfoViewModel> lstobj = new List<TripInfoViewModel>();

            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@SearchText", SqlDbType.VarChar, 250);
                objParam[0].Value = searchText;

                dt = daRepo.GetDataTable(ApiConstant.SPocGetAllTripTracking, objParam);

                foreach (DataRow dr in dt.Rows)
                {

                    obj = new TripInfoViewModel();
                    obj.TripId = Convert.ToString(dr["TRIPID"]).Trim();
                    obj.TripName = Convert.ToString(dr["TRIPNAME"]).Trim();
                    obj.TripDate = Convert.ToDateTime(dr["TRIPDATE"]).ToString("MM/dd/yyyy");
                    obj.DriverName = Convert.ToString(dr["DRIVERNAME"]).Trim();
                    obj.NoOfStop = Convert.ToInt32(dr["NO_OF_STOP"]);
                    obj.TotalOrder = Convert.ToInt32(dr["TOTALORDER"]);
                    obj.OrderAmount = Convert.ToDecimal(dr["ORDERAMOUNT"]);
                    obj.StatusText = Convert.ToString(dr["STATUSTEXT"]);
                    lstobj.Add(obj);

                    log.Debug("Debug logging: TripRepository - GetAllTripTrackingInfo");
                }
                if (dt.Rows.Count > 0)
                {
                    totalRecordNo = Convert.ToInt32(dt.Rows[0]["TotalRowNo"]);
                }

                return new Tuple<List<TripInfoViewModel>, int>(lstobj, totalRecordNo);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }

            return new Tuple<List<TripInfoViewModel>, int>(lstobj, totalRecordNo);
        }

        public TripTrackingViewModel GetTripTrackingDetail(string TripId, string apiConnectionAuthString)
        {
            dt = new DataTable();
            TripTrackingViewModel obj = null;
            TripTrackingDetailViewModel objdetail = null;
            SqlParameter[] objParam;
            List<TripOrderViewModel> lstobjdetail = new List<TripOrderViewModel>();
            List<TripTrackingDetailViewModel> lstdetailobj = new List<TripTrackingDetailViewModel>();
            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@TripId", SqlDbType.VarChar, 250);
                objParam[0].Value = TripId;

                dt = daRepo.GetDataTable(ApiConstant.SPocGetTripTrackingDetail, objParam);

                int rowcount = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    if (rowcount == 0)
                    {
                        obj = new TripTrackingViewModel();
                        obj.TripID = Convert.ToString(dr["TripID"]).Trim();
                        obj.TripName = Convert.ToString(dr["TripName"]).Trim();
                        obj.StartDate = Convert.ToDateTime(dr["TRIPDATE"]).ToString("MM/dd/yyyy");
                        obj.DriverID = Convert.ToString(dr["DriverID"]).Trim();
                        obj.DriverName = Convert.ToString(dr["DriverName"]).Trim();

                    }

                    objdetail = new TripTrackingDetailViewModel();
                    objdetail.CustomerId = Convert.ToString(dr["CustomerId"]).Trim();
                    objdetail.CustomerName = Convert.ToString(dr["CustomerName"]).Trim();
                    objdetail.CustomerAddress = Convert.ToString(dr["CustomerAddress"]).Trim();
                    objdetail.StopNo = Convert.ToString(dr["StopNo"]).Trim();
                    objdetail.StatusText = Convert.ToString(dr["StatusText"]).Trim();

                    lstdetailobj.Add(objdetail);

                }
                if (dt.Rows.Count > 0)
                {
                    obj.Trackings = lstdetailobj;
                }
                rowcount++;
                return obj;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }

            return obj;
        }

        public List<DeliverySummeryCustomerViewModel> GetDeliverySummeryTripId(string TripId, string apiConnectionAuthString)
        {
            dt = new DataTable();
            //DeliverySummeryCustomerViewModel cstobj = null;
            //DeliverySummeryOrderViewModel objordSummery = null;
            //TripDeleveryItemSummeryViewModel objDItem = null;
            SqlParameter[] objParam;
            List<DeliverySummeryCustomerViewModel> lstcstobj = new List<DeliverySummeryCustomerViewModel>();
            //List<DeliverySummeryOrderViewModel> lstordSummeryobj = new List<DeliverySummeryOrderViewModel>();
            //List<TripDeleveryItemSummeryViewModel> lstDItemobj = new List<TripDeleveryItemSummeryViewModel>();
            //List<DeliverySummeryCustomerViewModel> SortedList = new List<DeliverySummeryCustomerViewModel>();
            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@TripId", SqlDbType.VarChar, 250);
                objParam[0].Value = TripId;

                dt = daRepo.GetDataTable(ApiConstant.SPocGetDeleverySummeryTripId, objParam);

                string customerName = "";
                string preOrderNo = "";
                bool isCustomerChange = false;
                bool isOrderChange = false;
                int rowcount = 0;
                int itemcount = 0;
                int ordercount = 0;
                decimal TotalBillAmount = Convert.ToDecimal(0.00);
                decimal TotalGrossWeight = Convert.ToDecimal(0.00);
                int setHiestPriority = dt.Rows.Count;
                int setPriority = dt.Rows.Count;
                bool isAllItemAdded = false;
                bool isAllRowAdded = false;

                // ------------added by Bipul-----------------//
                try
                {
                    var order_id = "OrderNo";
                    //var lstOrders = new List<DataExportFinal>();
                    //Get Distinct Rows by ORder ID
                    var ordersDT = dt.DefaultView.ToTable(true, order_id);

                    //Get Current Order Items with Customer
                    foreach (DataRow orderDT in ordersDT.Rows)
                    {
                        string orderId = orderDT[order_id].ToString();

                        string filter = "OrderNo =" + orderId;
                        DataRow[] dr = dt.Select(filter);

                        //Get First matched Order & retrive Customer and Order details
                        var firstOrder = dr.FirstOrDefault(); //orderItems.FirstOrDefault();

                        //Get Matched Customer for order
                        var cstobj = new DeliverySummeryCustomerViewModel()
                        {
                            CustomerId = Convert.ToString(firstOrder["CustomerId"]).Trim(),
                            ZoneId = Convert.ToInt32(firstOrder["Zone"]),
                            CustomerName = Convert.ToString(firstOrder["CustomerName"]).Trim(),
                            Address = Convert.ToString(firstOrder["Address"]),
                            ShippingAddress = Convert.ToString(firstOrder["ShippingAddress"]),
                            BillingAddress = Convert.ToString(firstOrder["BillingAddress"])
                        };
                                               

                        //Get Matched Items for order
                        var lstDItemobj = new List<TripDeleveryItemSummeryViewModel>();
                        foreach (var orderItem in dr)
                        {
                            lstDItemobj.Add(new TripDeleveryItemSummeryViewModel()
                            {
                                ProductCode = Convert.ToString(orderItem["ProductCode"]),
                                ItemName = Convert.ToString(orderItem["ItemName"]),
                                OrdQty = Convert.ToInt32(orderItem["OrdQty"]),
                                DelQty = Convert.ToInt32(orderItem["DelQty"]),
                                //objDItem.CasePrice = Convert.ToDecimal(dr["CasePrice"]); ; ;
                                //objDItem.TotalCasePrice = Convert.ToDecimal(dr["TotalCasePrice"]); ;
                                //objDItem.DeliveryDate = (Convert.IsDBNull(dr["DeliveryDate"]) ? "" : Convert.ToDateTime(dr["DeliveryDate"]).ToString("MM/dd/yyyy"));
                                ExtraQuantity = Convert.ToInt32(orderItem["Extra_Quantity"]) > 0 ? Convert.ToString(orderItem["Extra_Quantity"]) : "",
                                ReturnQuantity = Convert.ToInt32(orderItem["Return_Quantity"]) > 0 ? Convert.ToString(orderItem["Return_Quantity"]) : ""
                            });
                        }

                        var objordSummery = new DeliverySummeryOrderViewModel()
                        {
                            DriverID = Convert.ToString(firstOrder["DeliveryID"]),
                            TripID = Convert.ToString(firstOrder["TripID"]),
                            OrderNo = Convert.ToString(firstOrder["OrderNo"]),
                            Items = lstDItemobj
                        };

                        var lstordSummeryobj=new List<DeliverySummeryOrderViewModel>();
                        lstordSummeryobj.Add(objordSummery);

                        cstobj.Orders = lstordSummeryobj;
                        lstcstobj.Add(cstobj);

                        //lstDItemobj.Clear();
                        //lstordSummeryobj.Clear();

                        //lstcstobj.Add(new DeliverySummeryCustomerViewModel()
                        //{
                        //    customer = cust,
                        //    item = items,
                        //    order = order,
                        //    status = 200
                        //});

                    }

                    //return lstOrders;
                }
                catch (Exception ex)
                {

                }
                // ---- end -------//

                //foreach (DataRow dr in dt.Rows)
                //{
                //    rowcount++;
                //    //itemcount++;
                //    if (customerName != Convert.ToString(dr["CustomerName"]).Trim())
                //    {
                //        cstobj = new DeliverySummeryCustomerViewModel();
                //        cstobj.CustomerId = Convert.ToString(dr["CustomerId"]).Trim();
                //        cstobj.ZoneId = Convert.ToInt32(dr["Zone"]);
                //        cstobj.CustomerName = Convert.ToString(dr["CustomerName"]).Trim();
                //        cstobj.Address = Convert.ToString(dr["Address"]);
                //        cstobj.ShippingAddress = Convert.ToString(dr["ShippingAddress"]);
                //        cstobj.BillingAddress = Convert.ToString(dr["BillingAddress"]);
                //        customerName = Convert.ToString(dr["CustomerName"]).Trim();
                //        isCustomerChange = true;
                //    }

                //    if (preOrderNo != Convert.ToString(dr["OrderNo"]).Trim())
                //    {
                //        objordSummery = new DeliverySummeryOrderViewModel();
                //        objordSummery.DriverID = Convert.ToString(dr["DeliveryID"]);
                //        objordSummery.TripID = Convert.ToString(dr["TripID"]);
                //        objordSummery.OrderNo = Convert.ToString(dr["OrderNo"]);
                //        preOrderNo = Convert.ToString(dr["OrderNo"]);
                //        ordercount++;
                //    }

                //    objDItem = new TripDeleveryItemSummeryViewModel();
                //    objDItem.ProductCode = Convert.ToString(dr["ProductCode"]);
                //    objDItem.ItemName = Convert.ToString(dr["ItemName"]);
                //    objDItem.OrdQty = Convert.ToInt32(dr["OrdQty"]);
                //    objDItem.DelQty = Convert.ToInt32(dr["DelQty"]);
                //    //objDItem.CasePrice = Convert.ToDecimal(dr["CasePrice"]); ; ;
                //    //objDItem.TotalCasePrice = Convert.ToDecimal(dr["TotalCasePrice"]); ;
                //    //objDItem.DeliveryDate = (Convert.IsDBNull(dr["DeliveryDate"]) ? "" : Convert.ToDateTime(dr["DeliveryDate"]).ToString("MM/dd/yyyy"));
                //    objDItem.ExtraQuantity = Convert.ToInt32(dr["Extra_Quantity"]) > 0 ? Convert.ToString(dr["Extra_Quantity"]) : "";
                //    objDItem.ReturnQuantity = Convert.ToInt32(dr["Return_Quantity"]) > 0 ? Convert.ToString(dr["Return_Quantity"]) : "";
                //    lstDItemobj.Add(objDItem);


                //    if (itemcount == Convert.ToInt32(dr["ItemCount"]))
                //    {
                //        objordSummery.Items = lstDItemobj;
                //        lstordSummeryobj.Add(objordSummery);
                //        itemcount = 0;

                //        ///lstDItemobj = new List<TripDeleveryItemSummeryViewModel>();

                //    }

                //    if (ordercount == Convert.ToInt32(dr["OrderCount"]) && itemcount == 0)
                //    {
                //        if (isCustomerChange)
                //        {
                //            cstobj.Orders = lstordSummeryobj;
                //            lstcstobj.Add(cstobj);
                //            lstordSummeryobj = new List<DeliverySummeryOrderViewModel>();
                //            ordercount = 0;
                //            isCustomerChange = false;
                //        }

                //    }
                //    //itemcount++;
                //    //if (rowcount == dt.Rows.Count)
                //    //{
                //    //    objordSummery.Items = lstDItemobj;
                //    //    lstordSummeryobj.Add(objordSummery);
                //    //    cstobj.Orders = lstordSummeryobj;
                //    //    lstcstobj.Add(cstobj);
                //    //}
                //    //SortedList = lstobj.OrderBy(o => o.PriorityCount).ToList();
                //    log.Debug("Debug logging: TripRepository - GetDeliverySummeryTripId");
                //}


                return lstcstobj;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                //cstobj = null;
            }

            return lstcstobj;
        }
    }
}
