﻿using DVSDomain.Model;
using DVSRepository.Helper;
using DVSRepository.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DVSRepository.Implementation
{
    public class UserAuthorizationRepository : IUserAuthorizationRepository
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        DBConnectionOAuth dbConnectionObj = null;
        SqlCommand com = null;
        SqlDataAdapter da = null;
        DataTable dt = null;
        private SqlConnection _con;

        #region Old implementation
        //public int GetRoleIdFromUserId(string userId)
        //{
        //    DataTable dt = null;
        //    int roleId = 0;

        //    try
        //    {
        //        dbConnectionObj = new DBConnectionOAuth();
        //        com = new SqlCommand(ApiConstant.SPocGetRoleIdFromUserId, dbConnectionObj.ApiConnection);
        //        com.CommandType = CommandType.StoredProcedure;
        //        com.Parameters.AddWithValue("@userId", userId);
        //        da = new SqlDataAdapter(com);
        //        dt = new DataTable();
        //        dbConnectionObj.ConnectionOpen();
        //        da.Fill(dt);
        //        dbConnectionObj.ConnectionClose();

        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            roleId = Convert.ToInt32(dr["RoleId"]);
        //        }

        //        log.Debug("Debug logging: AuthorizationWebApi.Repository -> UserAuthorizationRepository -> GetRoleIdFromUserId");

        //        return roleId;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: " + ex);
        //    }
        //    finally
        //    {
        //        dbConnectionObj = null;
        //        // custList = null;
        //        com = null;
        //        da = null;
        //        dt = null;
        //    }

        //    return roleId;
        //}

        //public string GetUserFullNameFromUserId(string userId)
        //{
        //    DataTable dt = null;
        //    string userFName = string.Empty;

        //    try
        //    {
        //        dbConnectionObj = new DBConnectionOAuth();
        //        com = new SqlCommand(ApiConstant.SPocGetUserFullName, dbConnectionObj.ApiConnection);
        //        com.CommandType = CommandType.StoredProcedure;
        //        com.Parameters.AddWithValue("@userId", userId);
        //        da = new SqlDataAdapter(com);
        //        dt = new DataTable();
        //        dbConnectionObj.ConnectionOpen();
        //        da.Fill(dt);
        //        dbConnectionObj.ConnectionClose();

        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            userFName = dr["UserFullName"].ToString();
        //        }

        //        log.Debug("Debug logging: AuthorizationWebApi.Repository -> UserAuthorizationRepository -> GetRoleIdFromUserId");

        //        return userFName;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: " + ex);
        //    }
        //    finally
        //    {
        //        dbConnectionObj = null;
        //        // custList = null;
        //        com = null;
        //        da = null;
        //        dt = null;
        //    }

        //    return userFName;
        //}

        //public ApplicationUser GetUserProfile(string userId)
        //{
        //    ApplicationUser userObj = null;

        //    try
        //    {
        //        userObj = new ApplicationUser();
        //        dbConnectionObj = new DBConnectionOAuth();
        //        com = new SqlCommand(ApiConstant.SPocGetUserMyProfile, dbConnectionObj.ApiConnection);
        //        com.CommandType = CommandType.StoredProcedure;
        //        com.Parameters.AddWithValue("@userId", userId);
        //        da = new SqlDataAdapter(com);
        //        dt = new DataTable();
        //        dbConnectionObj.ConnectionOpen();
        //        da.Fill(dt);
        //        dbConnectionObj.ConnectionClose();

        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            userObj.UserLoginID = dr["UserName"].ToString();
        //            //userObj.UserFullName = dr["UserFullName"].ToString();
        //            //userObj.CompanyId = dr["CompanyId"].ToString();
        //            //userObj.CompanyName = dr["CompanyName"].ToString();
        //            userObj.UserRole = Convert.ToInt32(dr["RoleId"]);
        //            userObj.Email = dr["Email"].ToString();
        //           // userObj.Phone = dr["PhoneNumber"].ToString();
        //           // userObj.BrokerPicName = dr["BrokerPicName"].ToString();
        //            //userObj.UserType = dr["UserType"].ToString();
        //            //if (dr["IsNewReleaseFlag"] is DBNull)
        //                //userObj.IsNewReleaseFlag = false;
        //            //else
        //               // userObj.IsNewReleaseFlag = Convert.ToBoolean(dr["IsNewReleaseFlag"]);
        //        }

        //        log.Debug("Debug logging: AuthorizationWebApi.Repository -> UserAuthorizationRepository -> GetRoleIdFromUserId");

        //        return userObj;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: " + ex);
        //    }
        //    finally
        //    {
        //        dbConnectionObj = null;
        //        // custList = null;
        //        com = null;
        //        da = null;
        //        dt = null;
        //    }

        //    return userObj;
        //}

        //public bool UpdateUserProfile(ApplicationUser userObj)
        //{
        //    bool success = false;
        //    SqlDataReader reader = null;

        //    try
        //    {
        //        dbConnectionObj = new DBConnectionOAuth();
        //        com = new SqlCommand(ApiConstant.SPocUpdateMyProfile, dbConnectionObj.ApiConnection);
        //        com.CommandType = CommandType.StoredProcedure;
        //        com.Parameters.AddWithValue("@UserName", userObj.UserLoginID);
        //        com.Parameters.AddWithValue("@Email", userObj.Email);
        //        //com.Parameters.AddWithValue("@Phone", userObj.Phone);
        //        com.Parameters.AddWithValue("@Comments", "Phone and Email got updated.");

        //        dbConnectionObj.ConnectionOpen();
        //        reader = com.ExecuteReader();
        //        success = true;

        //        reader.Close();
        //        dbConnectionObj.ConnectionClose();
        //    }
        //    catch (Exception ex)
        //    {
        //        success = false;
        //        log.Error("Error: " + ex);
        //        throw ex;
        //    }
        //    finally
        //    {
        //        dbConnectionObj = null;
        //        com = null;
        //        da = null;
        //        dt = null;
        //    }

        //    return success;
        //}
        //public bool UpdateNewUser(CreateNewUserBindingModel userObj)
        //{
        //    bool success = false;
        //    SqlDataReader reader = null;

        //    try
        //    {
        //        dbConnectionObj = new DBConnectionOAuth();
        //        com = new SqlCommand(ApiConstant.SPocUpdateUser, dbConnectionObj.ApiConnection);
        //        com.CommandType = CommandType.StoredProcedure;
        //        com.Parameters.AddWithValue("@UserName", userObj.UserName);
        //        com.Parameters.AddWithValue("@UserFullName", userObj.UserFullName);
        //        com.Parameters.AddWithValue("@Domain", userObj.Domain);
        //        com.Parameters.AddWithValue("@UserTypeCode", userObj.UserTypeCode);

        //        dbConnectionObj.ConnectionOpen();
        //        reader = com.ExecuteReader();
        //        success = true;

        //        reader.Close();
        //        dbConnectionObj.ConnectionClose();
        //    }
        //    catch (Exception ex)
        //    {
        //        success = false;
        //        log.Error("Error: " + ex);
        //        throw ex;
        //    }
        //    finally
        //    {
        //        dbConnectionObj = null;
        //        com = null;
        //        da = null;
        //        dt = null;
        //    }

        //    return success;
        //}

        //public bool UpdateUpdatedBy(string userName)
        //{
        //    bool success = false;
        //    SqlDataReader reader = null;
        //    string comments = "Password is been reset.";

        //    try
        //    {
        //        dbConnectionObj = new DBConnectionOAuth();
        //        com = new SqlCommand(ApiConstant.SPocUpdateUpdatedBy, dbConnectionObj.ApiConnection);
        //        com.CommandType = CommandType.StoredProcedure;
        //        com.Parameters.AddWithValue("@userName", userName);
        //        com.Parameters.AddWithValue("@comments", comments);

        //        dbConnectionObj.ConnectionOpen();
        //        reader = com.ExecuteReader();
        //        success = true;

        //        reader.Close();
        //        dbConnectionObj.ConnectionClose();
        //    }
        //    catch (Exception ex)
        //    {
        //        success = false;
        //        log.Error("Error: " + ex);
        //        throw ex;
        //    }
        //    finally
        //    {
        //        dbConnectionObj = null;
        //        com = null;
        //        da = null;
        //        dt = null;
        //    }

        //    return success;
        //}


        //public string GetUserIDFromUserName(string userName)
        //{
        //    DataTable dt = null;
        //    string userId = string.Empty;

        //    try
        //    {
        //        dbConnectionObj = new DBConnectionOAuth();
        //        com = new SqlCommand(ApiConstant.SPocGetUserIdFromUserName, dbConnectionObj.ApiConnection);
        //        com.CommandType = CommandType.StoredProcedure;
        //        com.Parameters.AddWithValue("@userName", userName);
        //        da = new SqlDataAdapter(com);
        //        dt = new DataTable();
        //        dbConnectionObj.ConnectionOpen();
        //        da.Fill(dt);
        //        dbConnectionObj.ConnectionClose();

        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            userId = dr["Id"].ToString();
        //        }

        //        log.Debug("Debug logging: AuthorizationWebApi.Repository -> UserAuthorizationRepository -> GetUserIDFromUserName");

        //        return userId;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: " + ex);
        //    }
        //    finally
        //    {
        //        dbConnectionObj = null;
        //        // custList = null;
        //        com = null;
        //        da = null;
        //        dt = null;
        //    }

        //    return userId;
        //}

        //public bool IfUserExistInOAuthDB(string userName, string email)
        //{
        //    DataTable dt = null;
        //    bool isExist = false;

        //    try
        //    {
        //        dbConnectionObj = new DBConnectionOAuth();
        //        com = new SqlCommand(ApiConstant.SPocGetIfUserOrEmailExist, dbConnectionObj.ApiConnection);
        //        com.CommandType = CommandType.StoredProcedure;
        //        com.Parameters.AddWithValue("@userName", userName);
        //        com.Parameters.AddWithValue("@email", email);
        //        da = new SqlDataAdapter(com);
        //        dt = new DataTable();
        //        dbConnectionObj.ConnectionOpen();
        //        da.Fill(dt);
        //        dbConnectionObj.ConnectionClose();

        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            if (Convert.ToInt32(dr[0]) == 1)
        //                isExist = true;
        //        }

        //        log.Debug("Method -> IfUserExistInOAuthDB");

        //        return isExist;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: " + ex);
        //    }
        //    finally
        //    {
        //        dbConnectionObj = null;
        //        // custList = null;
        //        com = null;
        //        da = null;
        //        dt = null;
        //    }

        //    return isExist;
        //}

        //public bool IfUserValidEmailOAuthDB(string userName, string email)
        //{
        //    DataTable dt = null;
        //    bool isExist = false;

        //    try
        //    {
        //        dbConnectionObj = new DBConnectionOAuth();
        //        com = new SqlCommand(ApiConstant.SPocCheckIfValidEmailExist, dbConnectionObj.ApiConnection);
        //        com.CommandType = CommandType.StoredProcedure;
        //        com.Parameters.AddWithValue("@userName", userName);
        //        com.Parameters.AddWithValue("@email", email);
        //        da = new SqlDataAdapter(com);
        //        dt = new DataTable();
        //        dbConnectionObj.ConnectionOpen();
        //        da.Fill(dt);
        //        dbConnectionObj.ConnectionClose();

        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            if (Convert.ToInt32(dr[0]) == 1)
        //                isExist = true;
        //        }

        //        log.Debug("Method -> IfUserExistInOAuthDB");

        //        return isExist;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: " + ex);
        //    }
        //    finally
        //    {
        //        dbConnectionObj = null;
        //        // custList = null;
        //        com = null;
        //        da = null;
        //        dt = null;
        //    }

        //    return isExist;
        //}

        ////  Reset Release Update flag
        //public string ResetReleaseUpdateFlag(string userId)
        //{
        //    DataTable dt = null;
        //    int roleId = 0;

        //    try
        //    {
        //        dbConnectionObj = new DBConnectionOAuth();
        //        com = new SqlCommand(ApiConstant.SPocResetReleaseUpdateFlag, dbConnectionObj.ApiConnection);
        //        com.CommandType = CommandType.StoredProcedure;
        //        com.Parameters.AddWithValue("@userId", userId);
        //        da = new SqlDataAdapter(com);
        //        dt = new DataTable();
        //        dbConnectionObj.ConnectionOpen();
        //        da.Fill(dt);
        //        dbConnectionObj.ConnectionClose();

        //        log.Debug("Debug logging: AuthorizationWebApi.Repository -> UserAuthorizationRepository -> ResetReleaseUpdateFlag");

        //        return "Updated";
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: " + ex);
        //    }
        //    finally
        //    {
        //        dbConnectionObj = null;
        //        // custList = null;
        //        com = null;
        //        da = null;
        //        dt = null;
        //    }

        //    return "Failed";
        //}

        #endregion


        #region New implementation
        public ApplicationUser GetUserProfileByUserLoginId(string userLoginId, string apiConnectionAuthString)
        {
            ApplicationUser userObj = null;

            try
            {
                userObj = new ApplicationUser();
                dbConnectionObj = new DBConnectionOAuth(apiConnectionAuthString);
                //_con = new SqlConnection(apiConnectionAuthString);
                com = new SqlCommand(ApiConstant.GetUserByUserLoginId, dbConnectionObj.GetApiConnection());
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@UserName", userLoginId);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                //dbConnectionObj.ConnectionOpen();

                if (_con.State != ConnectionState.Open)
                {
                    _con.Close();
                    _con.Open();
                }
                da.Fill(dt);
                if (_con.State == ConnectionState.Open)
                {
                    _con.Close();
                }
                //dbConnectionObj.ConnectionClose();

                foreach (DataRow dr in dt.Rows)
                {
                    userObj.UserLoginID = dr["UserLoginID"].ToString();
                    userObj.UserId = Convert.ToInt32(dr["UserId"]);
                    //userObj.UserFullName = dr["UserFullName"].ToString();
                    userObj.CompanyId = Convert.ToInt32(dr["CompanyId"].ToString());
                    userObj.Name = dr["Name"].ToString();
                    userObj.Address = dr["Address"].ToString();
                    userObj.Email = dr["Email"].ToString();
                    userObj.ContactNo = dr["ContactNo"].ToString();
                    userObj.City = dr["City"].ToString();
                    userObj.State = Convert.ToInt32(dr["State"].ToString());
                    userObj.Zip = dr["Zip"].ToString();
                    userObj.UserLoginID = dr["UserLoginID"].ToString();
                    userObj.UserPassword = dr["UserPassword"].ToString();
                    userObj.UserRole = Convert.ToInt32(dr["UserRole"].ToString());
                    userObj.IsActive = Convert.ToInt32(dr["IsActive"].ToString());
                    userObj.DefaultLanguage = dr["DefaultLanguage"].ToString();
                    userObj.IMEI = dr["IMEI"].ToString();
                    userObj.CreatedOn = Convert.ToDateTime(dr["CreatedOn"].ToString());
                    userObj.DateAuthentication = Convert.ToDateTime(dr["DateAuthentication"].ToString());
                    //userObj.UserRole = Convert.ToInt32(dr["RoleId"]);
                    //userObj.Email = dr["Email"].ToString();
                    // userObj.Phone = dr["PhoneNumber"].ToString();
                    // userObj.BrokerPicName = dr["BrokerPicName"].ToString();
                    //userObj.UserType = dr["UserType"].ToString();
                    //if (dr["IsNewReleaseFlag"] is DBNull)
                    //userObj.IsNewReleaseFlag = false;
                    //else
                    // userObj.IsNewReleaseFlag = Convert.ToBoolean(dr["IsNewReleaseFlag"]);
                }

                log.Debug("Debug logging: GetUserProfileByUserLoginId");

                return userObj;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                // custList = null;
                com = null;
                da = null;
                dt = null;
            }

            return userObj;
        }

        public ApplicationUser GetUserProfile(LogindBindingModel model, string apiConnectionAuthString)
        {
            ApplicationUser userObj = null;

            try
            {
                userObj = new ApplicationUser();
                //_con = new SqlConnection(apiConnectionAuthString);
                dbConnectionObj = new DBConnectionOAuth(apiConnectionAuthString);
                com = new SqlCommand(ApiConstant.GetLoginUserProfile, dbConnectionObj.GetApiConnection());
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@UserName", model.UserName);
                com.Parameters.AddWithValue("@UserPassword", model.Password);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                da.Fill(dt);

                foreach (DataRow dr in dt.Rows)
                {
                    userObj.UserLoginID = dr["UserLoginID"].ToString();
                    userObj.UserId = Convert.ToInt32(dr["UserId"]);
                    //userObj.UserFullName = dr["UserFullName"].ToString();
                    userObj.CompanyId = Convert.ToInt32(dr["CompanyId"].ToString());
                    userObj.Name = dr["Name"].ToString();
                    userObj.Address = dr["Address"].ToString();
                    userObj.Email = dr["Email"].ToString();
                    userObj.ContactNo = dr["ContactNo"].ToString();
                    userObj.City = dr["City"].ToString();
                    userObj.State = Convert.ToInt32(dr["State"].ToString());
                    userObj.Zip = dr["Zip"].ToString();
                    userObj.UserLoginID = dr["UserLoginID"].ToString();
                    //userObj.UserPassword = dr["UserPassword"].ToString();
                    userObj.UserRole = Convert.ToInt32(dr["UserRole"].ToString());
                    userObj.IsActive = Convert.ToInt32(dr["IsActive"].ToString());
                    userObj.DefaultLanguage = dr["DefaultLanguage"].ToString();
                    userObj.IMEI = dr["IMEI"].ToString();
                    userObj.CreatedOn = Convert.ToDateTime(dr["CreatedOn"].ToString());
                    userObj.DateAuthentication = Convert.ToDateTime(dr["DateAuthentication"].ToString());
                }

                log.Debug("Debug logging: GetUserProfile");

                return userObj;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                com = null;
                da = null;
                dt = null;
            }

            return userObj;
        }

        public bool UpdateUserPassword(ChangePasswordBindingModel model, string apiConnectionAuthString)
        {
            bool success = false;
            SqlDataReader reader = null;
            ApplicationUser userObj = null;
            try
            {
                dbConnectionObj = new DBConnectionOAuth(apiConnectionAuthString);
                userObj = new ApplicationUser();
                //_con = new SqlConnection(apiConnectionAuthString);
                com = new SqlCommand(ApiConstant.updateUserPassword, dbConnectionObj.GetApiConnection());
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@UserName", model.UserName);
                com.Parameters.AddWithValue("@OldPassword", model.OldPassword);
                com.Parameters.AddWithValue("@NewPassword", model.NewPassword);

                dbConnectionObj.ConnectionOpen();
                //if (_con.State != ConnectionState.Open)
                //{
                //    _con.Close();
                //    _con.Open();
                //}
                //reader = com.ExecuteReader();
                int i = com.ExecuteNonQuery();
                if (i >= 0)
                {
                    success = true;
                }

                //reader.Close();
                dbConnectionObj.ConnectionClose();
                //if (_con.State == ConnectionState.Open)
                //{
                //    _con.Close();
                //}
            }
            catch (Exception ex)
            {
                success = false;
                log.Error("Error: " + ex);
                throw ex;
            }
            finally
            {
                com = null;
                da = null;
                dt = null;
            }

            return success;
        }
        #endregion
    }
}
