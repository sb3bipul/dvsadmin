﻿using DVSDomain.Model;
using DVSRepository.Helper;
using DVSRepository.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DVSRepository.Implementation
{
    public class VehicleTypeRepository : IVehicleTypeRepository
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string apiConnectionString;
        DBConnectionOAuth dbConnectionObj = null;
        DataAccessReposity daRepo = null;
        SqlCommand com = null;
        SqlDataAdapter da = null;
        DataTable dt = null;
        private SqlConnection _con;
        private string MessageReturn = "";
        //public VehicalTypeRepository(string apiConnection)
        //{
        //    apiConnectionString = apiConnection;
        //}
        public Tuple<List<VehicleType>, int>  GetAllVehiclesType(GetDataBindingModel model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            SqlParameter[] objParam;
            List<VehicleType> lstVehicalType = new List<VehicleType>();
            VehicleType vehicalType = null;
            int totalRecordNo = 0;
            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[6];

                objParam[0] = new SqlParameter("@Id", SqlDbType.Int);
                objParam[0].Value = model.Id;

                objParam[1] = new SqlParameter("@SearchText", SqlDbType.VarChar, 250);
                objParam[1].Value = model.SearchText;

                objParam[2] = new SqlParameter("@PageNo", SqlDbType.Int);
                objParam[2].Value = model.PageNo;

                objParam[3] = new SqlParameter("@PageSize", SqlDbType.Int);
                objParam[3].Value = model.PageSize;

                objParam[4] = new SqlParameter("@SortBy", SqlDbType.VarChar, 100);
                objParam[4].Value = model.SortBy;

                objParam[5] = new SqlParameter("@SortOrder", SqlDbType.VarChar, 100);
                objParam[5].Value = model.SortOrder;

                dt = daRepo.GetDataTable(ApiConstant.SPocGetVehiclesTypes, objParam);
                
                foreach (DataRow dr in dt.Rows)
                {
                    vehicalType = new VehicleType();
                    vehicalType.TypeID = Convert.ToInt32(dr["TypeID"].ToString());
                    vehicalType.CompanyId = Convert.ToString(dr["CompanyId"]);
                    vehicalType.TypeName = Convert.ToString(dr["TypeName"].ToString());
                    vehicalType.TypeDescription = dr["TypeDescription"].ToString();
                    vehicalType.IsActive = Convert.ToBoolean(dr["IsActive"].ToString());
                    vehicalType.CreatedBy = dr["CreatedBy"].ToString();
                    vehicalType.CreatedAt = (dr["CreatedAt"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dr["CreatedAt"].ToString()));
                    lstVehicalType.Add(vehicalType);
                }
                if (dt.Rows.Count > 0)
                {
                    totalRecordNo = Convert.ToInt32(dt.Rows[0]["TotalRowNo"]);
                }
                log.Debug("Debug logging: VehicalTypeRepository - GetAllVehiclesType");

                return new Tuple<List<VehicleType>, int>(lstVehicalType, totalRecordNo);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                vehicalType = null;
            }

            return new Tuple<List<VehicleType>, int>(lstVehicalType, totalRecordNo);
        }

        public string CRUDVehicleType(VehicleTypeBindingModel model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            SqlParameter[] objParam;
            apiConnectionString = apiConnectionAuthString;
            MessageReturn = "";
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[5];

                objParam[0] = new SqlParameter("@ActionMode", SqlDbType.Int);
                objParam[0].Value = model.ActionMode;

                objParam[1] = new SqlParameter("@Id", SqlDbType.Int);
                objParam[1].Value = model.TypeID;

                objParam[2] = new SqlParameter("@TypeName", SqlDbType.NVarChar, 100);
                objParam[2].Value = model.TypeName;

                objParam[3] = new SqlParameter("@TypeDescription", SqlDbType.NVarChar, 100);
                objParam[3].Value = model.TypeDescription;

                objParam[4] = new SqlParameter("@IsActive", SqlDbType.Bit, 100);
                objParam[4].Value = model.IsActive;

                MessageReturn = daRepo.ExecuteOutParamProc(ApiConstant.SPocCRUDVehiclesType, objParam);

               

                log.Debug("Debug logging: VehicalTypeRepository - CRUDVehiclesType -" + model.ActionMode.ToString());

                return MessageReturn;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
            }

            return MessageReturn;
        }
    }
}
