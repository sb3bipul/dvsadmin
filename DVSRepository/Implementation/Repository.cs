﻿using DVSDomain.Model;
using DVSRepository.DBContext;
using DVSRepository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DVSRepository.Implementation
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly AppDBContext appDBContext;
        private readonly DbSet<T> entities;
        public Repository(AppDBContext _appDBContext)
        {
            this.appDBContext = _appDBContext;
            entities = appDBContext.Set<T>();
        }

        public IEnumerable<T> GetAll()
        {
            return entities.AsEnumerable();
        }
        public T Get(int id)
        {
            return entities.SingleOrDefault(p => p.ID == id);
        }
        public async Task<T> Create(T entity)
        {
            try
            {
                var result = await this.appDBContext.Set<T>().AddAsync(entity);
                this.appDBContext.SaveChanges();
                return result.Entity;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<IEnumerable<T>> Delete(T entity)
        {

            this.appDBContext.Set<T>().Remove(entity);
            await this.appDBContext.SaveChangesAsync();
            return await this.appDBContext.Set<T>().ToListAsync();
        }

        //public async Task<IEnumerable<T>> GetAll()
        //{
        //    return await this.appDBContext.Set<T>().ToListAsync();
        //}

        public async Task<IEnumerable<T>> GetByCondition(Expression<Func<T, bool>> expression)
        {
            return await this.appDBContext.Set<T>().Where(expression).ToListAsync();
        }

        public async Task<IEnumerable<T>> Update(T entity)
        {
            this.appDBContext.Set<T>().Update(entity);
            await this.appDBContext.SaveChangesAsync();
            return await this.appDBContext.Set<T>().ToListAsync();
        }
    }
}
