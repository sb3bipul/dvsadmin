﻿using DVSDomain.Model;
using DVSRepository.Helper;
using DVSRepository.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DVSRepository.Implementation
{
    public class VehicleRepository : IVehicleRepository
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string apiConnectionString;
        DataAccessReposity daRepo = null;
        DataTable dt = null;
        private string MessageReturn = "";
        public Tuple<List<Vehicle>, int> GetVehicle(GetDataBindingModel model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            SqlParameter[] objParam;
            List<Vehicle> lstobj = new List<Vehicle>();
            Vehicle obj = null;
            int totalRecordNo = 0;
            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@CompanyId", SqlDbType.Int);
                objParam[0].Value = model.Id;

                dt = daRepo.GetDataTable(ApiConstant.SProcDVS_GetVehicleDDL, objParam);

                foreach (DataRow dr in dt.Rows)
                {
                    obj = new Vehicle();
                    obj.VehicleID = Convert.ToInt32(dr["VehicleID"]);

                    obj.VehicleName = Convert.ToString(dr["VehicleName"]);

                    lstobj.Add(obj);
                }
                //if (dt.Rows.Count > 0)
                //{
                //    totalRecordNo = Convert.ToInt32(dt.Rows[0]["TotalRowNo"]);
                //}
                log.Debug("Debug logging: DriverRepository - GetVehicle");

                return new Tuple<List<Vehicle>, int>(lstobj, totalRecordNo);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }

            return new Tuple<List<Vehicle>, int>(lstobj, totalRecordNo);
        }

        public Tuple<List<Vehicle>, int> GetVehicles(GetDataBindingModel model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            SqlParameter[] objParam;
            List<Vehicle> lstobj = new List<Vehicle>();
            Vehicle obj = null;
            int totalRecordNo = 0;
            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[6];

                objParam[0] = new SqlParameter("@Id", SqlDbType.Int);
                objParam[0].Value = model.Id;

                objParam[1] = new SqlParameter("@SearchText", SqlDbType.VarChar, 250);
                objParam[1].Value = model.SearchText;

                objParam[2] = new SqlParameter("@PageNo", SqlDbType.Int);
                objParam[2].Value = model.PageNo;

                objParam[3] = new SqlParameter("@PageSize", SqlDbType.Int);
                objParam[3].Value = model.PageSize;

                objParam[4] = new SqlParameter("@SortBy", SqlDbType.VarChar, 100);
                objParam[4].Value = model.SortBy;

                objParam[5] = new SqlParameter("@SortOrder", SqlDbType.VarChar, 100);
                objParam[5].Value = model.SortOrder;

                dt = daRepo.GetDataTable(ApiConstant.SProcGetVehicles, objParam);

                foreach (DataRow dr in dt.Rows)
                {
                    obj = new Vehicle();
                    obj.VehicleID = Convert.ToInt32(dr["VehicleID"]);
                    obj.CompanyID = Convert.ToString(dr["CompanyID"]);
                    obj.VehicleNO = Convert.ToString(dr["VehicleNO"]);
                    obj.VehicleType = Convert.ToInt32(dr["VehicleType"]);
                    obj.VehicleTypeName = Convert.ToString(dr["TypeName"]);
                    obj.VehicleName = Convert.ToString(dr["VehicleName"]);
                    obj.VehicleImage = Convert.ToString(dr["VehicleImage"]);
                    obj.VehicleCapacity = Convert.ToDecimal(dr["VehicleCapacity"]);
                    obj.Status = Convert.ToString(dr["Status"]);
                    obj.IsActive = Convert.ToBoolean(dr["IsActive"]);

                    lstobj.Add(obj);
                }
                //if (dt.Rows.Count > 0)
                //{
                //    totalRecordNo = Convert.ToInt32(dt.Rows[0]["TotalRowNo"]);
                //}
                log.Debug("Debug logging: DriverRepository - GetVehicle");

                return new Tuple<List<Vehicle>, int>(lstobj, totalRecordNo);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }

            return new Tuple<List<Vehicle>, int>(lstobj, totalRecordNo);
        }

        public string CRUDVehicle(VehicleBindingModel model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            SqlParameter[] objParam;
            apiConnectionString = apiConnectionAuthString;
            MessageReturn = "";
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[8];

                objParam[0] = new SqlParameter("@ActionMode", SqlDbType.Int);
                objParam[0].Value = model.ActionMode;

                objParam[1] = new SqlParameter("@VehicleID", SqlDbType.Int);
                objParam[1].Value = model.VehicleID;

                objParam[2] = new SqlParameter("@CompanyID", SqlDbType.NVarChar, 100);
                objParam[2].Value = model.CompanyID;

                objParam[3] = new SqlParameter("@VehicleNO", SqlDbType.NVarChar, 100);
                objParam[3].Value = model.VehicleNO;

                objParam[4] = new SqlParameter("@VehicleType", SqlDbType.Int);
                objParam[4].Value = model.VehicleType;

                objParam[5] = new SqlParameter("@VehicleName", SqlDbType.NVarChar, 100);
                objParam[5].Value = model.VehicleName;

                objParam[6] = new SqlParameter("@VehicleCapacity", SqlDbType.NVarChar, 100);
                objParam[6].Value = model.VehicleCapacity;

                objParam[7] = new SqlParameter("@IsActive", SqlDbType.Bit);
                objParam[7].Value = model.IsActive;

                MessageReturn = daRepo.ExecuteOutParamProc(ApiConstant.SPocCRUDVehicle, objParam);



                log.Debug("Debug logging: VehicalTypeRepository - CRUDVehiclesType -" + model.ActionMode.ToString());

                return MessageReturn;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
            }

            return MessageReturn;
        }
    }
}
