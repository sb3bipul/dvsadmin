﻿using DVSDomain.Model;
using DVSRepository.Helper;
using DVSRepository.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DVSRepository.Implementation
{
    public class SettingRepository : ISettingRepository
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string apiConnectionString;
        DataAccessReposity daRepo = null;
        DataTable dt = null;
        private string MessageReturn = "";

        public Tuple<List<Location>, int> GetLocation(GetDataBindingModel model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            SqlParameter[] objParam;
            List<Location> lstObj = new List<Location>();
            Location  obj = null;
            int totalRecordNo = 0;
            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[6];

                objParam[0] = new SqlParameter("@Id", SqlDbType.Int);
                objParam[0].Value = model.Id;

                objParam[1] = new SqlParameter("@SearchText", SqlDbType.VarChar, 250);
                objParam[1].Value = model.SearchText;

                objParam[2] = new SqlParameter("@PageNo", SqlDbType.Int);
                objParam[2].Value = model.PageNo;

                objParam[3] = new SqlParameter("@PageSize", SqlDbType.Int);
                objParam[3].Value = model.PageSize;

                objParam[4] = new SqlParameter("@SortBy", SqlDbType.VarChar, 100);
                objParam[4].Value = model.SortBy;

                objParam[5] = new SqlParameter("@SortOrder", SqlDbType.VarChar, 100);
                objParam[5].Value = model.SortOrder;

                dt = daRepo.GetDataTable(ApiConstant.SPocGetLocation, objParam);

                foreach (DataRow dr in dt.Rows)
                {
                    obj = new Location();
                    obj.Id = Convert.ToInt32(dr["Id"].ToString());
                    obj.LocationName = Convert.ToString(dr["LocationName"]);
                    obj.LocationAddress = Convert.ToString(dr["LocationAddress"].ToString());
                    obj.IsActive = Convert.ToBoolean(dr["IsActive"].ToString());
                    obj.CreatedBy = Convert.ToInt32(dr["CreatedBy"]);
                    obj.CreatedAt = dr["CreatedAt"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dr["CreatedAt"].ToString());
                    obj.StartPoint = Convert.ToInt32(dr["StartPoint"]);
                    obj.EndPoint = Convert.ToInt32(dr["EndPoint"]);
                    lstObj.Add(obj);
                }
                if (dt.Rows.Count > 0)
                {
                    totalRecordNo = Convert.ToInt32(dt.Rows[0]["TotalRowNo"]);
                }
                log.Debug("Debug logging: VehicalTypeRepository - GetAllVehiclesType");

                return new Tuple<List<Location>, int>(lstObj, totalRecordNo);
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }

            return new Tuple<List<Location>, int>(lstObj, totalRecordNo);
        }

        public Setting GetSetting(GetDataBindingModel model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            Setting obj = null;

            apiConnectionString = apiConnectionAuthString;
            try
            {
                dt = daRepo.GetDataTable(ApiConstant.SPocGetSetting);

                foreach (DataRow dr in dt.Rows)
                {
                    obj = new Setting();
                    obj.Id = Convert.ToInt32(dt.Rows[0]["Id"]);
                    obj.StartPoint = Convert.ToInt32(dr["StartPoint"]);
                    obj.EndPoint = Convert.ToInt32(dr["EndPoint"]);
                    obj.CreatedBy = Convert.ToInt32(dr["CreatedBy"]);
                    obj.CreatedAt = dr["CreatedAt"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dr["CreatedAt"].ToString());

                    log.Debug("Debug logging: SettingRepository - GetSetting");
                }

                return obj;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }

            return obj;
        }

        public string CRUDLocation(LocationBindingModel model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            SqlParameter[] objParam;
            apiConnectionString = apiConnectionAuthString;
            MessageReturn = "";
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[5];

                objParam[0] = new SqlParameter("@ActionMode", SqlDbType.Int);
                objParam[0].Value = model.ActionMode;

                objParam[1] = new SqlParameter("@Id", SqlDbType.Int);
                objParam[1].Value = model.Id;

                objParam[2] = new SqlParameter("@LocationName", SqlDbType.NVarChar, 100);
                objParam[2].Value = model.LocationName;

                objParam[3] = new SqlParameter("@LocationAddress", SqlDbType.NVarChar, 100);
                objParam[3].Value = model.LocationAddress;

                objParam[4] = new SqlParameter("@IsActive", SqlDbType.Bit, 100);
                objParam[4].Value = model.IsActive;

                MessageReturn = daRepo.ExecuteOutParamProc(ApiConstant.SPocCRUDLocation, objParam);



                log.Debug("Debug logging: SettingRepository - CRUDLocation -" + model.ActionMode.ToString());

                return MessageReturn;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
            }

            return MessageReturn;
        }

        public string CRUDSetting(SettingBindingModel model, string apiConnectionAuthString)
        {
            dt = new DataTable();
            SqlParameter[] objParam;
            apiConnectionString = apiConnectionAuthString;
            MessageReturn = "";
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[5];

                objParam[0] = new SqlParameter("@ActionMode", SqlDbType.Int);
                objParam[0].Value = model.ActionMode;

                objParam[1] = new SqlParameter("@Id", SqlDbType.Int);
                objParam[1].Value = model.Id;

                objParam[2] = new SqlParameter("@StartPoint", SqlDbType.Int);
                objParam[2].Value = model.StartPoint;

                objParam[3] = new SqlParameter("@EndPoint", SqlDbType.Int);
                objParam[3].Value = model.EndPoint;

                objParam[4] = new SqlParameter("@IsActive", SqlDbType.Bit, 100);
                objParam[4].Value = model.IsActive;

                MessageReturn = daRepo.ExecuteOutParamProc(ApiConstant.SPocCRUDSetting, objParam);



                log.Debug("Debug logging: SettingRepository - CRUDSetting -" + model.ActionMode.ToString());

                return MessageReturn;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
            }

            return MessageReturn;
        }
    }
}
