﻿using DVSRepository.Helper;
using DVSRepository.Interface;
using DVSRepository.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DVSRepository.Implementation
{
    public class DashboardRepository : IDashboardRepository
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string apiConnectionString;
        DataAccessReposity daRepo = null;
        DataTable dt = null;
        private string MessageReturn = "";
        SqlParameter[] objParam;

        public DashboardViewModel GetDashboardItem(int companyId, string apiConnectionAuthString)
        {
            dt = new DataTable();
            DashboardViewModel obj = null;

            apiConnectionString = apiConnectionAuthString;
            try
            {
                daRepo = new DataAccessReposity(apiConnectionString);
                objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@CompanyId", SqlDbType.Int);
                objParam[0].Value = companyId;
                dt = daRepo.GetDataTable(ApiConstant.SPocGetDashboardItem, objParam);

                foreach (DataRow dr in dt.Rows)
                {
                    obj = new DashboardViewModel();
                    obj.TotalCustomer = Convert.ToInt32(dt.Rows[0]["TotalCustomer"]);
                    obj.TotalOrder = Convert.ToInt32(dr["TotalOrder"]);
                    obj.TotalTrip = Convert.ToInt32(dr["TotalTrip"]);
                    obj.TotalDriver = Convert.ToInt32(dr["TotalDriver"]);

                    log.Debug("Debug logging: DasboardRepository - GetDashboardItem");
                }

                return obj;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                obj = null;
            }

            return obj;
        }
    }
}
