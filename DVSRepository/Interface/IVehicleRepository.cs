﻿using DVSDomain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.Interface
{
    public interface IVehicleRepository
    {
        public Tuple<List<Vehicle>, int> GetVehicle(GetDataBindingModel model, string apiConnectionAuthString);
        public Tuple<List<Vehicle>, int> GetVehicles(GetDataBindingModel model, string apiConnectionAuthString);
        public string CRUDVehicle(VehicleBindingModel model, string apiConnectionAuthString);
    }
}
