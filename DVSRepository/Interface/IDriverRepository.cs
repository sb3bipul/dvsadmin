﻿using DVSDomain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.Interface
{
    public interface IDriverRepository
    {
        Tuple<List<Driver>, int> GetDriver(GetDataBindingModel model, string apiConnectionAuthString);
        public string CRUDDriver(DriverBindingModel model, string apiConnectionAuthString);
    }
}
