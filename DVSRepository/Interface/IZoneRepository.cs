﻿using DVSDomain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.Interface
{
    public interface IZoneRepository
    {
        Tuple<List<Zone>, int> GetZone(GetDataBindingModel model, string apiConnectionAuthString);
        public string CRUDZone(ZoneBindingModel model, string apiConnectionAuthString);
    }
}
