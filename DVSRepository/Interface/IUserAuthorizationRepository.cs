﻿using DVSDomain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.Interface
{
    public interface IUserAuthorizationRepository
    {
        //public int GetRoleIdFromUserId(string userId);
        //public string GetUserFullNameFromUserId(string userId);
        //public ApplicationUser GetUserProfile(string userId);
        //public bool UpdateUserProfile(ApplicationUser userObj);
        //public bool UpdateNewUser(CreateNewUserBindingModel userObj);
        //public bool UpdateUpdatedBy(string userName);
        //public string GetUserIDFromUserName(string userName);
        //public bool IfUserExistInOAuthDB(string userName, string email);
        //public bool IfUserValidEmailOAuthDB(string userName, string email);
        //public string ResetReleaseUpdateFlag(string userId);
        public ApplicationUser GetUserProfileByUserLoginId(string userLoginId, string apiConnectionAuthString);
        public ApplicationUser GetUserProfile(LogindBindingModel model, string apiConnectionAuthString);
        public bool UpdateUserPassword(ChangePasswordBindingModel model, string apiConnectionAuthString);
    }
}
