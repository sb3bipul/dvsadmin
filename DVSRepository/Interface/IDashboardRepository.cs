﻿using DVSRepository.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.Interface
{
    public interface IDashboardRepository
    {
        public DashboardViewModel GetDashboardItem(int companyId, string apiConnectionAuthString);
    }
}
