﻿using DVSDomain.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DVSRepository.Interface
{
    public interface IUserRepository : IRepository<User>
    {
        public Task<User> GetUser(User user);
    }
}
