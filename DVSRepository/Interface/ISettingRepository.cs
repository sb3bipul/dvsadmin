﻿using DVSDomain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.Interface
{
    public interface ISettingRepository
    {
        public Tuple<List<Location>, int> GetLocation(GetDataBindingModel model, string apiConnectionAuthString);
        public Setting GetSetting(GetDataBindingModel model, string apiConnectionAuthString);
        public string CRUDLocation(LocationBindingModel model, string apiConnectionAuthString);
        public string CRUDSetting(SettingBindingModel model, string apiConnectionAuthString);
    }
}
