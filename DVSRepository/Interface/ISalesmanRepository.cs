﻿using DVSDomain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.Interface
{
    public interface ISalesmanRepository
    {
        public Tuple<List<Salesman>, int> GetSalesMan(GetDataBindingModel model, string apiConnectionAuthString);
    }
}
