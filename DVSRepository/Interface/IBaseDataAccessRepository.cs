﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DVSRepository.Interface
{
    public interface IBaseDataAccessRepository
    {
        void ExecuteQuery(String strSQL);
        void ExecuteQuery(String strSPName, ref SqlParameter[] param);
        SqlDataReader GetDataReader(string StrSQL);
        DataTable GetDataTable(string strSQL);
        DataTable GetDataTable(string strSPName, SqlParameter[] param);
        DataTable GetDataTable(string strSPName, Boolean isSP);
        DataSet GetDataSet(string strSQL);
        DataSet GetDataSet(string strSPName, SqlParameter[] param);
        DataSet GetDataSet(string strSPName, Boolean isSP);
    }
}
