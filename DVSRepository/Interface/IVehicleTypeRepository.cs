﻿using DVSDomain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.Interface
{
    public interface IVehicleTypeRepository
    {
        public Tuple<List<VehicleType>, int> GetAllVehiclesType(GetDataBindingModel model, string apiConnectionAuthString);

        public string CRUDVehicleType(VehicleTypeBindingModel model, string apiConnectionAuthString);
    }
}
