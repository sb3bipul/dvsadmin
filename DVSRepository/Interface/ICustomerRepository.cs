﻿using DVSDomain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.Interface
{
    public interface ICustomerRepository
    {
        Tuple<List<Customer>, int> GetCustomer(GetDataBindingModel model, string apiConnectionAuthString);
        Tuple<List<Customer>, int> GetCustomers(GetDataBindingModel model, string apiConnectionAuthString);
    }
}
