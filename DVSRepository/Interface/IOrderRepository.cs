﻿using DVSDomain.Model;
using DVSRepository.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace DVSRepository.Interface
{
    public interface IOrderRepository
    {
        public Tuple<List<OrderInfoViewModel>, int> GetAllOrderInfo(GetOrderInfoDataBindingModel model, string apiConnectionAuthString);
        public Tuple<OrderDetailViewModel, int> GetOrderDetail(GetOrderDetailBindingModel model, string apiConnectionAuthString);
    }
}
