﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace DVSRepository.Helper
{
    public class ApiConstant
    {
        ///// <summary>
        ///// Conncetion String to connect to database
        ///// </summary>
        //public static string apiConnectionString = ConfigurationManager.ConnectionStrings["conStr"].ToString();
        //public static string apiConnectionStringDB2 = ConfigurationManager.ConnectionStrings["conStrDB2"].ToString();

        //public static string apiConnectionStringOAuth = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        //public static string apiConnectionAuthString = ConfigurationManager.ConnectionStrings["conStr"].ToString();

        ///// <summary>
        ///// User Authentication and if success response with Token
        ///// </summary> 
        //public static string userTokenApiUri = ConfigurationManager.AppSettings["UserTokenApiUri"];
        //public static string ldapAuthenticationApiUri = ConfigurationManager.AppSettings["LDAPAuthenticationApiUri"];
        //public static string userVerifyApiUri = ConfigurationManager.AppSettings["UserVerifyApiUri"];
        //public static string userAuthorizationApiUri = ConfigurationManager.AppSettings["UserAuthorizationApiUri"];
        //public static string getUserFullNameApiUri = ConfigurationManager.AppSettings["GetUserFullNameApiUri"];
        //public static string getClientName = ConfigurationManager.AppSettings["ClientName"];

        ///// <summary>
        ///// Token verification
        ///// </summary>
        //public static string userVerifyTokenApiUri = ConfigurationManager.AppSettings["UserVerifyTokenApiUri"];

        ///// <summary>
        ///// Change Password
        ///// </summary>
        //public static string changePasswordApiUri = ConfigurationManager.AppSettings["ChangePasswordApiUri"];

        //public static string brokerPicUri = ConfigurationManager.AppSettings["BrokerPicUri"];

        //public static string DefaultBrokerPwdPrefix = ConfigurationManager.AppSettings["DefaultBrokerPwdPrefix"];


        public static string apiConnectionAuthString = "";
        //OAuth Section SPs
        public static string SPocGetUserMyProfile = "sp_GetUserMyProfile";
        public static string SProcGetUserDetails = "sp_GetUserDetails";
        public static string SPocGetRoleIdFromUserId = "sp_GetRoleIdFromUserId";
        public static string GetUserByUserLoginId = "SProc_DVS_GetUserByUserLoginId";
        public static string GetLoginUserProfile = "SProc_DVS_GetLoginUserProfile";
        public static string updateUserPassword = "SProc_DVS_UpdateUserPassword";
        public static string SPocGetUserFullName = "sp_GetUserFullName";
        public static string SPocUpdateMyProfile = "sp_UpdateMyProfile";
        public static string SPocUpdateUser = "sp_UpdateUser";
        public static string spResetPasswordUser = "sp_ResetPasswordUser";
        public static string SPocUpdateUpdatedBy = "sp_UpdateUpdatedBy";
        public static string SPocGetUserIdFromUserName = "sp_GetUserIdFromUserName";
        public static string SPocGetIfUserOrEmailExist = "sp_GetIfUserOrEmailExist";
        public static string SPocCheckIfValidEmailExist = "sp_CheckIfValidEmailExist";
        //public static string SPocResetReleaseUpdateFlag = "sp_ResetReleaseUpdateFlag";
        public static string spGetUserProfile = "sp_GetUserProfile";
        //  Firebase Notification
        public static string spGetRefreshToken = "sp_GetRefreshToken";

        // Driver login ckecked
        public static string SProcGetLoginInfo = "SProc_GetLoginInfo";


        // Reset Update Release Flag 
        public static string SPocResetReleaseUpdateFlag = "sp_ResetReleaseUpdateFlag";

        public static string DefaultPwdPrefix = ConfigurationManager.AppSettings["DefaultPwdPrefix"];
        public static string apiNewUserUri = ConfigurationManager.AppSettings["apiNewUserUri"];
        public static string apiChangePassword = ConfigurationManager.AppSettings["apiChangePassword"];

        #region Vehical Type
        public static string SPocGetVehiclesTypes = "SProc_DVS_GetVehiclesType";
        public static string SPocCRUDVehiclesType = "SProc_DVS_CRUDVehiclesType";
        #endregion
        #region Driver
        public static string SPocGetDriver = "SProc_DVS_GetDriver";
        public static string SPocCRUDDriver = "SProc_CRUDDriverRecord";
        #endregion
        #region Driver
        public static string SPocSalesMan = "SProc_DVS_GetSalesMan";
        public static string SPocCRUDSalesMan = "SProc_DVS_CRUDSalesMan";
        #endregion
        #region Zone
        public static string SPocGetZone = "SProc_DVS_GetZone";
        public static string SPocCRUDZone = "SProc_DVS_CRUDZone";
        #endregion

        #region Customer
        public static string SPocGetCustomer = "SProc_DVS_GetCustomer";
        #endregion

        #region Setting
        public static string SPocGetLocation = "SProc_DVS_GetLocation";
        public static string SPocGetSetting = "SProc_DVS_GetSetting";
        public static string SPocCRUDLocation = "SProc_DVS_CRUDLocation";
        public static string SPocCRUDSetting = "SProc_DVS_CRUDSetting";
        #endregion

        #region Customer
        public static string SPocGetOrder = "SProc_DVS_GetOrder";
        #endregion

        #region Trip
        public static string SPocGetTripOrder = "SProc_DVS_GetTripOrder";
        public static string SPocCRUDTrip = "SProc_DVS_CRUDTrip";
        public static string SPocGetAllTrip = "SProc_DVS_GetAllTrip";
        public static string SPocGetTripById = "SProc_DVS_GetTripById";
        public static string SPocGetAllOrderByTripId = "SProc_DVS_GetAllOrderByTripId";
        public static string SPocGetDeleverySummeryTripId = "SProc_DVS_GetDeleverySummeryByTripId";
        #endregion

        #region Trip Tracking
        public static string SPocGetAllTripTracking = "SProc_DVS_GetAllTripTracking";
        public static string SPocGetTripTrackingDetail = "SProc_DVS_GetTripTrackingDetail";
        #endregion

        #region Order
        public static string SPocGetOrderInfo = "SProc_DVS_GetOrderInfo";
        public static string SPocGetOrderDetail = "SProc_DVS_GetOrderDetail";
        #endregion

        #region Dashboard
        public static string SPocGetDashboardItem = "SProc_DVS_GetDashboardItem";
        #endregion

        #region Vehicle
        public static string SProcDVS_GetVehicleDDL = "SProc_DVS_GetVehicleDDL";
        public static string SProcGetVehicles = "SProc_DVS_GetVehicles";
        public static string SPocCRUDVehicle = "SProc_DVS_CRUDVehicle";
        #endregion

        #region AR Invoice
        // Get inserted invoice for DVS Admin
        public static string GetARInvoiceList = "SProc_AR_GetInvoiceListForAdmin";
        public static string UpdateInvoiceStatus = "SP_AR_ApproveRejectInvoice";
        public static string GetCustomerCity = "SP_AR_GetAllCustomerCity";
        public static string GetDataForCSVExport = "SProc_AR_GetDataForExportCSV";
        #endregion

        #region DVS Invoice
        public static string UploadedInvoiceListForAdmin = "SProc_DVS_UploadedInvoiceListForAdmin";
        public static string UpdateInvoiceInfo = "SProc_DVS_UpdateInvoiceInfo";
        public static string GetInvoiceStatusChangeHistory = "SProc_DVS_GetInvoiceStatusChangeHistory";
        public static string InsertInvoiceStatus = "SProc_DVS_InsertInvoiceStatus";
        //public static string GetUplodedInvoiceList = "SProc_DVS_GetUplodedInvoiceList";
        #endregion
    }
}
