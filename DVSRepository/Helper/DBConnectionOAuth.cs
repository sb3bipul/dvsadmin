﻿using DVSRepository.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DVSRepository.Helper
{
    public class DBConnectionOAuth
    {
        private SqlConnection _con;
        private string apiConnectionAuthString;
        public DBConnectionOAuth(string connectionString) 
        {
            apiConnectionAuthString = connectionString;
        }
        /// <summary>
        /// Create instance of SqlConnection.
        /// </summary>
        //public SqlConnection ApiConnection
        //{
        //    get
        //    {
        //        _con = new SqlConnection(ApiConstant.apiConnectionAuthString);
        //        return this._con;
        //    }
        //}

        public SqlConnection GetApiConnection()
        {
            _con = new SqlConnection(apiConnectionAuthString);
            return this._con;

        }

        /// <summary>
        /// Open SQL Connection to connect to Database.
        /// </summary>
        public void ConnectionOpen()
        {
            if (_con.State != ConnectionState.Open)
            {
                _con.Close();
                _con.Open();
            }

        }

        /// <summary>
        /// Close SQL Connection from the Database.
        /// </summary>
        public void ConnectionClose()
        {
            if (_con.State == ConnectionState.Open)
            {
                _con.Close();
            }
        }

    }
}
